#ifndef PAINTER_COMMANDS_H
#define PAINTER_COMMANDS_H

#include <vector>

#include "nanosvg-src/nanovg.h"

class pntrcmd {
public:
    virtual void exec(NVGcontext *ctx) {
        std::cout << "abstract" <<std::endl;
    }
    virtual ~pntrcmd() {}
};



class lcmd : public pntrcmd {
private:
    float x, y;

public: 
    lcmd(float x, float y);
    virtual void exec(NVGcontext *ctx) override;
};

class mcmd : public pntrcmd {
private:
    float x, y;
public:
    mcmd(float x, float y);
    virtual void exec(NVGcontext *ctx) override;
    lcmd *getlcmd();
};

class ccmd : public pntrcmd {
private:
    float c1x, c1y, c2x, c2y, x, y;
public:
    ccmd(float c1x, float c1y, float c2x, float c2y, float x, float y);
    virtual void exec(NVGcontext *ctx) override;
};

class qcmd : public pntrcmd {
private:
    float cx, cy, x, y;
public:
    qcmd(float cx, float cy, float x, float y);
    virtual void exec(NVGcontext *ctx) override;
};

class acmd : public pntrcmd {
private:
    float cx, cy, r, a0, a1;
    int dir;
public:
    acmd(float cx, float cy, float r, float a0, float a1, int dir);
    virtual void exec(NVGcontext *ctx) override;
};

struct cmdcntr {
    bool outer;
    std::vector<pntrcmd *> *cmds;
};

struct cmdshp {
    int r, g, b;
    std::vector<cmdcntr> *cntrs;
};

std::vector<cmdshp> *
rdcmds(std::ifstream &stream);

#endif