#include <fstream>
#include <iostream>
#include <math.h>
#include <vector>

#include "painter_commands.h"
#include "painter_common.h"
#include "nanovg.h"

#define BEGIN_SHAPE 	"BEGIN_SHAPE"
#define END_SHAPE 		"END_SHAPE"
#define COLOR 			"COLOR"
#define LINE 			"LINE"
#define LINEZ 			"LINEZ"
#define BEZIER3 		"BEZIER3"
#define BEZIER2 		"BEZIER2"
#define ARC 			"ARC"

mcmd::mcmd(float x, float y)
{
    this->x = x;
    this->y = y;
}

void
mcmd::exec(NVGcontext *ctx)
{
    // nvgBeginPath(ctx);
    nvgMoveTo(ctx, this->x, this->y);
}

lcmd *
mcmd::getlcmd()
{
    return new lcmd(this->x, this->y);
}

lcmd::lcmd(float x, float y)
{
    this->x = x;
    this->y = y;
}

void
lcmd::exec(NVGcontext *ctx)
{
    nvgLineTo(ctx, this->x, this->y);
}

ccmd::ccmd(float c1x, float c1y, float c2x, float c2y, float x, float y)
{
    this->c1x = c1x;
    this->c1y = c1y;
    this->c2x = c2x;
    this->c2y = c2y;
    this->x = x;
    this->y = y;
}

void
ccmd::exec(NVGcontext *ctx)
{
    nvgBezierTo(ctx, c1x, c1y, c2x, c2y, x, y);
}

qcmd::qcmd(float cx, float cy, float x, float y)
{
    this->cx = cx;
    this->cy = cy;
    this->x = x;
    this->y = y;
}

void
qcmd::exec(NVGcontext *ctx)
{
    nvgQuadTo(ctx, cx, cy, x, y);
}

acmd::acmd(float cx, float cy, float r, float a0, float a1, int dir)
{
    this->cx = cx;
    this->cy = cy;
    this->r = r;
    this->a0 = a0;
    this->a1 = a1;
    this->dir = dir;
}

void
acmd::exec(NVGcontext *ctx)
{
    nvgArc(ctx, cx, cy, r, a0, a1, dir);
}

static float
svg_signum(float n)
{
    if (n < 0)
        return -1;
    else
        return 1;
}

static float
clamp_angle(float angle)
{
    int mult = angle < 0 ? -1 : 1;
    while (fabsf(angle) >= 360.0f)
        angle -= mult * 360.0f;

    return angle;
}

painter_pointf
elarcpt(float cx, float cy, float rx, float ry, float xangle, float t) {
    painter_pointf pt;

    /*pt.x = cx + rx * cosf(xangle) * cosf(t) - ry * sinf(xangle) * sinf(t),
    pt.y = cy + rx * cosf(xangle) * cosf(t) + ry * cosf(xangle) * sinf(t);*/

    pt.x = cosf(xangle) * cosf(t) * rx -
           sinf(xangle) * sinf(t) * ry + cx;
    pt.y = sinf(xangle) * cosf(t) * rx +
           cosf(xangle) * sinf(t) * ry + cy;

    return pt;
}

painter_pointf
delarcpt(float cx, float cy, float rx, float ry, float xangle, float t) {
    painter_pointf pt;

    pt.x = -rx * cosf(xangle) * sinf(t) - ry * sinf(xangle) * cosf(t),
    pt.y = -rx * sinf(xangle) * sinf(t) + ry * cosf(xangle) * cosf(t);

    return pt;
}

static void
arc_to_quads(std::vector<pntrcmd *> *cmds, float rx, float ry, float x1,
             float y1, float x2, float y2, float xaxis_rotation,
             int large_arc_flag, int sweep_flag)
{

    float radphi, newx, newy, lambda;
    float s, tcx, tcy, cx, cy, theta;
    float vx, vy, ux, uy, dtheta;
    float arc_step, ang;

    radphi = M_PI * xaxis_rotation / 180.0f;


    newx = cosf(radphi)*((x1-x2)/2) + sinf(radphi)*((y1-y2)/2);
    newy = (-1) * sinf(radphi)*((x1-x2)/2) + cosf(radphi)*((y1-y2)/2);
    lambda = (newx*newx)/(rx*rx) + (newy*newy)/(ry*ry);

    if (lambda > 1.0f) {
        rx = sqrtf(lambda) * rx;
        ry = sqrtf(lambda) * ry;
    }

    s = sqrtf(fabsf((rx*rx * ry*ry - rx*rx * newy*newy - ry*ry * newx*newx)
                    / (rx*rx * newy*newy + ry*ry * newx*newx)));
    if (large_arc_flag == sweep_flag)
        s = -s;

    tcx = s * (rx * newy) / ry;
    tcy = s * ((-1) * ry * newx) / rx;
    cx = cosf(radphi) * tcx - sinf(radphi) * tcy + (x1 + x2) / 2;
    cy = sinf(radphi) * tcx + cosf(radphi) * tcy + (y1 + y2) / 2;

    vx = (newx - tcx) / rx;
    vy = (newy - tcy) / ry;
    ux = 1;
    uy = 0;

    theta = (ux*vx + uy*vy) / (sqrtf(ux*ux + uy*uy) * sqrtf(vx*vx + vy*vy));
    theta = acosf(theta) * 180.0f / M_PI;
    theta = fabsf(theta) * svg_signum(ux*vy - vx*uy);

    ux = (newx - tcx) / rx;
    uy = (newy - tcy) / ry;
    vx = (-newx - tcx)/rx;
    vy = (-newy - tcy)/ry;
    dtheta = (ux*vx + uy*vy) / (sqrtf(ux*ux + uy*uy) * sqrtf(vx*vx + vy*vy));

    if (dtheta < -1.0)
        dtheta = -1.0;
    else if (dtheta > 1.0)
        dtheta = 1.0;

    dtheta = clamp_angle(acosf(dtheta) * 180.0f / M_PI);
    dtheta = fabsf(dtheta) * svg_signum(ux*vy - vx*uy);

    if (sweep_flag == 0 && dtheta > 0)
        dtheta -= 360;
    else if (sweep_flag == 1 && dtheta < 0)
        dtheta += 360;

    int ndivs = 1;
    ang = theta;
    arc_step = fabsf(ang - (theta + dtheta)) / (float)ndivs;
    while (arc_step > 45) {
        ndivs++;
        arc_step = fabsf(ang - (theta + dtheta)) / (float)ndivs;
    }

    arc_step = (sweep_flag == 0) ? -arc_step : arc_step;




    for (int i = 0; i < ndivs; i++, ang += arc_step)
    {
        painter_pointf p1, p2, dp1, dp2, c1, c2;

        float t = ang * M_PI / 180;
        float t1 = (ang + arc_step) * M_PI / 180;
        float tans = tanf((t1 - t) / 2.0f);
        tans *= tans;
        float alpha = sinf(t1 - t) * ((sqrtf(4.0f + 3.0f * tans) - 1.0f) / 3.0f);
       // if ((arc_step > 0 && t1 > theta + dtheta) || (arc_step < 0 && t1 < theta + dtheta))
         //   t1 = theta;

        p1 = elarcpt(cx, cy, rx, ry, radphi, t);
        p2 = elarcpt(cx, cy, rx, ry, radphi, t1);
        dp1 = delarcpt(cx, cy, rx, ry, radphi, t);
        dp2 = delarcpt(cx, cy, rx, ry, radphi, t1);

        c1.x = p1.x + alpha * dp1.x;
        c1.y = p1.y + alpha * dp1.y;
        c2.x = p2.x - alpha * dp2.x;
        c2.y = p2.y - alpha * dp2.y;

        cmds->push_back(new ccmd(c1.x, c1.y, c2.x, c2.y, p2.x, p2.y));
//        cmds->push_back(new lcmd(p2.x, p2.y));
    }
}


std::vector<cmdshp> *
rdcmds(std::ifstream &stream)
{
    cmdshp shp;
    cmdcntr cntr;
    std::vector<cmdshp> *shps;
    std::vector<cmdcntr> *cntrs;
    std::vector<pntrcmd *> *cmds;
    std::string str;

    shps = new std::vector<cmdshp>();



    while (stream >> str) {
        if (str == BEGIN_SHAPE) {
            cntrs = new std::vector<cmdcntr>();
            cmds = new std::vector<pntrcmd *>();
            shp = {};
            cntr = {};
        } else if (str == COLOR) {
            float r, g, b;

            stream >> r >> g >> b;

            shp.r = r;
            shp.g = g;
            shp.b = b;
        } else if (str == LINE) {
            float x1, y1, x2, y2;

            stream >> x1 >> y1 >> x2 >> y2;
            if (cmds->size() == 0)
                cmds->push_back(new mcmd(x1, y1));

            cmds->push_back(new lcmd(x2, y2));
        }  else if (str == LINEZ) {
            lcmd *zcmd;
            zcmd = ((mcmd *)cmds->at(0))->getlcmd();
            cmds->push_back(zcmd);
            cntr.cmds = cmds;
            cntrs->push_back(cntr);
            cmds = new std::vector<pntrcmd *>();
            cntr={};
        } else if (str == BEZIER3) {
            // std::cout << "b3" << std::endl;
            float x0, y0,  c1x, c1y, c2x, c2y, x, y;
            stream >> x0 >> y0 >> c1x >> c1y >>c2x >> c2y >> x >> y;
            if (cmds->size() == 0)
                cmds->push_back(new mcmd(x0, y0));

            cmds->push_back(new ccmd(c1x, c1y, c2x, c2y, x, y));
        } else if (str == BEZIER2) {
            float x0, y0, cx, cy, x, y;
            // std::cout << "b2" << std::endl;
            stream >> x0 >> y0 >> cx >> cy >> x >> y;
            if (cmds->size() == 0)
                cmds->push_back(new mcmd(x0, y0));

            cmds->push_back(new qcmd(cx, cy, x, y));
        } else if (str == ARC) {
            float rx, ry, x1, y1, x2, y2, xaxis_rotation;
            int large_arc_flag, sweep_flag;

            stream >> rx >> ry >> x1 >> y1 >> x2 >> y2 >> xaxis_rotation >>
                   large_arc_flag >> sweep_flag;
            if (cmds->size() == 0)
                cmds->push_back(new mcmd(x1, y1));

            arc_to_quads(cmds, rx, ry, x1, y1, x2, y2, xaxis_rotation,
                         large_arc_flag, sweep_flag);
        } else if (str == END_SHAPE) {
            shp.cntrs = cntrs;
            shps->push_back(shp);
            cntrs = new std::vector<cmdcntr>();
        }
    }


    return shps;
}