#include <bx/bx.h>
#include <bgfx/bgfx.h>
#include <dirent.h>
#include <errno.h>
#include <fstream>
#include <gdk/gdk.h>
#include <gtk/gtk.h>
#include <iostream>
#include <math.h>
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_mouse.h>
#include <stdio.h>
#include <string.h>
#include <strings.h>
#include <vector>

#include "painter_common.h"
/* #include "nanosvg-src/nanovg.h"
#define NANOVG_GLES3_IMPLEMENTATION
#include "nanosvg-src/nanovg_gl.h" */

#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

#include "painter_commands.h"
#include "painter_common.h"


#define DIMENSIONS_PATH "configs/dimensions"
#define STR_SIZE 256
#define DIMENSIONS_NUMBER 6
#define RESOURCES_PATH "resources/"

SDL_Window *window;
SDL_Renderer *renderer;
SDL_GLContext glctx;
// NVGcontext *vg;
//SDL_Texture *renderer_texture, *bg_texture;

bool grender_quad = true;
int ww = 1920, wh = 1080;
float pxratio = (float)ww  / (float)wh;

std::vector<painter_shape> *shapes;
std::vector<cmdshp> *shps;

void
render()
{
	int i, j, k;

	
	//Render quad
	if( grender_quad )
	{
		nvgBeginFrame(vg, ww, wh, pxratio);

		static float a = 0.0f;

		for (i = 0; i < shps->size(); i++) {
			float r, g, b;
			float cx, cy, radius;
			std::vector<cmdcntr> *cntrs;

			r = shps->at(i).r;
			g = shps->at(i).g;
			b = shps->at(i).b;

			cntrs = shps->at(i).cntrs;

			nvgStrokeWidth(vg, 2.0f);
			nvgStrokeColor(vg, nvgRGBA(r, g, b, 255));
			nvgFillColor(vg, nvgRGBA(shapes->at(i).r, shapes->at(i).g,
				shapes->at(i).b, 255));

			nvgBeginPath(vg);
			for(j = 0; j < cntrs->size(); j++) {
				std::vector<pntrcmd *> *cmds;

				cmds = cntrs->at(j).cmds;
				
				// nvgBeginPath(vg);
				for (k = 0; k < cmds->size(); k++)
					cmds->at(k)->exec(vg);
				// nvgClosePath(vg);
				if (!cntrs->at(j).outer)
					nvgPathWinding(vg, NVG_HOLE);


			}
						

			nvgClosePath(vg);

			cx = shapes->at(i).cx;
			cy = shapes->at(i).cy;
			radius = shapes->at(i).radius;

			if (shapes->at(i).r > 50 || shapes->at(i).g > 50 || 
				shapes->at(i).b > 50) {
					NVGpaint paint;
					NVGcolor incl, outcl;

					incl = nvgRGBA(shapes->at(i).r, shapes->at(i).g,
						shapes->at(i).b, 255);
					outcl = nvgRGBA(255, 255, 255, 0);
					
					paint = nvgRadialGradient(vg, cx,cy, radius * a,0, incl, outcl);
					nvgFillPaint(vg, paint);
				// nvgBeginPath(vg);
				// nvgCircle(vg, cx,cy,radius);
				// nvgRect(vg, l, b, r - l, t - b);
				// nvgFill(vg);
				// nvgClosePath(vg);
			 } else {
				// nvgFill(vg);

			}

			nvgStroke(vg);			
			nvgFill(vg);
			
			
		}

		nvgEndFrame(vg); 

		a += 0.01f;
		if (a > 1)
		 a = 0.0f;
	}

	// t += 0.05f;
}

// isLeft(): tests if a point is Left|On|Right of an infinite line.
//    Input:  three points P0, P1, and P2
//    Return: >0 for P2 left of the line through P0 and P1
//            =0 for P2  on the line
//            <0 for P2  right of the line
//    See: Algorithm 1 "Area of Triangles and Polygons"
inline int
islft(painter_point &p0, painter_point &p1, painter_point p2)
{
    return ((p1.x - p0.x) * (p2.y - p0.y)
            - (p2.x -  p0.x) * (p1.y - p0.y) );
}

// wn_PnPoly(): winding number test for a point in a polygon
//      Input:   P = a point,
//               V[] = vertex points of a polygon V[n+1] with V[n]=V[0]
//      Return:  wn = the winding number (=0 only when P is outside)
int
wnpnpoly(painter_point &p, std::vector<painter_point> *pts)
{
    int wn, i;    // the  winding number counter

    // loop through all edges of the polygon
	wn = 0;
    for (i = 0; i < pts->size(); i++) {  // edge from V[i] to  V[i+1]
		int i1;

		i1 = (i + 1) % pts->size();
        if (pts->at(i).y <= p.y) {          // start y <= P.y
            if (pts->at(i1).y  > p.y)      // an upward crossing
                 if (islft(pts->at(i), pts->at(i1), p) > 0)  // P left of  edge
                     wn++;            // have  a valid up intersect
        }
        else {                        // start y > P.y (no test needed)
            if (pts->at(i1).y  <= p.y)     // a downward crossing
                 if (islft(pts->at(i), pts->at(i1), p) < 0)  // P right of  edge
                     --wn;            // have  a valid down intersect
        }
    }
    return wn;
}

bool
isinshp(painter_shape &shp, int x, int y)
{
	int i;
	bool inouter, ininner;
	struct painter_point testpt;

	testpt.x = x;
	testpt.y = y;

	inouter = ininner = false;
	if (x >= shp.lt && x <= shp.rt && y >= shp.bm && y <= shp.tp) {
		
		for (i = 0 ; i < shp.contours->size(); i++) {
			if (!shp.contours->at(i).outer)
				continue;

			if (wnpnpoly(testpt, shp.contours->at(i).pts) != 0) {
				inouter = true;
				break;
			}
		}
	}

	if (inouter) {
		for (i = 0 ; i < shp.contours->size(); i++) {
			if (shp.contours->at(i).outer)
				continue;
		
			if (x >= shp.contours->at(i).lt && x <= shp.contours->at(i).rt &&
				y >= shp.contours->at(i).bm && y <= shp.contours->at(i).tp) {
				if (wnpnpoly(testpt, shp.contours->at(i).pts) != 0) {
					ininner = true;
					break;
				}
			}
		}
	}

	return inouter && !ininner;
}

void handle_keys( unsigned char key, int x, int y )
{
	//Toggle quad
	if( key == 'q' )
	{
		grender_quad = !grender_quad;
	}
}


int
main (int argc, char **argv)
{
	int i, j, k;
	std::ifstream stream_cmds, stream_collider;
	std::string shppath = "resources/greek.shp";


	stream_cmds.open(shppath);
	shps = rdcmds(stream_cmds);
	stream_cmds.close();
	std::cout << shps->size() << std::endl;
	stream_collider.open(shppath);
	shapes = shapes_fread(stream_collider);
	
/* 	if (shapes == NULL) {
		fprintf(stderr, "SHAPES IS NULL");
		exit(1);
	} */

	for (i = 0; i < shapes->size(); i++) {
		std::vector<painter_contour> *contours;
		std::vector<cmdcntr> *cmdcntrs;

		contours = shapes->at(i).contours;
		cmdcntrs = shps->at(i).cntrs;

		for (j = 0; j < contours->size(); j++) 
			cmdcntrs->at(j).outer = contours->at(j).outer;
	}

	if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
		fprintf(stderr, "error initializing SDL\n");
		exit(1);
	}

	window = SDL_CreateWindow("", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
		ww, wh, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);
	renderer = SDL_CreateRenderer(window, -1, 
		SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);


/* 	vg = nvgCreateGLES3(NVG_ANTIALIAS | NVG_STENCIL_STROKES | NVG_DEBUG);;
	if (vg == NULL) {
		std::cerr << "err creating nanovg context" << std::endl;
		exit(1);
	}
 */
	//Main loop flag
	bool quit = false;

	//Event handler
	SDL_Event e;
	
	//Enable text input
	SDL_StartTextInput();

	//While application is running
	while( !quit )
	{
		//Handle events on queue
		while( SDL_PollEvent( &e ) != 0 )
		{
			//User requests quit
			if (e.type == SDL_MOUSEBUTTONDOWN) {
				int x, y;

				SDL_GetMouseState(&x, &y);
				for (i = 0; i < shapes->size(); i++) {
					if (isinshp(shapes->at(i), x, y)) {
						std::cout << "isinshp true" << std::endl;
						break;
					}
				}				
			} else if( e.type == SDL_QUIT ) {
				quit = true;
			}
			//Handle keypress with current mouse position
			else if( e.type == SDL_TEXTINPUT )
			{
				int x = 0, y = 0;
				SDL_GetMouseState( &x, &y );
				handle_keys( e.text.text[ 0 ], x, y );
			}
		}

		//Render quad
		render();
		
		//Update screen
		SDL_GL_SwapWindow(window);
	}
	
	//Disable text input
	SDL_StopTextInput();

	SDL_Quit();
	return 0;
}