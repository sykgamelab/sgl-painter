#include <fstream>
#include <iostream>
#include <math.h>
#include <string>
#include <vector>

#include "painter_common.h"

#define STR_SIZE 		1024

#define BEGIN_SHAPE 	"BEGIN_SHAPE"
#define END_SHAPE 		"END_SHAPE"
#define COLOR 			"COLOR"
#define LINE 			"LINE"
#define LINEZ 			"LINEZ"
#define BEZIER3 		"BEZIER3"
#define BEZIER2 		"BEZIER2"
#define ARC 			"ARC"
#define CALCULATION_STEP 0.01f

static void
bezier3_to_pts(std::vector<painter_point> *pts, painter_pointf p0,
               painter_pointf p1, painter_pointf p2, painter_pointf p3, float step)
{
    float t;
    int flag = 1;


    t = 0;
    while (flag)
    {
        struct painter_point pt;
        float x, y;

        if (t > 1.0f) {
            t = 1.0f;
            flag = 0;
        }

        x = (1 - t)*(1 - t)*(1 - t) * p0.x +
            3*t * (1 - t)*(1 - t) * p1.x +
            3*t*t * (1 - t) * p2.x + t*t*t *
                                     p3.x;

        y = (1 - t)*(1 - t)*(1 - t) * p0.y +
            3*t * (1 - t)*(1 - t) * p1.y +
            3*t*t * (1 - t) * p2.y + t*t*t *
                                     p3.y;


        pt.x = x;
        pt.y = y;
        pts->push_back(pt);

        t += step;
    }
}

static void
bezier2_to_pts(std::vector<painter_point> *pts, painter_pointf p0,
               painter_pointf p1, painter_pointf p2, float step)
{
    float t;
    int flag = 1;


    t = 0;
    while (flag)
    {
        painter_point pt;
        float x, y;

        if (t > 1.0f) {
            t = 1.0f;
            flag = 0;
        }

        x = (1 - t)*(1 - t)*  p0.x +
            2 * t * (1 - t) * p1.x +
            t * t * p2.x;

        y = (1 - t)*(1 - t)*  p0.y +
            2 * t * (1 - t) * p1.y +
            t * t * p2.y;


        pt.x = x;
        pt.y = y;
        pts->push_back(pt);

        t += step;
    }

}

static float
svg_signum(float n)
{
    if (n < 0)
        return -1;
    else
        return 1;
}

static float
clamp_angle(float angle)
{
    int mult = angle < 0 ? -1 : 1;
    while (fabsf(angle) >= 360.0f)
        angle -= mult * 360.0f;

    return angle;
}

static void
arc_to_pts(std::vector<painter_point> *pts, float rx, float ry, float x1,
           float y1, float x2, float y2, float xaxis_rotation, int large_arc_flag,
           int sweep_flag, float step)
{
    float radphi, newx, newy, lambda;
    float s, tcx, tcy, cx, cy, theta;
    float vx, vy, ux, uy, dtheta;
    float arc_step, ang;

    radphi = M_PI * xaxis_rotation / 180.0f;


    newx = cosf(radphi)*((x1-x2)/2) + sinf(radphi)*((y1-y2)/2);
    newy = (-1) * sinf(radphi)*((x1-x2)/2) + cosf(radphi)*((y1-y2)/2);
    lambda = (newx*newx)/(rx*rx) + (newy*newy)/(ry*ry);

    if (lambda > 1.0f) {
        rx = sqrtf(lambda) * rx;
        ry = sqrtf(lambda) * ry;
    }

    s = sqrtf(fabsf((rx*rx * ry*ry - rx*rx * newy*newy - ry*ry * newx*newx)
                    / (rx*rx * newy*newy + ry*ry * newx*newx)));
    if (large_arc_flag == sweep_flag)
        s = -s;

    tcx = s * (rx * newy) / ry;
    tcy = s * ((-1) * ry * newx) / rx;
    cx = cosf(radphi) * tcx - sinf(radphi) * tcy + (x1 + x2) / 2;
    cy = sinf(radphi) * tcx + cosf(radphi) * tcy + (y1 + y2) / 2;

    vx = (newx - tcx) / rx;
    vy = (newy - tcy) / ry;
    ux = 1;
    uy = 0;

    theta = (ux*vx + uy*vy) / (sqrtf(ux*ux + uy*uy) * sqrtf(vx*vx + vy*vy));
    theta = acosf(theta) * 180.0f / M_PI;
    theta = fabsf(theta) * svg_signum(ux*vy - vx*uy);

    ux = (newx - tcx) / rx;
    uy = (newy - tcy) / ry;
    vx = (-newx - tcx)/rx;
    vy = (-newy - tcy)/ry;
    dtheta = (ux*vx + uy*vy) / (sqrtf(ux*ux + uy*uy) * sqrtf(vx*vx + vy*vy));

    if (dtheta < -1.0)
        dtheta = -1.0;
    else if (dtheta > 1.0)
        dtheta = 1.0;

    dtheta = clamp_angle(acosf(dtheta) * 180.0f / M_PI);
    dtheta = fabsf(dtheta) * svg_signum(ux*vy - vx*uy);

    if (sweep_flag == 0 && dtheta > 0)
        dtheta -= 360;
    else if (sweep_flag == 1 && dtheta < 0)
        dtheta += 360;

    arc_step = (sweep_flag == 0) ? -step : step;
    for (ang = theta; fabsf(ang - (theta + dtheta)) > CALCULATION_STEP; ang += arc_step)
    {
        painter_point pt;
        newx = cosf(radphi) * cosf(ang * M_PI / 180) * rx -
               sinf(radphi) * sinf((ang * M_PI)/180) * ry + cx;
        newy = sinf(radphi) * cosf(ang * M_PI/180) * rx +
               cosf(radphi) * sinf((ang * M_PI) / 180) * ry + cy;

        pt.x = newx;
        pt.y = newy;
        pts->push_back(pt);
    }
}

static void
simplify_shape(painter_shape *shape)
{
    size_t i, j;
    size_t npts;
    std::vector<painter_point> *pts;
    std::vector<painter_point> *pts_new;

    // std::cout << "simplify ncontours " << shape->contours->size() << std::endl;
    for (i = 0; i < shape->contours->size(); i++) {
        pts = shape->contours->at(i).pts;
        npts = pts->size();
        pts = shape->contours->at(i).pts;
        // std::cout << "size " << npts << std::endl;
        // std::cout << "before begin-end check" << " " << npts << std::endl;
        while ((pts->at(0).x == pts->at(npts-1).x) &&
               (pts->at(0).y == pts->at(npts-1).y) &&
               (npts > 1)) {
            pts->pop_back();
            npts--;
        }
    }


    for (i = 0; i < shape->contours->size(); i++) {
        int k;
        npts = 1;
        pts = shape->contours->at(i).pts;

        pts_new = new std::vector<painter_point>();
        k = 0;
        pts_new->push_back(pts->at(0));
        for (j = 1; j < pts->size(); j++) {
            painter_point prevpt, nextpt;
            prevpt = pts_new->at(k);
            nextpt = pts->at(j);
            if (nextpt.x != prevpt.x || nextpt.y != prevpt.y) {
                k++;
                pts_new->push_back(nextpt);
            }
        }

        delete pts;
        shape->contours->at(i).pts = pts_new;
    }
}

void
prepare_shapes(std::vector<painter_shape> *shapes)
{
    int i, j, k;
    for (i = 0; i < shapes->size(); i++) {
        std::vector<painter_contour> *contours;
        int minx, minidx;

        contours = shapes->at(i).contours;
        minx = contours->at(0).pts->at(0).x;
        minidx = 0;
        for (j = 0; j < contours->size(); j++) {
            float cl, cr, ct, cb;
            std::vector<painter_point> *pts;

            pts = contours->at(j).pts;

            cl = pts->at(0).x;
            cr = pts->at(0).x;
            ct = pts->at(0).y;
            cb = pts->at(0).y;

            for (k = 0; k < pts->size(); k++) {
                int x, y;

                x = pts->at(k).x;
                y = pts->at(k).y;

                if (x < minx) {
                    minidx = j;
                    minx = k;
                }

                if (y < cb)
                    cb = y;
                if (y > ct)
                    ct = y;
                if (x < cl)
                    cl = x;
                if (x > cr)
                    cr = x;
            }

            contours->at(j).lt = cl;
            contours->at(j).rt = cr;
            contours->at(j).bm = cb;
            contours->at(j).tp = ct;
        }

        for (j = 0; j < contours->size(); j++) {
            if (j == minidx)
                contours->at(j).outer = true;
            else
                contours->at(j).outer = false;
        }
    }

    for (i = 0; i < shapes->size(); i++) {
        std::vector<painter_contour> *contours;
        float l, r, t, b, cx, cy, radius;

        contours = shapes->at(i).contours;
        l = contours->at(0).pts->at(0).x;
        r = contours->at(0).pts->at(0).x;
        t = contours->at(0).pts->at(0).y;
        b = contours->at(0).pts->at(0).y;
        for (j = 0; j < contours->size(); j++) {

            std::vector<painter_point> *pts;
            if (!contours->at(j).outer)
                continue;

            pts = contours->at(j).pts;




            for (k = 1; k < pts->size(); k++) {
                float x, y;

                x = pts->at(k).x;
                y = pts->at(k).y;

                if (y < b)
                    b = y;
                if (y > t)
                    t = y;
                if (x < l)
                    l = x;
                if (x > r)
                    r = x;


            }

        }

        cx = (r + l) / 2.0f;
        cy = (t + b) / 2.0f;
        radius = sqrt((r-l) * (r - l) + (t-b)*(t-b));

        shapes->at(i).lt = l;
        shapes->at(i).rt = r;
        shapes->at(i).tp = t;
        shapes->at(i).bm = b;
        shapes->at(i).cx = cx;
        shapes->at(i).cy = cy;
        shapes->at(i).radius = radius;
    }

}

std::vector<painter_shape> *
shapes_fread(std::istream &stream)
{
    painter_shape shape;
    painter_contour contour;
    painter_point pt;
    std::vector<painter_shape> *shapes;
    std::vector<painter_contour> *contours;
    std::vector<painter_point> *pts;
    std::string str;

    shapes = new std::vector<painter_shape>();

    while (stream >> str) {
        if (str == BEGIN_SHAPE) {
            contours = new std::vector<painter_contour>();
            pts = new std::vector<painter_point>();
            shape = {};
            contour = {};
        } else if (str == COLOR) {
            float r, g, b;

            stream >> r >> g >> b;

            shape.r = r;
            shape.g = g;
            shape.b = b;
        } else if (str == LINE) {
            float x1, y1, x2, y2;

            stream >> x1 >> y1 >> x2 >> y2;
            if (pts->size() == 0) {
                pt.x = x1;
                pt.y = y1;
                pts->push_back(pt);
            }
            pt.x = x2;
            pt.y = y2;
            pts->push_back(pt);
        }  else if (str == LINEZ) {
            contour.pts = pts;
            contours->push_back(contour);
            // std::cout <<"linez " << contours->size() << std::endl;
            pts = new std::vector<painter_point>();
            contour={};
        } else if (str == BEZIER3) {
            // std::cout << "b3" << std::endl;
            painter_pointf p0, p1, p2, p3;
            stream >> p0.x >> p0.y >> p1.x >> p1.y >>p2.x >>p2.y >>p3.x >>p3.y;
            if (pts->size() == 0) {
                pt.x = p0.x;
                pt.y = p0.y;
                pts->push_back(pt);
            }

            // std::cout << "before b3 size " << pts->size() << std::endl;
            bezier3_to_pts(pts,  p0, p1, p2, p3, CALCULATION_STEP);
        } else if (str == BEZIER2) {
            painter_pointf p0, p1, p2;
            // std::cout << "b2" << std::endl;
            stream >> p0.x >> p0.y >> p1.x >> p1.y >> p2.x >> p2.y;
            if (pts->size() == 0) {
                pt.x = p0.x;
                pt.y = p0.y;
                pts->push_back(pt);
            }

            // std::cout << "before b2 size " << pts->size() << std::endl;
            bezier2_to_pts(pts,  p0, p1, p2, CALCULATION_STEP);
            // std::cout << "after b2 size " << pts->size() << std::endl;
        } else if (str == ARC) {
            float rx, ry, x1, y1, x2, y2, xaxis_rotation;
            int large_arc_flag, sweep_flag;
            stream >> rx >> ry >> x1 >> y1 >> x2 >> y2 >> xaxis_rotation >>
                   large_arc_flag >> sweep_flag;
            if (pts->size() == 0) {
                pt.x = x1;
                pt.y = y1;
                pts->push_back(pt);
            }

            arc_to_pts(pts, rx, ry, x1, y1, x2, y2, xaxis_rotation,
                       large_arc_flag, sweep_flag, CALCULATION_STEP);

            pt.x = x2;
            pt.y = y2;
            pts->push_back(pt);
        } else if (str == END_SHAPE) {
            size_t b, d;



            shape.contours = contours;
            simplify_shape(&shape);

            for (b = 0; b < shape.contours->size(); b++) {
                for (d = 0; d < shape.contours->at(b).pts->size() - 1; d++) {
                    struct painter_point p1, p2;
                    p1 = shape.contours->at(b).pts->at(d);
                    p2 = shape.contours->at(b).pts->at(d + 1);
                    if (p1.x == p2.x && p1.y == p2.y) {
                        std::cout << "size " << shape.contours->at(b).pts->size() << std::endl;
                        std::cerr << "SAME  (" << p1.x << ", " << p1.y <<
                                  ") (" << p2.x << ", " << p2.y << ")" << std::endl;
                        exit(1);
                    }
                }
            }


            shapes->push_back(shape);
            contours = new std::vector<painter_contour>();
        }
    }

    prepare_shapes(shapes);
    return shapes;
}