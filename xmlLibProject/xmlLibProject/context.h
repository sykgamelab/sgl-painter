#pragma once

#include <iostream>
#include <SDL.h>
#include <SDL_image.h>
#include <GL/glew.h>

enum render_type
{
	OPEN_GL3
};

class context
{
	

private:
	const int ww = 1080, wh = 1920;

	SDL_Window* window;
	SDL_Renderer* renderer;
	SDL_GLContext glctx;

public:
	~context();
	bool init(render_type type);
	void clear(int clear_flags, int color, int depth, int stencil);
	void setClearColor(float r, float g, float b, float a);
	void frame();
	void draw2d(SDL_Texture* texture, SDL_Rect* rect);

	SDL_Renderer* getRenderer() { return renderer; };

};

