#pragma once

#include "component.h"
#include "texture.h"
#include "context.h"

#include <SDL.h>
#include <memory>

class Sprite : public component
{
private:
	SDL_Rect rect;
	SDL_Texture* texture;

public:
	Sprite();
	Sprite(SDL_Texture *tex, SDL_Rect rect);
	
	void start();
	void update(context& ctx);
	Sprite &operator=(Sprite const &s2);

	SDL_Rect getRect();
};

