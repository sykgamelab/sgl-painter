#include <Windows.h>
#include <libxml/parser.h>
#include <iostream>
#include <list>
#include <locale>
#include <codecvt>

#include "context.h"
#include "InterfaceElement.h"
#include "AtlasReader.h"
#include "sprite.h"

void ParseNode(xmlNodePtr node, std::shared_ptr<InterfaceElement> parent, float width, float height);

context glctx;
AtlasReader atlasReader;

std::vector<Sprite> v_sprites;

static float referenceWidth, referenceHeight;
std::vector<InterfaceElement> treeInList;

std::wstring xmlCharToWideString(const xmlChar* xmlString)
{
	if (!xmlString) { abort(); } //provided string was null
	try
	{
		std::wstring_convert<std::codecvt_utf8<wchar_t>, wchar_t> conv;
		return conv.from_bytes((const char*)xmlString);
	}
	catch (const std::range_error& e)
	{
		abort(); //wstring_convert failed
	}
}

std::vector<std::wstring> Split(std::wstring str, std::wstring delimiter, bool StringSplitOptions = false)
{
	std::vector<std::wstring> result;

	int pos = 0;
	std::wstring token;
	while ((pos = str.find(delimiter)) != std::string::npos) {
		if (StringSplitOptions)
		{
			if (str.substr(0, pos) != L"")
			{
				token = str.substr(0, pos);
			}
		}
		else
		{
			token = str.substr(0, pos);
		}
		str.erase(0, pos + delimiter.length());
		result.push_back(token);
	}

	result.push_back(str);

	return result;
}

void GenerateElements(std::shared_ptr<InterfaceElement> curr, int &count, float width, float height, std::wstring className)
{
	int currCount = count;
	float x = 0.0f, y = 0.0f;

	std::vector<std::wstring> stringsInSprite;

	Sprite sprite;

	for (auto elem : curr->getChildren())
	{
		bool anchored = false;
		float elemWidth = width;
		float elemHeight = height;
		
		std::wstring type = elem.getTag();
		std::wstring varName = elem.ContainsAttribute(L"name") ? (std::wstring)elem.GetAttribute(L"name").s : L"";  // was    : null
		
		AtlasReader atlasReader;

		 if (elem.ContainsAttribute(L"width"))
		 {
		 	bool relative = elem.ContainsAttribute(L"relativeWidth");
		 	float val = elem.GetAttribute(L"width").f;
		 	if (relative) elemWidth *= val;
		 	else elemWidth = val;
		 }

		 if (elem.ContainsAttribute(L"height"))
		 {
		 	bool relative = elem.ContainsAttribute(L"relativeHeight");
		 	float val = elem.GetAttribute(L"height").f;
		 	if (relative) elemHeight *= val;
		 	else elemHeight = val;
		 }

		//Vector2 sizeDelta = new Vector2(elemWidth, elemHeight);
		//rectTransform.sizeDelta = sizeDelta;

		if (type == L"Button" || type == L"Image")
		{
			if (elem.ContainsAttribute(L"sprite"))
			{
				stringsInSprite = Split(elem.GetAttribute(L"sprite").s, L"/", false);
				sprite = atlasReader.LoadURPNG(stringsInSprite[0], std::stoi(stringsInSprite[1]), glctx);
			}

		}

		//if (type == L"Button")
		//{
		//	if (elem.ContainsAttribute(L"onClick"))
		//	{
		//		std::vector<std::wstring> onClickHandlers = (elem.GetAttribute(L"onClick").s).Split(new char[] { '/' },
		//			StringSplitOptions.RemoveEmptyEntries);

		//		//Type formType = form.GetType();
		//		//MethodInfo[] methods = formType.GetMethods(BindingFlags.NonPublic | BindingFlags.Instance);

		//		for(auto handler : onClickHandlers)
		//		{
		//			for(auto m : methods)
		//			{
		//				if (m.Name == handler)
		//				{
		//					UnityEngine.Events.UnityAction action = (UnityEngine.Events.UnityAction)Delegate.CreateDelegate(typeof(UnityEngine.Events.UnityAction), form, m.Name);
		//					btn.onClick.AddListener(action);
		//				}
		//			}
		//		}
		//	}

		//	btn.targetGraphic = img;
		//}
		
		count++;

		GenerateElements(std::make_shared<InterfaceElement>(elem), count, elemWidth, elemHeight, className);
	}
}

void GenerateForms(std::wstring className, std::shared_ptr<InterfaceElement> root, float width, float height)
{
	int count = 1;

	GenerateElements(root, count, width, height, className);
}

void ParseAttribute(xmlAttrPtr attr, xmlNodePtr node, std::shared_ptr<InterfaceElement> elem)
{
	object obj;

	std::wstring val;
	float denumerator = 1.0;
	
	if (xmlCharToWideString(attr->name) == L"width")
	{
		val = xmlCharToWideString(xmlNodeListGetString(node->doc, attr->children, 1));
		denumerator = 1.0f;
		if (val[val.length() - 1] == '%')
		{
			val = val.substr(0, val.length() - 1);

			obj = {};
			obj.type = BL;
			obj.b = true;
			
			elem->SetAttribute((wchar_t*)L"relativeWidth", obj);
			denumerator = 100.0f;
		}
		
		obj = {};
		obj.type = FLT;
		obj.f = std::stof(val.c_str()) / denumerator;


		elem->SetAttribute((wchar_t*)(char*)attr->name, obj);
	}

	if (xmlCharToWideString(attr->name) == L"height")
	{
		val = xmlCharToWideString(xmlNodeListGetString(node->doc, attr->children, 1));
		denumerator = 1.0f;
		if (val[val.length() - 1] == '%')
		{
			val = val.substr(0, val.length() - 1);

			obj = {};
			obj.type = BL;
			obj.b = true;

			elem->SetAttribute((wchar_t*)L"relativeHeight", obj);
			denumerator = 100.0f;
		}

		obj = {};
		obj.type = FLT;
		obj.f = std::stof(val.c_str()) / denumerator;

		elem->SetAttribute(xmlCharToWideString(attr->name), obj);
	}

	if (xmlCharToWideString(attr->name) == L"sprite")
	{
		//elem.SetAttribute(attr.Name, attr.Value);
		std::wstring val  = xmlCharToWideString(xmlNodeListGetString(node->doc, attr->children, 1));
		std::vector<std::wstring> stringsInSprite = Split(val, L"/");
		
		bool useNativeSize = true;

		xmlAttrPtr attr = node->properties;

		while (attr)
		{
			std::wstring attrValue = xmlCharToWideString(xmlNodeListGetString(node->doc, attr->children, 1));

			if (xmlCharToWideString(attr->name) == L"useNativeSize" && attrValue == L"false")
			{
				useNativeSize = false;
			}

			attr = attr->next;
		}

		if (useNativeSize)
		{
			Sprite sp = atlasReader.LoadURPNG(stringsInSprite[0], std::stoi(stringsInSprite[1]), glctx);

			obj = {};
			obj.type = FLT;
			obj.f = sp.getRect().w;
			
			elem->SetAttribute(L"width", obj);

			obj = {};
			obj.type = FLT;
			obj.f = sp.getRect().h;

			elem->SetAttribute(L"height", obj);
		}
	}

}

void ParseChildren(xmlNodePtr node, std::shared_ptr<InterfaceElement> parent, float width, float height)
{
	for (xmlNodePtr cur = node->children; cur != NULL; cur = cur->next)
	{
		ParseNode(cur, parent, width, height);
	}
}

void ParseNode(xmlNodePtr node, std::shared_ptr<InterfaceElement> parent, float width, float height)
{
	object obj;
	
	std::shared_ptr<InterfaceElement> elem;
	elem = std::make_shared<InterfaceElement>(parent, xmlCharToWideString(node->name));

	if (node->type == XML_COMMENT_NODE) return;

	xmlAttrPtr attr = node->properties;

	while (attr)
	{
		ParseAttribute(attr, node, elem);
		attr = attr->next;
	}

	if (elem->getTag() == (std::wstring)L"Row")
	{
		if (!elem->ContainsAttribute((std::wstring)L"height"))
		{
			obj = {};
			obj.type = FLT;
			obj.f = 1.0f / xmlChildElementCount(node->parent);

			elem->SetAttribute((std::wstring)L"height", obj);

			obj = {};
			obj.type = BL;
			obj.b = true;

			elem->SetAttribute((std::wstring)L"relativeHeight", obj);
		}

		obj = {};
		obj.type = BL;
		obj.b = true;

		elem->SetAttribute((std::wstring)L"relativeWidth", obj);

		obj = {};
		obj.type = FLT;
		obj.f = 1.0f;

		elem->SetAttribute((std::wstring)L"width", obj);
	}
	else if (elem->getTag() == (wchar_t*)L"Col")
	{
		if (!elem->ContainsAttribute((wchar_t*)L"width"))
		{

			obj = {};
			obj.type = FLT;
			obj.f = 1.0f / xmlChildElementCount(node->parent);

			elem->SetAttribute((wchar_t*)L"width", obj);

			obj = {};
			obj.type = BL;
			obj.b = true;

			elem->SetAttribute((wchar_t*)L"relativeWidth", obj);
		}

		obj = {};
		obj.type = BL;
		obj.b = true;

		elem->SetAttribute((wchar_t*)L"relativeHeight", obj);

		obj = {};
		obj.type = FLT;
		obj.f = 1.0f;

		elem->SetAttribute((wchar_t*)L"height", obj);
	}
	else if (elem->getTag() == (wchar_t*)L"Layer")
	{
		obj = {};
		obj.type = BL;
		obj.f = true;

		elem->SetAttribute((wchar_t*)L"relativeWidth", obj);
		elem->SetAttribute((wchar_t*)L"relativeHeight", obj);

		obj = {};
		obj.type = FLT;
		obj.f = 1.0f;

		elem->SetAttribute((wchar_t*)L"width", obj);
		elem->SetAttribute((wchar_t*)L"height", obj);
	}


	//treeInList.Add(elem);
	parent->AddChild(elem);
	ParseChildren(node, elem, width, height);
}

void Parse(const char *docPath)
{
	xmlDocPtr doc = xmlParseFile(docPath);
	xmlNodePtr root = xmlDocGetRootElement(doc);

	std::wstring className = (xmlCharToWideString(xmlGetProp(root,(xmlChar*)"name")));
	std::wstring value;

	xmlAttr* attr = root->properties;

	float width = 0, height = 0;

	while (attr)
	{
		value = xmlCharToWideString(xmlNodeListGetString(root->doc, attr->children, 1));
		if (xmlCharToWideString(attr->name) == L"width")
		{
			width = std::stof(value);
			referenceWidth = width;
		}

		if (xmlCharToWideString(attr->name) == L"height")
		{
			height = std::stof(value);
			referenceHeight = height;
		}
		
		attr = attr->next;
	}

	xmlFree(attr);
	
	std::shared_ptr<InterfaceElement> rootElement;
	rootElement = std::make_shared<InterfaceElement>(xmlCharToWideString(root->name));
	
	treeInList.push_back(*rootElement);
	
	ParseChildren(root, rootElement, width, height);
	GenerateForms(className, rootElement, width, height);
}

void printNode(xmlNodePtr node)
{
	xmlAttr* attribute = node->properties;

	printf("Name: %s; ", node->name);

	while (attribute)
	{
		xmlChar* value = xmlNodeListGetString(node->doc, attribute->children, 1);
		printf("%s: %s; ", attribute->name, value);
		xmlFree(value);
		attribute = attribute->next;
	}

	xmlFree(attribute);
	printf("\n");
	
	for (xmlNodePtr cur = node->children; cur != NULL; cur = cur->next)
	{
		if (cur->type == XML_COMMENT_NODE)
		{
			cur = cur->next;
			printNode(cur);
		}
		else
		{
			printNode(cur);
		}
	}


	/*xmlNodePtr child = node->children;
	if (child == NULL) return;

	do
	{
		if (child->type == XML_COMMENT_NODE && child->next != NULL)
		{
			child = child->next;
			printNode(child);
		}
		else
		{
			printNode(child);
		}

		if (child->next != NULL)
		{
			child = child->next;
		}

	} while (child->next != NULL);*/

}

bool xmlParse() 
{
	/*std::wstring XML_FILE_PATH = L"Lobby.xml";
	xmlDocPtr doc;
	xmlNodePtr cur;

	doc = xmlParseFile(XML_FILE_PATH);

	if (doc == NULL) {
		fprintf(stderr, "Document not parsed successfully. \n");
		exit(1);
	}

	cur = xmlDocGetRootElement(doc);

	if (cur == NULL) {
		fprintf(stderr, "empty document\n");
		xmlFreeDoc(doc);
		exit(1);
	}

	if (xmlStrcmp(cur->name, (const xmlChar*)"Form")) {
		fprintf(stderr, "document of the wrong type, root node != Form");
		xmlFreeDoc(doc);
		exit(1);
	}*/

	
	//printNode(cur);

	return true;
}

Sprite load_img()
{
	std::wstring BYTES_FILE_NAME = L"atlas_cards.bytes";
	//std::wstring BYTES_FILE_NAME = L"sprite_1.bytes";
	Sprite sprite;
	
	
	context& ctx = glctx;

	sprite = atlasReader.LoadURPNG(BYTES_FILE_NAME, 0, ctx);

	return sprite;
}

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PWSTR pCmdLine, int nCmdShow)
{
	xmlKeepBlanksDefault(0);

	SDL_Event e;
	
	if (glctx.init(OPEN_GL3)) 
		std::cout << "good" << std::endl;
	else
		exit(1);

	Sprite sprite;

	const char *XML_FILE_PATH = "Lobby.xml";
	Parse(XML_FILE_PATH);

	//sprite = load_img();

	glctx.setClearColor(0.9f, 0.4f, 0.0f, 1.0f);
	while (true)
	{
		glctx.clear(0, 0, 0, 0);
		//sprite.update(glctx);
		glctx.frame();
		
		if (SDL_PollEvent(&e))
		{
			if (e.type == SDL_QUIT)
			{
				break;
			}
		}

	}
	SDL_Quit();
	return 0;
}