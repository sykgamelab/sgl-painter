#include "context.h"

context::~context()
{
	SDL_GL_DeleteContext(glctx);
	SDL_DestroyRenderer(renderer);
	SDL_DestroyWindow(window);
}

bool context::init(render_type type)
{
	if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
		fprintf(stderr, "error initializing SDL\n");
		return false;
	}

	if (SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3) < 0) {
		std::cerr << "err setting major version of gl " << SDL_GetError() << std::endl;
		return false;
	}

	if (SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0) < 0) {
		std::cerr << "err setting minor version of gl " << SDL_GetError() << std::endl;
		return false;
	}

	if (SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
		SDL_GL_CONTEXT_PROFILE_CORE) < 0) {
		std::cerr << "err setting profile of gl " << SDL_GetError() << std::endl;
		return false;
	}

	if (SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 1) < 0) {
		std::cerr << "err setting stencil size to 1 " << SDL_GetError() << std::endl;
		return false;
	}

	if (IMG_Init(IMG_INIT_PNG) != IMG_INIT_PNG) {
		std::cerr << "err setting img init " << SDL_GetError() << std::endl;
		return false;
	}

	window = SDL_CreateWindow("Hello World!", 0, 0, ww, wh, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);
	if (window == nullptr) {
		std::cout << "SDL_CreateWindow Error: " << SDL_GetError() << std::endl;
		return false;
	}

	renderer = SDL_CreateRenderer(window, -1,
		SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
	if (renderer == nullptr) {
		std::cout << "SDL_CreateRenderer Error: " << SDL_GetError() << std::endl;
		return false;
	}


	glctx = SDL_GL_CreateContext(window);
	if (glctx == NULL) {
		std::cerr << "err creating gl context " << SDL_GetError() << " " << glGetError() << std::endl;

		SDL_GL_DeleteContext(glctx);
		SDL_DestroyWindow(window);

		return false;
	}

	

	return true;
}

void context::clear(int clear_flags, int color, int depth, int stencil)
{
	
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
}

void context::setClearColor(float r, float g, float b, float a)
{
	glClearColor(r, g, b, a);
}

void context::frame()
{
	
	SDL_GL_SwapWindow(window);
}

void context::draw2d(SDL_Texture* texture, SDL_Rect* rect)
{
	SDL_Rect dstrect;

	dstrect.x = 50;
	dstrect.y = 50;
	dstrect.w = rect->w;
	dstrect.h = rect->h;

	SDL_RenderCopy(renderer, texture, rect, &dstrect);
	SDL_RenderPresent(renderer);
	SDL_Delay(1000);
	SDL_RenderClear(renderer);
}
