#pragma once

#include <memory>
#include <string>
#include <map>
#include <vector>
#include <list>

enum object_type
{
	STR,
	FLT,
	BL
};

struct object
{
	object_type type;
	std::wstring s;
	float f;
	bool b;
};

class InterfaceElement
{

private:
	std::shared_ptr<InterfaceElement> Parent;
	std::wstring Tag;

	std::map<std::wstring, object> attrs;
	std::vector<InterfaceElement> Children;
		
public:
	InterfaceElement() {};
	InterfaceElement(std::shared_ptr<InterfaceElement> parent, std::wstring tag);
	InterfaceElement(std::wstring tag);

	~InterfaceElement();

	std::shared_ptr<InterfaceElement> getParent();
	void setParent(std::shared_ptr<InterfaceElement> parent);

	std::wstring getTag();
	void setTag(std::wstring tag);

	std::vector<InterfaceElement> getChildren();

	void SetAttribute(std::wstring key, object val);
	bool ContainsAttribute(std::wstring key);
	object GetAttribute(std::wstring key);

	void AddChild(std::shared_ptr<InterfaceElement> child);
	

};