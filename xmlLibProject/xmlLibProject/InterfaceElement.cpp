#include "InterfaceElement.h"

InterfaceElement::InterfaceElement(std::shared_ptr<InterfaceElement> parent, std::wstring tag)
{
	Parent = parent;
	Tag = tag;
}

InterfaceElement::InterfaceElement(std::wstring tag) : InterfaceElement(NULL, tag) { }
InterfaceElement::~InterfaceElement() { }

void InterfaceElement::setParent(std::shared_ptr<InterfaceElement> parent) { Parent = parent; }

std::shared_ptr<InterfaceElement> InterfaceElement::getParent() { return Parent; }

void InterfaceElement::setTag(std::wstring tag) { Tag = tag; }

std::wstring InterfaceElement::getTag() { return Tag; }

std::vector<InterfaceElement> InterfaceElement::getChildren()
{
	return Children;
}

void InterfaceElement::SetAttribute(std::wstring key, object val)
{
	if (attrs.find(key) == attrs.end())
		attrs.insert(std::pair<std::wstring, object>(key, val));
	else attrs[key] = val;
}

bool InterfaceElement::ContainsAttribute(std::wstring key)
{
	return !(attrs.find(key) == attrs.end());
}

object InterfaceElement::GetAttribute(std::wstring key)
{
	return attrs[key];
}

void InterfaceElement::AddChild(std::shared_ptr<InterfaceElement> child)
{
	Children.push_back(*child);
}


