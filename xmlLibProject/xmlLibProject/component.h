#pragma once

#include "context.h"

class component
{
protected:
	virtual void start() = 0;
	virtual void update(context&) = 0;
};

