#include "sprite.h"

Sprite::Sprite()
{
	rect = {};
	texture = nullptr;
}

Sprite::Sprite(SDL_Texture *tex, SDL_Rect rectangle)
{
	texture = tex;
	rect = rectangle;
}

void Sprite::start()
{
}

void Sprite::update(context &ctx)
{
	ctx.draw2d(texture, &rect);
}

Sprite& Sprite::operator=(Sprite const& s2)
{
	texture = s2.texture;
	rect = s2.rect;
	return *this;
}

SDL_Rect Sprite::getRect()
{
	return rect;
}
