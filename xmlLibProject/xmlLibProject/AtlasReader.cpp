#include "AtlasReader.h"


#include <fstream>
#include <iostream>


int AtlasReader::setBits(unsigned char* byte)
{
	int result = 0;
	result |= *byte;
	result |= *(byte + 1) << 8;
	result |= *(byte + 2) << 16;
	result |= *(byte + 3) << 24;

	return result;
}

Selection AtlasReader::setSelection(unsigned char* data)
{
	Selection result;

	result.num = setBits(data);
	result.x = setBits(data + 4);
	result.y = setBits(data + 8);
	result.w = setBits(data + 12);
	result.h = setBits(data + 16);
	result.bx = setBits(data + 20);
	result.by = setBits(data + 24);
	result.bz = setBits(data + 28);
	result.bw = setBits(data + 32);
	int crc = setBits(data + 36);

	return result;
}

std::tuple<std::map<int, Selection>, std::map<int, Sprite>, SDL_Texture*>
AtlasReader::ReadURPNG(std::wstring stream, context &ctx)
{
	std::ifstream file(stream, std::ios::binary);
	std::map<int, Sprite> sprites;

	if (file)
	{
		count = 0;

		file.seekg(0, file.end);
		int length = file.tellg();
		file.seekg(0, file.beg);

		char* data = new char[length];

		file.read(data, length);
		file.close();

		count = setBits((unsigned char*)data);

		Selection selection;

		for (int i = 0; i < count; i++)
		{
			selection = setSelection((unsigned char*)(data + 4 + (i * SELECTION_SIZE)));
			selections.insert(std::pair<int, Selection>(selection.num, selection));
		}

		int i = (count * 40) + 4;

		for (i; i < length; i++)
		{
			png.push_back(data[i]);
		}
	}
	
	SDL_RWops* rw = SDL_RWFromMem(png.data(), png.size());
	SDL_Surface* sur = IMG_Load_RW(rw, 0);
	SDL_Texture* texture = SDL_CreateTextureFromSurface(ctx.getRenderer(), sur);

	if (selections.size() > 0)
	{
		Selection s;

		for (std::pair<int,Selection> selection : selections) {
			s = selection.second;

			Vector4 border;

			if (s.bx != 0 || s.by != 0 || s.bz != 0 || s.bw != 0)
			{
				border.x = s.bx;
				border.y = s.by;
				border.z = s.bz;
				border.w = s.bw;
			}
			else
			{
				border.x = 0;
				border.y = 0;
				border.z = 0;
				border.w = 0;
			}

			SDL_Rect rect = {
				s.x,
				s.y,
				s.w,
				s.h
			};

			sprites.insert(std::pair<int, Sprite>(s.num, Sprite(texture, rect)));
		}
	}
	else
	{
		SDL_Rect rect = { 0, 0, sur->w, sur->h };
		sprites.insert(std::pair<int, Sprite>(0, Sprite(texture, rect)));
	}

	return std::make_tuple(selections, sprites, texture);
}

Sprite AtlasReader::LoadURPNG(std::wstring layoutName, int idx, context &ctx)
{
	if (layouts.find(layoutName) == layouts.end())
	{
		std::wstring prefix = L"Sprites/" + layoutName + L".bytes";
		std::map<int, Sprite> layout;
		std::tuple<std::map<int, Selection>, std::map<int, Sprite>, SDL_Texture*> tuple = ReadURPNG(prefix, ctx);

		for (auto t : std::get<0>(tuple)) {
			int num = t.second.num;
			layout.insert(std::make_pair(num, std::get<1>(tuple).at(num)));
		}
	
		layouts.insert(std::make_pair(layoutName, layout));
		if (layouts.find(layoutName) == layouts.end()) return Sprite();
	}

	if (layouts.at(layoutName).find(idx) == layouts.at(layoutName).end())
	{
		std::printf("index %d is not present al layout %s", idx, (char*)layoutName.c_str());
		return Sprite();
	}

	return layouts.at(layoutName).at(idx);
}
