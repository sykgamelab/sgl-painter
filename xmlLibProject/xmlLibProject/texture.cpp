#include "texture.h"

Texture::Texture(SDL_Texture *tex)
{
	texture = tex;
}

SDL_Texture *Texture::get_texture()
{
	return texture;
}

void Texture::set_texture(SDL_Texture *tex)
{
	texture = tex;
}
