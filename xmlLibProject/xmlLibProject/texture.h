#pragma once


#include <SDL.h>


class Texture
{
private:
	SDL_Texture *texture;

public:
	Texture(SDL_Texture *tex);
	SDL_Texture *get_texture();
	void set_texture(SDL_Texture *tex);
};

