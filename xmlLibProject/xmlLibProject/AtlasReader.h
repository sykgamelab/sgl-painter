#pragma once


#include <SDL.h>
#include <SDL_image.h>
#include <map>
#include <vector>

#include "sprite.h"


struct Selection {
	int num, x, y, w, h, bx, by, bz, bw;
};

class AtlasReader
{

private:
	const int SELECTION_SIZE = 40;

	struct Vector4
	{
		int x, y, z, w;
	};

	char* data;
	int count;

	std::vector<unsigned char> png;
	std::map<int, Selection> selections;
	std::map<std::wstring, std::map<int, Sprite>> layouts;

	int setBits(unsigned char*);
	Selection setSelection(unsigned char*);
	std::tuple<std::map<int, Selection>, std::map<int, Sprite>, SDL_Texture*> ReadURPNG(std::wstring, context &ctx);

public:
	std::vector<unsigned char> getPng() { return png; };
	std::map<int, Selection> getTextures() { return selections; };
	Sprite LoadURPNG(std::wstring layoutName, int idx, context &ctx);
};

