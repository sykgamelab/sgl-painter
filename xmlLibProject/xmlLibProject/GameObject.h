#pragma once

#include <vector>
#include <memory>
#include <map>


#include "component.h"

struct vector2
{
	float x;
	float y;
};

class GameObject
{

private:
	std::vector<std::shared_ptr<GameObject>> children;
	//std::map<int,component> components; �����������

	vector2 position;
	vector2 local_position;

public:
	void add_component();
	void update();
};

