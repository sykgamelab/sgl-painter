#include "AtlasReader.h"
#include <fstream>
#include <iostream>


int AtlasReader::setBits(unsigned char* byte)
{
	int result = 0;
	result |= *byte;
	result |= *(byte + 1) << 8;
	result |= *(byte + 2) << 16;
	result |= *(byte + 3) << 24;

	return result;
}

Selection AtlasReader::setSelection(unsigned char* data)
{
	Selection result;
	
	result.num = setBits(data);
	result.x = setBits(data + 4);
	result.y = setBits(data + 8);
	result.w = setBits(data + 12);
	result.h = setBits(data + 16);
	result.bx = setBits(data + 20);
	result.by = setBits(data + 24);
	result.bz = setBits(data + 28);
	result.bw = setBits(data + 32);
	int crc = setBits(data + 36);

	return result;
}

void AtlasReader::readFile(const char* fileName)
{
	std::ifstream file(fileName, std::ios::binary);
	
	if (file)
	{
		count = 0;

		file.seekg(0, file.end);
		int length = file.tellg();
		file.seekg(0, file.beg);

		char* data = new char[length];

		file.read(data, length);

		count = setBits((unsigned char*)data);

		Selection selection;

		for (int i = 0; i < count; i++)
		{
			selection = setSelection((unsigned char*)(data + 4 + (i * SELECTION_SIZE)));
			m.insert(std::pair<int, Selection>(selection.num,selection));
		}

		int i = (count * 40) + 4;

		for (i ; i < length; i++)
		{
			png.push_back(data[i]);
		}
	}

	file.close();

}

