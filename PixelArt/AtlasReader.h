#pragma once

#include <SDL.h>
#include <SDL_image.h>
#include <map>
#include <vector>

struct Selection {
		int num, x, y, w, h, bx, by, bz, bw;
	};

class AtlasReader
{

private:
	const int SELECTION_SIZE = 40;

	char* data;
	int count;

	std::vector<unsigned char> png;
	std::map<int, Selection> m;

	int setBits(unsigned char*);
	Selection setSelection(unsigned char*);

public:
	void readFile(const char*);
	std::vector<unsigned char> getPng() { return png; };
	std::map<int, Selection> getTextures() { return m; };
};

