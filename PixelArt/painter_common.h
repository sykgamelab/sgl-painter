#ifndef PAINTER_COMMON_H
#define PAINTER_COMMON_H

#include <array>
#include <vector>

struct painter_pointf {
    float x, y;
};

struct painter_point {
    int x, y;
};

struct painter_contour {
    std::vector<painter_point> *pts;
    bool outer;
    float lt, rt, tp, bm;
};

struct painter_shape {
    std::vector<painter_contour> *contours;
    float r, g, b;
    float lt, rt, tp, bm, cx, cy, radius;
};


std::vector<painter_shape> *
shapes_fread(std::ifstream &stream);

#endif