
//#include <errno.h>

#include <fstream>

#include <SDL.h>
#include <SDL_image.h>
#include <GL/glew.h>
#include <GL/glu.h>
#include <iostream>
#include <math.h>

#include <SDL_mouse.h>
//#include <SDL_opengl.h>
//#include <SDL_opengl_glext.h>
#include <stdio.h>
#include <string.h>
#include <vector>

//#include <libxml/encoding.h>
//#include <libxml/xmlwriter.h>

//#include "painter_common.h"
//#include "nanovg.h"
//#define NANOVG_GL3_IMPLEMENTATION
//#include "nanovg_gl.h"

#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include "painter_commands.h"
#include "painter_common.h"

#include "AtlasReader.h"

// SKIA
#include <include/gpu/GrBackendSurface.h>
#include <include/gpu/GrContext.h>
#include <include/gpu/gl/GrGLInterface.h>
#include <src/gpu/gl/GrGLDefines.h>
#include <src/gpu/gl/GrGLUtil.h>

#include <include/core/SkData.h>
#include <include/core/SkImage.h>
#include <include/core/SkStream.h>
#include <include/core/SkSurface.h>
#include <include/core/SkCanvas.h>
#include <include/core/SkSurface.h>
#include <include/core/SkRefCnt.h>
#include <include/core/SkShader.h>
#include <include/core/SkPaint.h>
#include <include/core/SkRect.h>

#include <include/effects/SkGradientShader.h>

#include <include/utils/SkRandom.h>
//end SKIA

#include <Windows.h>

#define DIMENSIONS_PATH "configs/dimensions"
#define STR_SIZE 256
#define DIMENSIONS_NUMBER 6
#define RESOURCES_PATH "resources/"


SDL_Window *window;
SDL_Renderer *renderer;
SDL_GLContext glctx;
SDL_Surface* imgSurface;
NVGcontext *vg;

SkCanvas* canvas;

//SDL_Texture *renderer_texture, *bg_texture;

GLuint gprogid = 0;
GLint gvtxpos_2dloc = -1;
GLuint gvbo = 0;
GLuint gibo = 0;
bool grender_quad = true;
int ww = 1920, wh = 1080;
float pxratio = (float)ww / (float)wh;

SDL_Color* map;
//unsigned char* map;

std::vector<painter_shape> *shapes;
std::vector<cmdshp> *shps;


Uint32 get_pixel(SDL_Surface* surface, int x, int y)
{
	int bpp = surface->format->BytesPerPixel;
	Uint8* p = (Uint8*)surface->pixels + y * surface->pitch + x * bpp;

	switch (bpp) {
	case 1:
		return *p;
		break;

	case 2:
		return *(Uint16*)p;
		break;

	case 3:
		if (SDL_BYTEORDER == SDL_BIG_ENDIAN)
			return p[0] << 16 | p[1] << 8 | p[2];
		else
			return p[0] | p[1] << 8 | p[2] << 16;
		break;

	case 4:
		std::cout << "Hello";
		return *(Uint32*)p;
		break;

	default:
		return 0;       /* shouldn't happen, but avoids warnings */
	}
}

void load_img()
{
	const char* PNG_FILE_PATH = "testImage.png";
	const char* BYTES_FILE_PATH = "atlas_cards.bytes";
	//const char* XML_FILE_PATH = "Game.xml";

	//xmlDocPtr doc;
	//xmlNodePtr cur;
	//
	//doc = xmlParseFile(XML_FILE_PATH);
	
	/*if (doc == NULL)
	{
		std::cout << "Xml document parse failed";
		exit(1);
	}

	cur = xmlDocGetRootElement(doc);*/



	AtlasReader ar;
	ar.readFile(BYTES_FILE_PATH);

	std::map<int, Selection> m = ar.getTextures();
	std::vector<unsigned char> png = ar.getPng();
	
	SDL_RWops* rw = SDL_RWFromMem(png.data(), png.size());
	SDL_Surface* sur = IMG_Load_RW(rw, 0);
	
	if (!sur)
	{
		std::cout << "IMG_Load_RW error %s", SDL_GetError();
		exit(1);
	}

	SDL_Texture* t = SDL_CreateTextureFromSurface(renderer,sur);
	SDL_Rect srcrect;
	SDL_Rect dstrect;

	dstrect.x = 0;
	dstrect.y = 0;
	/*
	for (std::pair<int,Selection> e : m)
	{
		srcrect.x = e.second.x;
		srcrect.y = e.second.y;
		srcrect.w = e.second.w;
		srcrect.h = e.second.h;

		dstrect.w = srcrect.w;
		dstrect.h = srcrect.h;

		SDL_RenderCopy(renderer, t, &srcrect, &dstrect);
		SDL_RenderPresent(renderer);
		SDL_Delay(100);
		SDL_RenderClear(renderer);
	}
	*/
	imgSurface = IMG_Load(PNG_FILE_PATH);

	if (!imgSurface) {
		std::cout << "Failed to load image. " << SDL_GetError() << std::endl;
		exit(1);
	}
	else {
		std::cout << "Image loaded." << std::endl;
	}

	map = new SDL_Color[(__int64)(imgSurface->w * imgSurface->h)];
	//map = new unsigned char[(__int64)(imgSurface->w * imgSurface->h)];
	
	SDL_LockSurface(imgSurface);

	for (int i = 0; i < imgSurface->h; i++)
	{
		for (int j = 0; j < imgSurface->w; j++)
		{
			Uint32 pixel = get_pixel(imgSurface, i, j);
			SDL_GetRGBA(pixel, imgSurface->format, &map[i * imgSurface->w + j].r,
				&map[i * imgSurface->w + j].g, &map[i * imgSurface->w + j].b, &map[i * imgSurface->w + j].a);
		}
	}

	SDL_UnlockSurface(imgSurface);
}

void
render()
{
	int i, j, k;

	SkPaint paint;
	//SkColor color;
	//SkRect rect;

	//SkPoint size = { 5, 5 };
	//SkPoint position = { 0, 0 };

	for (i = 0; i < imgSurface->h; i++)
	{
		for (j = 0; j < imgSurface->w; j++)
		{
			paint.setColor(SkColorSetARGB((unsigned char)map[i * imgSurface->w + j].a, (unsigned char)map[i * imgSurface->w + j].r,
				(unsigned char)map[i * imgSurface->w + j].g, (unsigned char)map[i * imgSurface->w + j].b));
			
			//position.iset(i * 5, j * 5);
			//size.iset(i * 5 + 5, j * 5 + 5);

			//rect.set(position, size);

			canvas->drawRect(SkRect::MakeXYWH(i * 2, j * 2, 2, 2), paint);

		}
	}
}

/*
void
render()
{
	int i, j, k;
	int x = 0;
	int y = 0;
	// static float t = 0.0f;
	//Clear color buffer
	// glClear( GL_COLOR_BUFFER_BIT );
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	//Render quad
	if (grender_quad)
	{
		glEnable(GL_BLEND);
		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
		glEnable(GL_CULL_FACE);
		glDisable(GL_DEPTH_TEST);
		glEnable(GL_STENCIL_TEST);



		nvgBeginFrame(vg, ww, wh, pxratio);
		
		for (i = 0; i < imgSurface->h; i++)
		{
			for (j = 0; j < imgSurface->w; j++)
			{
				nvgBeginPath(vg);
				nvgFillColor(vg, nvgRGBA((unsigned char)map[i * imgSurface->w + j].r, (unsigned char)map[i * imgSurface->w + j].g,
					(unsigned char)map[i * imgSurface->w + j].b, (unsigned char)map[i * imgSurface->w + j].a));
				nvgRect(vg, i * 5, j * 5, 5, 5);
				nvgClosePath(vg);
				nvgFill(vg);
				
			}
			x = 0;
		}
		
		static float a = 0.0f;
		
		//for (i = 0; i < shps->size(); i++) {
		//	float r, g, b;
		//	float cx, cy, radius;
		//	std::vector<cmdcntr> *cntrs;

		//	r = shps->at(i).r;
		//	g = shps->at(i).g;
		//	b = shps->at(i).b;

		//	cntrs = shps->at(i).cntrs;

		//	nvgStrokeWidth(vg, 2.0f);
		//	nvgStrokeColor(vg, nvgRGBA(r, g, b, 255));
		//	nvgFillColor(vg, nvgRGBA(shapes->at(i).r, shapes->at(i).g,
		//		shapes->at(i).b, 255));

		//	nvgBeginPath(vg);
		//	for (j = 0; j < cntrs->size(); j++) {
		//		std::vector<pntrcmd *> *cmds;

		//		cmds = cntrs->at(j).cmds;

		//		// nvgBeginPath(vg);
		//		for (k = 0; k < cmds->size(); k++)
		//			cmds->at(k)->exec(vg);
		//		// nvgClosePath(vg);
		//		if (!cntrs->at(j).outer)
		//			nvgPathWinding(vg, NVG_HOLE);


		//	}


		//	nvgClosePath(vg);

		//	cx = shapes->at(i).cx;
		//	cy = shapes->at(i).cy;
		//	radius = shapes->at(i).radius;

		//	if (shapes->at(i).r > 50 || shapes->at(i).g > 50 ||
		//		shapes->at(i).b > 50) {
		//		NVGpaint paint;
		//		NVGcolor incl, outcl;

		//		incl = nvgRGBA(shapes->at(i).r, shapes->at(i).g,
		//			shapes->at(i).b, 255);
		//		outcl = nvgRGBA(255, 255, 255, 0);

		//		paint = nvgRadialGradient(vg, cx, cy, radius * a, 0, incl, outcl);
		//		nvgFillPaint(vg, paint);
		//		// nvgBeginPath(vg);
		//		// nvgCircle(vg, cx,cy,radius);
		//		// nvgRect(vg, l, b, r - l, t - b);
		//		// nvgFill(vg);
		//		// nvgClosePath(vg);
		//	}
		//	else {
		//		// nvgFill(vg);

		//	}

		//	nvgStroke(vg);
		//	nvgFill(vg);


		//}

		 	/*nvgBeginPath(vg);
			NVGpaint paint = nvgRadialGradient(vg, 350,350, a * 450,0, nvgRGBA(0,0,0,255), nvgRGBA(255,0,0,255));
			nvgFillColor(vg, nvgRGBA(255, 0,
					0, 255));
			nvgRect(vg, 300,300,100,100);
			nvgPathWinding(vg, NVG_SOLID);



			 //nvgCircle(vg, 350,350,30);
			 //nvgPathWinding(vg, NVG_HOLE);

			nvgFillPaint(vg, paint);
			nvgFill(vg);

		nvgEndFrame(vg);

		a += 0.01f;
		if (a > 1)
			a = 0.0f;
	}

	// t += 0.05f;
}
*/

// isLeft(): tests if a point is Left|On|Right of an infinite line.
//    Input:  three points P0, P1, and P2
//    Return: >0 for P2 left of the line through P0 and P1
//            =0 for P2  on the line
//            <0 for P2  right of the line
//    See: Algorithm 1 "Area of Triangles and Polygons"
inline int
islft(painter_point &p0, painter_point &p1, painter_point p2)
{
	return ((p1.x - p0.x) * (p2.y - p0.y)
		- (p2.x - p0.x) * (p1.y - p0.y));
}

// wn_PnPoly(): winding number test for a point in a polygon
//      Input:   P = a point,
//               V[] = vertex points of a polygon V[n+1] with V[n]=V[0]
//      Return:  wn = the winding number (=0 only when P is outside)
int
wnpnpoly(painter_point &p, std::vector<painter_point> *pts)
{
	int wn, i;    // the  winding number counter

	// loop through all edges of the polygon
	wn = 0;
	for (i = 0; i < pts->size(); i++) {  // edge from V[i] to  V[i+1]
		int i1;

		i1 = (i + 1) % pts->size();
		if (pts->at(i).y <= p.y) {          // start y <= P.y
			if (pts->at(i1).y > p.y)      // an upward crossing
				if (islft(pts->at(i), pts->at(i1), p) > 0)  // P left of  edge
					wn++;            // have  a valid up intersect
		}
		else {                        // start y > P.y (no test needed)
			if (pts->at(i1).y <= p.y)     // a downward crossing
				if (islft(pts->at(i), pts->at(i1), p) < 0)  // P right of  edge
					--wn;            // have  a valid down intersect
		}
	}
	return wn;
}

bool
isinshp(painter_shape &shp, int x, int y)
{
	int i;
	bool inouter, ininner;
	struct painter_point testpt;

	testpt.x = x;
	testpt.y = y;

	inouter = ininner = false;
	if (x >= shp.lt && x <= shp.rt && y >= shp.bm && y <= shp.tp) {

		for (i = 0; i < shp.contours->size(); i++) {
			if (!shp.contours->at(i).outer)
				continue;

			if (wnpnpoly(testpt, shp.contours->at(i).pts) != 0) {
				inouter = true;
				break;
			}
		}
	}

	if (inouter) {
		for (i = 0; i < shp.contours->size(); i++) {
			if (shp.contours->at(i).outer)
				continue;

			if (x >= shp.contours->at(i).lt && x <= shp.contours->at(i).rt &&
				y >= shp.contours->at(i).bm && y <= shp.contours->at(i).tp) {
				if (wnpnpoly(testpt, shp.contours->at(i).pts) != 0) {
					ininner = true;
					break;
				}
			}
		}
	}

	return inouter && !ininner;
}


bool
init_gl()
{
	bool success = true;
	GLuint vtxshdr, fragshdr;
	GLint vtxshdr_compiled;
	//Get vertex source
	const GLchar* vtxshdr_src[] =
	{
		"#version 140\nin vec2 LVertexPos2D; void main() { gl_Position = vec4( LVertexPos2D.x, LVertexPos2D.y, 0, 1 ); }"
	};
	//Get fragment source
	const GLchar* fragshdr_src[] =
	{
		"#version 140\nout vec4 LFragment; void main() { LFragment = vec4( 1.0, 1.0, 1.0, 1.0 ); }"
	};

	//Generate program
	gprogid = glCreateProgram();

	vtxshdr = glCreateShader(GL_VERTEX_SHADER);



	//Set vertex source
	glShaderSource(vtxshdr, 1, vtxshdr_src, NULL);

	//Compile vertex source
	glCompileShader(vtxshdr);

	//Check vertex shader for errors
	vtxshdr_compiled = GL_FALSE;
	glGetShaderiv(vtxshdr, GL_COMPILE_STATUS, &vtxshdr_compiled);
	if (vtxshdr_compiled != GL_TRUE) {
		std::cerr << "Unable to compile vertex shader " <<
			vtxshdr << std::endl;
		// printShaderLog( vertexShader );
		success = false;
	}
	else {
		GLint fragshdr_compiled;

		//Attach vertex shader to program
		glAttachShader(gprogid, vtxshdr);

		//Create fragment shader
		fragshdr = glCreateShader(GL_FRAGMENT_SHADER);



		//Set fragment source
		glShaderSource(fragshdr, 1, fragshdr_src, NULL);

		//Compile fragment source
		glCompileShader(fragshdr);

		//Check fragment shader for errors
		fragshdr_compiled = GL_FALSE;
		glGetShaderiv(fragshdr, GL_COMPILE_STATUS, &fragshdr_compiled);
		if (fragshdr_compiled != GL_TRUE) {
			std::cerr << "Unable to compile fragment shader" << fragshdr << std::endl;
			success = false;
		}
		else {
			GLint progsuc;
			//Attach fragment shader to program
			glAttachShader(gprogid, fragshdr);


			//Link program
			glLinkProgram(gprogid);

			//Check for errors
			progsuc = GL_TRUE;
			glGetProgramiv(gprogid, GL_LINK_STATUS, &progsuc);
			if (progsuc != GL_TRUE) {
				// printf( "Error linking program %d!\n", gProgramID );
				// printProgramLog( gProgramID );
				std::cerr << "err getting program iv" << std::endl;
				success = false;
			}
			else {
				//Get vertex attribute location
				gvtxpos_2dloc = glGetAttribLocation(gprogid, "LVertexPos2D");
				if (gvtxpos_2dloc == -1) {
					std::cerr <<
						"LVertexPos2D is not a valid glsl program variable!" <<
						std::endl;
					success = false;
				}
				else {
					//Initialize clear color
					glClearColor(0.8f, 0.8f, 0.8f, 1.f);

					//VBO data
					GLfloat vertexData[] =
					{
						-0.5f, -0.5f,
						 0.5f, -0.5f,
						 0.5f,  0.5f,
						-0.5f,  0.5f
					};

					//IBO data
					GLuint indexData[] = { 0, 1, 2, 3 };

					//Create VBO
					glGenBuffers(1, &gvbo);
					glBindBuffer(GL_ARRAY_BUFFER, gvbo);
					glBufferData(GL_ARRAY_BUFFER, 2 * 4 * sizeof(GLfloat),
						vertexData, GL_STATIC_DRAW);

					//Create IBO
					glGenBuffers(1, &gibo);
					glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gibo);
					glBufferData(GL_ELEMENT_ARRAY_BUFFER, 4 * sizeof(GLuint),
						indexData, GL_STATIC_DRAW);
				}
			}
		}
	}

	return success;
}

void handlekeys(unsigned char key, int x, int y)
{
	//Toggle quad
	if (key == 'q')
	{
		grender_quad = !grender_quad;
	}
}

int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PWSTR pCmdLine, int nCmdShow)
{
	int i, j, k;
	std::ifstream stream_cmds, stream_collider;
	std::string shppath = "resources/pony.shp";

	stream_cmds.open(shppath);
	shps = rdcmds(stream_cmds);
	stream_cmds.close();
	std::cout << shps->size() << std::endl;
	stream_collider.open(shppath);
	shapes = shapes_fread(stream_collider);

	/* 	if (shapes == NULL) {
			fprintf(stderr, "SHAPES IS NULL");
			exit(1);
		} */

	for (i = 0; i < shapes->size(); i++) {
		std::vector<painter_contour> *contours;
		std::vector<cmdcntr> *cmdcntrs;

		contours = shapes->at(i).contours;
		cmdcntrs = shps->at(i).cntrs;

		for (j = 0; j < contours->size(); j++)
			cmdcntrs->at(j).outer = contours->at(j).outer;
	}

	if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
		fprintf(stderr, "error initializing SDL\n");
		exit(1);
	}

	if (SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3) < 0) {
		std::cerr << "err setting major version of gl " << SDL_GetError() << std::endl;
		exit(1);
	}
	if (SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0) < 0) {
		std::cerr << "err setting minor version of gl " << SDL_GetError() << std::endl;
		exit(1);
	}

	if (SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
		SDL_GL_CONTEXT_PROFILE_CORE) < 0) {
		std::cerr << "err setting profile of gl " << SDL_GetError() << std::endl;
		exit(1);
	}

	if (SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 1) < 0) {
		std::cerr << "err setting stencil size to 1 " << SDL_GetError() << std::endl;
		exit(1);
	}

	if (IMG_Init(IMG_INIT_PNG) != IMG_INIT_PNG) {
		std::cerr << "err setting img init " << SDL_GetError() << std::endl;
		exit(1);
	}

	window = SDL_CreateWindow("", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
		ww, wh, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);
	renderer = SDL_CreateRenderer(window, -1,
		SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);

	load_img();


	// Create an OpenGL context associated with the window.
	glctx = SDL_GL_CreateContext(window);
	//glcreateContext
	if (glctx == NULL) {
		std::cerr << "err creating gl context " << SDL_GetError() << " " << glGetError() << std::endl;
		exit(1);
	}
	else {
		 	GLenum glewError;

			glewError = glewInit();
			if( glewError != GLEW_OK ) {
				std::cerr << "Error initializing GLEW! " <<
					glewGetErrorString(glewError) << std::endl;
				exit(1);
			}
	 
		if (SDL_GL_SetSwapInterval(1) < 0) {
			std::cerr << "err setting swap interval " << SDL_GetError() << std::endl;
			// exit(1); 
		}

		if (!init_gl()) {
			std::cerr << "err initing opengl" << std::endl;
			exit(1);
		}
	}
	/*
	vg = nvgCreateGL3(NVG_ANTIALIAS | NVG_STENCIL_STROKES | NVG_DEBUG);;
	if (vg == NULL) {
		std::cerr << "err creating nanovg context" << std::endl;
		exit(1);
	}
	*/
	

	//Main loop flag
	bool quit = false;

	//Event handler
	SDL_Event e;

	//Enable text input
	SDL_StartTextInput();

	auto skia_interface = GrGLMakeNativeInterface();

	sk_sp<GrContext> context(GrContext::MakeGL(skia_interface));

	if (!context) {
		std::cerr << "Err creating skia context" << std::endl;
		return 1;
	}

	GrGLFramebufferInfo info;
	SkColorType colorType;

	uint32_t windowFormat = SDL_GetWindowPixelFormat(window);
	int contextType;
	SDL_GL_GetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, &contextType);

	if (SDL_PIXELFORMAT_RGBA8888 == windowFormat) {
		info.fFormat = GR_GL_RGBA8;
		colorType = kRGBA_8888_SkColorType;
	}
	else {
		colorType = kBGRA_8888_SkColorType;
		
		if (SDL_GL_CONTEXT_PROFILE_ES == windowFormat) {
			info.fFormat = GR_GL_BGRA8;
		}
		else
		{
			info.fFormat = GR_GL_RGBA8;
		}
	}

	static const int kMsaaSampleCount = 0;
	GrBackendRenderTarget target(ww, wh, kMsaaSampleCount, 8, info);

	SkSurfaceProps props(SkSurfaceProps::kLegacyFontHost_InitType);

	sk_sp<SkSurface> surface(SkSurface::MakeFromBackendRenderTarget(context.get(), target, kBottomLeft_GrSurfaceOrigin, colorType, nullptr, &props));

	if (!surface) {
		SkDebugf("SkSurface::MakeRenderTarget returned null\n");
		return 1;
	}

	canvas = surface->getCanvas();

	glClearColor(0.8f, 0.8f, 0.8f, 1.f);
	//While application is running
	while (!quit)
	{
		//Handle events on queue
		while (SDL_PollEvent(&e) != 0)
		{
			//User requests quit
			if (e.type == SDL_MOUSEBUTTONDOWN) {
				int x, y;

				SDL_GetMouseState(&x, &y);
				for (i = 0; i < shapes->size(); i++) {
					if (isinshp(shapes->at(i), x, y)) {
						std::cout << "isinshp true" << std::endl;
						break;
					}
				}
			}
			else if (e.type == SDL_QUIT) {
				quit = true;
			}
			//Handle keypress with current mouse position
			else if (e.type == SDL_TEXTINPUT)
			{
				int x = 0, y = 0;
				SDL_GetMouseState(&x, &y);
				handlekeys(e.text.text[0], x, y);
			}
		}

		//Render quad
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		render();

		canvas->flush();

		//Update screen
		SDL_GL_SwapWindow(window);
	}

	delete map;

	//Disable text input
	SDL_StopTextInput();

	SDL_Quit();
	return 0;
}