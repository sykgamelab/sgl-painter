﻿using SkiaSharp;
using System.Collections.Generic;
using System.Runtime.Remoting.Contexts;
using System.Threading;

namespace AndroidCS
{
    public struct PaletteColor
    {
        public byte r, g, b;

        public void Set(byte red, byte green, byte blue)
        {
            r = red;
            g = green;
            b = blue;
        }
    }

    public static class ColoringState
    {
        public static float Ww { get; set; }
        public static float Wh { get; set; }
        public static float Offsetx { get; set; }
        public static float Offsety { get; set; }
        public static float BufferOffsetx { get; set; }
        public static float BufferOffsety { get; set; }
        public static float Scale { get; set; }
        public static float TotalTime { get; set; }
        public static float Lt { get; set; }
        public static float Rt { get; set; }
        public static float Bm { get; set; }
        public static float Tp { get; set; }
        public static float MaxBufferL { get; set; }
        public static float MinBufferR { get; set; }
        public static float MaxBufferB { get; set; }
        public static float MinBufferT { get; set; }
        public static int ImgWH { get; set; }
        public static float MaxZoom { get; set; } = 7.0f;
        public static float MaxImgZoom { get; set; } = 7.0f;
        public static float MinZoom { get; set; } = 0.13f;
        public static float PalettePositionY { get; set; }
        public static float PaletteOffsetX { get; set; }
        public static float MaxPaletteLeft { get; set; }
        public static float MaxPaletteRight { get; set; }
        public static float PaletteWH { get; set; }
        public static float HistoryWH { get; set; }
        public static PaletteColor CurrentColor { get; set; }
        public static int PrevColorIndex { get; set; } = 0;
        public static int CurrentColorIndex { get; set; } = 0;
        public static List<CommandShape> CommandShapes { get; set; }
        public static List<(int, PaletteColor, (float x, float y))> Palette { get; set; }
        public static List<PainterCommon.PainterShape> Shapes { get; set; }
        public static List<PainterCommon.PainterPointf> Clicks { get; set; }
        public static Mutex Mut { get; set; }
        public static Queue<(int, PaletteColor, bool?, bool?, bool?, int?, int?)> Queue { get; set; }
        public static Context AppContext { get; set; }
        public static List<(SKImage image, SKRect rectSrc, SKRect rectDst)> controlsGUI { get; set; }
        public static Stack<ICommand> Undo { get; set; }
        public static Stack<ICommand> Redo { get; set; }
        public static int PaintedShapesCount { get; set; }
        public static float ProgressBarPercent { get; set; }
        public static SKCanvas OffscreenCanvas { get; set; }
        public static Dictionary<int, List<int>> HighlightedShapes { get; set; }
        public static int ViewBox { get; set; }
        public static (SKImage, SKImage) PngImages { get; set; }
        public static SKImage Contours { get; set; }
        public static SKImage Regions { get; set; }
        public static SKImage BgImage { get; set; }
        public static SKImage FgImage { get; set; }
    }
}
