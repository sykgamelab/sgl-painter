﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.IO.Compression;

namespace AndroidCS
{
    public class WebHelper
    {
        private string url = null;

        public WebHelper(string url)
        {
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            this.url = url;
        }

        public void GetColoring(Action<int, byte[]> callback, string request, string cid, bool isPixel)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(url);
            builder.Append(request);
            builder.Append($"?cid={WebUtility.UrlEncode(cid)}");
            builder.Append($"&apiKey={WebUtility.UrlEncode(ServerConstants.BigColoringBookApiKey)}");
            builder.Append($"&isPixel={isPixel}");
            GetRequest(builder.ToString(), callback);
        }

        private void GetRequest(string url, Action<int, byte[]> action, string ticket = null)
        {
            HttpWebResponse response = null;
            StreamReader reader = null;

            try
            {
                HttpWebRequest request = WebRequest.CreateHttp(url);
                if (ticket != null)
                {
                    if (request.CookieContainer == null)
                    {
                        request.CookieContainer = new CookieContainer();
                    }
                    Cookie cookie = new Cookie("ticket", ticket, "/", "192.168.1.60");
                    request.CookieContainer.Add(cookie);
                }

                request.Timeout = 10 * 60 * 1000;
                response = (HttpWebResponse)request.GetResponse();
                //string status = GetCorrectStatusCode((int)response.StatusCode);

                byte[] data = null;
#if __ANDROID__
                using (Stream stream = response.GetResponseStream())
                {
                    using (MemoryStream ms = new MemoryStream())
                    {
                        stream.CopyTo(ms);
                        data = ms.ToArray();
                    }

                    //using (ZipArchive archive = new ZipArchive(stream, ZipArchiveMode.Read))
                    //{
                    //    ZipArchiveEntry entry = archive.Entries[0];
                    //    using (Stream entryStream = entry.Open())
                    //    {
                    //        using (MemoryStream ms = new MemoryStream())
                    //        {
                    //            entryStream.CopyTo(ms);
                    //            data = ms.ToArray();
                    //        }
                    //    }
                    //}
                }
#endif
                action((int)response.StatusCode, data);
            }
            catch (WebException e)
            {
                response = e.Response as HttpWebResponse;
                if (response != null)
                {
                    string err = "";
                    reader = new StreamReader(response.GetResponseStream());
                    err = reader.ReadToEnd();
                    action((int)response.StatusCode, Encoding.ASCII.GetBytes($"{err} | {e.Message}"));
                    reader.Close();
                }
                else
                {
                    action(-1, Encoding.ASCII.GetBytes(e.Message));
                }
            }
            catch (Exception e)
            {
                action(-1, Encoding.ASCII.GetBytes(e.Message));
            }
        }

    }
}
