﻿
namespace AndroidCS
{
    public interface ICommand
    {
        void Exec();
        void Revert();
    }
}