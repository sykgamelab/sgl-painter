﻿using SkiaSharp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;

namespace AndroidCS
{
    public static class URPNGLoader
    {
        private static Dictionary<string, (SKImage, Dictionary<int, SKRect>)> layouts = null;
        private static Dictionary<string, Dictionary<int, URPNGReader.Selection>> selectionLayouts = null;

        public static Tuple<SKImage, SKRect> LoadURPNG(Assembly assembly, string resourceID, string layoutName, int idx)
        {
            SKRect rect = new SKRect();
            Tuple<SKImage, SKRect> emptyTuple = new Tuple<SKImage, SKRect>(null, rect);

            if (layouts == null) layouts = new Dictionary<string, (SKImage, Dictionary<int, SKRect>)>();

            if (!layouts.ContainsKey(layoutName))
            {
                using (Stream stream = assembly.GetManifestResourceStream(resourceID))
                {
                    URPNGData urpngData = URPNGReader.ReadURPNG(stream);

                    Dictionary<int, SKRect> layout = new Dictionary<int, SKRect>();

                    for (int i = 0; i < urpngData.Selections.Length; i++)
                    {
                        int num = urpngData.Selections[i].num;
                        layout.Add(num, urpngData.Rectangles[i]);
                    }

                    layouts.Add(layoutName, (urpngData.Texture, layout));
                    if (!layouts.ContainsKey(layoutName)) return emptyTuple;
                }
            }

            if (!layouts[layoutName].Item2.ContainsKey(idx)) 
            {
                Console.WriteLine($"URPNGLoader: index {idx} is not present at layout {layoutName}.");
                return emptyTuple;
            }

            Tuple<SKImage, SKRect> data = new Tuple<SKImage, SKRect>(layouts[layoutName].Item1, layouts[layoutName].Item2[idx]);
            return data;
        }

        public static (float w, float h) LoadOriginalSize(string layoutName, int idx)
        {
             return (selectionLayouts[layoutName][idx].w, selectionLayouts[layoutName][idx].h);
        }
    }
}
