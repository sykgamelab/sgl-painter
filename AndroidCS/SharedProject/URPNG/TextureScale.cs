﻿using SkiaSharp;
using System;
using System.Threading;

namespace AndroidCS
{
    public class TextureScale
    {
        public class ThreadData
        {
            public int start;
            public int end;
            public ThreadData(int s, int e)
            {
                start = s;
                end = e;
            }
        }

        private static SKColor[] texColors;
        private static SKColor[] newColors;
        private static int w;
        private static float ratioX;
        private static float ratioY;
        private static int w2;
        private static int finishCount;
        private static Mutex mutex;

        public static void Point(SKBitmap tex, int newWidth, int newHeight)
        {
            ThreadedScale(tex, newWidth, newHeight, false);
        }

        public static void Bilinear(SKBitmap tex, int newWidth, int newHeight)
        {
            ThreadedScale(tex, newWidth, newHeight, true);
        }

        private static void ThreadedScale(SKBitmap tex, int newWidth, int newHeight, bool useBilinear)
        {
            // уплощенный 2D-массив, в котором пиксели располагаются слева направо, снизу вверх (т.е. строка за строкой)
            texColors = tex.Pixels;
            /*texColors = new SKColor[tex.Width * tex.Height];
            int x = 0, y = -1;
            for (int i = 0; i < texColors.Length; i++)
            {
                if (i % tex.Width == 0)
                {
                    x = 0;
                    y++;
                }
                else
                {
                    x++;
                }
                texColors[i] = tex.GetPixel(x, y);
            }*/

            newColors = new SKColor[newWidth * newHeight];
            if (useBilinear)
            {
                ratioX = 1.0f / ((float)newWidth / (tex.Width - 1));
                ratioY = 1.0f / ((float)newHeight / (tex.Height - 1));
            }
            else
            {
                ratioX = ((float)tex.Width) / newWidth;
                ratioY = ((float)tex.Height) / newHeight;
            }
            w = tex.Width;
            w2 = newWidth;
            var cores = Math.Min(Environment.ProcessorCount, newHeight);
            var slice = newHeight / cores;

            finishCount = 0;
            if (mutex == null)
            {
                mutex = new Mutex(false);
            }
            if (cores > 1)
            {
                int i = 0;
                ThreadData threadData;
                for (i = 0; i < cores - 1; i++)
                {
                    threadData = new ThreadData(slice * i, slice * (i + 1));
                    ParameterizedThreadStart ts = useBilinear ? new ParameterizedThreadStart(BilinearScale) : new ParameterizedThreadStart(PointScale);
                    Thread thread = new Thread(ts);
                    thread.Start(threadData);
                }
                threadData = new ThreadData(slice * i, newHeight);
                if (useBilinear)
                {
                    BilinearScale(threadData);
                }
                else
                {
                    PointScale(threadData);
                }
                while (finishCount < cores)
                {
                    Thread.Sleep(1);
                }
            }
            else
            {
                ThreadData threadData = new ThreadData(0, newHeight);
                if (useBilinear)
                {
                    BilinearScale(threadData);
                }
                else
                {
                    PointScale(threadData);
                }
            }

            SKImageInfo resizeInfo = new SKImageInfo(newWidth, newHeight);
            tex = tex.Resize(resizeInfo, SKFilterQuality.None);
            
            int x = 0, y = -1;
            for (int i = 0; i < newColors.Length; i++)
            {
                if (i % newWidth == 0)
                {
                    x = 0;
                    y++;
                }
                else
                {
                    x++;
                }
                tex.SetPixel(x, y, newColors[i]);
            }

            mutex = null;
            texColors = null;
            newColors = null;
            GC.Collect();
        }

        public static void BilinearScale(object obj)
        {
            ThreadData threadData = (ThreadData)obj;
            for (var y = threadData.start; y < threadData.end; y++)
            {
                int yFloor = (int)Math.Floor(y * ratioY);
                var y1 = yFloor * w;
                var y2 = (yFloor + 1) * w;
                var yw = y * w2;

                for (var x = 0; x < w2; x++)
                {
                    int xFloor = (int)Math.Floor(x * ratioX);
                    var xLerp = x * ratioX - xFloor;
                    newColors[yw + x] = ColorLerpUnclamped(ColorLerpUnclamped(texColors[y1 + xFloor], texColors[y1 + xFloor + 1], xLerp),
                                                           ColorLerpUnclamped(texColors[y2 + xFloor], texColors[y2 + xFloor + 1], xLerp),
                                                           y * ratioY - yFloor);
                }
            }

            mutex.WaitOne();
            finishCount++;
            mutex.ReleaseMutex();
        }

        public static void PointScale(object obj)
        {
            ThreadData threadData = (ThreadData)obj;
            for (var y = threadData.start; y < threadData.end; y++)
            {
                var thisY = (int)(ratioY * y) * w;
                var yw = y * w2;
                for (var x = 0; x < w2; x++)
                {
                    newColors[yw + x] = texColors[(int)(thisY + ratioX * x)];
                }
            }

            mutex.WaitOne();
            finishCount++;
            mutex.ReleaseMutex();
        }

        private static SKColor ColorLerpUnclamped(SKColor c1, SKColor c2, float value)
        {
            byte red = (byte)(c1.Red + (c2.Red - c1.Red) * value);
            byte green = (byte)(c1.Green + (c2.Green - c1.Green) * value);
            byte blue = (byte)(c1.Blue + (c2.Blue - c1.Blue) * value);
            byte alpha = (byte)(c1.Alpha + (c2.Alpha - c1.Alpha) * value);
            SKColor color = new SKColor(red, green, blue, alpha);
            return color;
        }
    }
}
