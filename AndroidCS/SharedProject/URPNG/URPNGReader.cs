﻿using SkiaSharp;
using System;
using System.IO;

namespace AndroidCS
{
    public static class URPNGReader
    {
        /// <summary>
        /// Выделение на атласе
        /// </summary>
        /// <param name="bx">Координата x рамки</param>
        /// <param name="bw">Ширина рамки</param>
        public struct Selection
        {
            public int num, x, y, w, h, bx, by, bz, bw;
        }

        public static URPNGData ReadURPNG(Stream stream)
        {
            using (BinaryReader br = new BinaryReader(stream))
            {
                int count = br.ReadInt32();
                Selection[] selections = new Selection[count];
                for (int i = 0; i < count; i++)
                {
                    selections[i] = new Selection
                    {
                        num = br.ReadInt32(),
                        x = br.ReadInt32(),
                        y = br.ReadInt32(),
                        w = br.ReadInt32(),
                        h = br.ReadInt32(),
                        bx = br.ReadInt32(),
                        by = br.ReadInt32(),
                        bz = br.ReadInt32(),
                        bw = br.ReadInt32()
                    };
                    int crc = br.ReadInt32();
                }

                int chunkSize = 32768;
                byte[] data = new byte[chunkSize];

                count = 0;
                int read = 0;

                while ((read = br.Read(data, count, chunkSize)) > 0)
                {
                    count += read;
                    if (chunkSize + count > data.Length)
                    {
                        Array.Resize(ref data, data.Length * 2);
                    }
                }

                Array.Resize(ref data, count);

                MemoryStream memoryStream = new MemoryStream(data);
                SKBitmap bitmap = SKBitmap.Decode(memoryStream);
                SKImage texture = SKImage.FromBitmap(bitmap);

                SKRect[] rectangles;
                if (selections.Length > 0)
                {
                    rectangles = new SKRect[selections.Length];
                    for (int i = 0; i < selections.Length; i++)
                    {
                        Selection s = selections[i];
                        rectangles[i] = SKRect.Create(s.x, s.y, s.w, s.h);
                    }
                }
                else
                {
                    rectangles = new SKRect[1];
                    rectangles[0] = SKRect.Create(0, 0, bitmap.Info.Width, bitmap.Info.Height);
                }

                URPNGData urpngData = new URPNGData(selections, rectangles, texture);
                return urpngData;
            }
        }
    }
}
