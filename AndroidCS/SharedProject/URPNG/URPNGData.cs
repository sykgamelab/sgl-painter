﻿using SkiaSharp;
using System;
using System.Collections.Generic;
using System.Text;

namespace AndroidCS
{
    public class URPNGData
    {
        public URPNGReader.Selection[] Selections { get; private set; }
        public SKRect[] Rectangles { get; private set; }
        public SKImage Texture { get; private set; }

        public URPNGData(URPNGReader.Selection[] selections, SKRect[] rectangles, SKImage texture)
        {
            Selections = selections;
            Rectangles = rectangles;
            Texture = texture;
        }
    }
}
