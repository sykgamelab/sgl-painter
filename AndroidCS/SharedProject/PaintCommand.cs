﻿using System.Collections.Generic;
using System.Threading;


namespace AndroidCS
{
    class PaintCommand : ICommand
    {
        private int Index { get; set; }
        private Mutex Mut { get; set; }
        private Queue<(int, PaletteColor, bool?, bool?, bool?, int?, int?)> Queue { get; set; }
        private PaletteColor PrevColor { get; set; }
        private PaletteColor NewColor { get; set; }
        private bool PrevIsDrawn { get; set; }
        private bool PrevIsHighlighted { get; set; }
        private bool NewIsDrawn { get; set; }
        private bool NewIsHighlighted { get; set; }
        private int PrevProgress { get; set; }
        private int NewProgress { get; set; }

        public PaintCommand(int index, PaletteColor prevColor, PaletteColor newColor,
            bool prevIsDrawn, bool prevIsHighlighted, bool newIsDraw, bool newIsHighlighted,
            Mutex mut, Queue<(int, PaletteColor, bool?, bool?, bool?, int?, int?)> queue,
            int newProgress, int prevProgress)
        {
            Index = index;

            PrevColor = prevColor;
            NewColor = newColor;

            PrevIsDrawn = prevIsDrawn;
            NewIsDrawn = newIsDraw;

            PrevIsHighlighted = prevIsHighlighted;
            NewIsHighlighted = newIsHighlighted;

            NewProgress = newProgress;
            PrevProgress = prevProgress;

            Mut = mut;
            Queue = queue;
        }

        public void Exec()
        {
            //redo
            (int, PaletteColor, bool?, bool?, bool?, int?, int?) tuple = (Index, NewColor, NewIsHighlighted, false, NewIsDrawn, NewProgress, PrevProgress);

            Mut.WaitOne();
            Queue.Enqueue(tuple);
            Mut.ReleaseMutex();
        }

        public void Revert()
        {
            //undo
            (int, PaletteColor, bool?, bool?, bool?, int?, int?) tuple = (Index, PrevColor, PrevIsHighlighted, true, PrevIsDrawn, NewProgress, PrevProgress);

            Mut.WaitOne();
            Queue.Enqueue(tuple);
            Mut.ReleaseMutex();
        }
    }
}