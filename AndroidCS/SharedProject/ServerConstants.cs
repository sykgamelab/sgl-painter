﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AndroidCS
{
    public static class ServerConstants
    {
        public const string Url = "https://192.168.1.60:8080/";
        public const string BigColoringBookApiKey = "ed392b8bd07440902f4e7ee24f444bfd8f2fa8b79438549fa9be1a0a66bd5396";

        public const string Cid = "226";

        public const string GetColoring = "BigColoringBook/GetColoring";
    }
}
