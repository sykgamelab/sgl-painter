﻿using System;


#if __ANDROID__
using Android.Views;
using Android.Content;

//#if __IOS__
//using UIKit;
////using CoreGraphics;
////using Foundation;
//#endif


namespace AndroidCS
{
    internal class ScaleListener : ScaleGestureDetector.SimpleOnScaleGestureListener
    {
        public Action<float, float, float> Callback { get; set; } = null;
        public override bool OnScale(ScaleGestureDetector detector)
        {
            Callback?.Invoke(detector.ScaleFactor, detector.FocusX, detector.FocusY);
            return true;
        }
    }

    static class Gestures
    {
        static int nfingers = 0;
        static float clickStart = 0f;
        static float clickx, clicky;
        static bool mayBeClick = false;
        static float lastX, lastY, dx, dy;
        static float lastPaletteOffsetX, paletteDx;
        static bool isStartedOnPalette;
        static bool zeroUp;

        static ScaleListener scaleListener;
        static ScaleGestureDetector scaleDetector;

        public static void Initialize(Context context)
        {
            scaleListener = new ScaleListener();
            scaleListener.Callback = (float factor, float focusX, float focusY) =>
            {
                float newscale = ColoringState.Scale * factor;

                if (ColoringState.MinZoom > newscale)
                    newscale = ColoringState.MinZoom;
                else if (ColoringState.MaxZoom < newscale)
                    newscale = ColoringState.MaxZoom;

                ColoringState.Offsetx = ((ColoringState.Offsetx - focusX) * newscale) / ColoringState.Scale + focusX;
                ColoringState.Offsety = ((ColoringState.Offsety - focusY) * newscale) / ColoringState.Scale + focusY;

                ColoringState.BufferOffsetx = ((ColoringState.BufferOffsetx - focusX) * newscale) / ColoringState.Scale + focusX;
                ColoringState.BufferOffsety = ((ColoringState.BufferOffsety - focusY) * newscale) / ColoringState.Scale + focusY;

                ColoringState.Scale = newscale;
            };

            scaleDetector = new ScaleGestureDetector(context, scaleListener);
        }

        public static bool TouchEvent(MotionEvent e, bool handleHistory, bool handleSearch, Action<int, int> HighlightCallBack = null)
        {
            bool check = scaleDetector.OnTouchEvent(e);

            switch (e.ActionMasked)
            {
                case MotionEventActions.Down:
                    nfingers++;
                    clickStart = ColoringState.TotalTime;
                    mayBeClick = true;

                    lastX = e.RawX;
                    lastY = e.RawY;
                    lastPaletteOffsetX = e.RawX;

                    if (lastY > ColoringState.PalettePositionY)
                        isStartedOnPalette = true;
                    else
                        isStartedOnPalette = false;
                    break;
                case MotionEventActions.Up:
                    nfingers--;
                    if (mayBeClick)
                    {
                        if (ColoringState.TotalTime - clickStart < 0.5f)
                        {
                            clickx = e.GetX();
                            clicky = e.GetY();
                            HandleClick(clickx, clicky, handleHistory, handleSearch, HighlightCallBack);
                        }
                    }
                    break;
                case MotionEventActions.PointerDown:
                    nfingers++;
                    mayBeClick = false;
                    break;
                case MotionEventActions.PointerUp:
                    nfingers--;
                    if (e.ActionIndex == 0) zeroUp = true;
                    break;
                case MotionEventActions.Move:
                    HandleMove(e, lastY);
                    if (zeroUp == false)
                    {
                        return true;
                    }
                    break;
            }

            dx = 0;
            dy = 0;
            paletteDx = 0;
            return true;
        }

        private static void HandleClick(float x, float y, bool handleHistory, bool handleSearch, Action<int, int> HighlightCallBack = null)
        {
            bool isGUIButtonHandled = false;

            if (y < ColoringState.PalettePositionY)
            {
                if (handleHistory)
                {
                    if (x >= ColoringState.controlsGUI[0].rectDst.Left && x <= ColoringState.controlsGUI[0].rectDst.Right &&
                        y >= ColoringState.controlsGUI[0].rectDst.Top && y <= ColoringState.controlsGUI[0].rectDst.Bottom)
                    {
                        isGUIButtonHandled = true;
                        if (ColoringState.Undo.Count > 0)
                        {
                            PaintCommand cmd = (PaintCommand)ColoringState.Undo.Pop();
                            cmd.Revert();
                            ColoringState.Redo.Push(cmd);
                        }
                    }

                    if (x >= ColoringState.controlsGUI[1].rectDst.Left && x <= ColoringState.controlsGUI[1].rectDst.Right &&
                        y >= ColoringState.controlsGUI[1].rectDst.Top && y <= ColoringState.controlsGUI[1].rectDst.Bottom)
                    {
                        isGUIButtonHandled = true;
                        if (ColoringState.Redo.Count > 0)
                        {
                            PaintCommand cmd = (PaintCommand)ColoringState.Redo.Pop();
                            cmd.Exec();
                            ColoringState.Undo.Push(cmd);
                        }
                    }
                }

                if (handleSearch && x >= ColoringState.controlsGUI[2].rectDst.Left && x <= ColoringState.controlsGUI[2].rectDst.Right &&
                    y >= ColoringState.controlsGUI[2].rectDst.Top && y <= ColoringState.controlsGUI[2].rectDst.Bottom)
                {
                    bool done = false;
                    int idx = ColoringState.Palette[ColoringState.CurrentColorIndex].Item1;
                    (float rx, float ry, float rw) textRect;
                    float cx = 0f;
                    float cy = 0f;
                    float midX = ColoringState.Ww / 2;
                    float midY = ColoringState.Wh / 2;

                    isGUIButtonHandled = true;

                    if (ColoringState.HighlightedShapes.ContainsKey(idx))
                    {
                        foreach (var item in ColoringState.HighlightedShapes[idx])
                        {
                            if (ColoringState.CommandShapes[item].isDrawn)
                                continue;
                            textRect = ColoringState.CommandShapes[item].textRect;
                            cx = (2 * textRect.rx + textRect.rw) / 2;
                            cy = (2 * textRect.ry + textRect.rw) / 2;
                            done = true;
                            break;
                        }
                    }

                    if (!done)
                    {
                        foreach (var palette in ColoringState.HighlightedShapes)
                        {
                            if (done)
                                break;
                            foreach (var shape in palette.Value)
                            {
                                if (!ColoringState.CommandShapes[shape].isDrawn)
                                {
                                    done = true;
                                    textRect = ColoringState.CommandShapes[shape].textRect;
                                    cx = (2 * textRect.rx + textRect.rw) / 2;
                                    cy = (2 * textRect.ry + textRect.rw) / 2;
                                    ColoringState.CurrentColorIndex = palette.Key;
                                    ColoringState.CurrentColor = ColoringState.Palette[palette.Key].Item2;
                                    ColoringState.PaletteOffsetX = 0 - ColoringState.CurrentColorIndex * ColoringState.PaletteWH;
                                    break;
                                }
                            }
                        }
                    }



                    ColoringState.Scale = ColoringState.MaxZoom;
                    ColoringState.BufferOffsetx = -cx * ColoringState.Scale + midX;
                    ColoringState.BufferOffsety = -cy * ColoringState.Scale + midY;
                    ColoringState.Offsetx = ColoringState.BufferOffsetx;
                    ColoringState.Offsety = ColoringState.BufferOffsety;

                    OutOfScreenCheck();
                }

                if (!isGUIButtonHandled)
                {
                    for (int i = 0; i < ColoringState.Shapes.Count; i++)
                    {
                        if (Check.IsContour(ColoringState.CommandShapes[i]))
                            continue;
                        if (Check.IsInShape(ColoringState.Shapes[i], (int)x, (int)y, ColoringState.Offsetx, ColoringState.Offsety, ColoringState.Scale))
                        {
                            if (handleHistory && handleSearch && (ColoringState.CommandShapes[i].isDrawn || ColoringState.CommandShapes[i].isHighlighted
                                && ColoringState.CommandShapes[i].r == ColoringState.CurrentColor.r
                                && ColoringState.CommandShapes[i].g == ColoringState.CurrentColor.g
                                && ColoringState.CommandShapes[i].b == ColoringState.CurrentColor.b))
                            {
                                break;
                            }

                            (int, PaletteColor, bool?, bool?, bool?, int?, int?) tuple = (i, ColoringState.CurrentColor, null, null, null, null, null);

                            ColoringState.Mut.WaitOne();
                            ColoringState.Queue.Enqueue(tuple);
                            ColoringState.Mut.ReleaseMutex();

                            break;
                        }
                    }
                }
            }
            else
            {
                ColoringState.PrevColorIndex = ColoringState.CurrentColorIndex;

                for (int i = 0; i < ColoringState.Palette.Count; i++)
                {
                    if (x >= ColoringState.Palette[i].Item3.x + ColoringState.PaletteOffsetX && x <= ColoringState.Palette[i].Item3.x + ColoringState.PaletteWH + ColoringState.PaletteOffsetX &&
                        y >= ColoringState.Palette[i].Item3.y && y <= ColoringState.Palette[i].Item3.y + ColoringState.PaletteWH)
                    {
                        ColoringState.CurrentColor = ColoringState.Palette[i].Item2;

                        if (ColoringState.CurrentColorIndex == i)
                            return;
                        ColoringState.CurrentColorIndex = i;

                        //HighlightCallBack?.Invoke(ColoringState.CurrentColorIndex, ColoringState.PrevColorIndex);
                        break;
                    }
                }
            }
        }

        private static void HandleMove(MotionEvent e, float y)
        {
            if (nfingers == 1)
            {
                if (y < ColoringState.PalettePositionY && isStartedOnPalette == false || y > ColoringState.PalettePositionY && isStartedOnPalette == false)
                {
                    dx = e.RawX - lastX;
                    dy = e.RawY - lastY;

                    lastX = e.RawX;
                    lastY = e.RawY;

                    if (zeroUp)
                    {
                        zeroUp = false;
                        return;
                    }

                    if (System.Math.Abs(dx) >= 0.01f * ColoringState.Ww || System.Math.Abs(dy) > 0.01f * ColoringState.Wh)
                    {
                        mayBeClick = false;
                        ColoringState.Offsetx += dx;
                        ColoringState.BufferOffsetx += dx;

                        ColoringState.Offsety += dy;
                        ColoringState.BufferOffsety += dy;

                        OutOfScreenCheck();
                    }

                }
                else if (isStartedOnPalette == true)
                {
                    paletteDx = e.RawX - lastPaletteOffsetX;

                    lastPaletteOffsetX = e.RawX;

                    if (System.Math.Abs(paletteDx) >= 0.01f * ColoringState.Ww)
                    {
                        mayBeClick = false;
                        ColoringState.PaletteOffsetX += paletteDx;

                        if (ColoringState.PaletteOffsetX > ColoringState.MaxPaletteLeft)
                            ColoringState.PaletteOffsetX = 0;
                        if (ColoringState.PaletteOffsetX < ColoringState.MaxPaletteRight)
                            ColoringState.PaletteOffsetX = ColoringState.MaxPaletteRight;
                    }
                }
            }
        }

        private static void OutOfScreenCheck()
        {
            if (ColoringState.BufferOffsetx > ColoringState.MaxBufferL)
            {
                ColoringState.BufferOffsetx = ColoringState.MaxBufferL;
                ColoringState.Offsetx = ColoringState.BufferOffsetx;
            }

            if (ColoringState.BufferOffsetx + ColoringState.ImgWH * ColoringState.Scale / ColoringState.MaxImgZoom < ColoringState.MinBufferR)
            {
                ColoringState.BufferOffsetx = ColoringState.MinBufferR - ColoringState.ImgWH * ColoringState.Scale / ColoringState.MaxImgZoom;
                ColoringState.Offsetx = ColoringState.BufferOffsetx;
            }
            
            if (ColoringState.Scale * (ColoringState.Tp - ColoringState.Bm) > 0.9 * ColoringState.Wh)
            {
                if (ColoringState.BufferOffsety > ColoringState.MaxBufferB)
                {
                    ColoringState.BufferOffsety = ColoringState.MaxBufferB;
                    ColoringState.Offsety = ColoringState.BufferOffsety;
                }

                if (ColoringState.BufferOffsety + ColoringState.ImgWH * ColoringState.Scale / ColoringState.MaxImgZoom < ColoringState.MinBufferT)
                {
                    ColoringState.BufferOffsety = ColoringState.MinBufferT - ColoringState.ImgWH * ColoringState.Scale / ColoringState.MaxImgZoom;
                    ColoringState.Offsety = ColoringState.BufferOffsety;
                }
            }
            else
            {
                if (ColoringState.BufferOffsety < ColoringState.MaxBufferB)
                {
                    ColoringState.BufferOffsety = ColoringState.MaxBufferB;
                    ColoringState.Offsety = ColoringState.BufferOffsety;
                }

                if (ColoringState.BufferOffsety + ColoringState.ImgWH * ColoringState.Scale / ColoringState.MaxImgZoom > ColoringState.MinBufferT)
                {
                    ColoringState.BufferOffsety = ColoringState.MinBufferT - ColoringState.ImgWH * ColoringState.Scale / ColoringState.MaxImgZoom;
                    ColoringState.Offsety = ColoringState.BufferOffsety;
                }
            }
        }

    }
}
#endif