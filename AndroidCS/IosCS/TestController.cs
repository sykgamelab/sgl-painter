﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using CoreGraphics;
using Foundation;
using UIKit;
using SkiaSharp;
using SkiaSharp.Views.iOS;

namespace AndroidCS
{
    public class TestController : UIViewController
    {
        SKGLView glView;

        bool isInitialized = false;
        SKPoint point = new SKPoint(50, 500);

        public TestController()
        {
        }

        private void DrawCallback(object sender, SKPaintGLSurfaceEventArgs args)
        {
            var skiaSurface = args.Surface;
            var skiaCanvas = skiaSurface.Canvas;

            skiaCanvas.Clear(new SKColor(25, 115, 51));

            if (!isInitialized)
            {
                ColoringState.Ww = glView.CanvasSize.Width;
                ColoringState.Wh = glView.CanvasSize.Height;

                isInitialized = true;
            }

            SKPaint paint = new SKPaint();
            paint.Color = new SKColor(255, 255, 255);

            skiaCanvas.DrawCircle(point, 100, paint);
            skiaCanvas.Flush();

            glView.SetNeedsDisplay();
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            glView = new SKGLView(UIScreen.MainScreen.Bounds);
            glView.PaintSurface += DrawCallback;

            View.AddSubview(glView);
        }
    }
}