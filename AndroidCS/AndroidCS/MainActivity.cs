using System;
using System.Threading;
using Android.App;
using Android.Util;
using Android.OS;
using Android.Runtime;
using Android.Support.Design.Widget;
using Android.Support.V7.App;
using Android.Views;
using Android.Widget;
using SkiaSharp;
using SkiaSharp.Views;
using SkiaSharp.Views.Android;
using Android.Content.Res;
using System.IO;
using System.IO.Compression;
using System.Collections.Generic;
using Android.Graphics;
using Android.Opengl;
using System.Runtime.InteropServices;
using Javax.Microedition.Khronos.Opengles;
using Android.Content;
using System.Reflection;

namespace AndroidCS
{
    //public struct PaletteColor
    //{
    //    public byte r, g, b;

    //    public void Set(byte red, byte green, byte blue)
    //    {
    //        r = red;
    //        g = green;
    //        b = blue;
    //    }
    //}

    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = false)]
	public class MainActivity : AppCompatActivity
	{
		bool isInitialized = false;

		const float MaxEdge = 3500;
		const int PaletteSize = 8;
        const string Tag = "Painter";

        SKGLSurfaceView glView;

		List<PainterCommon.PainterPointf> clicks = new List<PainterCommon.PainterPointf>();

        int imgscale;
		GRContext offscreenContext;
		SKSurface offscreenSurface;
		float start;
		float paletteOffset, paletteSpacing;
		float paletteY, userPaletteY;
		float paletteSpaceH, paletteR, paletteX;
		DateTime duration;


		private void RenderShape(CommandShape shape, SKCanvas target, float imgw, PaletteColor? color = null, int? idx = null)
		{
			float imgscale = imgw / (ColoringState.ViewBox);

			byte r, g, b;

            if (color != null)
            {
                if (!shape.isDrawn)
                {
                    r = color.Value.r;
                    g = color.Value.g;
                    b = color.Value.b;
                    shape.isDrawn = true;
                }
                else
                {
                    if (color.Value.r == shape.r && color.Value.g == shape.g && color.Value.b == shape.b)
                    {
                        r = g = b = 255;
                        shape.isDrawn = false;
                    }
                    else
                    {
                        r = color.Value.r;
                        g = color.Value.g;
                        b = color.Value.b;
                        shape.isDrawn = true;
                        if (r == 255 && g == 255 && b == 255)
                            shape.isDrawn = false;
                    }
                }
            }
            else
            {
                r = (byte)shape.r;
                g = (byte)shape.g;
                b = (byte)shape.b;
            }

            if (idx != null)
            {
                shape.r = r;
                shape.g = g;
                shape.b = b;
                ColoringState.CommandShapes[idx.Value] = shape;
            }

            var contours = shape.contours;
			SKPaint paint = new SKPaint();
			paint.IsAntialias = true;
			//paint.StrokeWidth = 2.0f;
			paint.Color = new SKColor(r, g, b);

			SKPath path = new SKPath();
			for (int i = 0; i < contours.Count; i++)
			{
				var commands = contours[i].commands;
				for (int k = 0; k < commands.Count; k++)
				{
					commands[k].Exec(path, 0, 0, imgscale);
				}
				path.Close();
			}

			target.DrawPath(path, paint);
            //	target.Flush();
        }

        private void DrawCallback(object sender, SKPaintGLSurfaceEventArgs args)
		{
			var skiaSurface = args.Surface;
			var skiaCanvas = skiaSurface.Canvas;

			if (!isInitialized)
			{
                ColoringState.Palette = new List<(int, PaletteColor, (float, float))>();

                ColoringState.Ww = glView.Width;
                ColoringState.Wh = glView.Height;

                ColoringState.Lt = ColoringState.Shapes[0].lt;
				ColoringState.Rt = ColoringState.Shapes[0].rt;
				ColoringState.Bm = ColoringState.Shapes[0].bm;
				ColoringState.Tp = ColoringState.Shapes[0].tp;

				for (int i = 1; i < ColoringState.Shapes.Count; i++)
				{
					if (ColoringState.Shapes[i].lt < ColoringState.Lt)
						ColoringState.Lt = ColoringState.Shapes[i].lt;
					if (ColoringState.Shapes[i].rt > ColoringState.Rt)
						ColoringState.Rt = ColoringState.Shapes[i].rt;

					if (ColoringState.Shapes[i].bm < ColoringState.Bm)
						ColoringState.Bm = ColoringState.Shapes[i].bm;
					if (ColoringState.Shapes[i].tp > ColoringState.Tp)
						ColoringState.Tp = ColoringState.Shapes[i].tp;
				}

                if (ColoringState.Lt < 0) ColoringState.Lt = 0;
                if (ColoringState.Lt > ColoringState.Ww) ColoringState.Lt = ColoringState.Ww;
                if (ColoringState.Bm < 0) ColoringState.Bm = 0;
                if (ColoringState.Bm > ColoringState.Wh) ColoringState.Bm = ColoringState.Wh;

                paletteOffset = 0.03f * ColoringState.Ww;
                paletteSpacing = 0.005f * ColoringState.Ww;
                ColoringState.PaletteWH = 0.1f * ColoringState.Ww;
                paletteY = ColoringState.Wh - ColoringState.PaletteWH - paletteSpacing;
                userPaletteY = paletteY - ColoringState.PaletteWH - paletteSpacing;

                paletteSpaceH = 2 * ColoringState.PaletteWH + 3 * paletteSpacing;
                ColoringState.PalettePositionY = ColoringState.Wh - paletteSpaceH;
                paletteR = 0.05f * ColoringState.PaletteWH;
                paletteX = paletteOffset + ColoringState.PaletteWH + paletteSpacing;

                ColoringState.Scale = 0.9f * ColoringState.Ww / (ColoringState.ViewBox);
                ColoringState.MaxImgZoom = MaxEdge / (ColoringState.ViewBox);
                ColoringState.MaxZoom = ColoringState.MaxImgZoom * 2.5f;
                ColoringState.MinZoom = ColoringState.Scale;

                float dpi = Resources.DisplayMetrics.Xdpi;
                dpi = (dpi / 25.4f);
                float minW = dpi * 3;

                float cx, cy;
                float cxNew, cyNew;

                CommandShape shape;

                for (int i = 0; i < ColoringState.CommandShapes.Count; i++)
                {
                    if (!Check.IsContour(ColoringState.CommandShapes[i]))
                        if (ColoringState.CommandShapes[i].textRect.rw * ColoringState.MaxZoom < minW)
                        {
                            shape = ColoringState.CommandShapes[i];
                            cx = (2 * ColoringState.CommandShapes[i].textRect.rx + ColoringState.CommandShapes[i].textRect.rw) / 2;
                            cy = (2 * ColoringState.CommandShapes[i].textRect.ry + ColoringState.CommandShapes[i].textRect.rw) / 2;

                            shape.textRect.rw = minW / ColoringState.MaxZoom;
                            cxNew = (2 * shape.textRect.rx + shape.textRect.rw) / 2;
                            cyNew = (2 * shape.textRect.ry + shape.textRect.rw) / 2;

                            shape.textRect.rx -= cxNew - cx;
                            shape.textRect.ry -= cyNew - cy;

                            ColoringState.CommandShapes[i] = shape;
                        }
                }

                ColoringState.ImgWH = (int)((ColoringState.ViewBox) * ColoringState.MaxImgZoom);

                ColoringState.BufferOffsetx = 0.05f * ColoringState.Ww;
                ColoringState.BufferOffsety = (ColoringState.Wh - ColoringState.ImgWH * ColoringState.Scale / ColoringState.MaxImgZoom) / 2.0f;

                //Offsetx = BufferOffsetx + Lt * Scale;
                ColoringState.Offsetx = ColoringState.BufferOffsetx;
                //Offsety = BufferOffsety + Bm * Scale;
                ColoringState.Offsety = ColoringState.BufferOffsety;

                ColoringState.MaxBufferL = 0.05f * ColoringState.Ww;
                ColoringState.MinBufferR = 0.95f * ColoringState.Ww;
                ColoringState.MaxBufferB = 0.05f * ColoringState.Wh;
                ColoringState.MinBufferT = userPaletteY - 0.05f * ColoringState.Wh;

                ColoringState.HistoryWH = ColoringState.PaletteWH * 0.9f;

                AssetManager assetManager = this.Assets;
                using (Stream stream = assetManager.Open("candies.plt"))
                {
                    using (BinaryReader br = new BinaryReader(stream))
                    {
                        for (int i = 0; i < PaletteSize; i++)
                        {
                            var color = new PaletteColor()
                            {
                                r = br.ReadByte(),
                                g = br.ReadByte(),
                                b = br.ReadByte()
                            };
                            ColoringState.Palette.Add((i, color, (paletteX, paletteY)));
                            paletteX += ColoringState.PaletteWH + paletteSpacing;
                        }
                    }
                }

                paletteX = paletteOffset + ColoringState.PaletteWH + paletteSpacing;

                using (Stream stream = assetManager.Open("asia.plt"))
                {
                    using (BinaryReader br = new BinaryReader(stream))
                    {
                        for (int i = PaletteSize; i < 16; i++)
                        {
                            var color = new PaletteColor()
                            {
                                r = br.ReadByte(),
                                g = br.ReadByte(),
                                b = br.ReadByte()
                            };
                            ColoringState.Palette.Add((i, color, (paletteX, userPaletteY)));
                            paletteX += ColoringState.PaletteWH + paletteSpacing;
                        }
                    }
                }

                ColoringState.CurrentColor = ColoringState.Palette[0].Item2;

                // define the surface properties

                var offscreenInfo = new SKImageInfo(ColoringState.ImgWH, ColoringState.ImgWH, SKColorType.Bgra8888, SKAlphaType.Premul);
				// create the surface
				offscreenContext = GRContext.Create(GRBackend.OpenGL);
				offscreenSurface = SKSurface.Create(offscreenContext, false, offscreenInfo);
				ColoringState.OffscreenCanvas = offscreenSurface.Canvas;

                ColoringState.OffscreenCanvas.Clear(new SKColor(255, 255, 255, 0));
                for (int i = 0; i < ColoringState.CommandShapes.Count; i++)
                    if (Check.IsContour(ColoringState.CommandShapes[i]))
                        RenderShape(ColoringState.CommandShapes[i], ColoringState.OffscreenCanvas, ColoringState.ImgWH);
                //offscreenCanvas.Flush();

                SKRect srcRect = new SKRect(0, 0, ColoringState.ViewBox, ColoringState.ViewBox);
                SKRect dstRect = new SKRect(0, 0, ColoringState.ImgWH, ColoringState.ImgWH);

                if (ColoringState.PngImages.Item2 != null)
                    ColoringState.OffscreenCanvas.DrawImage(ColoringState.PngImages.Item2, srcRect, dstRect);

                ColoringState.Contours = offscreenSurface.Snapshot();
                ColoringState.OffscreenCanvas.Clear(new SKColor(255, 255, 255, 0));

                if (ColoringState.PngImages.Item1 != null)
                {
                    ColoringState.OffscreenCanvas.DrawImage(ColoringState.PngImages.Item1, srcRect, dstRect);
                    ColoringState.BgImage = offscreenSurface.Snapshot();
                    ColoringState.OffscreenCanvas.Clear(new SKColor(255, 255, 255, 0));
                }

                for (int i = 0; i < ColoringState.CommandShapes.Count; i++)
                    if (!Check.IsContour(ColoringState.CommandShapes[i]))
                    {
                        CommandShape temp = ColoringState.CommandShapes[i];
                        temp.r = 255;
                        temp.g = 255;
                        temp.b = 255;
                        ColoringState.CommandShapes[i] = temp;
                        RenderShape(ColoringState.CommandShapes[i], ColoringState.OffscreenCanvas, ColoringState.ImgWH);
                    }

                ColoringState.Regions = offscreenSurface.Snapshot();

                Assembly assembly = GetType().GetTypeInfo().Assembly;
                string resourceID = $"AndroidCS.Assets.arrows.bytes";

                Tuple<SKImage, SKRect> pair;
                SKImage image;
                SKRect rectSrc;
                SKRect rectDst = new SKRect(paletteOffset, ColoringState.Wh - paletteSpaceH - ColoringState.HistoryWH - paletteSpacing * 4, paletteOffset + ColoringState.HistoryWH, ColoringState.Wh - paletteSpaceH - paletteSpacing * 4);

                for (int i = 0; i < 2; i++)
                {
                    pair = URPNGLoader.LoadURPNG(assembly, resourceID, "atlas", i);
                    image = pair.Item1;
                    rectSrc = pair.Item2;
                    ColoringState.controlsGUI.Add((image, rectSrc, rectDst));
                    rectDst.Left = ColoringState.Ww - paletteOffset - ColoringState.HistoryWH;
                    rectDst.Right = ColoringState.Ww - paletteOffset;
                }

                isInitialized = true;
			}

            float curr = (float)(DateTime.UtcNow - duration).TotalMilliseconds;

			float delta = curr - start;

			ColoringState.TotalTime += delta / 1000f;

			if (delta >= 1.0f)
			{
				start = curr;
			}

            ColoringState.Mut.WaitOne();

			if (ColoringState.Queue.Count > 0)
			{
				var request = ColoringState.Queue.Dequeue();
				int idx = request.Item1;
				PaletteColor color = request.Item2;
                bool? undo = request.Item4;
                bool? isDrawn = request.Item5;
                int? redoProgress = request.Item6;
                int? undoProgress = request.Item7;

                bool prevIsDrawn = ColoringState.CommandShapes[idx].isDrawn;
                PaletteColor prevColor = new PaletteColor();
                prevColor.Set((byte)ColoringState.CommandShapes[idx].r, (byte)ColoringState.CommandShapes[idx].g, (byte)ColoringState.CommandShapes[idx].b);

                if (!prevIsDrawn)
                    prevColor.Set(255, 255, 255);

                if (undo == true)
                {
                    RenderShape(ColoringState.CommandShapes[idx], ColoringState.OffscreenCanvas, ColoringState.ImgWH, color, idx);
                    if (undoProgress != null)
                    {
                        ColoringState.PaintedShapesCount = undoProgress.Value;
                        ColoringState.ProgressBarPercent = ((float)ColoringState.PaintedShapesCount * 1f) / (float)ColoringState.CommandShapes.Count;
                    }
                }
                else if (undo == false)
                {
                    RenderShape(ColoringState.CommandShapes[idx], ColoringState.OffscreenCanvas, ColoringState.ImgWH, color, idx);
                    if (redoProgress != null)
                    {
                        ColoringState.PaintedShapesCount = redoProgress.Value;
                        ColoringState.ProgressBarPercent = ((float)ColoringState.PaintedShapesCount * 1f) / (float)ColoringState.CommandShapes.Count;
                    }
                }
                else
                {
                    int prevProgress = ColoringState.PaintedShapesCount;

                    if (color.r == ColoringState.CommandShapes[idx].r && color.g == ColoringState.CommandShapes[idx].g && color.b == ColoringState.CommandShapes[idx].b)
                    {
                        ColoringState.PaintedShapesCount--;
                        ColoringState.ProgressBarPercent = ((float)ColoringState.PaintedShapesCount * 1f) / (float)ColoringState.CommandShapes.Count;
                    }

                    RenderShape(ColoringState.CommandShapes[idx], ColoringState.OffscreenCanvas, ColoringState.ImgWH, color, idx);

                    if (ColoringState.CommandShapes[idx].isDrawn && prevIsDrawn == false)
                    {
                        ColoringState.PaintedShapesCount++;
                        ColoringState.ProgressBarPercent = ((float)ColoringState.PaintedShapesCount * 1f) / (float)ColoringState.CommandShapes.Count;
                    }

                    int newProgress = ColoringState.PaintedShapesCount;
                    PaletteColor newColor = ColoringState.CurrentColor;
                    PaintCommand command = new PaintCommand(idx, prevColor, newColor, prevIsDrawn, false,
                        ColoringState.CommandShapes[idx].isDrawn, ColoringState.CommandShapes[idx].isHighlighted, ColoringState.Mut, ColoringState.Queue, newProgress, prevProgress);

                    ColoringState.Undo.Push(command);
                    ColoringState.Redo.Clear();
                }

                ColoringState.Regions.Dispose();
				ColoringState.Regions = null;
				ColoringState.Regions = offscreenSurface.Snapshot();
			}

			ColoringState.Mut.ReleaseMutex();


			skiaCanvas.Clear(new SKColor(200, 200, 200));
            skiaCanvas.Save();
			skiaCanvas.Translate(ColoringState.BufferOffsetx, ColoringState.BufferOffsety);
			skiaCanvas.Scale(ColoringState.Scale / ColoringState.MaxImgZoom, ColoringState.Scale / ColoringState.MaxImgZoom);
            if (ColoringState.PngImages.Item1 != null)
                skiaCanvas.DrawImage(ColoringState.BgImage, 0.0f, 0.0f);
            skiaCanvas.DrawImage(ColoringState.Regions, 0.0f, 0.0f);
            skiaCanvas.DrawImage(ColoringState.Contours, 0.0f, 0.0f);
			skiaCanvas.Restore();


            SKPaint paint = new SKPaint();
			paint.IsAntialias = true;

			paint.Color = new SKColor(0, 0, 0);

            SKPaint strokPaint = new SKPaint();
            strokPaint.Style = SKPaintStyle.Stroke;
            strokPaint.StrokeWidth = 5.0f;
            strokPaint.StrokeCap = SKStrokeCap.Round;
            strokPaint.Color = SKColors.White;

            SKColor progressBarGradientA = new SKColor(243, 131, 152);
            SKColor progressBarGradientB = new SKColor(153, 50, 127);

            byte gradientR = (byte)(progressBarGradientA.Red + ColoringState.ProgressBarPercent * (progressBarGradientB.Red - progressBarGradientA.Red));
            byte gradientG = (byte)(progressBarGradientA.Green + ColoringState.ProgressBarPercent * (progressBarGradientB.Green - progressBarGradientA.Green));
            byte gradientB = (byte)(progressBarGradientA.Blue + ColoringState.ProgressBarPercent * (progressBarGradientB.Blue - progressBarGradientA.Blue));

            SKColor curProgressBarGradientPoint = new SKColor(gradientR, gradientG, gradientB);

            SKRect rect = new SKRect(paletteOffset, paletteOffset, ColoringState.Ww - paletteOffset, ColoringState.PaletteWH);
            skiaCanvas.DrawRoundRect(rect, paletteR + 10, paletteR + 10, strokPaint);

            rect.Right = ColoringState.Ww * ColoringState.ProgressBarPercent + paletteOffset;

            paint.Shader = SKShader.CreateLinearGradient(
                      new SKPoint(rect.Left, rect.Left),
                      new SKPoint(rect.Right, rect.Right),
                      new SKColor[] { progressBarGradientA, curProgressBarGradientPoint },
                      new float[] { 0, 1 },
                      SKShaderTileMode.Repeat);

            skiaCanvas.DrawRoundRect(rect, paletteR + 10, paletteR + 10, paint);
            paint.Shader = null;

            rect.Top = ColoringState.PalettePositionY;
            rect.Left = 0;
            rect.Right = ColoringState.Ww;
            rect.Bottom = ColoringState.Wh;
            skiaCanvas.DrawRect(rect, paint);

			paletteX = paletteOffset;
			rect.Left = paletteX;
			rect.Top = paletteY;
			rect.Right = paletteX + ColoringState.PaletteWH;
			rect.Bottom = paletteY + ColoringState.PaletteWH;
			paint.Color = new SKColor(0, 255, 0);
			skiaCanvas.DrawRoundRect(rect, paletteR, paletteR, paint);

			paletteX = paletteOffset;
			rect.Left = paletteX;
			rect.Top = userPaletteY;
			rect.Right = paletteX + ColoringState.PaletteWH;
			rect.Bottom = userPaletteY + ColoringState.PaletteWH;
			paint.Color = new SKColor(255, 255, 0);
			skiaCanvas.DrawRoundRect(rect, paletteR, paletteR, paint);

            skiaCanvas.DrawImage(ColoringState.controlsGUI[0].image, ColoringState.controlsGUI[0].rectSrc, ColoringState.controlsGUI[0].rectDst, paint);
            skiaCanvas.DrawImage(ColoringState.controlsGUI[1].image, ColoringState.controlsGUI[1].rectSrc, ColoringState.controlsGUI[1].rectDst, paint);

            for (int i = 0; i < 16; i++)
            {
                rect.Left = ColoringState.Palette[i].Item3.x;
                rect.Top = ColoringState.Palette[i].Item3.y;
                rect.Right = ColoringState.Palette[i].Item3.x + ColoringState.PaletteWH;
                rect.Bottom = ColoringState.Palette[i].Item3.y + ColoringState.PaletteWH;
                paint.Color = new SKColor(ColoringState.Palette[i].Item2.r, ColoringState.Palette[i].Item2.g, ColoringState.Palette[i].Item2.b);
                skiaCanvas.DrawRoundRect(rect, paletteR, paletteR, paint);

                if (i == ColoringState.CurrentColorIndex)
                {
                    strokPaint.Style = SKPaintStyle.Stroke;
                    strokPaint.StrokeWidth = 5.0f;
                    strokPaint.StrokeCap = SKStrokeCap.Round;
                    strokPaint.Color = SKColors.White;
                    skiaCanvas.DrawRoundRect(rect, paletteR, paletteR, strokPaint);
                }

            }

            paint.Color = new SKColor(0, 0, 255);
            paint.IsStroke = true;


    //        for (int i = 0; i < CommandShapes.Count; i++)
    //        {
    //            SKPath path = new SKPath();

    //            paint.Color = new SKColor(0, 255, 0);
    //            var contours = CommandShapes[i].contours;
    //            for (int j = 0; j < contours.Count; j++)
    //            {
    //                var commands = contours[j].commands;
    //                for (int k = 0; k < commands.Count; k++)
    //                {
    //                    commands[k].Exec(path, Offsetx, Offsety, Scale);
    //                }
    //                path.Close();
    //            }

    //            skiaCanvas.DrawPath(path, paint);
    //        }

    //        for (int i = 0; i < clicks.Count; i++)
				//skiaCanvas.DrawCircle(clicks[i].x, clicks[i].y, 10, paint);
			//skiaCanvas.Flush();
		}

        protected override void OnCreate(Bundle savedInstanceState)
		{
			base.OnCreate(savedInstanceState);

            ColoringState.Mut = new Mutex();
            ColoringState.Queue = new Queue<(int, PaletteColor, bool?, bool?, bool?, int?, int?)>();
            ColoringState.controlsGUI = new List<(SKImage, SKRect, SKRect)>();
            ColoringState.Undo = new Stack<ICommand>();
            ColoringState.Redo = new Stack<ICommand>();

            Gestures.Initialize(ApplicationContext);

            //GameLoop loop = new GameLoop();

            //View view = loop.Services.GetService<View>();
            //SetContentView(view);

			duration = new DateTime(2019, 1, 1, 0, 0, 0, DateTimeKind.Utc);
			start = (float)(DateTime.UtcNow - duration).TotalMilliseconds;

			glView = new SKGLSurfaceView(ApplicationContext);
			glView.PaintSurface += DrawCallback;
			
			SetContentView(glView);
        }


		public override void OnWindowFocusChanged(bool hasFocus)
		{
			if (hasFocus)
			{
				var uiOptions =
				SystemUiFlags.HideNavigation |
				SystemUiFlags.LayoutHideNavigation |
				SystemUiFlags.LayoutFullscreen |
				SystemUiFlags.Fullscreen |
				SystemUiFlags.LayoutStable |
				SystemUiFlags.ImmersiveSticky;

				Window.DecorView.SystemUiVisibility = (StatusBarVisibility)uiOptions;
			}
		}

		protected override void OnStart()
		{
			base.OnStart();
			this.Window.AddFlags(WindowManagerFlags.Fullscreen);

            WebHelper wh = new WebHelper(ServerConstants.Url);
            wh.GetColoring(
                (code, data) =>
                {
                    if (code == 200)
                    {
                        using (Stream stream = new MemoryStream(data))
                        {
                            using (ZipArchive archive = new ZipArchive(stream, ZipArchiveMode.Read))
                            {
                                ZipArchiveEntry entry = archive.Entries[0];
                                using (Stream entryStream = entry.Open())
                                {
                                    using (MemoryStream ms = new MemoryStream())
                                    {
                                        entryStream.CopyTo(ms);
                                        data = ms.ToArray();
                                    }
                                }
                            }
                        }

                        var tuple = PainterRead.Read(data);
                        ColoringState.Shapes = tuple.Item1;
                        ColoringState.CommandShapes = tuple.Item2;
                        ColoringState.ViewBox = tuple.Item3;
                        ColoringState.PngImages = tuple.Item4;
                    }
                }, ServerConstants.GetColoring, ServerConstants.Cid, false);
        }

		protected override void OnResume()
		{
			base.OnResume();
			//glView.OnResume();
		}

		protected override void OnPause()
		{
			base.OnPause();
			//glView.OnPause();
		}

		public override bool OnTouchEvent(MotionEvent e)
		{
            return Gestures.TouchEvent(e, true, false);
		}

		public void SurfaceDestroyed(ISurfaceHolder holder)
		{
			//   throw new NotImplementedException();
		}
	}
}

