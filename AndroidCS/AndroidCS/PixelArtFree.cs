﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using System.IO;
using Android.Views;
using Android.Widget;
using Android.Content.Res;

using SkiaSharp;
using SkiaSharp.Views.Android;

namespace AndroidCS
{
	[Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = true)]
	class PixelArtFree : AppCompatActivity
	{

        struct PaletteColor
        {
            public byte r, g, b;
        }
        //struct PixelColor
        //{
        //    public byte r, g, b, l;
        //    public byte cr, cg, cb;
        //    //public bool 

        //    public bool isDrawn, isHighlighted;

        //}

        internal class ScaleListener : ScaleGestureDetector.SimpleOnScaleGestureListener
        {
            public Action<float, float, float> Callback { get; set; } = null;
            public override bool OnScale(ScaleGestureDetector detector)
            {
                Callback?.Invoke(detector.ScaleFactor, detector.FocusX, detector.FocusY);
                return true;
            }
        }

        static Mutex mut = new Mutex();

        const float MaxEdge = 3500;
        const int PaletteSize = 8;

        bool isInitialized = false;

        PixelColor[,] pixels;
		SKGLSurfaceView glView;

        // Image positioning
        float scale = 10;
        ScaleListener scaleListener;
        ScaleGestureDetector scaleDetector;
        float minzoom, maxzoom;
        float offsetx, offsety;
        float bufferOffsetx, bufferOffsety;
        float lt, rt, bm, tp, ww, wh;
        int imgw, imgh, imgscale;
        float maxl, minr, maxb, mint;

        // Time
        float start, totalTime = 0f, clickStart = 0f;
        DateTime duration;

        // Touch events
        bool mayBeClick = false;
        int nfingers = 0;
        float lastX, lastY, dx, dy;
        bool zeroUp = false;
        float clickx, clicky;
        bool drawInMoving = false;

        // Palette
        PaletteColor[,] palettes;
        float paletteOffset, paletteSpacing;
        float paletteWH, paletteX, paletteY, userPaletteY;
        float paletteSpaceH, palettePositionY;
        float userPaletteSpaceY, paletteR;
        (float x, float y)[] paletteCoordinates;
        PaletteColor currentColor;
        int currentColorIndex = 0;



        private void DrawCallback(object sender, SKPaintGLSurfaceEventArgs args)
		{
			var skiaSurface = args.Surface;
			var skiaCanvas = skiaSurface.Canvas;

            if (!isInitialized)
            {
                ww = glView.Width;
                wh = glView.Height;

                lt = 0;
                rt = pixels.GetLength(0);
                bm = 0;
                tp = pixels.GetLength(1);

                scale = 0.9f * ww / (rt - lt);

                offsetx = 0.05f * ww;
                offsety = (wh - (tp - bm) * scale) / 2.0f;

                maxzoom = MaxEdge / (rt - lt);
                minzoom = scale;

                imgw = (int)((rt - lt) * maxzoom);
                imgh = (int)((tp - bm) * maxzoom);
                imgscale = (int)(imgw / (rt - lt));

                bufferOffsetx = 0.05f * ww;
                bufferOffsety = (wh - imgh * scale / maxzoom) / 2.0f;

                paletteOffset = 0.03f * ww;
                paletteSpacing = 0.005f * ww;
                paletteWH = 0.1f * ww;
                paletteY = wh - paletteWH - paletteSpacing;
                userPaletteY = paletteY - paletteWH - paletteSpacing;

                palettePositionY = wh;

                maxl = 0.05f * ww;
                minr = 0.95f * ww;
                maxb = 0.05f * wh;
                mint = userPaletteY - 0.05f * wh;

                paletteSpaceH = 2 * paletteWH + 3 * paletteSpacing;
                palettePositionY = wh - paletteSpaceH;
                userPaletteSpaceY = wh - 2 * paletteSpaceH;
                paletteR = 0.05f * paletteWH;

                paletteCoordinates = new (float, float)[PaletteSize * 2];

                paletteX = paletteOffset + paletteWH + paletteSpacing;

                for (int i = 0; i < PaletteSize; i++)
                {
                    paletteCoordinates[i] = (paletteX, paletteY);
                    paletteCoordinates[i + PaletteSize] = (paletteX, userPaletteY);
                    paletteX += paletteWH + paletteSpacing;
                }

                isInitialized = true;
            }

            float curr = (float)(DateTime.UtcNow - duration).TotalMilliseconds;

            float delta = curr - start;

            totalTime += delta / 1000f;

            if (delta >= 1.0f)
            {
                start = curr;
            }

            SKPaint paint = new SKPaint();
            SKRect rect;
			skiaCanvas.Clear(new SKColor(200, 200, 200));

            mut.WaitOne();
            for (int x = 0; x < pixels.GetLength(0); x++)
            {
                for (int y = 0; y < pixels.GetLength(1); y++)
                {


                    paint.Color = new SKColor(pixels[x, y].cr, pixels[x, y].cg, pixels[x, y].cb);

                    float left = offsetx + x * scale;
                    float top = offsety + y * scale;
                    float right = offsetx + (x + 1) * scale;
                    float bottom = offsety + (y + 1) * scale;
                    rect = new SKRect(left, top, right, bottom);
                    skiaCanvas.DrawRect(rect, paint);
                }
            }
            mut.ReleaseMutex();

            paint = new SKPaint();
            paint.IsAntialias = true;

            paint.Color = new SKColor(0, 0, 0);

            rect = new SKRect(0, palettePositionY, ww, wh);
            skiaCanvas.DrawRect(rect, paint);

            paletteX = paletteOffset;
            rect.Left = paletteX;
            rect.Top = paletteY;
            rect.Right = paletteX + paletteWH;
            rect.Bottom = paletteY + paletteWH;
            paint.Color = new SKColor(0, 255, 0);
            skiaCanvas.DrawRoundRect(rect, paletteR, paletteR, paint);

            paletteX = paletteOffset;
            rect.Left = paletteX;
            rect.Top = userPaletteY;
            rect.Right = paletteX + paletteWH;
            rect.Bottom = userPaletteY + paletteWH;
            paint.Color = new SKColor(255, 255, 0);
            skiaCanvas.DrawRoundRect(rect, paletteR, paletteR, paint);

            paletteX += paletteWH + paletteSpacing;
            rect.Top = paletteY;
            rect.Bottom = paletteY + paletteWH;

            for (int i = 0; i < 2; i++)
            {
                for (int j = 0; j < PaletteSize; j++)
                {
                    rect.Left = paletteX;
                    rect.Right = paletteX + paletteWH;
                    paint.Color = new SKColor(palettes[i, j].r, palettes[i, j].g, palettes[i, j].b);
                    skiaCanvas.DrawRoundRect(rect, paletteR, paletteR, paint);
                    paletteX += paletteWH + paletteSpacing;

                    if (i * PaletteSize + j == currentColorIndex)
                    {
                        SKPaint strokPaint = new SKPaint();
                        strokPaint.Style = SKPaintStyle.Stroke;
                        strokPaint.StrokeWidth = 5.0f;
                        strokPaint.StrokeCap = SKStrokeCap.Round;
                        strokPaint.Color = SKColors.White;
                        skiaCanvas.DrawRoundRect(rect, paletteR, paletteR, strokPaint);
                    }

                }

                paletteX = paletteOffset;
                rect.Left = paletteX;
                rect.Top = userPaletteY;
                rect.Right = paletteX + paletteWH;
                rect.Bottom = userPaletteY + paletteWH;
                paletteX += paletteWH + paletteSpacing;
            }
        }

		protected override void OnCreate(Bundle savedInstanceState)
		{
			Bitmap bmp = null;
			AssetManager assetManager = this.Assets;
			base.OnCreate(savedInstanceState);

            scaleListener = new ScaleListener();
            scaleListener.Callback = (float factor, float focusX, float focusY) => 
            {
                mut.WaitOne();

                float newscale = scale * factor;

                if (minzoom > newscale)
                    newscale = minzoom;
                else if (maxzoom < newscale)
                    newscale = maxzoom;

                offsetx = ((offsetx - focusX) * newscale) / scale + focusX;
                offsety = ((offsety - focusY) * newscale) / scale + focusY;

                bufferOffsetx = ((bufferOffsetx - focusX) * newscale) / scale + focusX;
                bufferOffsety = ((bufferOffsety - focusY) * newscale) / scale + focusY;

                scale = newscale;

                mut.ReleaseMutex();
            };
            scaleDetector = new ScaleGestureDetector(ApplicationContext, scaleListener);

            WebHelper wh = new WebHelper(ServerConstants.Url);
            wh.GetColoring((code, data) =>
            {
                if (code == 200)
                {
                    using (Stream stream = new MemoryStream(data))
                    {
                        bmp = BitmapFactory.DecodeStream(stream);
                    }
                }
                else
                {
                    throw new Exception();
                }

            }, ServerConstants.GetColoring, ServerConstants.Cid, true);

            pixels = new PixelColor[bmp.Width, bmp.Height];
			for (int x = 0; x < bmp.Width; x++)
			{
				for (int y = 0; y < bmp.Height; y++)
				{
					int argb = bmp.GetPixel(x, y);
					int r = (argb >> 16) & 0xFF;
					int g = (argb >> 8) & 0xFF;
					int b = argb & 0xFF;
					int min = Math.Min(r, g);
					min = Math.Min(min, b);

					int max= Math.Max(r, g);
					max = Math.Max(max, b);

					int l = min + max / 2;
                    
                    pixels[x, y].r = (byte)r;
					pixels[x, y].g = (byte)g;
					pixels[x, y].b = (byte)b;

                    if (!Check.IsContour(pixels[x, y]))
                    {
                        l = (byte)MathF.Min(l * 1.1f, 255.0f);
                    }

					pixels[x, y].l = (byte)l;

                    pixels[x, y].cr = (byte)l;
                    pixels[x, y].cg = (byte)l;
                    pixels[x, y].cb = (byte)l;
                }
			}

            palettes = new PaletteColor[2, 8];

            using (Stream stream = assetManager.Open("candies.plt"))
            {
                using (BinaryReader br = new BinaryReader(stream))
                {
                    for (int i = 0; i < PaletteSize; i++)
                    {
                        var color = new PaletteColor()
                        {
                            r = br.ReadByte(),
                            g = br.ReadByte(),
                            b = br.ReadByte()
                        };
                        palettes[0, i] = color;
                    }
                }
            }

            using (Stream stream = assetManager.Open("asia.plt"))
            {
                using (BinaryReader br = new BinaryReader(stream))
                {
                    for (int i = 0; i < PaletteSize; i++)
                    {
                        var color = new PaletteColor()
                        {
                            r = br.ReadByte(),
                            g = br.ReadByte(),
                            b = br.ReadByte()
                        };
                        palettes[1, i] = color;
                    }
                }
            }

            currentColor.r = palettes[0, 0].r;
            currentColor.g = palettes[0, 0].g;
            currentColor.b = palettes[0, 0].b;

            duration = new DateTime(2019, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            start = (float)(DateTime.UtcNow - duration).TotalMilliseconds;

            glView = new SKGLSurfaceView(ApplicationContext);
			glView.PaintSurface += DrawCallback;

			SetContentView(glView);
			//		Android.Graphics.Bitmap bitmap = BitmapFactory.DecodeByteArray
		}

        public override void OnWindowFocusChanged(bool hasFocus)
        {
            if (hasFocus)
            {
                var uiOptions =
                SystemUiFlags.HideNavigation |
                SystemUiFlags.LayoutHideNavigation |
                SystemUiFlags.LayoutFullscreen |
                SystemUiFlags.Fullscreen |
                SystemUiFlags.LayoutStable |
                SystemUiFlags.ImmersiveSticky;

                Window.DecorView.SystemUiVisibility = (StatusBarVisibility)uiOptions;
            }
        }

        private bool cheakPixel(float x, float y, PaletteColor color)
        {
            int px = (int)((x - offsetx) / scale);
            int py = (int)((y - offsety) / scale);

            if (px < 0 || py < 0 ||
                px >= pixels.GetLength(0) ||
                py >= pixels.GetLength(1))
                return false;

            if (Check.IsContour(pixels[px, py]))
                return false;

            //if (color.r != pixels[px, py].r || color.g != pixels[px, py].g || color.b != pixels[px, py].b)
            //    return false;

            return true;
        }

        private (int x, int y) lastChangedPixel = (-1, -1);
        private bool changePixelColor(float x, float y, PaletteColor color)
        {
            int px = (int)((x - offsetx) / scale);
            int py = (int)((y - offsety) / scale);

            if (drawInMoving && lastChangedPixel.x == px && lastChangedPixel.y == py)
                return false;
            lastChangedPixel = (px, py);

            if (!cheakPixel(x, y, color))
                return false;

            if (color.r == pixels[px, py].cr && color.g == pixels[px, py].cg && color.b == pixels[px, py].cb)
            {
                pixels[px, py].cr = pixels[px, py].l;
                pixels[px, py].cg = pixels[px, py].l;
                pixels[px, py].cb = pixels[px, py].l;
            }
            else
            {
                pixels[px, py].cr = color.r;
                pixels[px, py].cg = color.g;
                pixels[px, py].cb = color.b;
            }

            return true;
        }

        private void HandleClick(float x, float y)
		{
			if (y < palettePositionY)
			{

                changePixelColor(x, y, currentColor);

			}
            else
            {
                int idx = -1;
                for (int i = 0; i < paletteCoordinates.Length; i++)
                {
                    if (x >= paletteCoordinates[i].x && x <= paletteCoordinates[i].x + paletteWH &&
                        y >= paletteCoordinates[i].y && y <= paletteCoordinates[i].y + paletteWH)
                    {

                        if (i < 8)
                        {
                            currentColor.r = palettes[0, i].r;
                            currentColor.g = palettes[0, i].g;
                            currentColor.b = palettes[0, i].b;
                        }
                        else
                        {
                            currentColorIndex = i - PaletteSize;

                            currentColor.r = palettes[1, currentColorIndex].r;
                            currentColor.g = palettes[1, currentColorIndex].g;
                            currentColor.b = palettes[1, currentColorIndex].b;
                        }

                        currentColorIndex = i;
                        idx = i;
                        break;
                    }
                }

                //Log.Info(Tag, $"palette index {idx}");
            }
        }

        public override bool OnTouchEvent(MotionEvent e)
        {
            bool check = scaleDetector.OnTouchEvent(e);

            switch (e.ActionMasked)
            {
                case MotionEventActions.Down:
                    nfingers++;
                    clickStart = totalTime;
                    mayBeClick = true;

                    drawInMoving = cheakPixel(e.RawX, e.RawY, currentColor);

                    //Log.Info(Tag, $"nfingers down {nfingers} index {e.ActionIndex}");
                    lastX = e.RawX;
                    lastY = e.RawY;
                    break;
                case MotionEventActions.Up:
                    nfingers--;
                    drawInMoving = false;
                    if (mayBeClick)
                    {
                        //Log.Info(Tag, $"TT {totalTime}/ CS {clickStart}");
                        if (totalTime - clickStart < 0.5f)
                        {
                            clickx = e.GetX();
                            clicky = e.GetY();
                            HandleClick(clickx, clicky);

                            //clicks.Add(new PainterCommon.PainterPointf()
                            //{
                            //    x = clickx,
                            //    y = clicky
                            //});
                        }
                    }
                    //Log.Info(Tag, $"nfingers up {nfingers} index {e.ActionIndex}");
                    break;
                case MotionEventActions.PointerDown:
                    nfingers++;
                    mayBeClick = false;
                    //Log.Info(Tag, $"nfingers pointer down {nfingers} index {e.ActionIndex}");
                    break;
                case MotionEventActions.PointerUp:
                    drawInMoving = false;
                    nfingers--;
                    //Log.Info(Tag, $"nfingers pointer up {nfingers} index {e.ActionIndex}");
                    if (e.ActionIndex == 0) zeroUp = true;

                    break;
                case MotionEventActions.Move:


                    if (nfingers == 1)
                    {
                        

                        dx = e.RawX - lastX;
                        dy = e.RawY - lastY;

                        lastX = e.RawX;
                        lastY = e.RawY;

                        if (zeroUp)
                        {
                            zeroUp = false;
                            return true;
                        }

                        if (Math.Abs(dx) > 0.001f * ww || Math.Abs(dy) > 0.001f * wh)
                        {
                            if (drawInMoving && (totalTime - clickStart) > 0.5)
                            {
                                changePixelColor(e.RawX, e.RawY, currentColor);
                                return true;
                            }
                            drawInMoving = false;
                        }

                        if (System.Math.Abs(dx) >= 0.01f * ww || System.Math.Abs(dy) > 0.01f * wh)
                        {

                            mut.WaitOne();

                            mayBeClick = false;
                            //Log.Info(Tag, $"nfingers move index {e.ActionIndex} dx={dx} dy={dy}");

                            offsetx += dx;
                            bufferOffsetx += dx;

                            if (lt * scale + offsetx > maxl)
                                offsetx = maxl - lt * scale;
                            if (rt * scale + offsetx < minr)
                                offsetx = minr - rt * scale;

                            if (bufferOffsetx > maxl)
                                bufferOffsetx = maxl;
                            if (bufferOffsetx + imgw * scale / maxzoom < minr)
                                bufferOffsetx = minr - imgw * scale / maxzoom;

                            offsety += dy;
                            bufferOffsety += dy;

                            if (scale * (tp - bm) > 0.9 * wh)
                            {
                                if (bm * scale + offsety > maxb)
                                    offsety = maxb - bm * scale;
                                if (tp * scale + offsety < mint)

                                    offsety = mint - tp * scale;

                                if (bufferOffsety > maxb)
                                    bufferOffsety = maxb;
                                if (bufferOffsety + imgh * scale / maxzoom < mint)

                                    bufferOffsety = mint - imgh * scale / maxzoom;
                            }
                            else
                            {
                                if (bm * scale + offsety < maxb)

                                    offsety = maxb - bm * scale;
                                if (tp * scale + offsety > mint)
                                    offsety = mint - tp * scale;

                                if (bufferOffsety < maxb)

                                    bufferOffsety = maxb;
                                if (bufferOffsety + imgh * scale / maxzoom > mint)
                                    bufferOffsety = mint - imgh * scale / maxzoom;
                            }

                            mut.ReleaseMutex();
                        }
                    }


                    break;

            }

            dx = 0;
            dy = 0;

            return true;
        }
    }
}