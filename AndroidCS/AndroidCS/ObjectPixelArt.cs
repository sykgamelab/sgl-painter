﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;

using Android.Graphics;
using Android.App;
using Android.Content.Res;
using Android.OS;
using Android.Views;
using SkiaSharp;
using SkiaSharp.Views.Android;

namespace AndroidCS
{
    [Activity(Label = "ObjectPixelArt", MainLauncher = false)]
    class ObjectPixelArt : Activity
    {
        bool isInitialized = false;

        private DateTime duration;
        private float start, totalTime;

        SKGLSurfaceView glView;

        PixelImage image;
        Palette palette;
        ProgressBar progressBar;
        RedoButton redoButton;
        UndoButton undoButton;
        ColorIndexGrid colorIndexGrid;
        ImageBox imageBox;

        Stack<ICommand> Redo;
        Stack<ICommand> Undo;

        ScaleListener scaleListener;
        ScaleGestureDetector scaleDetector;

        Mutex mutex = new Mutex();

        //del

        Color a, b, c;

        internal class ScaleListener : ScaleGestureDetector.SimpleOnScaleGestureListener
        {
            public Action<float, float, float> Callback { get; set; } = null;
            public override bool OnScale(ScaleGestureDetector detector)
            {
                Callback?.Invoke(detector.ScaleFactor, detector.FocusX, detector.FocusY);
                return true;
            }
        }

        private void DrawCallback(object sender, SKPaintGLSurfaceEventArgs args)
        {
            SKCanvas skiaCanvas = args.Surface.Canvas;

            if (!isInitialized)
            {
                Bitmap bmp;

                using (Stream stream = this.Assets.Open("217.png"))
                {
                    bmp = BitmapFactory.DecodeStream(stream);
                }

                Redo = new Stack<ICommand>();
                Undo = new Stack<ICommand>();

                image = new PixelImage(bmp);

                palette = new Palette(bmp, glView.Width, glView.Height);
                progressBar = new ProgressBar(bmp, glView.Width, glView.Height);

                //progressBar = new ProgressBar();

                //for (int i = 0; i < image.colors.Count; i++)
                //{
                //    palette.Add(image.colors[i].color, image.colors[i].count);
                //}

                colorIndexGrid = new ColorIndexGrid(bmp);

                //for (int x = 0; x < image.width; x++)
                //{
                //    for (int y = 0; y < image.height; y++)
                //    {
                //        colorIndexGrid.setCell(x, y, image.get) 
                //    }
                //}

                image.rightPixelPaintNotify += palette.reduceColorCount;
                image.rightPixelPaintNotify += colorIndexGrid.clearCell;
                image.rightPixelPaintNotify += progressBar.incProgress;

                image.clearPixelNotify += palette.incColorCount;
                image.clearPixelNotify += colorIndexGrid.showOrigCell;

                image.wrongPixelPaintNotify += colorIndexGrid.highlightCell;

                redoButton = new RedoButton(glView.Width, glView.Height);
                undoButton = new UndoButton(glView.Width, glView.Height);

                imageBox = new ImageBox(image.width, image.height, glView.Width, glView.Height);

                isInitialized = true;
            }

            skiaCanvas.Clear(new SKColor(200, 200, 200));

            float curr = (float)(DateTime.UtcNow - duration).TotalMilliseconds;
            float delta = curr - start;
            totalTime += delta / 1000f;

            if (delta >= 1.0f)
            {
                start = curr;
            }

            mutex.WaitOne();
            image.Draw(skiaCanvas, imageBox);
            colorIndexGrid.Draw(skiaCanvas, imageBox);
            mutex.ReleaseMutex();

            palette.Draw(skiaCanvas);
            redoButton.Draw(skiaCanvas);
            undoButton.Draw(skiaCanvas);
            progressBar.Draw(skiaCanvas, palette.getSelectedColorIndex());
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            scaleListener = new ScaleListener();
            scaleListener.Callback = (float factor, float focusX, float focusY) =>
            {
                mutex.WaitOne();

                float newscale = imageBox.scale * factor;

                if (imageBox.zoom.min > newscale)
                    newscale = imageBox.zoom.min;
                else if (imageBox.zoom.max < newscale)
                    newscale = imageBox.zoom.max;

                imageBox.offset.x = ((imageBox.offset.x - focusX) * newscale) / imageBox.scale + focusX;
                imageBox.offset.y = ((imageBox.offset.y - focusY) * newscale) / imageBox.scale + focusY;

                imageBox.scale = newscale;

                mutex.ReleaseMutex();
            };
            scaleDetector = new ScaleGestureDetector(ApplicationContext, scaleListener);


            duration = new DateTime(2019, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            start = (float)(DateTime.UtcNow - duration).TotalMilliseconds;

            glView = new SKGLSurfaceView(ApplicationContext);
            glView.PaintSurface += DrawCallback;

            SetContentView(glView);

            
        }

        public override void OnWindowFocusChanged(bool hasFocus)
        {
            if (hasFocus)
            {
                var uiOptions =
                SystemUiFlags.HideNavigation |
                SystemUiFlags.LayoutHideNavigation |
                SystemUiFlags.LayoutFullscreen |
                SystemUiFlags.Fullscreen |
                SystemUiFlags.LayoutStable |
                SystemUiFlags.ImmersiveSticky;

                Window.DecorView.SystemUiVisibility = (StatusBarVisibility)uiOptions;
            }
        }

        private void TouchHandler(MotionEvent e)
        {

            if (palette.pointInArea(e.RawX, e.RawY))
            {

            }
            else if (undoButton.pointInArea(e.RawX, e.RawY))
            {
                if (Undo.Count > 0)
                {
                    PixelPaintCommand cmd = (PixelPaintCommand)Undo.Pop();
                    Redo.Push(cmd);
                    cmd.Revert();
                }   
            }
            else if (redoButton.pointInArea(e.RawX, e.RawY))
            {
                if (Redo.Count > 0)
                {
                    PixelPaintCommand cmd = (PixelPaintCommand)Redo.Pop();
                    Undo.Push(cmd);
                    cmd.Exec();
                }
            }
            else if(imageBox.pointInArea(e.RawX, e.RawY))
            {
                (int x, int y) cord = imageBox.convertCoord(e.RawX, e.RawY);

                if (image.checkCoord(cord))
                {
                    if (image.isRightColor(cord))
                        return;

                    PixelPaintCommand cmd = new PixelPaintCommand(image, cord.x, cord.y, palette.getSelectedColor());
                    Undo.Push(cmd);
                    cmd.Exec();

                    if (Redo.Count > 0)
                        Redo.Clear();

                    //palette.Update();
                    //progressBar.Update();
                    
                }
                
            }

        }

        private void MoveHandler(MotionEvent e)
        {
            if (palette.pointInArea(e.GetX(), e.GetY()))
            {
                if (e.PointerCount == 1)
                {
                    palette.addDelta(e.GetX() - lastTouch.x);
                }
            }
            else
            {
                if (e.PointerCount == 1)
                {
                    imageBox.addDelta(e.GetX() - lastTouch.x, e.GetY() - lastTouch.y);
                }
            }


        }

        private int nfingers = 0;
        private bool mayBeClick = false;
        private float clickStart = 0.0f;
        private (float x, float y) lastTouch;
        public override bool OnTouchEvent(MotionEvent e)
        {

            bool check = scaleDetector.OnTouchEvent(e);

            switch (e.ActionMasked)
            {
                case MotionEventActions.Down:
                    nfingers++;
                    lastTouch = (e.GetX(), e.GetY());
                    clickStart = totalTime;
                    mayBeClick = true;
                    break;

                case MotionEventActions.Up:
                    nfingers--;

                    if (mayBeClick && totalTime - clickStart < 0.5f)
                    {
                        TouchHandler(e);
                    }

                    break;
                case MotionEventActions.PointerDown:
                    nfingers++;
                    mayBeClick = false;
                    break;

                case MotionEventActions.PointerUp:
                    nfingers--;
                    break;
                case MotionEventActions.Move:

                    mutex.WaitOne();
                    MoveHandler(e);
                    mutex.ReleaseMutex();
                    lastTouch = (e.GetX(), e.GetY());

                    break;
            }

            return true;
        }
    }

    public struct Color
    {
        public byte r, g, b;

        public Color(int ir, int ig, int ib)
        {
            r = (byte)ir;
            g = (byte)ig;
            b = (byte)ib;
        }
    }

    

    class PixelImage
    {
        private Color[,] originalPixels;
        private byte[,] grayPixels;
        private Color[,] currentPixels;

        public int width, height;
        public float scale;
        public (float x, float y) offset;
        public (float min, float max) zoom;

        public List<Color> colors;

        public delegate void ObserverUpdate((int x, int y) coordinates, int colorIndex);
        public event ObserverUpdate onPaint; // del

        public event ObserverUpdate rightPixelPaintNotify;
        public event ObserverUpdate wrongPixelPaintNotify;
        public event ObserverUpdate clearPixelNotify;

        public PixelImage(Bitmap bmp)
        {
            width = bmp.Width;
            height = bmp.Height;

            originalPixels = new Color[bmp.Width, bmp.Height];
            grayPixels = new byte[bmp.Width, bmp.Height];
            currentPixels = new Color[bmp.Width, bmp.Height];

            colors = new List<Color>();

            for (int x = 0; x < bmp.Width; x++)
            {
                for (int y = 0; y < bmp.Height; y++)
                {
                    int argb = bmp.GetPixel(x, y);
                    int r = (argb >> 16) & 0xFF;
                    int g = (argb >> 8) & 0xFF;
                    int b = argb & 0xFF;
                    int min = Math.Min(r, g);
                    min = Math.Min(min, b);

                    int max = Math.Max(r, g);
                    max = Math.Max(max, b);

                    int l = min + max / 2;

                    Color c = new Color(r, g, b);

                    originalPixels[x, y] = c;

                    int index = colors.FindIndex(item => (item.r == c.r && item.g == c.g && item.b == c.b));

                    if (index < 0)
                    {
                        colors.Add(c);
                    }

                    //if (!Check.IsContour(pixels[x, y]))
                    //{
                    //    l = (byte)MathF.Min(l * 1.1f, 255.0f);
                    //}

                    grayPixels[x, y] = (byte)l;

                    currentPixels[x, y] = new Color(l, l, l);
                }
            }
        }

        //public PixelImage(AssetManager assetManager, string path, float windowWidth, float windowHeight)
        //{
        //    Bitmap bmp;

        //    using (Stream stream = assetManager.Open(path))
        //    {
        //        bmp = BitmapFactory.DecodeStream(stream);
        //    }

        //    width = bmp.Width;
        //    height = bmp.Height;

        //    originalPixels = new Color[bmp.Width, bmp.Height];
        //    grayPixels = new byte[bmp.Width, bmp.Height];
        //    currentPixels = new Color[bmp.Width, bmp.Height];

        //    colors = new List<(Color color, int count)>();

        //    for (int x = 0; x < bmp.Width; x++)
        //    {
        //        for (int y = 0; y < bmp.Height; y++)
        //        {
        //            int argb = bmp.GetPixel(x, y);
        //            int r = (argb >> 16) & 0xFF;
        //            int g = (argb >> 8) & 0xFF;
        //            int b = argb & 0xFF;
        //            int min = Math.Min(r, g);
        //            min = Math.Min(min, b);

        //            int max = Math.Max(r, g);
        //            max = Math.Max(max, b);

        //            int l = min + max / 2;

        //            Color c = new Color(r, g, b);

        //            originalPixels[x, y] = c;

        //            int index = colors.FindIndex(item => (item.color.r == c.r && item.color.g == c.g && item.color.b == c.b));

        //            if (index < 0)
        //            {
        //                colors.Add((c, 1));
        //            }
        //            else
        //            {
        //                colors[index] = (c, colors[index].count + 1);
        //            }

        //            //if (!Check.IsContour(pixels[x, y]))
        //            //{
        //            //    l = (byte)MathF.Min(l * 1.1f, 255.0f);
        //            //}

        //            grayPixels[x, y] = (byte)l;

        //            currentPixels[x, y] = new Color(l, l, l);
        //        }
        //    }

        //    scale = .9f * windowWidth / width;
        //    offset.x = .05f * windowWidth;
        //    offset.y = (windowHeight - height * scale) / 2.0f;

        //    zoom.min = scale;
        //    zoom.max = scale * 4;

        //}
        
        //public void addDelta(float dx, float dy)
        //{
        //    offset.x += dx;
        //    offset.y += dy;
        //}

        //public int getColorCount()
        //{
        //    return 0;
        //}

        //public Color getColorByIndex(int index)
        //{
        //    return colors[index].color;
        //}

        //public int getColorCountByIndex(int index)
        //{
        //    return colors[index].count;
        //}

        public Color getColor(int x, int y)
        {
            return currentPixels[x, y];
        }

        public bool isRightColor((int x, int y) c)
        {
            if (currentPixels[c.x, c.y].r == originalPixels[c.x, c.y].r &&
                currentPixels[c.x, c.y].g == originalPixels[c.x, c.y].g &&
                currentPixels[c.x, c.y].b == originalPixels[c.x, c.y].b)
            {
                return true;
            }

            return false;
        }

        public void changePixel(int x, int y, Color color)
        {

            int index = colors.FindIndex(item => (item.r == color.r && item.g == color.g && item.b == color.b));

            //bool emptyCell = false;
            //if (grayPixels[x, y] == currentPixels[x, y].r &&
            //    grayPixels[x, y] == currentPixels[x, y].g &&
            //    grayPixels[x, y] == currentPixels[x, y].b)
            //{
            //    emptyCell = true;
            //}

            //bool rightColor = false;
            //if (originalPixels[x, y].r == color.r && 
            //    originalPixels[x, y].g == color.g && 
            //    originalPixels[x, y].b == color.b)
            //{
            //    rightColor = true;
            //}

            //bool sameColor = false;
            //if (currentPixels[x, y].r == color.r &&
            //    currentPixels[x, y].g == color.g &&
            //    currentPixels[x, y].b == color.b)
            //{
            //    sameColor = true;
            //}

            /////

            if (grayPixels[x, y] == color.r &&
                grayPixels[x, y] == color.g &&
                grayPixels[x, y] == color.b)
            {
                clearPixelNotify((x, y), index);
            }
            else if (originalPixels[x, y].r == color.r &&
                originalPixels[x, y].g == color.g &&
                originalPixels[x, y].b == color.b)
            {
                rightPixelPaintNotify((x, y), index);
            }
            else if (currentPixels[x, y].r == color.r &&
                currentPixels[x, y].g == color.g &&
                currentPixels[x, y].b == color.b)
            {
                clearPixelNotify((x, y), index);
                color = new Color(grayPixels[x, y], grayPixels[x, y], grayPixels[x, y]);
            }
            else
            {
                wrongPixelPaintNotify((x, y), index);
            }

            currentPixels[x, y] = color;

        }

        public void fillArea(int x, int y, Color color)
        {

        }

        public bool checkCoord((int x, int y) coord)
        {
            if (coord.x < 0 || coord.y < 0 ||
                coord.x >= width ||
                coord.y >= height)
                return false;

            return true;
        }

        

        public void Draw(SKCanvas canvas, ImageBox imageBox)
        {
            SKPaint paint = new SKPaint();
            SKRect rect;

            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    paint.Color = new SKColor(currentPixels[x, y].r, currentPixels[x, y].g, currentPixels[x, y].b);

                    float left = imageBox.offset.x + x * imageBox.scale;
                    float top = imageBox.offset.y + y * imageBox.scale;
                    float right = imageBox.offset.x + (x + 1) * imageBox.scale;
                    float bottom = imageBox.offset.y + (y + 1) * imageBox.scale;
                    rect = new SKRect(left, top, right, bottom);

                    canvas.DrawRect(rect, paint);
                }
            }
        }

        public void setOffset(float x, float y)
        {
            offset.x = x;
            offset.y = y;
        }

        public void setScale(float s)
        {
            scale = s;
        }

        public bool pointInArea(float x, float y)
        {
            if (x >= offset.x && x <= offset.x + width * scale &&
                y >= offset.y && y <= offset.y + height * scale)
                return true;
            else
                return false;
        }

        public (int x, int y) convertCoord(float x, float y)
        {
            return ((int)((x - offset.x) / scale), (int)((y - offset.y) / scale));
        }

    }

    class ImageBox
    {
        public float scale;
        public (float x, float y) offset;
        private float width, height;
        public (float min, float max) zoom;
        public (float width, float height) window;

        public ImageBox(int imageWidth, int imageHeight, float windowWidth, float windowHeight)
        {
            scale = .9f * windowWidth / imageWidth;
            offset.x = .05f * windowWidth;
            offset.y = (windowHeight - imageHeight * scale) / 2.0f;

            zoom.min = scale;
            zoom.max = scale * 4;

            width = imageWidth;
            height = imageHeight;

            window = (windowWidth, windowHeight);
        }

        public float getScale()
        {
            return scale;
        }

        public void addDelta(float dx, float dy)
        {
            offset.x += dx;
            offset.y += dy;
        }

        public (int x, int y) getPixelCoord((float x, float y) coord)
        {
            return ((int)((coord.x - offset.x) / scale), (int)((coord.y - offset.y) / scale));
        }

        public (float, float) getOffset()
        {
            return offset;
        }

        public void setOffset(float x, float y)
        {
            offset = (x, y);
        }

        public void setScale(float s)
        {
            scale = s;

        }

        public bool pointInArea(float x, float y)
        {
            if (x >= offset.x && x <= offset.x + width * scale &&
                y >= offset.y && y <= offset.y + height * scale)
                return true;
            else
                return false;
        }

        public (int x, int y) convertCoord(float x, float y)
        {
            return ((int)((x - offset.x) / scale), (int)((y - offset.y) / scale));
        }
    }

    class Palette
    {
        //private Dictionary<int, Color> colors;
        private List<Color> colors;
        private List<int> count;

        private int length = 0;
        //private (float x, float y) offset;
        private float offset = 0;
        private (float left, float right) offsetLimit = (0, 0);

        private SKRect backgroundRect;
        private SKRect colorCellRect;

        private float cellWidth, cellRadius, cellMargin;

        private int currentCellIndex = 0;

        public Palette(Bitmap bmp, float windowWidth, float windowHeight)
        {
            cellWidth = windowWidth * 0.1f;
            cellRadius = cellWidth * 0.05f;
            cellMargin = windowWidth * 0.005f;

            backgroundRect = new SKRect(0, windowHeight - (cellWidth + 2 * cellMargin), windowWidth, windowHeight);
            colorCellRect = new SKRect(0, windowHeight - cellWidth - cellMargin, 
                                       0, windowHeight - cellMargin);

            colors = new List<Color>();
            count = new List<int>();

            offsetLimit.left = backgroundRect.Right;

            for (int x = 0; x < bmp.Width; x++)
            {
                for (int y = 0; y < bmp.Height; y++)
                {
                    int argb = bmp.GetPixel(x, y);
                    int r = (argb >> 16) & 0xFF;
                    int g = (argb >> 8) & 0xFF;
                    int b = argb & 0xFF;

                    Color c = new Color(r, g, b);

                    int index = colors.FindIndex(item => (item.r == c.r && item.g == c.g && item.b == c.b));

                    if (index < 0)
                    {
                        colors.Add(c);
                        count.Add(1);

                        offsetLimit.left -= cellMargin + cellWidth;
                    }
                    else
                    {
                        count[index]++;
                    }
                }
            }
        }

        public void Add(Color color, int c)
        {
            //colors.Add(length, color);
            //length++;

            colors.Add(color);
            count.Add(c);

            offsetLimit.left -= cellMargin + cellWidth;
        }

        public void addDelta(float delta)
        {
            
            offset += delta;

            if (offset < offsetLimit.left)
                offset = offsetLimit.left;
            else if (offset > offsetLimit.right)
                offset = offsetLimit.right;

        }

        public int getSelectedColorIndex()
        {
            return currentCellIndex;
        }

        public Color getSelectedColor()
        {
            return colors[currentCellIndex];
        }

        public bool pointInArea(float x, float y)
        {
            if (x >= backgroundRect.Left && x <= backgroundRect.Right &&
                y >= backgroundRect.Top && y <= backgroundRect.Bottom)
            {
                currentCellIndex = (int)((x - offset) / (cellMargin + cellWidth));
            }
            else
                return false;

            return true;
        }

        public void reduceColorCount((int x, int y) coordinates, int colorIndex)
        {
            count[colorIndex]--;
        }

        public void incColorCount((int x, int y) coordinates, int colorIndex)
        {
            //count[colorIndex]++;
        }

        public void Draw(SKCanvas canvas)
        {
            SKPaint paint = new SKPaint();

            SKPaint strokPaint = new SKPaint();
            strokPaint.Style = SKPaintStyle.Stroke;
            strokPaint.StrokeWidth = 5.0f;
            strokPaint.StrokeCap = SKStrokeCap.Round;
            strokPaint.Color = SKColors.White;

            paint.Color = new SKColor(0, 0, 0);
            canvas.DrawRect(backgroundRect, paint);

            colorCellRect.Left = offset - cellWidth;

            for (int i = 0; i < colors.Count; i++)
            {
                paint.Color = new SKColor(colors[i].r, colors[i].g, colors[i].b);

                //colorCellRect.Left = offset + cellMargin * (i + 1) + cellWidth * i;
                colorCellRect.Left += cellMargin + cellWidth;

                if (colorCellRect.Left > backgroundRect.Right)
                {
                    break;
                }

                colorCellRect.Right = colorCellRect.Left + cellWidth;

                if (colorCellRect.Right < 0)
                {
                    continue;
                }

                canvas.DrawRoundRect(colorCellRect, cellRadius, cellRadius, paint);

                SKPaint tPaint = new SKPaint();
                tPaint.Color = new SKColor(255, 255, 255);
                tPaint.TextSize = colorCellRect.Width * 0.5f;
                tPaint.TextAlign = SKTextAlign.Center;

                canvas.DrawText( (i + 1).ToString(), colorCellRect.MidX, colorCellRect.Bottom - colorCellRect.Height / 3, tPaint);

                if (currentCellIndex == i)
                {   
                    canvas.DrawRoundRect(colorCellRect, cellRadius, cellRadius, strokPaint);
                }

                

            }

        }
    }

    class ProgressBar
    {
        SKRect frame;
        float frameRadius = 0;

        List<Color> colors;
        List<int> count;
        List<int> painted;

        public ProgressBar(float windowWidth, float windowHeight)
        {
            frame = new SKRect(windowWidth * 0.005f, windowWidth * 0.005f,
                windowWidth - windowWidth * 0.005f, windowWidth * 0.005f + windowWidth * 0.1f);

            frameRadius = windowWidth * 0.1f * 0.05f + 10;
        }

        public ProgressBar(Bitmap bmp, float windowWidth, float windowHeight)
        {
            frame = new SKRect(windowWidth * 0.005f, windowWidth * 0.005f,
                windowWidth - windowWidth * 0.005f, windowWidth * 0.005f + windowWidth * 0.1f);

            frameRadius = windowWidth * 0.1f * 0.05f + 10;

            colors = new List<Color>();
            count = new List<int>();
            painted = new List<int>();

            for (int x = 0; x < bmp.Width; x++)
            {
                for (int y = 0; y < bmp.Height; y++)
                {
                    int argb = bmp.GetPixel(x, y);
                    int r = (argb >> 16) & 0xFF;
                    int g = (argb >> 8) & 0xFF;
                    int b = argb & 0xFF;

                    Color c = new Color(r, g, b);

                    int index = colors.FindIndex(item => (item.r == c.r && item.g == c.g && item.b == c.b));

                    if (index < 0)
                    {
                        colors.Add(c);
                        count.Add(1);
                        painted.Add(0);
                    }
                    else
                    {
                        count[index]++;
                    }
                }
            }
        }

        public void incProgress((int x, int y) coordinates, int colorIndex)
        {
            painted[colorIndex]++;
        }

        public void Draw(SKCanvas canvas, int colorIndex)
        {
            SKPaint strokPaint = new SKPaint();
            strokPaint.Style = SKPaintStyle.Stroke;
            strokPaint.StrokeWidth = 5.0f;
            strokPaint.StrokeCap = SKStrokeCap.Round;
            strokPaint.Color = SKColors.White;

            SKPaint paint = new SKPaint();
            paint.Color = new SKColor(colors[colorIndex].r, colors[colorIndex].g, colors[colorIndex].b);
            SKRect progress = new SKRect(frame.Left, frame.Top, 0, frame.Bottom);
            progress.Right = (float)painted[colorIndex] / (float)count[colorIndex] * frame.Width;

            canvas.DrawRoundRect(progress, frameRadius, frameRadius, paint);

            canvas.DrawRoundRect(frame, frameRadius, frameRadius, strokPaint);
        }
    }

    class UndoButton
    {
        private SKRect source, dest;
        private SKImage image;

        public UndoButton(float windowWidth, float windowHeight)
        {
            Tuple<SKImage, SKRect> tmp = URPNGLoader.LoadURPNG(GetType().GetTypeInfo().Assembly,
                $"AndroidCS.Assets.arrows.bytes", "atlas", 0);
            image = tmp.Item1;
            source = tmp.Item2;

            dest = new SKRect();
            dest.Left = windowWidth * 0.005f;
            dest.Right = dest.Left + windowWidth * 0.1f;
            dest.Bottom = windowHeight - windowWidth * 0.1f - 3 * windowWidth * 0.005f;
            dest.Top = dest.Bottom - windowWidth * 0.1f;
        }

        public bool pointInArea(float x, float y)
        {
            if (x >= dest.Left && x <= dest.Right &&
                y >= dest.Top && y <= dest.Bottom)
                return true;
            else
                return false;
        }

        public void Draw(SKCanvas canvas)
        {
            canvas.DrawImage(image, source, dest);
        }
    }

    class RedoButton
    {
        private SKRect source, dest;
        private SKImage image;

        public RedoButton(float windowWidth, float windowHeight)
        {
            Tuple<SKImage, SKRect> tmp = URPNGLoader.LoadURPNG(GetType().GetTypeInfo().Assembly,
                $"AndroidCS.Assets.arrows.bytes", "atlas", 1);
            image = tmp.Item1;
            source = tmp.Item2;

            dest = new SKRect();
            dest.Right = windowWidth - windowWidth * 0.005f;
            dest.Left = dest.Right - windowWidth * 0.1f;
            dest.Bottom = windowHeight - windowWidth * 0.1f - 3 * windowWidth * 0.005f;
            dest.Top = dest.Bottom - windowWidth * 0.1f;
        }

        public bool pointInArea(float x, float y)
        {
            if (x >= dest.Left && x <= dest.Right &&
                y >= dest.Top && y <= dest.Bottom)
                return true;
            else
                return false;
        }

        public void Draw(SKCanvas canvas)
        {
            canvas.DrawImage(image, source, dest);
        }
    }

    class ColorIndexGrid
    {
        int [,] cellMap;
        bool [,] highlightMap;
        bool[,] showMap;

        List<byte> textColors;

        int width, height;


        public ColorIndexGrid(Bitmap bmp)
        {
            List<Color> colors = new List<Color>();
            textColors = new List<byte>();

            cellMap = new int[bmp.Width, bmp.Height];
            highlightMap = new bool[bmp.Width, bmp.Height];
            showMap = new bool[bmp.Width, bmp.Height];

            width = bmp.Width;
            height = bmp.Height;

            for (int x = 0; x < bmp.Width; x++)
            {
                for (int y = 0; y < bmp.Height; y++)
                {
                    int argb = bmp.GetPixel(x, y);
                    int r = (argb >> 16) & 0xFF;
                    int g = (argb >> 8) & 0xFF;
                    int b = argb & 0xFF;

                    int min = Math.Min(r, g);
                    min = Math.Min(min, b);

                    int max = Math.Max(r, g);
                    max = Math.Max(max, b);

                    int l = min + max / 2;

                    Color c = new Color(r, g, b);

                    int index = colors.FindIndex(item => (item.r == c.r && item.g == c.g && item.b == c.b));

                    if (index < 0)
                    {
                        colors.Add(c);
                        index = colors.Count - 1;
                        textColors.Add((byte)(255 - l));
                    }

                    cellMap[x, y] = index;
                    showMap[x, y] = true;
                    highlightMap[x, y] = false;
                }
            }
        }

        public ColorIndexGrid(int w, int h)
        {
            width = w;
            height = h;

            cellMap = new int[width, height];
            highlightMap = new bool[width, height];
            showMap = new bool[width, height];
        }

        public void setCell(int x, int y, int index)
        {
            cellMap[x, y] = index;
            highlightMap[x, y] = false;
            showMap[x, y] = true;
        }

        public void clearCell((int x, int y) coordinates, int colorIndex)
        {
            showMap[coordinates.x, coordinates.y] = false;
            highlightMap[coordinates.x, coordinates.y] = false;
        }

        public void highlightCell((int x, int y) coordinates, int colorIndex)
        {
            highlightMap[coordinates.x, coordinates.y] = true;
            showMap[coordinates.x, coordinates.y] = true;
        }

        public void showOrigCell((int x, int y) coordinates, int colorIndex)
        {
            showMap[coordinates.x, coordinates.y] = true;
            highlightMap[coordinates.x, coordinates.y] = false;
        }

        public void Draw(SKCanvas canvas, ImageBox imageBox)
        {
            for (int x = 0; x < width; x++)
            {
                for (int y = 0; y < height; y++)
                {
                    if (!showMap[x, y])
                        continue;

                    SKRect textRect = new SKRect();
                    textRect.Left = imageBox.offset.x + imageBox.scale * x;
                    textRect.Top = imageBox.offset.y + imageBox.scale * y;
                    textRect.Right = textRect.Left + imageBox.scale;
                    textRect.Bottom = textRect.Top + imageBox.scale;

                    if (textRect.Width < 60)
                        return;

                    if (textRect.Right < 0 ||
                        textRect.Bottom < 0 ||
                        textRect.Left > imageBox.window.width ||
                        textRect.Top > imageBox.window.height)
                    {
                        continue;
                    }

                    SKPaint tPaint = new SKPaint();
                    byte l = textColors[cellMap[x, y]];
                    tPaint.Color = new SKColor(l, l, l);
                    tPaint.TextSize = textRect.Width * 0.5f;
                    tPaint.TextAlign = SKTextAlign.Center;

                    if (highlightMap[x, y])
                    {
                        tPaint.Typeface = SKTypeface.FromFamilyName("Arial", SKFontStyleWeight.ExtraBold, SKFontStyleWidth.UltraExpanded, SKFontStyleSlant.Oblique);
                        tPaint.FakeBoldText = true;
                    }

                    canvas.DrawText( (cellMap[x, y] + 1).ToString(), textRect.MidX, textRect.Bottom - textRect.Height / 3, tPaint);

                }
            }
            
        }
    }
}