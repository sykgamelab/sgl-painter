﻿using System;
using System.Collections.Generic;
using System.IO;
using System.IO.Compression;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading;

using Android.App;
using Android.Content;
using Android.Content.Res;
using Android.OS;
using Android.Runtime;
using Android.Util;
using Android.Views;
using Android.Widget;
using SkiaSharp;
using SkiaSharp.Views;
using SkiaSharp.Views.Android;

namespace AndroidCS
{
    [Activity(Label = "ContourNumberHardActivity", MainLauncher = false)]
    public class ContourNumberHardActivity : Activity
    {
        bool isInitialized = false;

        const float MaxEdge = 3500; //3500
        const string Tag = "Painter";

        SKGLSurfaceView glView;

        float renderTextThreshold;

        GRContext offscreenContext;
        SKSurface offscreenSurface;
        float start;
        float paletteOffset, paletteSpacing;
        float paletteY, paletteSpaceH, paletteR;
        float paletteX;
        DateTime duration;
        

        private void RenderHistory(CommandShape shape, SKCanvas target, float imgw, PaletteColor color, int idx, bool isDrawn, bool isHighlighted)
        {
            byte r, g, b;

            r = g = b = 255;

            if (isDrawn == false && isHighlighted == false)
            {
                r = 255;
                g = 255;
                b = 255;
                shape.isDrawn = false;
                shape.isHighlighted = false;
            }

            if (isHighlighted && isDrawn == false)
            {
                r = color.r;
                g = color.g;
                b = color.b;
                shape.isDrawn = false;
                shape.isHighlighted = true;
            }

            if (isDrawn && isHighlighted == false)
            {
                r = color.r;
                g = color.g;
                b = color.b;
                shape.isDrawn = true;
                shape.isHighlighted = false;
            }

            shape.r = r;
            shape.g = g;
            shape.b = b;

            ColoringState.CommandShapes[idx] = shape;
            Draw(shape, target, imgw, r, g, b);
        }

        private void RenderShape(CommandShape shape, SKCanvas target, float imgw, bool? isHighlighted = null, PaletteColor? color = null, int? idx = null)
        {
            byte r, g, b;

            if (color != null)
            {
                if (!shape.isDrawn)
                {
                    if (color.Value.r == ColoringState.Shapes[idx.Value].r && color.Value.g == ColoringState.Shapes[idx.Value].g && color.Value.b == ColoringState.Shapes[idx.Value].b)
                    {
                        r = color.Value.r;
                        g = color.Value.g;
                        b = color.Value.b;
                        shape.isDrawn = true;
                        shape.isHighlighted = false;
                    }
                    else
                    {
                        r = color.Value.r;
                        g = color.Value.g;
                        b = color.Value.b;
                        shape.isDrawn = false;
                        shape.isHighlighted = true;
                    }
                }
                else
                {
                    return;
                }
            }
            else
            {
                r = (byte)shape.r;
                g = (byte)shape.g;
                b = (byte)shape.b;
            }

            if (idx != null)
            {
                shape.r = r;
                shape.g = g;
                shape.b = b;
                ColoringState.CommandShapes[idx.Value] = shape;
            }

            Draw(shape, target, imgw, r, g, b);
        }

        private void Draw(CommandShape shape, SKCanvas target, float imgw, byte r, byte g, byte b)
        {
            float imgscale = imgw / (ColoringState.ViewBox);

            var contours = shape.contours;
            SKPaint paint = new SKPaint();
            paint.IsAntialias = true;
            paint.Color = new SKColor(r, g, b);

            SKPath path = new SKPath();
            for (int i = 0; i < contours.Count; i++)
            {
                var commands = contours[i].commands;
                for (int k = 0; k < commands.Count; k++)
                {
                    commands[k].Exec(path, 0, 0, imgscale);
                }
                path.Close();
            }

            target.DrawPath(path, paint);
        }

        private void AddToPalette(PaletteColor color, int idx)
        {
            int i;

            if (ColoringState.Palette.Count == 0)
            {
                ColoringState.Palette.Add((0, color, (paletteX, paletteY)));
                ColoringState.HighlightedShapes.Add(0, new List<int>());
                ColoringState.HighlightedShapes[0].Add(idx);
                paletteX += ColoringState.PaletteWH + paletteSpacing;
            }
            else
            {
                for (i = 0; i < ColoringState.Palette.Count; i++)
                {
                    if (ColoringState.Palette[i].Item2.r == color.r && ColoringState.Palette[i].Item2.g == color.g && ColoringState.Palette[i].Item2.b == color.b)
                    {
                        ColoringState.HighlightedShapes[i].Add(idx);
                        return;
                    }
                }
                ColoringState.Palette.Add((i, color, (paletteX, paletteY)));
                paletteX += ColoringState.PaletteWH + paletteSpacing;
                if (!ColoringState.HighlightedShapes.ContainsKey(idx))
                    ColoringState.HighlightedShapes.Add(i, new List<int>());
                ColoringState.HighlightedShapes[i].Add(idx);
            }
        }

        private void RenderShapeText(string value, CommandShape shape, SKCanvas target, float imgw, bool isDrawnCorrectly, bool isNumberHighlighted)
        {
            float left = shape.textRect.rx * ColoringState.Scale + ColoringState.Offsetx;
            float right = left + shape.textRect.rw * ColoringState.Scale;

            if ((right - left) <= renderTextThreshold)
                return;

            float top = shape.textRect.ry * ColoringState.Scale + ColoringState.Offsety;
            float bottom = top + shape.textRect.rw * ColoringState.Scale;

            if (left > ColoringState.Ww || right < 0 || top > ColoringState.Wh || bottom < 0)
                return;

            SKRect textRect = new SKRect(left, top, right, bottom);
            SKPaint tPaint = new SKPaint();
            tPaint.Color = new SKColor(0, 0, 0, 255);
            tPaint.TextSize = textRect.Width * 0.5f;
            tPaint.TextAlign = SKTextAlign.Center;

            int v = Math.Max(shape.r, shape.g);
            v = Math.Max(v, shape.b);

            tPaint.Color = new SKColor(0, 0, 0); // ?

            if (v <= 40)
                tPaint.Color = new SKColor(255, 255, 255);

            if (isDrawnCorrectly == false && isNumberHighlighted != true)
                tPaint.Color = new SKColor(0, 0, 0);

            if (!isDrawnCorrectly)
            {
                if (isNumberHighlighted)
                {
                    tPaint.Typeface = SKTypeface.FromFamilyName("Arial", SKFontStyleWeight.ExtraBold, SKFontStyleWidth.UltraExpanded, SKFontStyleSlant.Upright);
                    tPaint.FakeBoldText = true;
                    tPaint.TextSize = textRect.Width * 0.5f;
                    target.DrawText(value, textRect.MidX, textRect.Bottom - textRect.Height / 3, tPaint);
                }
                else
                {
                    tPaint.TextSize = textRect.Width * 0.5f;
                    target.DrawText(value, textRect.MidX, textRect.Bottom - textRect.Height / 3, tPaint);
                }
            }
        }

        private void DrawCallback(object sender, SKPaintGLSurfaceEventArgs args)
        {
            var skiaSurface = args.Surface;
            var skiaCanvas = skiaSurface.Canvas;

            if (!isInitialized)
            {
                ColoringState.Ww = glView.Width;
                ColoringState.Wh = glView.Height;

                ColoringState.Lt = ColoringState.Shapes[0].lt;
                ColoringState.Rt = ColoringState.Shapes[0].rt;
                ColoringState.Bm = ColoringState.Shapes[0].bm;
                ColoringState.Tp = ColoringState.Shapes[0].tp;

                ColoringState.Palette = new List<(int, PaletteColor, (float, float))>();
                ColoringState.HighlightedShapes = new Dictionary<int, List<int>>();

                paletteOffset = 0.03f * ColoringState.Ww;
                paletteSpacing = 0.005f * ColoringState.Ww;
                ColoringState.PaletteWH = 0.1f * ColoringState.Ww;
                paletteY = ColoringState.Wh - ColoringState.PaletteWH - paletteSpacing;

                paletteX = paletteOffset + paletteSpacing + ColoringState.PaletteOffsetX;

                paletteSpaceH = ColoringState.PaletteWH + 2 * paletteSpacing;
                ColoringState.PalettePositionY = ColoringState.Wh - paletteSpaceH;
                paletteR = 0.05f * ColoringState.PaletteWH;

                var color = new PaletteColor()
                {
                    r = (byte)ColoringState.Shapes[0].r,
                    g = (byte)ColoringState.Shapes[0].g,
                    b = (byte)ColoringState.Shapes[0].b
                };

                if (!Check.IsContour(ColoringState.CommandShapes[0]))
                {
                    AddToPalette(color, 0);
                    ColoringState.CurrentColor = ColoringState.Palette[0].Item2;
                }

                for (int i = 1; i < ColoringState.Shapes.Count; i++)
                {
                    if (ColoringState.Shapes[i].lt < ColoringState.Lt)
                        ColoringState.Lt = ColoringState.Shapes[i].lt;
                    if (ColoringState.Shapes[i].rt > ColoringState.Rt)
                        ColoringState.Rt = ColoringState.Shapes[i].rt;

                    if (ColoringState.Shapes[i].bm < ColoringState.Bm)
                        ColoringState.Bm = ColoringState.Shapes[i].bm;
                    if (ColoringState.Shapes[i].tp > ColoringState.Tp)
                        ColoringState.Tp = ColoringState.Shapes[i].tp;

                    color.r = (byte)ColoringState.Shapes[i].r;
                    color.g = (byte)ColoringState.Shapes[i].g;
                    color.b = (byte)ColoringState.Shapes[i].b;

                    if (!Check.IsContour(ColoringState.CommandShapes[i]))
                    {
                        AddToPalette(color, i);
                    }
                }

                if (ColoringState.Lt < 0) ColoringState.Lt = 0;
                if (ColoringState.Lt > ColoringState.Ww) ColoringState.Lt = ColoringState.Ww;
                if (ColoringState.Bm < 0) ColoringState.Bm = 0;
                if (ColoringState.Bm > ColoringState.Wh) ColoringState.Bm = ColoringState.Wh;

                ColoringState.CurrentColor = ColoringState.Palette[0].Item2;

                ColoringState.Scale = 0.9f * ColoringState.Ww / (ColoringState.ViewBox);
                ColoringState.MaxImgZoom = MaxEdge / (ColoringState.ViewBox);
                ColoringState.MaxZoom = ColoringState.MaxImgZoom * 2.5f;
                ColoringState.MinZoom = ColoringState.Scale;

                float dpi = Resources.DisplayMetrics.Xdpi;
                dpi = (dpi / 25.4f);
                float minW = dpi * 3;

                renderTextThreshold = minW * 0.9f;

                float cx, cy;
                float cxNew, cyNew;

                CommandShape shape;

                for (int i = 0; i < ColoringState.CommandShapes.Count; i++)
                {
                    if (!Check.IsContour(ColoringState.CommandShapes[i]))
                        if (ColoringState.CommandShapes[i].textRect.rw * ColoringState.MaxZoom < minW)
                        {
                            shape = ColoringState.CommandShapes[i];
                            cx = (2 * ColoringState.CommandShapes[i].textRect.rx + ColoringState.CommandShapes[i].textRect.rw) / 2;
                            cy = (2 * ColoringState.CommandShapes[i].textRect.ry + ColoringState.CommandShapes[i].textRect.rw) / 2;

                            shape.textRect.rw = minW / ColoringState.MaxZoom;
                            cxNew = (2 * shape.textRect.rx + shape.textRect.rw) / 2;
                            cyNew = (2 * shape.textRect.ry + shape.textRect.rw) / 2;

                            shape.textRect.rx -= cxNew - cx;
                            shape.textRect.ry -= cyNew - cy;

                            ColoringState.CommandShapes[i] = shape;
                        }
                }

                ColoringState.ImgWH = (int)((ColoringState.ViewBox) * ColoringState.MaxImgZoom);

                ColoringState.BufferOffsetx = 0.05f * ColoringState.Ww;
                ColoringState.BufferOffsety = (ColoringState.Wh - ColoringState.ImgWH * ColoringState.Scale / ColoringState.MaxImgZoom) / 2.0f;

                ColoringState.Offsetx = ColoringState.BufferOffsetx;
                ColoringState.Offsety = ColoringState.BufferOffsety;

                ColoringState.MaxBufferL = 0.05f * ColoringState.Ww;
                ColoringState.MinBufferR = 0.95f * ColoringState.Ww;
                ColoringState.MaxBufferB = 0.05f * ColoringState.Wh;
                ColoringState.MinBufferT = paletteY - 0.05f * ColoringState.Wh;

                ColoringState.MaxPaletteLeft = 0;
                ColoringState.MaxPaletteRight = (ColoringState.Palette.Count - 9) * (ColoringState.PaletteWH + paletteSpacing) * -1;

                ColoringState.HistoryWH = ColoringState.PaletteWH * 0.9f;

                // define the surface properties
                var offscreenInfo = new SKImageInfo(ColoringState.ImgWH, ColoringState.ImgWH, SKColorType.Bgra8888, SKAlphaType.Premul);
                // create the surface
                offscreenContext = GRContext.Create(GRBackend.OpenGL);
                offscreenSurface = SKSurface.Create(offscreenContext, false, offscreenInfo);
                ColoringState.OffscreenCanvas = offscreenSurface.Canvas;

                ColoringState.OffscreenCanvas.Clear(new SKColor(255, 255, 255, 0));
                for (int i = 0; i < ColoringState.CommandShapes.Count; i++)
                    if (Check.IsContour(ColoringState.CommandShapes[i]))
                        RenderShape(ColoringState.CommandShapes[i], ColoringState.OffscreenCanvas, ColoringState.ImgWH);
                //offscreenCanvas.Flush();

                SKRect srcRect = new SKRect(0, 0, ColoringState.ViewBox, ColoringState.ViewBox);
                SKRect dstRect = new SKRect(0, 0, ColoringState.ImgWH, ColoringState.ImgWH);

                if (ColoringState.PngImages.Item2 != null)
                    ColoringState.OffscreenCanvas.DrawImage(ColoringState.PngImages.Item2, srcRect, dstRect);

                ColoringState.Contours = offscreenSurface.Snapshot();
                ColoringState.OffscreenCanvas.Clear(new SKColor(255, 255, 255, 0));

                if (ColoringState.PngImages.Item1 != null)
                {
                    ColoringState.OffscreenCanvas.DrawImage(ColoringState.PngImages.Item1, srcRect, dstRect);
                    ColoringState.BgImage = offscreenSurface.Snapshot();
                    ColoringState.OffscreenCanvas.Clear(new SKColor(255, 255, 255, 0));
                }

                ColoringState.OffscreenCanvas.Clear(new SKColor(255, 255, 255, 0));

                for (int i = 0; i < ColoringState.CommandShapes.Count; i++)
                    if (!Check.IsContour(ColoringState.CommandShapes[i]))
                    {
                        CommandShape temp = ColoringState.CommandShapes[i];
                        temp.r = 255;
                        temp.g = 255;
                        temp.b = 255;
                        ColoringState.CommandShapes[i] = temp;
                        RenderShape(ColoringState.CommandShapes[i], ColoringState.OffscreenCanvas, ColoringState.ImgWH);
                    }

                ColoringState.Regions = offscreenSurface.Snapshot();

                Assembly assembly = GetType().GetTypeInfo().Assembly;
                string resourceID = $"AndroidCS.Assets.arrows.bytes";

                Tuple<SKImage, SKRect> pair;
                SKImage image;
                SKRect rectSrc;
                SKRect rectDst = new SKRect(paletteOffset, ColoringState.Wh - ColoringState.PaletteWH - ColoringState.HistoryWH - paletteSpacing * 4, paletteOffset + ColoringState.HistoryWH, ColoringState.Wh - ColoringState.PaletteWH - paletteSpacing * 4);

                for (int i = 0; i < 2; i++)
                {
                    pair = URPNGLoader.LoadURPNG(assembly, resourceID, "arrows", i);
                    image = pair.Item1;
                    rectSrc = pair.Item2;
                    ColoringState.controlsGUI.Add((image, rectSrc, rectDst));
                    rectDst.Left = ColoringState.Ww - paletteOffset - ColoringState.HistoryWH;
                    rectDst.Right = ColoringState.Ww - paletteOffset;
                }

                rectDst.Left = paletteOffset;
                rectDst.Top = ColoringState.Wh - paletteSpaceH - paletteSpacing * 6 - ColoringState.HistoryWH * 2;
                rectDst.Right = rectDst.Left + ColoringState.HistoryWH;
                rectDst.Bottom = ColoringState.controlsGUI[0].rectDst.Top - paletteSpacing * 4;

                pair = URPNGLoader.LoadURPNG(assembly, resourceID, "arrows", 2);
                image = pair.Item1;
                rectSrc = pair.Item2;
                ColoringState.controlsGUI.Add((image, rectSrc, rectDst));
                

                isInitialized = true;
            }

            float curr = (float)(DateTime.UtcNow - duration).TotalMilliseconds;

            float delta = curr - start;

            ColoringState.TotalTime += delta / 1000f;

            if (delta >= 1.0f)
            {
                start = curr;
            }

            ColoringState.Mut.WaitOne();

            if (ColoringState.Queue.Count > 0)
            {
                var request = ColoringState.Queue.Dequeue();
                int idx = request.Item1;
                PaletteColor color = request.Item2;
                bool? isHighlighted = request.Item3;
                bool? undo = request.Item4;
                bool? isDrawn = request.Item5;
                int? redoProgress = request.Item6;
                int? undoProgress = request.Item7;

                bool prevIsDrawn = ColoringState.CommandShapes[idx].isDrawn;
                bool prevIsHighlighted = ColoringState.CommandShapes[idx].isHighlighted;

                int prevProgress = ColoringState.PaintedShapesCount;

                if (ColoringState.CurrentColor.r == ColoringState.Shapes[idx].r
                    && ColoringState.CurrentColor.g == ColoringState.Shapes[idx].g
                    && ColoringState.CurrentColor.b == ColoringState.Shapes[idx].b && !ColoringState.CommandShapes[idx].isDrawn)
                {
                    ColoringState.PaintedShapesCount++;
                    ColoringState.ProgressBarPercent = ((float)ColoringState.PaintedShapesCount * 1f) / (float)ColoringState.CommandShapes.Count;
                }

                int newProgress = ColoringState.PaintedShapesCount;

                PaletteColor prevColor = new PaletteColor();
                prevColor.Set((byte)ColoringState.CommandShapes[idx].r, (byte)ColoringState.CommandShapes[idx].g, (byte)ColoringState.CommandShapes[idx].b);

                if (!prevIsDrawn && !prevIsHighlighted)
                    prevColor.Set(255, 255, 255);

                if (undo == true)
                {
                    RenderHistory(ColoringState.CommandShapes[idx], ColoringState.OffscreenCanvas, ColoringState.ImgWH, color, idx, isDrawn.Value, isHighlighted.Value);
                    if (undoProgress != null)
                    {
                        ColoringState.PaintedShapesCount = undoProgress.Value;
                        ColoringState.ProgressBarPercent = ((float)ColoringState.PaintedShapesCount * 1f) / (float)ColoringState.CommandShapes.Count;
                    }
                }
                else if (undo == false)
                {
                    RenderHistory(ColoringState.CommandShapes[idx], ColoringState.OffscreenCanvas, ColoringState.ImgWH, color, idx, isDrawn.Value, isHighlighted.Value);
                    if (redoProgress != null)
                    {
                        ColoringState.PaintedShapesCount = redoProgress.Value;
                        ColoringState.ProgressBarPercent = ((float)ColoringState.PaintedShapesCount * 1f) / (float)ColoringState.CommandShapes.Count;
                    }
                }
                else
                {
                    RenderShape(ColoringState.CommandShapes[idx], ColoringState.OffscreenCanvas, ColoringState.ImgWH, isHighlighted, color, idx);

                    PaletteColor newColor = ColoringState.CurrentColor;
                    PaintCommand command = new PaintCommand(idx, prevColor, newColor, prevIsDrawn, prevIsHighlighted,
                        ColoringState.CommandShapes[idx].isDrawn, ColoringState.CommandShapes[idx].isHighlighted, ColoringState.Mut, ColoringState.Queue, newProgress, prevProgress);

                    ColoringState.Undo.Push(command);
                    ColoringState.Redo.Clear();
                }

                ColoringState.Regions.Dispose();
                ColoringState.Regions = null;
                ColoringState.Regions = offscreenSurface.Snapshot();
            }

            ColoringState.Mut.ReleaseMutex();

            skiaCanvas.Clear(new SKColor(200, 200, 200));

            skiaCanvas.Save();
            skiaCanvas.Translate(ColoringState.BufferOffsetx, ColoringState.BufferOffsety);
            skiaCanvas.Scale(ColoringState.Scale / ColoringState.MaxImgZoom, ColoringState.Scale / ColoringState.MaxImgZoom);
            if (ColoringState.PngImages.Item1 != null)
                skiaCanvas.DrawImage(ColoringState.BgImage, 0.0f, 0.0f);
            skiaCanvas.DrawImage(ColoringState.Regions, 0.0f, 0.0f);
            skiaCanvas.DrawImage(ColoringState.Contours, 0.0f, 0.0f);
            skiaCanvas.Restore();

            for (int i = 0; i < ColoringState.Palette.Count; i++)
            {
                int idx = ColoringState.Palette[i].Item1;
                for (int j = 0; j < ColoringState.HighlightedShapes[idx].Count; j++)
                {
                    int curShapeIdx = ColoringState.HighlightedShapes[idx][j];
                    if (!ColoringState.CommandShapes[curShapeIdx].isDrawn)
                        RenderShapeText((ColoringState.Palette[i].Item1 + 1).ToString(), ColoringState.CommandShapes[curShapeIdx], skiaCanvas, ColoringState.ImgWH, ColoringState.CommandShapes[curShapeIdx].isDrawn, ColoringState.CommandShapes[curShapeIdx].isHighlighted);
                    else
                        RenderShapeText((ColoringState.Palette[i].Item1 + 1).ToString(), ColoringState.CommandShapes[curShapeIdx], skiaCanvas, ColoringState.ImgWH, ColoringState.CommandShapes[curShapeIdx].isDrawn, false);
                }
            }

            SKPaint paint = new SKPaint();
            paint.IsAntialias = true;
            paint.Color = new SKColor(0, 0, 0);

            SKPaint strokPaint = new SKPaint();
            strokPaint.Style = SKPaintStyle.Stroke;
            strokPaint.StrokeWidth = 5.0f;
            strokPaint.StrokeCap = SKStrokeCap.Round;
            strokPaint.Color = SKColors.White;

            SKColor progressBarGradientA = new SKColor(243, 131, 152);
            SKColor progressBarGradientB = new SKColor(153, 50, 127);

            byte gradientR = (byte)(progressBarGradientA.Red + ColoringState.ProgressBarPercent * (progressBarGradientB.Red - progressBarGradientA.Red));
            byte gradientG = (byte)(progressBarGradientA.Green + ColoringState.ProgressBarPercent * (progressBarGradientB.Green - progressBarGradientA.Green));
            byte gradientB = (byte)(progressBarGradientA.Blue + ColoringState.ProgressBarPercent * (progressBarGradientB.Blue - progressBarGradientA.Blue));

            SKColor curProgressBarGradientPoint = new SKColor(gradientR, gradientG, gradientB);

            SKRect rect = new SKRect(paletteOffset, paletteOffset, ColoringState.Ww - paletteOffset, ColoringState.PaletteWH);
            skiaCanvas.DrawRoundRect(rect, paletteR + 10, paletteR + 10, strokPaint);

            rect.Right = ColoringState.Ww * ColoringState.ProgressBarPercent + paletteOffset;

            paint.Shader = SKShader.CreateLinearGradient(
                      new SKPoint(rect.Left, rect.Left),
                      new SKPoint(rect.Right, rect.Right),
                      new SKColor[] { progressBarGradientA, curProgressBarGradientPoint },
                      new float[] { 0, 1 },
                      SKShaderTileMode.Repeat);

            skiaCanvas.DrawRoundRect(rect, paletteR + 10, paletteR + 10, paint);
            paint.Shader = null;

            rect.Top = ColoringState.PalettePositionY;
            rect.Left = 0;
            rect.Right = ColoringState.Ww;
            rect.Bottom = ColoringState.Wh;
            skiaCanvas.DrawRect(rect, paint);

            skiaCanvas.DrawImage(ColoringState.controlsGUI[2].image, ColoringState.controlsGUI[2].rectSrc, ColoringState.controlsGUI[2].rectDst, paint);
            skiaCanvas.DrawImage(ColoringState.controlsGUI[0].image, ColoringState.controlsGUI[0].rectSrc, ColoringState.controlsGUI[0].rectDst, paint);
            skiaCanvas.DrawImage(ColoringState.controlsGUI[1].image, ColoringState.controlsGUI[1].rectSrc, ColoringState.controlsGUI[1].rectDst, paint);



            paletteX = paletteOffset + ColoringState.PaletteOffsetX;
            rect.Top = paletteY;
            rect.Bottom = paletteY + ColoringState.PaletteWH;



            for (int i = 0; i < ColoringState.Palette.Count; i++)
            {
                rect.Left = paletteX;
                rect.Right = paletteX + ColoringState.PaletteWH;
                paletteX += ColoringState.PaletteWH + paletteSpacing;
                if (rect.Left > ColoringState.Ww || rect.Right < 0)
                    continue;

                paint.Color = new SKColor(ColoringState.Palette[i].Item2.r, ColoringState.Palette[i].Item2.g, ColoringState.Palette[i].Item2.b);
                skiaCanvas.DrawRoundRect(rect, paletteR, paletteR, paint);


                paint.Color = new SKColor(255, 255, 255, 255);
                paint.TextSize = ColoringState.PaletteWH * 0.5f;
                paint.TextAlign = SKTextAlign.Center;
                skiaCanvas.DrawText((ColoringState.Palette[i].Item1 + 1).ToString(), rect.MidX, rect.Bottom - rect.Height / 3, paint);

                if (i == ColoringState.CurrentColorIndex)
                    skiaCanvas.DrawRoundRect(rect, paletteR, paletteR, strokPaint);
            }

            SKPoint findRegionNumberPosition = new SKPoint(ColoringState.controlsGUI[2].rectDst.Left + ColoringState.HistoryWH * 0.2f, ColoringState.controlsGUI[2].rectDst.Top + ColoringState.HistoryWH * 0.3f);
            paint.TextSize = ColoringState.HistoryWH * 0.35f;
            paint.Typeface = SKTypeface.FromFamilyName("Arial", SKFontStyleWeight.ExtraBold, SKFontStyleWidth.UltraExpanded, SKFontStyleSlant.Upright);
            paint.Color = new SKColor(102, 118, 203);
            skiaCanvas.DrawText("2", findRegionNumberPosition, paint);

            paint.Color = new SKColor(0, 0, 255);
            paint.IsStroke = true;
            
            //for (int i = 0; i < CommandShapes.Count; i++)
            //{
            //    SKPath path = new SKPath();

            //    paint.Color = new SKColor(0, 255, 0);
            //    var contours = CommandShapes[i].contours;
            //    for (int j = 0; j < contours.Count; j++)
            //    {
            //        var commands = contours[j].commands;
            //        for (int k = 0; k < commands.Count; k++)
            //        {
            //            commands[k].Exec(path, Offsetx, Offsety, Scale);
            //        }
            //        path.Close();
            //    }

            //    skiaCanvas.DrawPath(path, paint);
            //}
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            ColoringState.Clicks = new List<PainterCommon.PainterPointf>();

            ColoringState.Mut = new Mutex();
            ColoringState.Queue = new Queue<(int, PaletteColor, bool?, bool?, bool?, int?, int?)>();
            ColoringState.controlsGUI = new List<(SKImage, SKRect, SKRect)>();
            ColoringState.Undo = new Stack<ICommand>();
            ColoringState.Redo = new Stack<ICommand>();

            Gestures.Initialize(ApplicationContext);

            //GameLoop loop = new GameLoop();
            //View view = loop.Services.GetService<View>();
            //SetContentView(view);

            duration = new DateTime(2019, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            start = (float)(DateTime.UtcNow - duration).TotalMilliseconds;

            glView = new SKGLSurfaceView(ApplicationContext);
            glView.PaintSurface += DrawCallback;

            SetContentView(glView);
        }


        public override void OnWindowFocusChanged(bool hasFocus)
        {
            if (hasFocus)
            {
                var uiOptions =
                SystemUiFlags.HideNavigation |
                SystemUiFlags.LayoutHideNavigation |
                SystemUiFlags.LayoutFullscreen |
                SystemUiFlags.Fullscreen |
                SystemUiFlags.LayoutStable |
                SystemUiFlags.ImmersiveSticky;

                Window.DecorView.SystemUiVisibility = (StatusBarVisibility)uiOptions;
            }
        }

        protected override void OnStart()
        {
            base.OnStart();
            this.Window.AddFlags(WindowManagerFlags.Fullscreen);

            WebHelper wh = new WebHelper(ServerConstants.Url);
            wh.GetColoring(
                (code, data) =>
                {
                    if (code == 200)
                    {
                        using (Stream stream = new MemoryStream(data))
                        {
                            using (ZipArchive archive = new ZipArchive(stream, ZipArchiveMode.Read))
                            {
                                ZipArchiveEntry entry = archive.Entries[0];
                                using (Stream entryStream = entry.Open())
                                {
                                    using (MemoryStream ms = new MemoryStream())
                                    {
                                        entryStream.CopyTo(ms);
                                        data = ms.ToArray();
                                    }
                                }
                            }
                        }

                        var tuple = PainterRead.Read(data);
                        ColoringState.Shapes = tuple.Item1;
                        ColoringState.CommandShapes = tuple.Item2;
                        ColoringState.ViewBox = tuple.Item3;
                        ColoringState.PngImages = tuple.Item4;
                    }
                }, ServerConstants.GetColoring, ServerConstants.Cid, false);
        }

        protected override void OnResume()
        {
            base.OnResume();
            //glView.OnResume();
        }

        protected override void OnPause()
        {
            base.OnPause();
            //glView.OnPause();
        }

        public override bool OnTouchEvent(MotionEvent e)
        {
            return Gestures.TouchEvent(e, true, true, null);
        }

        public void SurfaceDestroyed(ISurfaceHolder holder)
        {

        }
    }
}