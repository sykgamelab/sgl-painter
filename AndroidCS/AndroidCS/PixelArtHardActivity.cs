﻿using System;
using System.Threading;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.Graphics;
using Android.OS;
using Android.Runtime;
using Android.Support.V7.App;
using System.IO;
using Android.Views;
using Android.Widget;
using Android.Content.Res;

using SkiaSharp;
using SkiaSharp.Views.Android;

namespace AndroidCS
{
    

    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme.NoActionBar", MainLauncher = false)]
	class PixelArtHardActivity : AppCompatActivity
	{

        struct PaletteColor
        {
            public byte r, g, b;
        }
        

        internal class ScaleListener : ScaleGestureDetector.SimpleOnScaleGestureListener
        {
            public Action<float, float, float> Callback { get; set; } = null;
            public override bool OnScale(ScaleGestureDetector detector)
            {
                Callback?.Invoke(detector.ScaleFactor, detector.FocusX, detector.FocusY);
                return true;
            }
        }

        static Mutex mut = new Mutex();

        const float MaxEdge = 3500;
        const int PaletteSize = 8;

        bool isInitialized = false;

        PixelColor[,] pixels;
		SKGLSurfaceView glView;

        // Image positioning
        float scale = 10;
        ScaleListener scaleListener;
        ScaleGestureDetector scaleDetector;
        float minzoom, maxzoom;
        float offsetx, offsety;
        float bufferOffsetx, bufferOffsety;
        float lt, rt, bm, tp, ww, wh;
        int imgw, imgh, imgscale;
        float maxl, minr, maxb, mint;

        // Time
        float start, totalTime = 0f, clickStart = 0f;
        DateTime duration;

        // Touch events
        bool mayBeClick = false;
        int nfingers = 0;
        float lastX, lastY, dx, dy;
        bool zeroUp = false;
        float clickx, clicky;
        bool drawInMoving = false;

        // Palette
        PaletteColor[,] palettes;
        List<(int, PaletteColor, (float x, float y))> palette;
        float paletteOffset, paletteSpacing, paletteOffsetX, lastPaletteOffsetX;
        float paletteWH, paletteX, paletteY, userPaletteY;
        float maxPaletteLeft, maxPaletteRight;
        float paletteSpaceH, palettePositionY;
        float userPaletteSpaceY, paletteR;
        float paletteDx;
        bool isStartedOnPalette;
        (float x, float y)[] paletteCoordinates;
        PaletteColor currentColor;
        int currentColorIndex = 0, prevColorIndex = 0;

        Dictionary<int, List<int>> highlightedShapes;
        //Dictionary<int, List<int>> setPixels;

        // Progress Bar
        float ProgressBarPercent = 0.0f;
        int PaintedShapesCount = 0;


        private void RenderPixelText(string value, SKCanvas target, float x, float y, byte color, bool isHighlighted)
        {

            float left = x * scale + offsetx;
            float right = (x + 1) * scale + offsetx;

            if ((right - left) <= 30)
                return;

            float top = y * scale + offsety;
            float bottom = (y + 1) * scale + offsety;

            if (left > ww || right < 0 || top > wh || bottom < 0)
                return;

            SKRect textRect = new SKRect(left, top, right, bottom);

            SKPaint tPaint = new SKPaint();
            color = (byte)(255 - color);
            tPaint.Color = new SKColor(color, color, color, 255);
            tPaint.TextSize = textRect.Width * 0.5f;
            tPaint.TextAlign = SKTextAlign.Center;
            
            if (isHighlighted)
            {
                tPaint.Typeface = SKTypeface.FromFamilyName("Arial", SKFontStyleWeight.ExtraBold, SKFontStyleWidth.UltraExpanded, SKFontStyleSlant.Oblique);
                tPaint.FakeBoldText = true;
            }

            target.DrawText(value, textRect.MidX, textRect.Bottom - textRect.Height / 3, tPaint);
        }

        private void AddToPalette(PaletteColor color, int idx)
        {
            int i;

            if (palette.Count == 0)
            {
                palette.Add((0, color, (paletteX, paletteY)));
                highlightedShapes.Add(0, new List<int>());
                highlightedShapes[0].Add(idx);
                paletteX += paletteWH + paletteSpacing;
            }
            else
            {
                for (i = 0; i < palette.Count; i++)
                {
                    if (palette[i].Item2.r == color.r && palette[i].Item2.g == color.g && palette[i].Item2.b == color.b)
                    {
                        highlightedShapes[i].Add(idx);
                        return;
                    }
                }
                palette.Add((i, color, (paletteX, paletteY)));
                paletteX += paletteWH + paletteSpacing;
                if (!highlightedShapes.ContainsKey(idx))
                    highlightedShapes.Add(i, new List<int>());
                highlightedShapes[i].Add(idx);
            }
        }

        private void DrawCallback(object sender, SKPaintGLSurfaceEventArgs args)
		{
			var skiaSurface = args.Surface;
			var skiaCanvas = skiaSurface.Canvas;

            if (!isInitialized)
            {
                ww = glView.Width;
                wh = glView.Height;

                lt = 0;
                rt = pixels.GetLength(0);
                bm = 0;
                tp = pixels.GetLength(1);


                palette = new List<(int, PaletteColor, (float, float))>();
                highlightedShapes = new Dictionary<int, List<int>>();

                paletteOffset = 0.03f * ww;
                paletteSpacing = 0.005f * ww;
                paletteWH = 0.1f * ww;
                paletteY = wh - paletteWH - paletteSpacing;

                paletteX = paletteOffset + paletteSpacing + paletteOffsetX;

                var color = new PaletteColor()
                {
                    r = 0,//(byte)pixels[0, 0].r,
                    g = 0,//(byte)pixels[0, 0].g,
                    b = 0//(byte)pixels[0, 0].b
                };

                int index = 0;

                for (int x = 0; x < pixels.GetLength(0); x++)
                {
                    for (int y = 0; y < pixels.GetLength(1); y++)
                    {
                        if (!Check.IsContour(pixels[x, y]))
                        {
                            color.r = (byte)pixels[x, y].r;
                            color.g = (byte)pixels[x, y].g;
                            color.b = (byte)pixels[x, y].b;

                            AddToPalette(color, index++);
                        }
                        
                    }
                }

                currentColor.r = palette[0].Item2.r;
                currentColor.g = palette[0].Item2.g;
                currentColor.b = palette[0].Item2.b;

                scale = 0.9f * ww / (rt - lt);

                offsetx = 0.05f * ww;
                offsety = (wh - (tp - bm) * scale) / 2.0f;

                maxzoom = MaxEdge / (rt - lt);
                minzoom = scale;

                imgw = (int)((rt - lt) * maxzoom);
                imgh = (int)((tp - bm) * maxzoom);
                imgscale = (int)(imgw / (rt - lt));

                bufferOffsetx = 0.05f * ww;
                bufferOffsety = (wh - imgh * scale / maxzoom) / 2.0f;

                maxl = 0.05f * ww;
                minr = 0.95f * ww;
                maxb = 0.05f * wh;
                mint = paletteY - 0.05f * wh;

                maxPaletteLeft = 0;
                maxPaletteRight = (palette.Count - 9) * (paletteWH + paletteSpacing) * -1;

                paletteSpaceH = paletteWH + 2 * paletteSpacing;
                palettePositionY = wh - paletteSpaceH;
                paletteR = 0.05f * paletteWH;



                //userPaletteSpaceY = wh - 2 * paletteSpaceH;
                //paletteR = 0.05f * paletteWH;

                //paletteCoordinates = new (float, float)[PaletteSize * 2];

                //paletteX = paletteOffset + paletteWH + paletteSpacing;

                //for (int i = 0; i < PaletteSize; i++)
                //{
                //    paletteCoordinates[i] = (paletteX, paletteY);
                //    paletteCoordinates[i + PaletteSize] = (paletteX, userPaletteY);
                //    paletteX += paletteWH + paletteSpacing;
                //}

                //Highlight(currentColorIndex, prevColorIndex);

                isInitialized = true;
            }

            float curr = (float)(DateTime.UtcNow - duration).TotalMilliseconds;

            float delta = curr - start;

            totalTime += delta / 1000f;

            if (delta >= 1.0f)
            {
                start = curr;
            }

            SKPaint paint = new SKPaint();

            SKPaint strokPaint = new SKPaint();
            strokPaint.Style = SKPaintStyle.Stroke;
            strokPaint.StrokeWidth = 5.0f;
            strokPaint.StrokeCap = SKStrokeCap.Round;
            strokPaint.Color = SKColors.White;

            SKRect rect;
			skiaCanvas.Clear(new SKColor(200, 200, 200));

            mut.WaitOne();
			for (int x = 0; x < pixels.GetLength(0); x++)
			{
				for (int y = 0; y < pixels.GetLength(1); y++)
				{
                    //if (pixels[x, y].isDrawn)
                    //{
                    //    paint.Color = new SKColor(pixels[x, y].r, pixels[x, y].g, pixels[x, y].b);
                    //}
                    //else
                    //{
                    //    paint.Color = new SKColor(pixels[x, y].l, pixels[x, y].l, pixels[x, y].l);
                    //}

                    paint.Color = new SKColor(pixels[x, y].cr, pixels[x, y].cg, pixels[x, y].cb);

                    float left = offsetx + x * scale;
					float top = offsety + y * scale;
					float right = offsetx + (x + 1) * scale;
					float bottom = offsety + (y + 1) * scale;
					rect = new SKRect(left, top, right, bottom);
					skiaCanvas.DrawRect(rect, paint);
				}
			}
            mut.ReleaseMutex();

            SKColor progressBarGradientA = new SKColor(243, 131, 152);
            SKColor progressBarGradientB = new SKColor(153, 50, 127);

            byte gradientR = (byte)(progressBarGradientA.Red + ProgressBarPercent *
                (progressBarGradientB.Red - progressBarGradientA.Red));
            byte gradientG = (byte)(progressBarGradientA.Green + ProgressBarPercent *
                (progressBarGradientB.Green - progressBarGradientA.Green));
            byte gradientB = (byte)(progressBarGradientA.Blue + ProgressBarPercent *
                (progressBarGradientB.Blue - progressBarGradientA.Blue));

            SKColor curProgressBarGradientPoint = new SKColor(gradientR, gradientG, gradientB);

            rect = new SKRect(paletteOffset, paletteOffset, ww - paletteOffset, paletteWH);
            skiaCanvas.DrawRoundRect(rect, paletteR + 10, paletteR + 10, strokPaint);

            rect.Right = ww * ProgressBarPercent + paletteOffset;

            paint.Shader = SKShader.CreateLinearGradient(
                      new SKPoint(rect.Left, rect.Left),
                      new SKPoint(rect.Right, rect.Right),
                      new SKColor[] { progressBarGradientA, curProgressBarGradientPoint },
                      new float[] { 0, 1 },
                      SKShaderTileMode.Repeat);

            skiaCanvas.DrawRoundRect(rect, paletteR + 10, paletteR + 10, paint);
            paint.Shader = null;


            paint = new SKPaint();
            paint.IsAntialias = true;

            paint.Color = new SKColor(0, 0, 0);

            rect = new SKRect(0, palettePositionY, ww, wh);
            skiaCanvas.DrawRect(rect, paint);

            paletteX = paletteOffset + paletteOffsetX;
            rect.Top = paletteY;
            rect.Bottom = paletteY + paletteWH;

            mut.WaitOne();
            for (int i = 0; i < palette.Count; i++)
            {
                int idx = palette[i].Item1;
                for (int j = 0; j < highlightedShapes[idx].Count; j++)
                {
                    int curShapeIdx = highlightedShapes[idx][j];

                    int pixelX = curShapeIdx / pixels.GetLength(0);
                    int pixelY = curShapeIdx % pixels.GetLength(0);

                    RenderPixelText((palette[i].Item1 + 1).ToString(), skiaCanvas, pixelX, pixelY, pixels[pixelX, pixelY].l,
                        pixels[pixelX, pixelY].isHighlighted);
                       
                }
            }
            mut.ReleaseMutex();

            for (int i = 0; i < palette.Count; i++)
            {
                rect.Left = paletteX;
                rect.Right = paletteX + paletteWH;
                paletteX += paletteWH + paletteSpacing;
                if (rect.Left > ww || rect.Right < 0)
                    continue;

                paint.Color = new SKColor(palette[i].Item2.r, palette[i].Item2.g, palette[i].Item2.b);
                skiaCanvas.DrawRoundRect(rect, paletteR, paletteR, paint);


                paint.Color = new SKColor(255, 255, 255, 255);
                paint.TextSize = paletteWH * 0.5f;
                paint.TextAlign = SKTextAlign.Center;
                skiaCanvas.DrawText((palette[i].Item1 + 1).ToString(), rect.MidX, rect.Bottom - rect.Height / 3, paint);

                if (i == currentColorIndex)
                {
                    skiaCanvas.DrawRoundRect(rect, paletteR, paletteR, strokPaint);
                }
            }

            paint.Color = new SKColor(0, 0, 255);
            paint.IsStroke = false;
        
        }

		protected override void OnCreate(Bundle savedInstanceState)
		{
			Bitmap bmp = null;
			AssetManager assetManager = this.Assets;
			base.OnCreate(savedInstanceState);

            scaleListener = new ScaleListener();
            scaleListener.Callback = (float factor, float focusX, float focusY) => 
            {
                mut.WaitOne();

                

                float newscale = scale * factor;

                if (minzoom > newscale)
                    newscale = minzoom;
                else if (maxzoom < newscale)
                    newscale = maxzoom;

                offsetx = ((offsetx - focusX) * newscale) / scale + focusX;
                offsety = ((offsety - focusY) * newscale) / scale + focusY;

                bufferOffsetx = ((bufferOffsetx - focusX) * newscale) / scale + focusX;
                bufferOffsety = ((bufferOffsety - focusY) * newscale) / scale + focusY;

                scale = newscale;

                mut.ReleaseMutex();
            };
            scaleDetector = new ScaleGestureDetector(ApplicationContext, scaleListener);

            WebHelper wh = new WebHelper(ServerConstants.Url);
            wh.GetColoring((code,data) =>
            {
                if (code == 200)
                {
                    using (Stream stream = new MemoryStream(data))
                    {
                        bmp = BitmapFactory.DecodeStream(stream);
                    }
                }
                else
                {
                    throw new Exception();
                }

            }, ServerConstants.GetColoring, ServerConstants.Cid, true);

			pixels = new PixelColor[bmp.Width, bmp.Height];
			for (int x = 0; x < bmp.Width; x++)
			{
				for (int y = 0; y < bmp.Height; y++)
				{
					int argb = bmp.GetPixel(x, y);
					int r = (argb >> 16) & 0xFF;
					int g = (argb >> 8) & 0xFF;
					int b = argb & 0xFF;
					int min = Math.Min(r, g);
					min = Math.Min(min, b);

					int max= Math.Max(r, g);
					max = Math.Max(max, b);

					int l = min + max / 2;

					pixels[x, y].r = (byte)r;
					pixels[x, y].g = (byte)g;
					pixels[x, y].b = (byte)b;

                    if (!Check.IsContour(pixels[x, y]))
                    {
                        l = (byte)MathF.Min(l * 1.1f, 255.0f);
                    }

                    pixels[x, y].l = (byte)l;

                    pixels[x, y].cr = (byte)l;
                    pixels[x, y].cg = (byte)l;
                    pixels[x, y].cb = (byte)l;
                }
			}

            palettes = new PaletteColor[2, 8];

            using (Stream stream = assetManager.Open("candies.plt"))
            {
                using (BinaryReader br = new BinaryReader(stream))
                {
                    for (int i = 0; i < PaletteSize; i++)
                    {
                        var color = new PaletteColor()
                        {
                            r = br.ReadByte(),
                            g = br.ReadByte(),
                            b = br.ReadByte()
                        };
                        palettes[0, i] = color;
                    }
                }
            }

            using (Stream stream = assetManager.Open("asia.plt"))
            {
                using (BinaryReader br = new BinaryReader(stream))
                {
                    for (int i = 0; i < PaletteSize; i++)
                    {
                        var color = new PaletteColor()
                        {
                            r = br.ReadByte(),
                            g = br.ReadByte(),
                            b = br.ReadByte()
                        };
                        palettes[1, i] = color;
                    }
                }
            }

            //currentColor.r = palettes[0, 0].r;
            //currentColor.g = palettes[0, 0].g;
            //currentColor.b = palettes[0, 0].b;

            duration = new DateTime(2019, 1, 1, 0, 0, 0, DateTimeKind.Utc);
            start = (float)(DateTime.UtcNow - duration).TotalMilliseconds;

            glView = new SKGLSurfaceView(ApplicationContext);
			glView.PaintSurface += DrawCallback;

			SetContentView(glView);
			//		Android.Graphics.Bitmap bitmap = BitmapFactory.DecodeByteArray
		}

        public override void OnWindowFocusChanged(bool hasFocus)
        {
            if (hasFocus)
            {
                var uiOptions =
                SystemUiFlags.HideNavigation |
                SystemUiFlags.LayoutHideNavigation |
                SystemUiFlags.LayoutFullscreen |
                SystemUiFlags.Fullscreen |
                SystemUiFlags.LayoutStable |
                SystemUiFlags.ImmersiveSticky;

                Window.DecorView.SystemUiVisibility = (StatusBarVisibility)uiOptions;
            }
        }

        private bool cheakPixel(float x, float y, PaletteColor color)
        {
            int px = (int)((x - offsetx) / scale);
            int py = (int)((y - offsety) / scale);

            if (px < 0 || py < 0 ||
                px >= pixels.GetLength(0) ||
                py >= pixels.GetLength(1))
                return false;

            if (Check.IsContour(pixels[px, py]))
                return false;

            

            return true;          
        }

        private (int x, int y) lastChangedPixel = (-1, -1);
        
        private bool changePixelColor(float x, float y, PaletteColor color)
        {
            int px = (int)((x - offsetx) / scale);
            int py = (int)((y - offsety) / scale);
            int idx = py + px * pixels.GetLength(0);

            if (drawInMoving && lastChangedPixel.x == px && lastChangedPixel.y == py)
                return false;
            lastChangedPixel = (px, py);

            if (!cheakPixel(x, y, color))
                return false;

            if (pixels[px, py].isDrawn)
                return false;

            if (color.r == pixels[px, py].cr && color.g == pixels[px, py].cg && color.b == pixels[px, py].cb)
            {
                pixels[px, py].cr = pixels[px, py].l;
                pixels[px, py].cg = pixels[px, py].l;
                pixels[px, py].cb = pixels[px, py].l;

                pixels[px, py].isHighlighted = false;
            }
            else
            {
                pixels[px, py].cr = color.r;
                pixels[px, py].cg = color.g;
                pixels[px, py].cb = color.b;

                pixels[px, py].isHighlighted = true;
            }

            if (color.r != pixels[px, py].r || color.g != pixels[px, py].g || color.b != pixels[px, py].b)
            {
                //pixels[px, py].isHighlighted = !pixels[px, py].isHighlighted;
                return false;
            }

            pixels[px, py].isDrawn = true;
            //pixels[px, py].isHighlighted = false;

            int curHighlightIdx = palette[currentColorIndex].Item1;
            int highlightIdx = highlightedShapes[curHighlightIdx].FindIndex(_ => _ == idx);
            if (highlightIdx != -1)
            {
                highlightedShapes[curHighlightIdx].RemoveAt(highlightIdx);
                ProgressBarPercent = (float)(++PaintedShapesCount) / (pixels.GetLength(0) * pixels.GetLength(1));
            }

            //if (highlightedShapes[curHighlightIdx].Count == 0)
            //{
            //    (int, PaletteColor, (float x, float y)) tuple;

            //    for (int i = currentColorIndex + 1; i < palette.Count; i++)
            //    {
            //        tuple = palette[i];
            //        tuple.Item3.x = palette[i].Item3.x - paletteWH - paletteSpacing;
            //        palette[i] = tuple;
            //    }

            //    prevColorIndex = currentColorIndex;
            //    if (currentColorIndex + 1 == palette.Count)
            //    {
            //        // Тут вылетает, когда закрашиваешь всю картинку
            //        currentColor = palette[currentColorIndex - 1].Item2;
            //        Highlight(currentColorIndex - 1, prevColorIndex);
            //    }
            //    else
            //    {
            //        currentColor = palette[currentColorIndex + 1].Item2;
            //        Highlight(currentColorIndex + 1, prevColorIndex);
            //    }

            //    highlightedShapes.Remove(curHighlightIdx);
            //    palette.RemoveAt(currentColorIndex);
            //    if (currentColorIndex == palette.Count)
            //    {
            //        currentColorIndex--;
            //        maxPaletteRight += paletteWH + paletteSpacing;
            //    }
            //}

            

            return true;
        }

        PixelColor selectedAreaColor;

        private bool fillPixelRecursive(int px, int py, PaletteColor color)
        {
            if (px < 0 || py < 0 ||
                px >= pixels.GetLength(0) ||
                py >= pixels.GetLength(1))
                return false;

            if (Check.IsContour(pixels[px, py]))
                return false;

            if (selectedAreaColor.r != pixels[px, py].r ||
                selectedAreaColor.g != pixels[px, py].g ||
                selectedAreaColor.b != pixels[px, py].b)
                return false;

            if (color.r == pixels[px, py].cr && color.g == pixels[px, py].cg && color.b == pixels[px, py].cb)
            {
                return false;
            }

            pixels[px, py].cr = color.r;
            pixels[px, py].cg = color.g;
            pixels[px, py].cb = color.b;

            for (int i = -1; i < 2; i++)
            {
                for (int j = -1; j < 2; j++)
                {
                    if (i != 0 || j != 0)
                    {
                        fillPixelRecursive(px + i, py + j, color);
                    }
                }
            }

            return true;
        }

        private bool fillArea(float x, float y, PaletteColor color)
        {
            int px = (int)((x - offsetx) / scale);
            int py = (int)((y - offsety) / scale);

            if (px < 0 || py < 0 ||
                px >= pixels.GetLength(0) ||
                py >= pixels.GetLength(1))
                return false;

            if (Check.IsContour(pixels[px, py]))
                return false;

            selectedAreaColor.r = pixels[px, py].r;
            selectedAreaColor.g = pixels[px, py].g;
            selectedAreaColor.b = pixels[px, py].b;

            if (color.r == pixels[px, py].cr && color.g == pixels[px, py].cg && color.b == pixels[px, py].cb)
            {
                color.r = pixels[px, py].l;
                color.g = color.r;
                color.b = color.r;
            }

            fillPixelRecursive(px, py, color);

            return true;
        }

        private void HandleMove(MotionEvent e, float y)
        {
            if (nfingers == 1)
            {
                if (y < palettePositionY && isStartedOnPalette == false || y > palettePositionY && isStartedOnPalette == false)
                {

                    dx = e.RawX - lastX;
                    dy = e.RawY - lastY;

                    lastX = e.RawX;
                    lastY = e.RawY;

                    if (zeroUp)
                    {
                        zeroUp = false;
                        return;
                    }

                    if (Math.Abs(dx) > 0.001f * ww || Math.Abs(dy) > 0.001f * wh)
                    {
                        if (drawInMoving && (totalTime - clickStart) > 0.5)
                        {
                            changePixelColor(e.RawX, e.RawY, currentColor);
                            return;
                        }
                        drawInMoving = false;
                    }

                    if (System.Math.Abs(dx) >= 0.01f * ww || System.Math.Abs(dy) > 0.01f * wh)
                    {
                        mut.WaitOne();
                        mayBeClick = false;
                        //Log.Info(Tag, $"nfingers move index {e.ActionIndex} dx={dx} dy={dy}");

                        offsetx += dx;
                        bufferOffsetx += dx;

                        if (lt * scale + offsetx > maxl)
                            offsetx = maxl - lt * scale;
                        if (rt * scale + offsetx < minr)
                            offsetx = minr - rt * scale;

                        if (bufferOffsetx > maxl)
                            bufferOffsetx = maxl;
                        if (bufferOffsetx + imgw * scale / maxzoom < minr)
                            bufferOffsetx = minr - imgw * scale / maxzoom;

                        offsety += dy;
                        bufferOffsety += dy;

                        if (scale * (tp - bm) > 0.9 * wh)
                        {
                            if (bm * scale + offsety > maxb)
                                offsety = maxb - bm * scale;
                            if (tp * scale + offsety < mint)

                                offsety = mint - tp * scale;

                            if (bufferOffsety > maxb)
                                bufferOffsety = maxb;
                            if (bufferOffsety + imgh * scale / maxzoom < mint)

                                bufferOffsety = mint - imgh * scale / maxzoom;
                        }
                        else
                        {
                            if (bm * scale + offsety < maxb)
                                offsety = maxb - bm * scale;

                            if (tp * scale + offsety > mint)
                                offsety = mint - tp * scale;

                            if (bufferOffsety < maxb)
                                bufferOffsety = maxb;

                            if (bufferOffsety + imgh * scale / maxzoom > mint)
                                bufferOffsety = mint - imgh * scale / maxzoom;
                        }
                        mut.ReleaseMutex();
                    }

                }
                else if (isStartedOnPalette == true)
                {
                    paletteDx = e.RawX - lastPaletteOffsetX;

                    lastPaletteOffsetX = e.RawX;

                    if (System.Math.Abs(paletteDx) >= 0.01f * ww)
                    {
                        mayBeClick = false;
                        paletteOffsetX += paletteDx;

                        if (paletteOffsetX > maxPaletteLeft)
                            paletteOffsetX = 0;
                        if (paletteOffsetX < maxPaletteRight)
                            paletteOffsetX = maxPaletteRight;
                    }
                }
            }
        }

        private void HandleClick(float x, float y)
		{
			if (y < palettePositionY)
			{

                //changePixelColor(x, y, currentColor);
                fillArea(x, y, currentColor);


            }
            else
            {
                prevColorIndex = currentColorIndex;

                for (int i = 0; i < palette.Count; i++)
                {
                    if (x >= palette[i].Item3.x + paletteOffsetX && x <= palette[i].Item3.x + paletteWH + paletteOffsetX &&
                        y >= palette[i].Item3.y && y <= palette[i].Item3.y + paletteWH)
                    {
                        currentColor = palette[i].Item2;

                        if (currentColorIndex == i)
                            return;
                        currentColorIndex = i;

                        //Highlight(currentColorIndex, prevColorIndex);
                        break;
                    }
                }
            }
        }

        //private void Highlight(int cur, int prev)
        //{
        //    int curHighlightIdx = palette[cur].Item1;
        //    int prevHighlightIdx = palette[prev].Item1;

        //    (int, PaletteColor, bool) tuple;

        //    PaletteColor highlightColor;
        //    highlightColor.r = 255;
        //    highlightColor.g = 255;
        //    highlightColor.b = 255;

        //    if (highlightedShapes.ContainsKey(prevHighlightIdx))
        //    {
        //        for (int j = 0; j < highlightedShapes[prevHighlightIdx].Count; j++)
        //        {
        //            int idx = highlightedShapes[prevHighlightIdx][j];
        //            int pixelX = idx / pixels.GetLength(0);
        //            int pixelY = idx % pixels.GetLength(0);

        //            if (!pixels[pixelX, pixelY].isDrawn && pixels[pixelX, pixelY].isHighlighted)
        //            {
        //                //mut.WaitOne();
        //                //tuple.Item1 = idx;
        //                //tuple.Item2 = highlightColor;
        //                //tuple.Item3 = false;

        //                //queue.Enqueue(tuple);
        //                //mut.ReleaseMutex();

        //                pixels[pixelX, pixelY].isHighlighted = false;
        //            }
        //        }
        //    }

        //    highlightColor.r = 125;
        //    highlightColor.g = 125;
        //    highlightColor.b = 125;

        //    if (highlightedShapes.ContainsKey(curHighlightIdx))
        //    {
        //        for (int j = 0; j < highlightedShapes[curHighlightIdx].Count; j++)
        //        {
        //            int idx = highlightedShapes[curHighlightIdx][j];
        //            int pixelX = idx / pixels.GetLength(0);
        //            int pixelY = idx % pixels.GetLength(0);

        //            if (!pixels[pixelX, pixelY].isDrawn && !pixels[pixelX, pixelY].isHighlighted)
        //            {
        //                //mut.WaitOne();
        //                //tuple.Item1 = idx;
        //                //tuple.Item2 = highlightColor;
        //                //tuple.Item3 = true;

        //                //queue.Enqueue(tuple);
        //                //mut.ReleaseMutex();

        //                pixels[pixelX, pixelY].isHighlighted = true;
        //            }
        //        }
        //    }
        //}


        public override bool OnTouchEvent(MotionEvent e)
        {
            //(float)(canvasView.CanvasSize.Width * pt.X / canvasView.Width),
            //          (float)(canvasView.CanvasSize.Height * pt.Y / canvasView.Height)
            bool check = scaleDetector.OnTouchEvent(e);

            switch (e.ActionMasked)
            {
                case MotionEventActions.Down:
                    nfingers++;
                    clickStart = totalTime;
                    mayBeClick = true;

                    //Log.Info(Tag, $"nfingers down {nfingers} index {e.ActionIndex}");
                    lastX = e.RawX;
                    lastY = e.RawY;
                    lastPaletteOffsetX = e.RawX;

                    drawInMoving = cheakPixel(e.RawX, e.RawY, currentColor);

                    if (lastY > palettePositionY)
                        isStartedOnPalette = true;
                    else
                        isStartedOnPalette = false;
                    break;
                case MotionEventActions.Up:
                    nfingers--;
                    drawInMoving = false;
                    if (mayBeClick)
                    {
                        //Log.Info(Tag, $"TT {totalTime}/ CS {clickStart}");
                        if (totalTime - clickStart < 0.5f)
                        {
                            clickx = e.GetX();
                            clicky = e.GetY();
                            HandleClick(clickx, clicky);

                            //clicks.Add(new PainterCommon.PainterPointf()
                            //{
                            //    x = clickx,
                            //    y = clicky
                            //});
                        }
                    }
                    //isStartedOnPalette = null;
                    //Log.Info(Tag, $"nfingers up {nfingers} index {e.ActionIndex}");
                    break;
                case MotionEventActions.PointerDown:
                    nfingers++;
                    mayBeClick = false;
                    //Log.Info(Tag, $"nfingers pointer down {nfingers} index {e.ActionIndex}");
                    break;
                case MotionEventActions.PointerUp:
                    nfingers--;
                    drawInMoving = false;
                    //Log.Info(Tag, $"nfingers pointer up {nfingers} index {e.ActionIndex}");
                    if (e.ActionIndex == 0) zeroUp = true;
                    break;
                case MotionEventActions.Move:
                    HandleMove(e, lastY);
                    break;
            }

            dx = 0;
            dy = 0;
            paletteDx = 0;
            return true;
        }
    }
}