﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace AndroidCS
{
    class PixelPaintCommand : ICommand
    {
        private Color beforeColor, afterColor;
        private (int x, int y) pixel;

        private PixelImage pImage;

        public PixelPaintCommand(PixelImage pi, int x, int y, Color color)
        {
            pImage = pi;

            beforeColor = pImage.getColor(x, y);
            afterColor = color;
            pixel.x = x;
            pixel.y = y;
        }

        public void Exec()
        {
            pImage.changePixel(pixel.x, pixel.y, afterColor);
        }

        public void Revert()
        {
            pImage.changePixel(pixel.x, pixel.y, beforeColor);
        }
    }

    //public interface PixelartActivity
    //{
    //    void fillPixel(int x, int y, PaletteColor color);
    //    void fillArea(int x, int y, PaletteColor color);
    //}

    //class PixelPaintCommand : ICommand
    //{
    //    private PaletteColor color, beforeColor;
    //    private PixelartActivity pActivity;
    //    private int x, y;
    //    public PixelPaintCommand(PixelartActivity activity, int x, int y, PaletteColor color, PaletteColor bColor)
    //    {
    //        pActivity = activity;

    //    }

    //    public void Exec()
    //    {
    //        //pActivity.fillPixel()
    //    }

    //    public void Revert()
    //    {

    //    }
    //}
}