﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;

namespace AndroidCS
{
   /* class Cell
    {
        public float x, y, h, d, max;
        public Cell(float cx, float cy, float ch, ref PainterCommon.PainterShape shape)
        {
            x = cx;
            y = cy;
            h = ch;
            d = PointToShapeDist(x, y, ref shape);
            max = d + h * (float)Math.Sqrt(2);
        }

        private static float PointToShapeDist(float x, float y, ref PainterCommon.PainterShape shape)
        {
            bool inside = Check.IsInShape(shape, (int)x, (int)y, 0, 0, 1.0f);
            float minDist = float.PositiveInfinity;

            foreach (PainterCommon.PainterContour contur in shape.contours)
            {
                for (int i = 0, len = contur.points.Count, j = len - 1; i < len; j = i++)
                {
                    minDist = Math.Min(minDist, GetSeqDistSq((x, y), (contur.points[i].x, contur.points[i].y), (contur.points[j].x, contur.points[j].y)));
                }
            }

            return (float)Math.Sqrt(minDist) * (inside ? 1 : -1);
        }

        private static float GetSeqDistSq((float x, float y) point, (float x, float y) a, (float x, float y) b)
        {
            float x = a.x;
            float y = a.y;
            float dx = b.x - x;
            float dy = b.y - y;

            if (dx != 0 || dy != 0)
            {
                float t = ((point.x - x) * dx + (point.y - y) * dy) / (dx * dx + dy * dy);

                if (t > 1)
                {
                    x = b.x;
                    y = b.y;
                }
                else if (t > 0)
                {
                    x += dx * t;
                    y += dy * t;
                }
            }

            dx = point.x - x;
            dy = point.y - y;

            return dx * dx + dy * dy;
        }
        
    }

	static class TextRectanglesHelper
	{
        public static (float, float, float) Calculate(ref PainterCommon.PainterShape shape, float precision)
        {
            float width = shape.rt - shape.lt;
            float height = shape.tp - shape.bm;
            float cellSize = Math.Min(width, height);
            float h = cellSize / 2;

            Queue<Cell> cellQueue = new Queue<Cell>();

            if (cellSize == 0)
                return (shape.lt, shape.bm, 0.0f);

            for (var x = shape.lt; x < shape.rt; x += cellSize)
            {
                for (var y = shape.bm; y < shape.tp; y += cellSize)
                {
                    cellQueue.Enqueue(new Cell(x + h, y + h, h, ref shape));
                }
            }

            Cell bestCell = new Cell(shape.lt + width, shape.bm + height, 0, ref shape);

            int numProbes = cellQueue.Count;

            while (cellQueue.Count != 0)
            {

                Cell cell = cellQueue.Dequeue();

                if (cell.d > bestCell.d)
                {
                    bestCell = cell;
                }

                if (cell.max - bestCell.d <= precision)
                    continue;

                h = cell.h / 2;
                cellQueue.Enqueue(new Cell(cell.x - h, cell.y - h, h, ref shape));
                cellQueue.Enqueue(new Cell(cell.x + h, cell.y - h, h, ref shape));
                cellQueue.Enqueue(new Cell(cell.x - h, cell.y + h, h, ref shape));
                cellQueue.Enqueue(new Cell(cell.x + h, cell.y + h, h, ref shape));
                numProbes += 4;
            }

            return (bestCell.x - bestCell.d / (float)Math.Sqrt(2), bestCell.y - bestCell.d / (float)Math.Sqrt(2), bestCell.d / (float)Math.Sqrt(2) * 2);
        }

        public static (float, float, float) Calculate(ref PainterCommon.PainterShape shape)
		{
			float sw = shape.rt - shape.lt;
			float sh = shape.tp - shape.bm;
			float rw = (float)Math.Min(sw, sh);
			//float stepx = sw / 100.0f;
			//float stepy = sh / 100.0f;
            float stepx = MathF.Min(1, sw / 100.0f);
            float stepy = MathF.Min(1, sh / 100.0f);
            float br = rw * 2;
			float bl = 0;
			float lastRw = -1.0f;
			float lastX = -1.0f, lastY = -1.0f;

			bool first = true;
			bool found = false;
			while ((br - bl) > 0.5f)
			{
				for (float y = shape.bm; y + rw < shape.tp; y += stepy)
				{
					for (float x = shape.lt; x + rw < shape.rt; x += stepx)
					{
						int x0 = (int)x;
						int y0 = (int)y;
						int x1 = (int)(x + rw);
						int y1 = (int)(y + rw);
						bool check1 = Check.IsInShape(shape, x0, y0, 0, 0, 1.0f);
						bool check2 = Check.IsInShape(shape, x0, y1, 0, 0, 1.0f);
						bool check3 = Check.IsInShape(shape, x1, y0, 0, 0, 1.0f);
						bool check4 = Check.IsInShape(shape, x1, y1, 0, 0, 1.0f);
						if (check1 && check2 && check3 && check4)
						{
							int a = 2 + 2;
							found = true;
							lastRw = rw;
							lastX = x;
							lastY = y;
							break;
						}

						if (found) break;
					}
				}

				if (found)
				{
					if (first) break;

					bl = rw;
					rw = (br + bl) / 2;
				}
				else
				{
					br = rw;
					rw = (br + bl) / 2.0f;
				}

				first = false;
			}

			return (lastX, lastY, lastRw);
		}
	}*/
}