﻿using Foundation;
using System;
using UIKit;
using SkiaSharp;
using SkiaSharp.Views.iOS;

namespace App1
{
    public partial class ViewController : UIViewController
    {
        SKGLView glView;

        bool isInitialized = false;
        SKPoint point = new SKPoint(50, 500);

        public ViewController(IntPtr handle) : base(handle)
        {
        }

        private void DrawCallback(object sender, SKPaintGLSurfaceEventArgs args)
        {
            glView.SetNeedsDisplay();
            var skiaSurface = args.Surface;
            var skiaCanvas = skiaSurface.Canvas;

            skiaCanvas.Clear(new SKColor(25, 115, 51));

            if (!isInitialized)
            {
                //ColoringState.Ww = glView.CanvasSize.Width;
                //ColoringState.Wh = glView.CanvasSize.Height;

                isInitialized = true;
            }

            SKPaint paint = new SKPaint();
            paint.Color = new SKColor(255, 255, 255);

            skiaCanvas.DrawCircle(point, 100, paint);
            skiaCanvas.Flush();
            glView.SetNeedsDisplay();
        }

        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            glView = new SKGLView(UIScreen.MainScreen.Bounds);
            glView.BackgroundColor = new UIColor(0.1f, 1f, 0.1f, 1f);

            View.AddSubview(glView);

            glView.PaintSurface += DrawCallback;
        }

        public override void DidReceiveMemoryWarning ()
        {
            base.DidReceiveMemoryWarning ();
            // Release any cached data, images, etc that aren't in use.
        }
    }
}