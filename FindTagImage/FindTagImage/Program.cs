﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Linq;


namespace FindTagImage
{
    class Program
    {
        static void Main(string[] args)
        {
            string dir = args[0];
            string currentDir = Directory.GetCurrentDirectory();

            XmlDocument doc = new XmlDocument();
            List<string> filesWithImage = new List<string>();

            IEnumerable<string> files = Directory.EnumerateFiles(dir);

            foreach (var item in files)
            {
                try
                {
                    doc.Load(item);
                }
                catch (Exception)
                {
                    continue;
                }

                XmlNodeList imagesNodes = doc.GetElementsByTagName("image");
                if (imagesNodes.Count > 0)
                {
                    string name = item.Split('\\').Last();
                    filesWithImage.Add(name);
                }
            }

            using (StreamWriter sw = new StreamWriter($"{currentDir}\\output.txt", false, System.Text.Encoding.Default))
            {
                foreach (var item in filesWithImage)
                {
                    sw.WriteLine(item);
                }
            }

            Console.WriteLine("Done");
        }
    }
}
