#ifndef PAINTER_READ_H
#define PAINTER_READ_H

#include "painter_common.h"
#include "painter_commands.h"

std::tuple<std::vector<painter_shape> *, std::vector<cmdshp> *> 
painter_read(unsigned char *data, int size);

#endif
