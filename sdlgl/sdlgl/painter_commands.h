#ifndef PAINTER_COMMANDS_H
#define PAINTER_COMMANDS_H

#include <iostream>
#include <vector>
#include <include/core/SkPath.h>


class pntrcmd {
public:
	virtual void exec(SkPath &path, float offsetx, float offsety, float scale) {
		std::cout << "abstract" << std::endl;
	}

	void exec(SkPath &path) {
		exec(path, 0.0f, 0.0f, 1.0f);
	}
	virtual ~pntrcmd() {}
};



class lcmd : public pntrcmd {
private:
	float x, y;

public:
	lcmd(float x, float y);
	virtual void exec(SkPath &path, float offsetx, float offsety, float scale) override;
};

class mcmd : public pntrcmd {
private:
	float x, y;
public:
	mcmd(float x, float y);
	virtual void exec(SkPath &path, float offsetx, float offsety, float scale) override;
	lcmd *getlcmd();
};

class ccmd : public pntrcmd {
private:
	float c1x, c1y, c2x, c2y, x, y;
public:
	ccmd(float c1x, float c1y, float c2x, float c2y, float x, float y);
	virtual void exec(SkPath &path, float offsetx, float offsety, float scale) override;
};

class qcmd : public pntrcmd {
private:
	float cx, cy, x, y;
public:
	qcmd(float cx, float cy, float x, float y);
	virtual void exec(SkPath &path, float offsetx, float offsety, float scale) override;
};

struct cmdcntr {
	bool outer;
	std::vector<pntrcmd *> *cmds;
};

struct cmdshp {
	int r, g, b;
	std::vector<cmdcntr> *cntrs;
};

void arc_to_quads(std::vector<pntrcmd *> *cmds, float rx, float ry, float x1,
	float y1, float x2, float y2, float xaxis_rotation,
	int large_arc_flag, int sweep_flag);

#endif