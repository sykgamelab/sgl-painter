
#include <SDL.h>
#include <GL/glew.h>
#include <GL/glu.h>
#include <fstream>
#include <iostream>
#include <math.h>

#include <SDL_mouse.h>
#include <stdio.h>
#include <string.h>
#include <vector>

#include <stdio.h>
#include <stdlib.h>

#include "painter_read.h"

#include <include/gpu/GrBackendSurface.h>
#include <include/gpu/GrContext.h>
#include <include/gpu/gl/GrGLInterface.h>
#include <src/gpu/gl/GrGLDefines.h>
#include <src/gpu/gl/GrGLUtil.h>

#include <include/core/SkData.h>
#include <include/core/SkImage.h>
#include <include/core/SkStream.h>
#include <include/core/SkSurface.h>
#include <include/core/SkCanvas.h>
#include <include/core/SkSurface.h>
#include <include/core/SkRefCnt.h>
#include <include/core/SkShader.h>
#include <include/core/SkFont.h>

#include <include/effects/SkGradientShader.h>

#include <include/utils/SkRandom.h>

#include <Windows.h>



#define DIMENSIONS_PATH "configs/dimensions"
#define STR_SIZE 256
#define DIMENSIONS_NUMBER 6
#define RESOURCES_PATH "resources/"

struct precalc_path {
	SkPath path;
	SkPaint paint;
	bool user_shader;
	SkPoint center;
	float radius;
	SkColor color;
};


SDL_Window *window;
SDL_Renderer *renderer;
SDL_GLContext glctx;
SkCanvas *canvas;
//SDL_Texture *renderer_texture, *bg_texture;

GLuint gprogid = 0;
GLint gvtxpos_2dloc = -1;
GLuint gvbo = 0;
GLuint gibo = 0;
bool grender_quad = true;
int ww = 1920, wh = 1080;
float pxratio = (float)ww / (float)wh;
float offsetx = 0.0f, offsety = 0.0f, scale = 1.0f;

std::vector<painter_shape> *shapes;
std::vector<cmdshp> *shps;
std::vector<precalc_path> paths;

void
calc_paths() 
{
	int i, j, k;

	for (i = 0; i < shps->size(); i++) {
		precalc_path ppath;
		SkPath path;
		float r, g, b;
		float cx, cy, radius;
		std::vector<cmdcntr> *cntrs;

		r = shps->at(i).r;
		g = shps->at(i).g;
		b = shps->at(i).b;

		cntrs = shps->at(i).cntrs;

		SkPaint paint;
		paint.setAntiAlias(true);
		//paint.setStrokeWidth(2.0f);
		//if ((shapes->at(i).r > 50 || shapes->at(i).g > 50 ||
			//shapes->at(i).b > 50))
			//paint.setColor(SkColorSetARGB(a * 0xFF, shapes->at(i).r, shapes->at(i).g, shapes->at(i).b));
		//else
			//paint.setColor(SkColorSetARGB(a * 0xFF, shapes->at(i).r, shapes->at(i).g, shapes->at(i).b));





		for (j = 0; j < cntrs->size(); j++) {
			std::vector<pntrcmd *> *cmds;

			cmds = cntrs->at(j).cmds;

			for (k = 0; k < cmds->size(); k++)
				cmds->at(k)->exec(path, offsetx, offsety, scale);
		}

		cx = shapes->at(i).cx * scale + offsetx;
		cy = shapes->at(i).cy * scale + offsety;
		radius = shapes->at(i).radius * scale;

		ppath.user_shader = false;
		paint.setColor(SkColorSetARGB(0xFF, shapes->at(i).r, shapes->at(i).g, shapes->at(i).b));
		if ((shapes->at(i).r > 30 || shapes->at(i).g > 30 ||
			shapes->at(i).b > 30)) {

			ppath.user_shader = true;
			SkColor target = SkColorSetRGB(shapes->at(i).r, shapes->at(i).g, shapes->at(i).b);
			SkColor colors[2] = { target, SkColorSetARGB(0, 255, 255, 255) };

			SkPoint center = SkPoint::Make(cx, cy);
			//auto shader = SkGradientShader::MakeRadial(center, a * radius * 4, colors, NULL, 2, SkTileMode::kClamp);
			//shader
			//paint.setColor(SkColorSetARGB(0xFF, shapes->at(i).r, shapes->at(i).g, shapes->at(i).b));
			//	paint.setShader(shader);
		}

		ppath.path = path;
		ppath.paint = paint;
		paths.push_back(ppath);
		//canvas->drawPath(path, paint);
	}
}

void
precalc_render() {
	for (int i = 0; i < paths.size(); i++) {
		canvas->drawPath(paths[i].path, paths[i].paint);
	}
}

void
render()
{
	int i, j, k;

	static float a = 0.0f;
	static int c = 0;
	for (i = 0; i < shps->size(); i++) {
		SkPath path;
		float r, g, b;
		float cx, cy, radius;
		std::vector<cmdcntr> *cntrs;
	
		r = shps->at(i).r;
		g = shps->at(i).g;
		b = shps->at(i).b;

		cntrs = shps->at(i).cntrs;

		SkPaint paint;
		paint.setAntiAlias(true);
		//paint.setStrokeWidth(2.0f);
	//	if ((shapes->at(i).r > 30 || shapes->at(i).g > 30 ||
		//	shapes->at(i).b > 30))
			//paint.setColor(SkColorSetARGB(a * 0xFF, shapes->at(i).r, shapes->at(i).g, shapes->at(i).b));
		//else
			//paint.setColor(SkColorSetARGB(0xFF, shapes->at(i).r, shapes->at(i).g, shapes->at(i).b));
		
		



		for (j = 0; j < cntrs->size(); j++) {
			std::vector<pntrcmd *> *cmds;

			cmds = cntrs->at(j).cmds;

			for (k = 0; k < cmds->size(); k++)
				cmds->at(k)->exec(path, offsetx, offsety, scale);
		}

		cx = shapes->at(i).cx * scale + offsetx;
		cy = shapes->at(i).cy * scale + offsety;
		radius = shapes->at(i).radius * scale;

		paint.setColor(SkColorSetARGB(0xFF, shapes->at(i).r, shapes->at(i).g, shapes->at(i).b));
	if ((shapes->at(i).r > 30 || shapes->at(i).g > 30 ||
		shapes->at(i).b > 30)) {

			SkColor target = SkColorSetRGB(shapes->at(i).r, shapes->at(i).g, shapes->at(i).b);
			SkColor colors[2] = { target, SkColorSetARGB(0, 255, 255, 255) };

			SkPoint center = SkPoint::Make(cx, cy);
			auto shader = SkGradientShader::MakeRadial(center, a * radius * 4, colors, NULL, 2, SkTileMode::kClamp);
			//shader
			
			paint.setShader(shader);
		}
		
		canvas->drawPath(path, paint);
	}
	


	a += 0.01f;
	if (a > 1) {
		a = 0.0f;
		c = (c + 1) % shps->size();
	}
	
}


void handlekeys(unsigned char key, int x, int y)
{
	//Toggle quad
	if (key == 'q')
	{
		grender_quad = !grender_quad;
	}
}

bool
init_gl()
{
	bool success = true;
	GLuint vtxshdr, fragshdr;
	GLint vtxshdr_compiled;
	//Get vertex source
	const GLchar* vtxshdr_src[] =
	{
		"#version 140\nin vec2 LVertexPos2D; void main() { gl_Position = vec4( LVertexPos2D.x, LVertexPos2D.y, 0, 1 ); }"
	};
	//Get fragment source
	const GLchar* fragshdr_src[] =
	{
		"#version 140\nout vec4 LFragment; void main() { LFragment = vec4( 1.0, 1.0, 1.0, 1.0 ); }"
	};

	//Generate program
	gprogid = glCreateProgram();

	vtxshdr = glCreateShader(GL_VERTEX_SHADER);



	//Set vertex source
	glShaderSource(vtxshdr, 1, vtxshdr_src, NULL);

	//Compile vertex source
	glCompileShader(vtxshdr);

	//Check vertex shader for errors
	vtxshdr_compiled = GL_FALSE;
	glGetShaderiv(vtxshdr, GL_COMPILE_STATUS, &vtxshdr_compiled);
	if (vtxshdr_compiled != GL_TRUE) {
		std::cerr << "Unable to compile vertex shader " <<
			vtxshdr << std::endl;
		// printShaderLog( vertexShader );
		success = false;
	}
	else {
		GLint fragshdr_compiled;

		//Attach vertex shader to program
		glAttachShader(gprogid, vtxshdr);

		//Create fragment shader
		fragshdr = glCreateShader(GL_FRAGMENT_SHADER);



		//Set fragment source
		glShaderSource(fragshdr, 1, fragshdr_src, NULL);

		//Compile fragment source
		glCompileShader(fragshdr);

		//Check fragment shader for errors
		fragshdr_compiled = GL_FALSE;
		glGetShaderiv(fragshdr, GL_COMPILE_STATUS, &fragshdr_compiled);
		if (fragshdr_compiled != GL_TRUE) {
			std::cerr << "Unable to compile fragment shader" << fragshdr << std::endl;
			success = false;
		}
		else {
			GLint progsuc;
			//Attach fragment shader to program
			glAttachShader(gprogid, fragshdr);


			//Link program
			glLinkProgram(gprogid);

			//Check for errors
			progsuc = GL_TRUE;
			glGetProgramiv(gprogid, GL_LINK_STATUS, &progsuc);
			if (progsuc != GL_TRUE) {
				// printf( "Error linking program %d!\n", gProgramID );
				// printProgramLog( gProgramID );
				std::cerr << "err getting program iv" << std::endl;
				success = false;
			}
			else {
				//Get vertex attribute location
				gvtxpos_2dloc = glGetAttribLocation(gprogid, "LVertexPos2D");
				if (gvtxpos_2dloc == -1) {
					std::cerr <<
						"LVertexPos2D is not a valid glsl program variable!" <<
						std::endl;
					success = false;
				}
				else {
					//Initialize clear color
					glClearColor(0.8f, 0.8f, 0.8f, 1.f);

					//VBO data
					GLfloat vertexData[] =
					{
						-0.5f, -0.5f,
						 0.5f, -0.5f,
						 0.5f,  0.5f,
						-0.5f,  0.5f
					};

					//IBO data
					GLuint indexData[] = { 0, 1, 2, 3 };

					//Create VBO
					glGenBuffers(1, &gvbo);
					glBindBuffer(GL_ARRAY_BUFFER, gvbo);
					glBufferData(GL_ARRAY_BUFFER, 2 * 4 * sizeof(GLfloat),
						vertexData, GL_STATIC_DRAW);

					//Create IBO
					glGenBuffers(1, &gibo);
					glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, gibo);
					glBufferData(GL_ELEMENT_ARRAY_BUFFER, 4 * sizeof(GLuint),
						indexData, GL_STATIC_DRAW);
				}
			}
		}
	}

	return success;
}

static SkPath create_star() {
	static const int kNumPoints = 5;
	SkPath concavePath;
	SkPoint points[kNumPoints] = { {0, SkIntToScalar(-50)} };
	SkMatrix rot;
	rot.setRotate(SkIntToScalar(360) / kNumPoints);
	for (int i = 1; i < kNumPoints; ++i) {
		rot.mapPoints(points + i, points + i - 1, 1);
	}
	concavePath.moveTo(points[0]);
	for (int i = 0; i < kNumPoints; ++i) {
		concavePath.lineTo(points[(2 * i) % kNumPoints]);
	}
	concavePath.setFillType(SkPath::kEvenOdd_FillType);
	SkASSERT(!concavePath.isConvex());
	concavePath.close();
	return concavePath;
}

void
render_to_image(SkCanvas *target, float imgw, float imgh)
{
	int i, j, k;

	for (i = 0; i < shps->size(); i++) {
		SkPath path;
		float r, g, b;
		float cx, cy, radius;
		std::vector<cmdcntr> *cntrs;

		// if ((shapes->at(i).r > 30 || shapes->at(i).g > 30 ||
		//    shapes->at(i).b > 30)) continue;

		r = shps->at(i).r;
		g = shps->at(i).g;
		b = shps->at(i).b;

		cntrs = shps->at(i).cntrs;

		SkPaint paint;
		paint.setAntiAlias(true);

		//paint.setStrokeWidth(2.0f);
		//	if ((shapes->at(i).r > 30 || shapes->at(i).g > 30 ||
		//	shapes->at(i).b > 30))
		//paint.setColor(SkColorSetARGB(a * 0xFF, shapes->at(i).r, shapes->at(i).g, shapes->at(i).b));
		//else
		//paint.setColor(SkColorSetARGB(0xFF, shapes->at(i).r, shapes->at(i).g, shapes->at(i).b));





		for (j = 0; j < cntrs->size(); j++) {
			std::vector<pntrcmd *> *cmds;

			cmds = cntrs->at(j).cmds;

			for (k = 0; k < cmds->size(); k++)
				cmds->at(k)->exec(path, 0, 0, 1);
		}
		
		paint.setColor(SkColorSetARGB(0xFF, shapes->at(i).r, shapes->at(i).g, shapes->at(i).b));
		//path.setFillType(SkPath::kEvenOdd_FillType);
		target->drawPath(path, paint);
	}

	target->flush();
}



int WINAPI wWinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, PWSTR pCmdLine, int nCmdShow)
{
	int i, j, k;
	std::string shppath = "resources/ucat.bshp";

	std::ifstream ifs(shppath, std::ios::binary | std::ios::ate);
	std::ifstream::pos_type pos = ifs.tellg();

	//std::vector<unsigned char>  data(pos);
	int size = pos;
	unsigned char *data = new unsigned char[size];

	ifs.seekg(0, std::ios::beg);
	ifs.read((char *)&data[0], pos);

	ifs.close();

	auto tuple = painter_read(data,size);
	shapes = std::get<0>(tuple);
	shps = std::get<1>(tuple);

	for (int i = 0; i < shapes->size(); i++) {
		std::vector<painter_contour> *contours;
		std::vector<cmdcntr> *cmdcntrs;

		contours = shapes->at(i).contours;
		cmdcntrs = shps->at(i).cntrs;

		for (int j = 0; j < contours->size(); j++)
			cmdcntrs->at(j).outer = contours->at(j).outer;
	}

	if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
		fprintf(stderr, "error initializing SDL\n");
		exit(1);
	}

	if (SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 3) < 0) {
		std::cerr << "err setting major version of gl " << SDL_GetError() << std::endl;
		exit(1);
	}
	if (SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0) < 0) {
		std::cerr << "err setting minor version of gl " << SDL_GetError() << std::endl;
		exit(1);
	}

	if (SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
		SDL_GL_CONTEXT_PROFILE_CORE) < 0) {
		std::cerr << "err setting profile of gl " << SDL_GetError() << std::endl;
		exit(1);
	}

	if (SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 8) < 0) {
		std::cerr << "err setting stencil size to 8 " << SDL_GetError() << std::endl;
		exit(1);
	}

	window = SDL_CreateWindow("", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
		ww, wh, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);
	renderer = SDL_CreateRenderer(window, -1,
		SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);



	// Create an OpenGL context associated with the window.
	glctx = SDL_GL_CreateContext(window);
	//glcreateContext
	if (glctx == NULL) {
		std::cerr << "err creating gl context " << SDL_GetError() << " " << glGetError() << std::endl;
		exit(1);
	}
	else {
		 	GLenum glewError;

			glewError = glewInit();
			if( glewError != GLEW_OK ) {
				std::cerr << "Error initializing GLEW! " <<
					glewGetErrorString(glewError) << std::endl;
				exit(1);
			}
	 
		if (SDL_GL_SetSwapInterval(1) < 0) {
			std::cerr << "err setting swap interval " << SDL_GetError() << std::endl;
			// exit(1); 
		}

		if (!init_gl()) {
			std::cerr << "err initing opengl" << std::endl;
			exit(1);
		}
	}


	//Main loop flagbri
	bool quit = false;

	//Event handler
	SDL_Event e;

	//Enable text input
	SDL_StartTextInput();

	//auto interface = GrGLMakeNativeInterface();
	auto skia_interface= GrGLMakeNativeInterface();
	//const GrGLInterface* interface = nullptr;
	sk_sp<GrContext> context(GrContext::MakeGL(skia_interface));
	if (context == NULL) {
		std::cerr << "Err creating skia context" << std::endl;
		return 1;
	}

	GrGLint buffer;
	GR_GL_GetIntegerv(skia_interface.get(), GR_GL_FRAMEBUFFER_BINDING, &buffer);
	GrGLFramebufferInfo info;
	info.fFBOID = (GrGLuint)buffer;
	SkColorType colorType;

	//SkDebugf("%s", SDL_GetPixelFormatName(windowFormat));
	// TODO: the windowFormat is never any of these?
	uint32_t windowFormat = SDL_GetWindowPixelFormat(window);
	int contextType;
	SDL_GL_GetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, &contextType);
	if (SDL_PIXELFORMAT_RGBA8888 == windowFormat) {
		info.fFormat = GR_GL_RGBA8;
		colorType = kRGBA_8888_SkColorType;
	}
	else {
		colorType = kBGRA_8888_SkColorType;
		if (SDL_GL_CONTEXT_PROFILE_ES == contextType) {
			info.fFormat = GR_GL_BGRA8;
		}
		else {
			// We assume the internal format is RGBA8 on desktop GL
			info.fFormat = GR_GL_RGBA8;
		}
	}

	// If you want multisampling, uncomment the below lines and set a sample count
	static const int kMsaaSampleCount = 0; //4;
	// SDL_GL_SetAttribute(SDL_GL_MULTISAMPLEBUFFERS, 1);
	// SDL_GL_SetAttribute(SDL_GL_MULTISAMPLESAMPLES, kMsaaSampleCount);

	GrBackendRenderTarget target(ww, wh, kMsaaSampleCount, 8, info);

	//setup SkSurface
		// To use distance field text, use commented out SkSurfaceProps instead
		// SkSurfaceProps props(SkSurfaceProps::kUseDeviceIndependentFonts_Flag,
		//                      SkSurfaceProps::kLegacyFontHost_InitType);
	SkSurfaceProps props(SkSurfaceProps::kLegacyFontHost_InitType);

	sk_sp<SkSurface> surface(SkSurface::MakeFromBackendRenderTarget(context.get(), target,
		kBottomLeft_GrSurfaceOrigin,
		colorType, nullptr, &props));



	if (!surface) {
		SkDebugf("SkSurface::MakeRenderTarget returned null\n");
		return 1;
	}

	canvas = surface->getCanvas();
	//	canvas->scale((float)ww / dm.w, (float)wh / dm.h);

	/*SkImageInfo imageInfo = SkImageInfo::MakeN32Premul(ww, wh);
	sk_sp<SkSurface> contour_surface(SkSurface::MakeRaster(imageInfo));
	SkCanvas* contour_canvas = contour_surface->getCanvas();*/

	SkImageInfo imginfo = SkImageInfo::MakeN32(256, 64, kOpaque_SkAlphaType);
	sk_sp<SkSurface> gpuSurface = SkSurface::MakeRenderTarget(context.get(), SkBudgeted::kNo, imginfo, 0,
		kTopLeft_GrSurfaceOrigin, nullptr);
	auto surfaceCanvas = gpuSurface->getCanvas();
	surfaceCanvas->clear(SK_ColorWHITE);
	SkPaint paint;
	SkFont font;
	/*font.setSize(32);
	paint.setColor(SK_ColorBLACK);
	surfaceCanvas->drawString("GPU Rocks", 20, 40, font, paint);
//	surfaceCanvas->drawString("GPU rocks!", 20, 40, paint);
	surfaceCanvas->flush();
	sk_sp<SkImage> image(gpuSurface->makeImageSnapshot());
	canvas->drawImage(image, 0, 0);*/

	SkImageInfo imageInfo = SkImageInfo::MakeN32(ww, wh, kOpaque_SkAlphaType);
	GrBackendTexture backTexture = context->createBackendTexture(ww, wh, colorType, GrMipMapped::kNo, GrRenderable::kYes);
	bool vvalid = backTexture.isValid();
	std::cout << vvalid << std::endl;

	sk_sp<SkColorSpace> color_space = SkColorSpace::MakeSRGB();
	//sk_sp<SkSurface> contour_surface = SkSurface::MakeFromBackendTextureAsRenderTarget(context.get(), backTexture, kTopLeft_GrSurfaceOrigin, 0, colorType,
		//color_space, nullptr);
	sk_sp<SkSurface> contour_surface = SkSurface::MakeRenderTarget(context.get(), SkBudgeted::kNo, imageInfo, 0,
		kTopLeft_GrSurfaceOrigin, nullptr, true);
	SkCanvas* contour_canvas = contour_surface->getCanvas();
	contour_canvas->clear(SK_ColorWHITE);
	/*SkPaint paint;
	contour_canvas->save();
	contour_canvas->translate(50.0f, 50.0f);
	contour_canvas->drawPath(create_star(), paint);
	contour_canvas->restore();*/

	//SkCanvas* contour_canvas = contour_surface->getCanvas();
	render_to_image(contour_canvas, ww, wh);
	contour_canvas->flush();

	//	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
	sk_sp<SkImage> contour_img = contour_surface->makeImageSnapshot();

	calc_paths();

	//While application is running
	glClearColor(0.8f, 0.8f, 0.8f, 1.f);
	while (!quit)
	{
		//Handle events on queue
		while (SDL_PollEvent(&e) != 0)
		{
			if (e.type == SDL_QUIT) {
				quit = true;
			}
			//Handle keypress with current mouse position
			else if (e.type == SDL_TEXTINPUT)
			{
				int x = 0, y = 0;
				SDL_GetMouseState(&x, &y);
				handlekeys(e.text.text[0], x, y);
			}
		}

		//Render quad
		//glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
		//render();
		//precalc_render();



		canvas->clear(SK_ColorBLUE);

		canvas->drawImage(contour_img, 0, 0);

		canvas->flush();
		//Update screen
		SDL_GL_SwapWindow(window);
	}

	//Disable text input
	SDL_StopTextInput();

	SDL_Quit();
	return 0;
}