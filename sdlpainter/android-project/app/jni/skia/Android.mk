LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := skia

LOCAL_C_INCLUDES := $(LOCAL_PATH)
#LOCAL_SRC_FILES := libfoo.so

#ifeq ($(TARGET_ARCH),armeabi-v7a)
#	LOCAL_SRC_FILES := arm/libskia.a
#else
#    LOCAL_SRC_FILES := arm64/libskia.a
#endif

#LOCAL_SRC_FILES := arm64/libskia.a
LOCAL_SRC_FILES := arm/libskia.a

# Add your application source files here...


include $(PREBUILT_STATIC_LIBRARY)
