LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := nanovg

LOCAL_C_INCLUDES := $(LOCAL_PATH)/include

# Add your application source files here...
LOCAL_CPPFLAGS += -std=c++17

LOCAL_SRC_FILES := nanovg.c

LOCAL_LDLIBS := -lGLESv2 -lGLESv3

include $(BUILD_SHARED_LIBRARY)
