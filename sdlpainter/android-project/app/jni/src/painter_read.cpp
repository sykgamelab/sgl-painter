#include "painter_read.h"

#define CALCULATION_STEP 0.1f

enum Commands
{
    BEGIN_SHAPE,
    COLOR,
    LINE,
    LINEZ,
    BEZIER2,
    BEZIER3,
    ARC,
    END_SHAPE
};

static float
bytes_to_float(bool minus, uint16_t b1, uint16_t b2)
{
    uint16_t val = (b1 << 8) | b2;
    return (float)(((minus) ? -1 : 1) * val) / 10.0f;
}


std::tuple<std::vector<painter_shape> *, std::vector<cmdshp> *>
painter_read(unsigned char *data, int size)
{
    painter_shape shape;
    painter_contour contour;
    painter_point pt;
    std::vector<painter_shape> *shapes;
    std::vector<painter_contour> *contours;
    std::vector<painter_point> *pts;
    cmdshp shp;
    cmdcntr cntr;
    std::vector<cmdshp> *shps;
    std::vector<cmdcntr> *cntrs;
    std::vector<pntrcmd *> *cmds;

    shapes = new std::vector<painter_shape>();
    shps = new std::vector<cmdshp>();


    for (int pos = 0; pos < size;) {
        if (data[pos] == BEGIN_SHAPE) {
            contours = new std::vector<painter_contour>();
            pts = new std::vector<painter_point>();
            shape = {};
            contour = {};

            cntrs = new std::vector<cmdcntr>();
            cmds = new std::vector<pntrcmd *>();
            shp = {};
            cntr = {};
            pos++;
        }
        else if (data[pos] == COLOR) {
            shape.r = data[pos + 1];
            shape.g = data[pos + 2];
            shape.b = data[pos + 3];

            shp.r = data[pos + 1];
            shp.g = data[pos + 2];
            shp.b = data[pos + 3];

            pos += 4;
        }
        else if (data[pos] == LINE) {
            float x1, y1, x2, y2;
            uint8_t neg;

            neg = 0;

            x1 = bytes_to_float(data[pos + 1] > 0, data[pos + 2], data[pos + 3]);
            y1 = bytes_to_float(data[pos + 4] > 0, data[pos + 5], data[pos + 6]);
            x2 = bytes_to_float(data[pos + 7] > 0, data[pos + 8], data[pos + 9]);
            y2 = bytes_to_float(data[pos + 10] > 0, data[pos + 11], data[pos + 12]);

            if (pts->size() == 0) {
                pt.x = x1;
                pt.y = y1;
                pts->push_back(pt);
            }
            pt.x = x2;
            pt.y = y2;
            pts->push_back(pt);

            if (cmds->size() == 0)
                cmds->push_back(new mcmd(x1, y1));

            cmds->push_back(new lcmd(x2, y2));

            pos += 13;
        }
        else if (data[pos] == LINEZ) {
            contour.pts = pts;
            contours->push_back(contour);
            // std::cout <<"linez " << contours->size() << std::endl;
            pts = new std::vector<painter_point>();
            contour = {};

            lcmd *zcmd;
            zcmd = ((mcmd *)cmds->at(0))->getlcmd();
            cmds->push_back(zcmd);
            cntr.cmds = cmds;
            cntrs->push_back(cntr);
            cmds = new std::vector<pntrcmd *>();
            cntr = {};

            pos++;
        }
        else if (data[pos] == BEZIER3) {
            // std::cout << "b3" << std::endl;
            painter_pointf p0, p1, p2, p3;
//			stream >> p0.x >> p0.y >> p1.x >> p1.y >> p2.x >> p2.y >> p3.x >> p3.y;

            p0.x = bytes_to_float(data[pos + 1] > 0, data[pos + 2], data[pos + 3]);
            p0.y = bytes_to_float(data[pos + 4] > 0, data[pos + 5], data[pos + 6]);

            p1.x = bytes_to_float(data[pos + 7] > 0, data[pos + 8], data[pos + 9]);
            p1.y = bytes_to_float(data[pos + 10] > 0, data[pos + 11], data[pos + 12]);

            p2.x = bytes_to_float(data[pos + 13] > 0, data[pos + 14], data[pos + 15]);
            p2.y = bytes_to_float(data[pos + 16] > 0, data[pos + 17], data[pos + 18]);

            p3.x = bytes_to_float(data[pos + 19] > 0, data[pos + 20], data[pos + 21]);
            p3.y = bytes_to_float(data[pos + 22] > 0, data[pos + 23], data[pos + 24]);


            if (pts->size() == 0) {
                pt.x = p0.x;
                pt.y = p0.y;
                pts->push_back(pt);
            }

            if (cmds->size() == 0)
                cmds->push_back(new mcmd(p0.x, p0.y));

            cmds->push_back(new ccmd(p1.x, p1.y, p2.x, p2.y, p3.x, p3.y));

            // std::cout << "before b3 size " << pts->size() << std::endl;
            bezier3_to_pts(pts, p0, p1, p2, p3, CALCULATION_STEP);

            pos += 25;
        }
        else if (data[pos] == BEZIER2) {
            painter_pointf p0, p1, p2;
            // std::cout << "b2" << std::endl;
            //stream >> p0.x >> p0.y >> p1.x >> p1.y >> p2.x >> p2.y;

            p0.x = bytes_to_float(data[pos + 1] > 0, data[pos + 2], data[pos + 3]);
            p0.y = bytes_to_float(data[pos + 4] > 0, data[pos + 5], data[pos + 6]);

            p1.x = bytes_to_float(data[pos + 7] > 0, data[pos + 8], data[pos + 9]);
            p1.y = bytes_to_float(data[pos + 10] > 0, data[pos + 11], data[pos + 12]);

            p2.x = bytes_to_float(data[pos + 13] > 0, data[pos + 14], data[pos + 15]);
            p2.y = bytes_to_float(data[pos + 16] > 0, data[pos + 17], data[pos + 18]);

            if (pts->size() == 0) {
                pt.x = p0.x;
                pt.y = p0.y;
                pts->push_back(pt);
            }

            // std::cout << "before b2 size " << pts->size() << std::endl;
            bezier2_to_pts(pts, p0, p1, p2, CALCULATION_STEP);

            if (cmds->size() == 0)
                cmds->push_back(new mcmd(p0.x, p0.y));

            cmds->push_back(new qcmd(p1.x, p1.y, p2.x, p2.y));

            pos += 19;
            // std::cout << "after b2 size " << pts->size() << std::endl;
        }
        else if (data[pos] == ARC) {
            float rx, ry, x1, y1, x2, y2, xaxis_rotation;
            int large_arc_flag, sweep_flag;
            //stream >> rx >> ry >> x1 >> y1 >> x2 >> y2 >> xaxis_rotation >>
            //large_arc_flag >> sweep_flag;

            rx = bytes_to_float(data[pos + 1] > 0, data[pos + 2], data[pos + 3]);
            ry = bytes_to_float(data[pos + 4] > 0, data[pos + 5], data[pos + 6]);

            x1 = bytes_to_float(data[pos + 7] > 0, data[pos + 8], data[pos + 9]);
            y1 = bytes_to_float(data[pos + 10] > 0, data[pos + 11], data[pos + 12]);

            x2 = bytes_to_float(data[pos + 13] > 0, data[pos + 14], data[pos + 15]);
            y2 = bytes_to_float(data[pos + 16] > 0, data[pos + 17], data[pos + 18]);

            xaxis_rotation = bytes_to_float(data[pos + 19] > 0, data[pos + 20], data[pos + 21]);

            large_arc_flag = data[pos + 22];
            sweep_flag = data[pos + 23];

            if (pts->size() == 0) {
                pt.x = x1;
                pt.y = y1;
                pts->push_back(pt);
            }

            arc_to_pts(pts, rx, ry, x1, y1, x2, y2, xaxis_rotation,
                       large_arc_flag, sweep_flag, CALCULATION_STEP);

            pt.x = x2;
            pt.y = y2;
            pts->push_back(pt);

            if (cmds->size() == 0)
                cmds->push_back(new mcmd(x1, y1));

            arc_to_quads(cmds, rx, ry, x1, y1, x2, y2, xaxis_rotation,
                         large_arc_flag, sweep_flag);

            pos += 24;
        }
        else if (data[pos] == END_SHAPE) {
            if (contours->size() > 0) {
                shape.contours = contours;
                simplify_shape(&shape);

                shapes->push_back(shape);
                contours = new std::vector<painter_contour>();
            }

            if (cntrs->size() > 0) {
                shp.cntrs = cntrs;
                shp.drawn = false;
                shps->push_back(shp);
                cntrs = new std::vector<cmdcntr>();
            }

            pos++;

        }
    }

    prepare_shapes(shapes);

    std::tuple<std::vector<painter_shape> *, std::vector<cmdshp> *> tuple =
                                                     std::make_tuple(shapes, shps);
    return tuple;
}