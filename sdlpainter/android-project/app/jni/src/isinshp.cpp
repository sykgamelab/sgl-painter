
#include "painter_common.h"
#include <android/log.h>
#include <cmath>

//del
#include <../skia/include/core/SkData.h>
#include <../skia/include/core/SkImage.h>
#include <../skia/include/core/SkStream.h>
#include <../skia/include/core/SkSurface.h>
#include <../skia/include/core/SkCanvas.h>
#include <../skia/include/core/SkSurface.h>
#include <../skia/include/core/SkRefCnt.h>
#include <../skia/include/core/SkShader.h>


int
pt_in_polygon2(painter_point &p, std::vector<painter_point> *pts,
        float offsetx, float offsety, float scale)
{

    static const int q_patt[2][2] = { {0,1}, {3,2} };

    if (pts->size() < 3)
        return false;

//    std::vector::const_iterator end = pts->end();
    painter_point pred_pt = pts->back();

    pred_pt.x = pred_pt.x * scale + offsetx;
    pred_pt.y = pred_pt.y * scale + offsety;

    pred_pt.x -= p.x;
    pred_pt.y -= p.y;

    int pred_q = q_patt[pred_pt.y < 0][pred_pt.x < 0];

    int w = 0;

    for (int i = 0; i < pts->size(); i++) {
        struct painter_point cur_pt = pts->at(i);

        cur_pt.x = cur_pt.x * scale + offsetx;
        cur_pt.y = cur_pt.y * scale + offsety;

        cur_pt.x -= p.x;
        cur_pt.y -= p.y;

        int q = q_patt[cur_pt.y < 0][cur_pt.x < 0];

        switch (q - pred_q)
        {
            case -3:
                ++w;
                break;
            case 3:
                --w;
                break;
            case -2:
                if(pred_pt.x*cur_pt.y>=pred_pt.y*cur_pt.x)
                    ++w;
                break;
            case 2:
                if(!(pred_pt.x * cur_pt.y >= pred_pt.y * cur_pt.x))
                    --w;
                break;
        }

        pred_pt = cur_pt;
        pred_q = q;

    }

    return w;

}




static inline int
islft(painter_point &p0, painter_point &p1, painter_point p2)
{
    return ((p1.x - p0.x) * (p2.y - p0.y)
            - (p2.x -  p0.x) * (p1.y - p0.y) );
}

// wn_PnPoly(): winding number test for a point in a polygon
//      Input:   P = a point,
//               V[] = vertex points of a polygon V[n+1] with V[n]=V[0]
//      Return:  wn = the winding number (=0 only when P is outside)
static int
wnpnpoly(painter_point &p, std::vector<painter_point> *pts,
        float offsetx, float offsety, float scale)
{
    int wn, i;    // the  winding number counter

    // loop through all edges of the polygon
    wn = 0;
    for (i = 0; i < pts->size(); i++) {  // edge from V[i] to  V[i+1]
        int i1;

        i1 = (i + 1) % pts->size();
        struct painter_point p1 = pts->at(i);
        p1.x = p1.x * scale + offsetx;
        p1.y = p1.y * scale + offsety;

        struct painter_point p2 = pts->at(i1);
        p2.x = p2.x * scale + offsetx;
        p2.y = p2.y * scale + offsety;

        if (p1.y <= p.y) {          // start y <= P.y
            if (p2.y  > p.y)      // an upward crossing
                if (islft(p1, p2, p) > 0)  // P left of  edge
                    wn++;            // have  a valid up intersect
        }
        else {                        // start y > P.y (no test needed)
            if (p2.y  <= p.y)     // a downward crossing
                if (islft(p1, p2, p) < 0)  // P right of  edge
                    --wn;            // have  a valid down intersect
        }
    }
    return wn;
}

bool
// isinshp(SkCanvas *target, painter_shape &shp, int x, int y, float offsetx, float offsety, float scale, float imgscale)
isinshp(painter_shape &shp, int x, int y, float offsetx, float offsety, float scale)
{
    int i;
    bool inouter, ininner; // во внешнем, во внутреннем
    struct painter_point testpt;

    testpt.x = x;
    testpt.y = y;

    inouter = ininner = false;

    int lt = shp.lt * scale + offsetx;
    int rt = shp.rt * scale + offsetx;
    int bm = shp.bm * scale + offsety;
    int tp = shp.tp * scale + offsety;

    int llt = lt;
    int rrt = rt;
    int bbm = bm;
    int ttp = tp;

//    SkPaint paint;
//    paint.setColor(SK_ColorGREEN);
//    target->drawCircle(100, 100, 10.0f, paint);
//    paint.setStyle(SkPaint::Style::kStroke_Style);
//    paint.setStrokeWidth(10.0f);

    if (x >= lt && x <= rt && y >= bm && y <= tp) {

        __android_log_print(ANDROID_LOG_INFO, "clickx", "coord: %d %d %d %d", lt, rt, bm, tp);

//        paint.setColor(SkColorSetARGB(0xFF, 255, 0, 0));
//        target->drawRect(SkRect::MakeLTRB(shp.lt * imgscale, shp.tp * imgscale, shp.rt * imgscale, shp.bm * imgscale), paint);
        __android_log_print(ANDROID_LOG_INFO, "PAINTER", "isinshp is in outer rect");
//        __android_log_print(ANDROID_LOG_INFO, "PAINTER", "contur size: %d", shp.contours->size());

        for (i = 0 ; i < shp.contours->size(); i++) {
            __android_log_print(ANDROID_LOG_INFO, "PAINTER_", "cnt is");
            if (!shp.contours->at(i).outer) {
                __android_log_print(ANDROID_LOG_INFO, "PAINTER_", "not outer");
                continue;
            }
            __android_log_print(ANDROID_LOG_INFO, "PAINTER_", "outer");

            __android_log_print(ANDROID_LOG_INFO, "PAINTER", "outer %d", i);
            //if (wnpnpoly(testpt, shp.contours->at(i).pts, offsetx, offsety, scale) != 0) {
            if (pt_in_polygon2(testpt, shp.contours->at(i).pts, offsetx, offsety, scale) != 0) {
                __android_log_print(ANDROID_LOG_INFO, "PAINTER", "isinshp is in outer");
                inouter = true;
                break;
            }
        }
    }

    if (inouter) {
        for (i = 0 ; i < shp.contours->size(); i++) {
            __android_log_print(ANDROID_LOG_INFO, "PAINTER", "ininner");
            if (shp.contours->at(i).outer || i == 0) {
                //__android_log_print(ANDROID_LOG_INFO, "PAINTER", "continue %d", i);
                continue;
            }

            lt = shp.contours->at(i).lt * scale + offsetx;
            rt = shp.contours->at(i).rt * scale + offsetx;
            bm = shp.contours->at(i).bm * scale + offsety;
            tp = shp.contours->at(i).tp * scale + offsety;

            if (llt > lt || rrt < rt || bbm > bm || ttp < tp)
                continue;

            ///lt = shp.lt * scale + offsetx;
            //rt = shp.rt * scale + offsetx;
            //bm = shp.bm * scale + offsety;
            //tp = shp.tp * scale + offsety;

            __android_log_print(ANDROID_LOG_INFO, "PAINTER", "coord: %d %d %d %d", lt, rt, bm, tp);

            if (x >= lt && x <= rt &&
                y >= bm && y <= tp) {
                if (pt_in_polygon2(testpt, shp.contours->at(i).pts, offsetx, offsety, scale) != 0) {
                    __android_log_print(ANDROID_LOG_INFO, "PAINTER", "is in inner %d", i);
                    ininner = true;
                    break;
                }
            }
        }
    }
    //if(inouter && !ininner) {
    //    return true;
    //}
    return inouter && !ininner;
}
