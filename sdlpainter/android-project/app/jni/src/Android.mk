LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_CPPFLAGS += -std=c++17

LOCAL_MODULE := main

SDL_PATH := ../SDL
SDL_IMAGE_PATH := ../SDL2_image
SLE_RESOURCE_MANAGER_PATH := ../sle_rsrcmgr
NANOVG_PATH := ../nanovg
SKIA_PATH := ../skia
SKIA64_PATH := ../skia64

LOCAL_C_INCLUDES := $(LOCAL_PATH)/$(SDL_PATH)/include
LOCAL_C_INCLUDES := $(LOCAL_PATH)/$(SDL_IMAGE_PATH)/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/$(SLE_RESOURCE_MANAGER_PATH)/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/$(NANOVG_PATH)/include
LOCAL_C_INCLUDES += $(LOCAL_PATH)/$(SKIA_PATH)

# Add your application source files here...
LOCAL_SRC_FILES := main.cpp painter_common.cpp painter_commands.cpp isinshp.cpp painter_read.cpp

LOCAL_SHARED_LIBRARIES := SDL2 SDL2_image sle_rsrcmgr nanovg
#LOCAL_STATIC_LIBRARIES := skia
#LOCAL_STATIC_LIBRARIES += skia64

#ifeq ($(TARGET_ARCH),armeabi-v7a)
#	LOCAL_STATIC_LIBRARIES := skia
#else
#    LOCAL_STATIC_LIBRARIES += skia64
#endif

LOCAL_STATIC_LIBRARIES := skia
#LOCAL_STATIC_LIBRARIES := skia64

LOCAL_LDLIBS := -lGLESv1_CM -lGLESv2 -llog -landroid -lGLESv2 -lGLESv3 -lEGL

include $(BUILD_SHARED_LIBRARY)
