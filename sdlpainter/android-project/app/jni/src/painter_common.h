#ifndef PAINTER_COMMON_H
#define PAINTER_COMMON_H

#include <array>
#include <iostream>
#include <vector>

struct painter_pointf {
    float x, y;
};

struct painter_point {
    int x, y;
};

struct painter_contour {
    std::vector<painter_point> *pts;
    bool outer;
    float lt, rt, tp, bm;
};

struct painter_shape {
    std::vector<painter_contour> *contours;
    float r, g, b;
    float lt, rt, tp, bm, cx, cy, radius;
};

void bezier3_to_pts(std::vector<painter_point> *pts, painter_pointf p0,
                    painter_pointf p1, painter_pointf p2, painter_pointf p3, float step);
void bezier2_to_pts(std::vector<painter_point> *pts, painter_pointf p0,
                    painter_pointf p1, painter_pointf p2, float step);
float svg_signum(float n);
float clamp_angle(float angle);
painter_pointf elarcpt(float cx, float cy, float rx, float ry, float xangle, float t);
painter_pointf delarcpt(float cx, float cy, float rx, float ry, float xangle, float t);
void arc_to_pts(std::vector<painter_point> *pts, float rx, float ry, float x1,
                float y1, float x2, float y2, float xaxis_rotation, int large_arc_flag,
                int sweep_flag, float step);
void simplify_shape(painter_shape *shape);
void prepare_shapes(std::vector<painter_shape> *shapes);


#endif