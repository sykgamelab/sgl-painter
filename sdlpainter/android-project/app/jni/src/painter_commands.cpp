#include <fstream>
#include <iostream>
#include <math.h>
#include <vector>
#include <string>

#include "painter_commands.h"
#include "painter_common.h"

//#define M_PI 3.14159265359

mcmd::mcmd(float x, float y)
{
    this->x = x;
    this->y = y;
}

void
mcmd::exec(SkPath &path, float offsetx, float offsety, float scale)
{
    // nvgBeginPath(ctx);
    //nvgMoveTo(ctx, this->x * scale + offsetx, this->y * scale + offsety);
    path.moveTo(this->x * scale + offsetx, this->y * scale + offsety);
}

lcmd *
mcmd::getlcmd()
{
    return new lcmd(this->x, this->y);
}

lcmd::lcmd(float x, float y)
{
    this->x = x;
    this->y = y;
}

void
lcmd::exec(SkPath &path, float offsetx, float offsety, float scale)
{
    //nvgLineTo(ctx, this->x * scale + offsetx, this->y * scale + offsety);
    path.lineTo(this->x * scale + offsetx, this->y * scale + offsety);
}

ccmd::ccmd(float c1x, float c1y, float c2x, float c2y, float x, float y)
{
    this->c1x = c1x;
    this->c1y = c1y;
    this->c2x = c2x;
    this->c2y = c2y;
    this->x = x;
    this->y = y;
}

void
ccmd::exec(SkPath &path, float offsetx, float offsety, float scale)
{
    /*nvgBezierTo(ctx, c1x * scale + offsetx, c1y * scale + offsety,
        c2x * scale + offsetx, c2y * scale + offsety,
        x  * scale + offsetx, y * scale + offsety);*/
    SkPoint p1 = SkPoint::Make(c1x * scale + offsetx, c1y * scale + offsety);
    SkPoint p2 = SkPoint::Make(c2x * scale + offsetx, c2y * scale + offsety);
    SkPoint p3 = SkPoint::Make(x  * scale + offsetx, y * scale + offsety);

    path.cubicTo(p1, p2, p3);
}

qcmd::qcmd(float cx, float cy, float x, float y)
{
    this->cx = cx;
    this->cy = cy;
    this->x = x;
    this->y = y;
}

void
qcmd::exec(SkPath &path, float offsetx, float offsety, float scale)
{
    /*nvgQuadTo(ctx, cx * scale + offsetx, cy * scale + offsety,
        x * scale + offsetx, y  * scale + offsety);*/

    SkPoint p1 = SkPoint::Make(cx * scale + offsetx, cy * scale + offsety);
    SkPoint p2 = SkPoint::Make(x  * scale + offsetx, y * scale + offsety);

    path.quadTo(p1, p2);
}

void
arc_to_quads(std::vector<pntrcmd *> *cmds, float rx, float ry, float x1,
             float y1, float x2, float y2, float xaxis_rotation,
             int large_arc_flag, int sweep_flag)
{

    float radphi, newx, newy, lambda;
    float s, tcx, tcy, cx, cy, theta;
    float vx, vy, ux, uy, dtheta;
    float arc_step, ang;

    radphi = M_PI * xaxis_rotation / 180.0f;


    newx = cosf(radphi)*((x1 - x2) / 2) + sinf(radphi)*((y1 - y2) / 2);
    newy = (-1) * sinf(radphi)*((x1 - x2) / 2) + cosf(radphi)*((y1 - y2) / 2);
    lambda = (newx*newx) / (rx*rx) + (newy*newy) / (ry*ry);

    if (lambda > 1.0f) {
        rx = sqrtf(lambda) * rx;
        ry = sqrtf(lambda) * ry;
    }

    s = sqrtf(fabsf((rx*rx * ry*ry - rx * rx * newy*newy - ry * ry * newx*newx)
                    / (rx*rx * newy*newy + ry * ry * newx*newx)));
    if (large_arc_flag == sweep_flag)
        s = -s;

    tcx = s * (rx * newy) / ry;
    tcy = s * ((-1) * ry * newx) / rx;
    cx = cosf(radphi) * tcx - sinf(radphi) * tcy + (x1 + x2) / 2;
    cy = sinf(radphi) * tcx + cosf(radphi) * tcy + (y1 + y2) / 2;

    vx = (newx - tcx) / rx;
    vy = (newy - tcy) / ry;
    ux = 1;
    uy = 0;

    theta = (ux*vx + uy * vy) / (sqrtf(ux*ux + uy * uy) * sqrtf(vx*vx + vy * vy));
    theta = acosf(theta) * 180.0f / M_PI;
    theta = fabsf(theta) * svg_signum(ux*vy - vx * uy);

    ux = (newx - tcx) / rx;
    uy = (newy - tcy) / ry;
    vx = (-newx - tcx) / rx;
    vy = (-newy - tcy) / ry;
    dtheta = (ux*vx + uy * vy) / (sqrtf(ux*ux + uy * uy) * sqrtf(vx*vx + vy * vy));

    if (dtheta < -1.0)
        dtheta = -1.0;
    else if (dtheta > 1.0)
        dtheta = 1.0;

    dtheta = clamp_angle(acosf(dtheta) * 180.0f / M_PI);
    dtheta = fabsf(dtheta) * svg_signum(ux*vy - vx * uy);

    if (sweep_flag == 0 && dtheta > 0)
        dtheta -= 360;
    else if (sweep_flag == 1 && dtheta < 0)
        dtheta += 360;

    int ndivs = 1;
    ang = theta;
    arc_step = fabsf(ang - (theta + dtheta)) / (float)ndivs;
    while (arc_step > 45) {
        ndivs++;
        arc_step = fabsf(ang - (theta + dtheta)) / (float)ndivs;
    }

    arc_step = (sweep_flag == 0) ? -arc_step : arc_step;




    for (int i = 0; i < ndivs; i++, ang += arc_step)
    {
        painter_pointf p1, p2, dp1, dp2, c1, c2;

        float t = ang * M_PI / 180;
        float t1 = (ang + arc_step) * M_PI / 180;
        float tans = tanf((t1 - t) / 2.0f);
        tans *= tans;
        float alpha = sinf(t1 - t) * ((sqrtf(4.0f + 3.0f * tans) - 1.0f) / 3.0f);
        // if ((arc_step > 0 && t1 > theta + dtheta) || (arc_step < 0 && t1 < theta + dtheta))
        //   t1 = theta;

        p1 = elarcpt(cx, cy, rx, ry, radphi, t);
        p2 = elarcpt(cx, cy, rx, ry, radphi, t1);
        dp1 = delarcpt(cx, cy, rx, ry, radphi, t);
        dp2 = delarcpt(cx, cy, rx, ry, radphi, t1);

        c1.x = p1.x + alpha * dp1.x;
        c1.y = p1.y + alpha * dp1.y;
        c2.x = p2.x - alpha * dp2.x;
        c2.y = p2.y - alpha * dp2.y;

        cmds->push_back(new ccmd(c1.x, c1.y, c2.x, c2.y, p2.x, p2.y));
        //        cmds->push_back(new lcmd(p2.x, p2.y));
    }
}