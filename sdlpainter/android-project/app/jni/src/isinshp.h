//
// Created by Vadim on 11.06.2019.
//

#ifndef ISINSHP_H
#define ISINSHP_H

#include "painter_common.h"
bool isinshp(painter_shape &shp, int x, int y, float offsetx, float offsety, float scale);
#endif //ANDROID_PROJECT_ISINSHP_H
