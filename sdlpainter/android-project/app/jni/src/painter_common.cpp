#include <fstream>
#include <iostream>
#include <math.h>
#include <string>
#include <vector>

#include "painter_common.h"

#define CALCULATION_STEP 0.1f

void
bezier3_to_pts(std::vector<painter_point> *pts, painter_pointf p0,
               painter_pointf p1, painter_pointf p2, painter_pointf p3, float step)
{
    float t;
    int flag = 1;


    t = 0;
    while (flag)
    {
        struct painter_point pt;
        float x, y;

        if (t > 1.0f) {
            t = 1.0f;
            flag = 0;
        }

        x = (1 - t)*(1 - t)*(1 - t) * p0.x +
            3 * t * (1 - t)*(1 - t) * p1.x +
            3 * t*t * (1 - t) * p2.x + t * t*t *
                                       p3.x;

        y = (1 - t)*(1 - t)*(1 - t) * p0.y +
            3 * t * (1 - t)*(1 - t) * p1.y +
            3 * t*t * (1 - t) * p2.y + t * t*t *
                                       p3.y;


        pt.x = x;
        pt.y = y;
        pts->push_back(pt);

        t += step;
    }
}

void
bezier2_to_pts(std::vector<painter_point> *pts, painter_pointf p0,
               painter_pointf p1, painter_pointf p2, float step)
{
    float t;
    int flag = 1;


    t = 0;
    while (flag)
    {
        painter_point pt;
        float x, y;

        if (t > 1.0f) {
            t = 1.0f;
            flag = 0;
        }

        x = (1 - t)*(1 - t)*  p0.x +
            2 * t * (1 - t) * p1.x +
            t * t * p2.x;

        y = (1 - t)*(1 - t)*  p0.y +
            2 * t * (1 - t) * p1.y +
            t * t * p2.y;


        pt.x = x;
        pt.y = y;
        pts->push_back(pt);

        t += step;
    }

}

float
svg_signum(float n)
{
    if (n < 0)
        return -1;
    else
        return 1;
}

float
clamp_angle(float angle)
{
    int mult = angle < 0 ? -1 : 1;
    while (fabsf(angle) >= 360.0f)
        angle -= mult * 360.0f;

    return angle;
}
painter_pointf
elarcpt(float cx, float cy, float rx, float ry, float xangle, float t) {
    painter_pointf pt;
    pt.x = cosf(xangle) * cosf(t) * rx -
           sinf(xangle) * sinf(t) * ry + cx;
    pt.y = sinf(xangle) * cosf(t) * rx +
           cosf(xangle) * sinf(t) * ry + cy;

    return pt;
}

painter_pointf
delarcpt(float cx, float cy, float rx, float ry, float xangle, float t) {
    painter_pointf pt;

    pt.x = -rx * cosf(xangle) * sinf(t) - ry * sinf(xangle) * cosf(t),
            pt.y = -rx * sinf(xangle) * sinf(t) + ry * cosf(xangle) * cosf(t);

    return pt;
}

void
arc_to_pts(std::vector<painter_point> *pts, float rx, float ry, float x1,
           float y1, float x2, float y2, float xaxis_rotation, int large_arc_flag,
           int sweep_flag, float step)
{
    float radphi, newx, newy, lambda;
    float s, tcx, tcy, cx, cy, theta;
    float vx, vy, ux, uy, dtheta;
    float arc_step, ang;

    radphi = M_PI * xaxis_rotation / 180.0f;


    newx = cosf(radphi)*((x1 - x2) / 2) + sinf(radphi)*((y1 - y2) / 2);
    newy = (-1) * sinf(radphi)*((x1 - x2) / 2) + cosf(radphi)*((y1 - y2) / 2);
    lambda = (newx*newx) / (rx*rx) + (newy*newy) / (ry*ry);

    if (lambda > 1.0f) {
        rx = sqrtf(lambda) * rx;
        ry = sqrtf(lambda) * ry;
    }

    s = sqrtf(fabsf((rx*rx * ry*ry - rx * rx * newy*newy - ry * ry * newx*newx)
                    / (rx*rx * newy*newy + ry * ry * newx*newx)));
    if (large_arc_flag == sweep_flag)
        s = -s;

    tcx = s * (rx * newy) / ry;
    tcy = s * ((-1) * ry * newx) / rx;
    cx = cosf(radphi) * tcx - sinf(radphi) * tcy + (x1 + x2) / 2;
    cy = sinf(radphi) * tcx + cosf(radphi) * tcy + (y1 + y2) / 2;

    vx = (newx - tcx) / rx;
    vy = (newy - tcy) / ry;
    ux = 1;
    uy = 0;

    theta = (ux*vx + uy * vy) / (sqrtf(ux*ux + uy * uy) * sqrtf(vx*vx + vy * vy));
    theta = acosf(theta) * 180.0f / M_PI;
    theta = fabsf(theta) * svg_signum(ux*vy - vx * uy);

    ux = (newx - tcx) / rx;
    uy = (newy - tcy) / ry;
    vx = (-newx - tcx) / rx;
    vy = (-newy - tcy) / ry;
    dtheta = (ux*vx + uy * vy) / (sqrtf(ux*ux + uy * uy) * sqrtf(vx*vx + vy * vy));

    if (dtheta < -1.0)
        dtheta = -1.0;
    else if (dtheta > 1.0)
        dtheta = 1.0;

    dtheta = clamp_angle(acosf(dtheta) * 180.0f / M_PI);
    dtheta = fabsf(dtheta) * svg_signum(ux*vy - vx * uy);

    if (sweep_flag == 0 && dtheta > 0)
        dtheta -= 360;
    else if (sweep_flag == 1 && dtheta < 0)
        dtheta += 360;

    int ndivs = 1;
    ang = theta;
    arc_step = fabsf(ang - (theta + dtheta)) / (float)ndivs;
    while (arc_step > 45) {
        ndivs++;
        arc_step = fabsf(ang - (theta + dtheta)) / (float)ndivs;
    }

    arc_step = (sweep_flag == 0) ? -arc_step : arc_step;
    for (int i = 0; i < ndivs; i++, ang += arc_step)
    {
        painter_pointf p1, p2, dp1, dp2, c1, c2;

        float t = ang * M_PI / 180;
        float t1 = (ang + arc_step) * M_PI / 180;
        float tans = tanf((t1 - t) / 2.0f);
        tans *= tans;
        float alpha = sinf(t1 - t) * ((sqrtf(4.0f + 3.0f * tans) - 1.0f) / 3.0f);

        p1 = elarcpt(cx, cy, rx, ry, radphi, t);
        p2 = elarcpt(cx, cy, rx, ry, radphi, t1);
        dp1 = delarcpt(cx, cy, rx, ry, radphi, t);
        dp2 = delarcpt(cx, cy, rx, ry, radphi, t1);

        c1.x = p1.x + alpha * dp1.x;
        c1.y = p1.y + alpha * dp1.y;
        c2.x = p2.x - alpha * dp2.x;
        c2.y = p2.y - alpha * dp2.y;

        bezier3_to_pts(pts, p1, c1, c2, p2, CALCULATION_STEP);
    }
}

void
simplify_shape(painter_shape *shape)
{
    size_t i, j;
    size_t npts;
    std::vector<painter_point> *pts;
    std::vector<painter_point> *pts_new;

    // std::cout << "simplify ncontours " << shape->contours->size() << std::endl;
    for (i = 0; i < shape->contours->size(); i++) {
        pts = shape->contours->at(i).pts;
        npts = pts->size();
        pts = shape->contours->at(i).pts;
        // std::cout << "size " << npts << std::endl;
        // std::cout << "before begin-end check" << " " << npts << std::endl;
        while ((pts->at(0).x == pts->at(npts - 1).x) &&
               (pts->at(0).y == pts->at(npts - 1).y) &&
               (npts > 1)) {
            pts->pop_back();
            npts--;
        }
    }


    for (i = 0; i < shape->contours->size(); i++) {
        int k;
        npts = 1;
        pts = shape->contours->at(i).pts;

        pts_new = new std::vector<painter_point>();
        k = 0;
        pts_new->push_back(pts->at(0));
        for (j = 1; j < pts->size(); j++) {
            painter_point prevpt, nextpt;
            prevpt = pts_new->at(k);
            nextpt = pts->at(j);
            if (nextpt.x != prevpt.x || nextpt.y != prevpt.y) {
                k++;
                pts_new->push_back(nextpt);
            }
        }

        delete pts;
        shape->contours->at(i).pts = pts_new;
    }
}

void
prepare_shapes(std::vector<painter_shape> *shapes)
{
    int i, j, k;
    for (i = 0; i < shapes->size(); i++) {
        std::vector<painter_contour> *contours;
        int minx, minidx;

        contours = shapes->at(i).contours;
        minx = contours->at(0).pts->at(0).x;
        minidx = 0;
        for (j = 0; j < contours->size(); j++) {
            float cl, cr, ct, cb;
            std::vector<painter_point> *pts;

            pts = contours->at(j).pts;

            cl = pts->at(0).x;
            cr = pts->at(0).x;
            ct = pts->at(0).y;
            cb = pts->at(0).y;

            for (k = 0; k < pts->size(); k++) {
                int x, y;

                x = pts->at(k).x;
                y = pts->at(k).y;

                if (x < minx) {
                    minidx = j;
                    minx = x;
                }

                if (y < cb)
                    cb = y;
                if (y > ct)
                    ct = y;
                if (x < cl)
                    cl = x;
                if (x > cr)
                    cr = x;
            }

            contours->at(j).lt = cl;
            contours->at(j).rt = cr;
            contours->at(j).bm = cb;
            contours->at(j).tp = ct;
        }

        for (j = 0; j < contours->size(); j++) {
            if (j == minidx)
                contours->at(j).outer = true;
            else
                contours->at(j).outer = false;
        }
    }

    for (i = 0; i < shapes->size(); i++) {
        std::vector<painter_contour> *contours;
        float l, r, t, b, cx, cy, radius;

        contours = shapes->at(i).contours;
        l = contours->at(0).pts->at(0).x;
        r = contours->at(0).pts->at(0).x;
        t = contours->at(0).pts->at(0).y;
        b = contours->at(0).pts->at(0).y;
        for (j = 0; j < contours->size(); j++) {

            std::vector<painter_point> *pts;
            if (!contours->at(j).outer)
                continue;

            pts = contours->at(j).pts;




            for (k = 1; k < pts->size(); k++) {
                float x, y;

                x = pts->at(k).x;
                y = pts->at(k).y;

                if (y < b)
                    b = y;
                if (y > t)
                    t = y;
                if (x < l)
                    l = x;
                if (x > r)
                    r = x;


            }

        }

        cx = (r + l) / 2.0f;
        cy = (t + b) / 2.0f;
        radius = sqrt((r - l) * (r - l) + (t - b)*(t - b));

        shapes->at(i).lt = l;
        shapes->at(i).rt = r;
        shapes->at(i).tp = t;
        shapes->at(i).bm = b;
        shapes->at(i).cx = cx;
        shapes->at(i).cy = cy;
        shapes->at(i).radius = radius;
    }

}