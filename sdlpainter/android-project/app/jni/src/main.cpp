//
// Created by Vadim on 27.05.2019.
//

#include <android/log.h>
#include <android/asset_manager.h>
#include <android/asset_manager_jni.h>
#include <chrono>
#include <GLES2/gl2.h>
#include <jni.h>
#include <math.h>
#include <iostream>
#include <sstream>
#include <tuple>
//del
#include <time.h>
#include <chrono>


/*********SKIA INCLUCES ***********/
#include <../skia/include/gpu/GrBackendSurface.h>
#include <../skia/include/gpu/GrContext.h>
#include <../skia/include/gpu/gl/GrGLInterface.h>
#include <../skia/src/gpu/gl/GrGLDefines.h>
#include <../skia/src/gpu/gl/GrGLUtil.h>

#include <../skia/include/core/SkData.h>
#include <../skia/include/core/SkImage.h>
#include <../skia/include/core/SkStream.h>
#include <../skia/include/core/SkSurface.h>
#include <../skia/include/core/SkCanvas.h>
#include <../skia/include/core/SkSurface.h>
#include <../skia/include/core/SkRefCnt.h>
#include <../skia/include/core/SkShader.h>
/*********************************/

#include "painter_read.h"
#include "SDL.h"
//#include "SDL_image.h"
#include "SDL_events.h"
#include "SDL_opengles.h"
#include "sle_rsrcmgr.h"
#include "nanovg.h"
#include "isinshp.h"

#define NANOVG_GLES2_IMPLEMENTATION
#include "nanovg_gl.h"
#include "../SDL2_image/SDL_image.h"
#include "../skia/include/gpu/gl/GrGLTypes.h"
#include "../skia/src/gpu/gl/GrGLUtil.h"
#include "../skia/src/gpu/gl/GrGLDefines.h"
#include "../skia/include/core/SkColor.h"
#include "../skia/include/core/SkPaint.h"

#define TAG "painter"

std::vector<painter_shape> *shapes;
std::vector<cmdshp> *shps;
SDL_Window *window;
SDL_Renderer *renderer;
SDL_Surface* imgSurface;
SkCanvas *canvas;
NVGcontext *vg;

SDL_GLContext glctx;
GLuint gprogid = 0;
GLint gvtxpos_2dloc = -1;
GLuint gvbo = 0;
GLuint gibo  = 0;
bool grender_quad = true;
float ww, wh, diag, pxratio;
float offsetx = 0.0f, offsety = 0.0f, scale = 1.0f;
float maxzoom = 7.0f, minzoom = 0.13f;
float lt, rt, bm, tp;
float maxl, minr, maxb, mint;
int nfingers = 0;

SDL_Color* map;

struct plt_cl {
    int r, g, b;
};

Uint32 get_pixel(SDL_Surface* surface, int x, int y)
{
    int bpp = surface->format->BytesPerPixel;
    Uint8* p = (Uint8*)surface->pixels + y * surface->pitch + x * bpp;

    switch (bpp) {
        case 1:
            return *p;
            break;

        case 2:
            return *(Uint16*)p;
            break;

        case 3:
            if (SDL_BYTEORDER == SDL_BIG_ENDIAN)
                return p[0] << 16 | p[1] << 8 | p[2];
            else
                return p[0] | p[1] << 8 | p[2] << 16;
            break;

        case 4:
            std::cout << "Hello";
            return *(Uint32*)p;
            break;

        default:
            return 0;       /* shouldn't happen, but avoids warnings */
    }
}

void load_img()
{
    const char* PNG_FILE_PATH = "testImage.png";

    imgSurface = IMG_Load(PNG_FILE_PATH);

    if (!imgSurface) {
        std::cout << "Failed to load image. " << SDL_GetError() << std::endl;
        exit(1);
    }
    else {
        std::cout << "Image loaded." << std::endl;
    }

    map = new SDL_Color[imgSurface->w * imgSurface->h];

    SDL_LockSurface(imgSurface);

    for (int i = 0; i < imgSurface->h; i++)
    {
        for (int j = 0; j < imgSurface->w; j++)
        {
            Uint32 pixel = get_pixel(imgSurface, i, j);
            SDL_GetRGBA(pixel, imgSurface->format, &map[i * imgSurface->w + j].r,
                        &map[i * imgSurface->w + j].g, &map[i * imgSurface->w + j].b, &map[i * imgSurface->w + j].a);

        }
    }

    SDL_UnlockSurface(imgSurface);
}

void
render()
{
    static float a = 0.0f;

    int i, j, k;
    // static float t = 0.0f;
    //Clear color buffer
    // glClear( GL_COLOR_BUFFER_BIT );
//    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT|GL_STENCIL_BUFFER_BIT);

    //Render quad

   /* glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_STENCIL_TEST);



    nvgBeginFrame(vg, ww, wh, pxratio);

    for (i = 0; i < imgSurface->h; i++)
    {
        for (j = 0; j < imgSurface->w; j++)
        {
            nvgBeginPath(vg);
            nvgFillColor(vg, nvgRGBA((unsigned char)map[i * imgSurface->w + j].r, (unsigned char)map[i * imgSurface->w + j].g,
                                     (unsigned char)map[i * imgSurface->w + j].b, (unsigned char)map[i * imgSurface->w + j].a));
            nvgRect(vg, i * 5, j * 5, 5, 5);
            nvgClosePath(vg);
            nvgFill(vg);
        }
    }

    nvgEndFrame(vg);*/

    /*int i, j, k;
    // static float t = 0.0f;
    //Clear color buffer
    // glClear( GL_COLOR_BUFFER_BIT );
    glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT|GL_STENCIL_BUFFER_BIT);

    //Render quad

    glEnable(GL_BLEND);
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
    glEnable(GL_CULL_FACE);
    glDisable(GL_DEPTH_TEST);
    glEnable(GL_STENCIL_TEST);



    nvgBeginFrame(vg, ww, wh, pxratio);


    for (i = 0; i < shps->size(); i++) {
        float r, g, b;
        float cx, cy, radius;
        std::vector<cmdcntr> *cntrs;

        r = shps->at(i).r;
        g = shps->at(i).g;
        b = shps->at(i).b;

        cntrs = shps->at(i).cntrs;

     //   nvgStrokeWidth(vg, 2.0f);
      //  nvgStrokeColor(vg, nvgRGBA(r, g, b, 255));
        if ((shapes->at(i).r > 50 || shapes->at(i).g > 50 ||
             shapes->at(i).b > 50))
            continue;

        nvgFillColor(vg, nvgRGBA(shapes->at(i).r, shapes->at(i).g,
                shapes->at(i).b, 255));

        //nvgFillColor(vg, nvgRGBA(255, 255, 255, 255));

        nvgBeginPath(vg);
        for(j = 0; j < cntrs->size(); j++) {
            std::vector<pntrcmd *> *cmds;

            cmds = cntrs->at(j).cmds;

            // nvgBeginPath(vg);
            for (k = 0; k < cmds->size(); k++)
                cmds->at(k)->exec(vg, offsetx, offsety, scale);
            // nvgClosePath(vg);
            if (!cntrs->at(j).outer)
                nvgPathWinding(vg, NVG_HOLE);


        }


        nvgClosePath(vg);

        cx = shapes->at(i).cx * scale + offsetx;
        cy = shapes->at(i).cy * scale + offsety;
        radius = shapes->at(i).radius * scale;

        if ((shapes->at(i).r > 50 || shapes->at(i).g > 50 ||
             shapes->at(i).b > 50)) {
            NVGpaint paint;
            NVGcolor incl, outcl;

            incl = nvgRGBA(shapes->at(i).r, shapes->at(i).g,
                           shapes->at(i).b, 255);
            outcl = nvgRGBA(255, 255, 255, 0);

            paint = nvgRadialGradient(vg, cx,cy, radius * a,0, incl, outcl);
            nvgFillPaint(vg, paint);
            // nvgBeginPath(vg);
            // nvgCircle(vg, cx,cy,radius);
            // nvgRect(vg, l, b, r - l, t - b);
            // nvgFill(vg);
            // nvgClosePath(vg);
        } else {
            // nvgFill(vg);

        }

       // nvgStroke(vg);
        nvgFill(vg);


    }

    for (i = 0; i < shps->size(); i++) {
        float r, g, b;
        float cx, cy, radius;
        std::vector<cmdcntr> *cntrs;

        r = shps->at(i).r;
        g = shps->at(i).g;
        b = shps->at(i).b;

        cntrs = shps->at(i).cntrs;

        nvgStrokeWidth(vg, 2.0f);
        nvgStrokeColor(vg, nvgRGBA(r, g, b, 255));
        nvgFillColor(vg, nvgRGBA(shapes->at(i).r, shapes->at(i).g,
                                 shapes->at(i).b, 255));

        if ((shapes->at(i).r < 30 && shapes->at(i).g < 30 &&
             shapes->at(i).b < 30))
            continue;
        //nvgFillColor(vg, nvgRGBA(255, 255, 255, 255));

        nvgBeginPath(vg);
        for(j = 0; j < cntrs->size(); j++) {
            std::vector<pntrcmd *> *cmds;

            cmds = cntrs->at(j).cmds;

            // nvgBeginPath(vg);
            for (k = 0; k < cmds->size(); k++)
                cmds->at(k)->exec(vg, offsetx, offsety, scale);
            // nvgClosePath(vg);
            if (!cntrs->at(j).outer)
                nvgPathWinding(vg, NVG_HOLE);


        }


        nvgClosePath(vg);



        nvgStroke(vg);
        nvgFill(vg);


    }


    nvgEndFrame(vg);*/

    for (i = 0; i < shps->size(); i++) {
        SkPath path;
        float r, g, b;
        float cx, cy, radius;
        std::vector<cmdcntr> *cntrs;

        if ((shapes->at(i).r < 50 && shapes->at(i).g < 50 &&
             shapes->at(i).b < 50)) continue;

       // if ((shapes->at(i).r > 30 || shapes->at(i).g > 30 ||
         //    shapes->at(i).b > 30)) continue;

        r = shps->at(i).r;
        g = shps->at(i).g;
        b = shps->at(i).b;

        cntrs = shps->at(i).cntrs;

        SkPaint paint;
      //  paint.setAntiAlias(true);
        //paint.setStrokeWidth(2.0f);
        //	if ((shapes->at(i).r > 30 || shapes->at(i).g > 30 ||
        //	shapes->at(i).b > 30))
        //paint.setColor(SkColorSetARGB(a * 0xFF, shapes->at(i).r, shapes->at(i).g, shapes->at(i).b));
        //else
        //paint.setColor(SkColorSetARGB(0xFF, shapes->at(i).r, shapes->at(i).g, shapes->at(i).b));


        paint.setStyle(SkPaint::Style::kStroke_Style);


        for (j = 0; j < cntrs->size(); j++) {
            std::vector<pntrcmd *> *cmds;

            cmds = cntrs->at(j).cmds;

            for (k = 0; k < cmds->size(); k++)
                cmds->at(k)->exec(path, offsetx, offsety, scale);
        }

        cx = shapes->at(i).cx * scale + offsetx;
        cy = shapes->at(i).cy * scale + offsety;
        radius = shapes->at(i).radius * scale;

        paint.setColor(SkColorSetARGB(0xFF, 0, 0xFF, 0));
       /* if ((shapes->at(i).r > 30 || shapes->at(i).g > 30 ||
             shapes->at(i).b > 30)) {

            SkColor target = SkColorSetRGB(shapes->at(i).r, shapes->at(i).g, shapes->at(i).b);
            SkColor colors[2] = { target, SkColorSetARGB(0, 255, 255, 255) };

            SkPoint center = SkPoint::Make(cx, cy);
            auto shader = SkGradientShader::MakeRadial(center, a * radius * 4, colors, NULL, 2, SkTileMode::kClamp);
            //shader

            paint.setShader(shader);
        }*/


        canvas->drawPath(path, paint);
    }
    a += 0.01f;
    if (a > 1) {
        a = 0.0f;
    }

}

void
render_to_image(SkCanvas *target, float imgw, float imgh, bool contour)
{
    float imgscale = imgw / (rt - lt);
    float imgoffsetx = -lt * imgscale;
    float imgoffsety = -bm * imgscale;
    int i, j, k;

    for (i = 0; i < shps->size(); i++) {
        SkPath path;
        float r, g, b;
        float cx, cy, radius;
        std::vector<cmdcntr> *cntrs;

        if (contour) {
            if ((shapes->at(i).r > 50 || shapes->at(i).g > 50 ||
                shapes->at(i).b > 50)) continue;
        } else {
            if ((shapes->at(i).r < 50 && shapes->at(i).g < 50 &&
                shapes->at(i).b < 50)) continue;
        }


        r = shps->at(i).r;
        g = shps->at(i).g;
        b = shps->at(i).b;

        cntrs = shps->at(i).cntrs;

        SkPaint paint;
        paint.setAntiAlias(true);

        for (j = 0; j < cntrs->size(); j++) {
            std::vector<pntrcmd *> *cmds;

            cmds = cntrs->at(j).cmds;

            for (k = 0; k < cmds->size(); k++)
                cmds->at(k)->exec(path, imgoffsetx, imgoffsety, imgscale);

     //       for (k = 0; k < cmds->size(); k++)
       //         cmds->at(k)->exec(path, offsetx, offsety, scale);
        }

        paint.setColor(SkColorSetARGB(0xFF, shapes->at(i).r, shapes->at(i).g, shapes->at(i).b));
        target->drawPath(path, paint);
    }

    target->flush();
}

void
render_shp(SkCanvas *target, cmdshp &shp, float imgw, float imgh)
{
    SkPath path;
    float r, g, b;
    float cx, cy, radius;
    std::vector<cmdcntr> *cntrs;

    float imgscale = imgw / (rt - lt);
    float imgoffsetx = -lt * imgscale;
    float imgoffsety = -bm * imgscale;


    r = shp.r;
    g = shp.g;
    b = shp.b;

    cntrs = shp.cntrs;

    SkPaint paint;
    paint.setAntiAlias(true);



   // paint.setStyle(SkPaint::Style::kStroke_Style);


    for (int j = 0; j < cntrs->size(); j++) {
        std::vector<pntrcmd *> *cmds;

        cmds = cntrs->at(j).cmds;

        for (int k = 0; k < cmds->size(); k++)
            cmds->at(k)->exec(path, imgoffsetx, imgoffsety, imgscale);
    }


    if (!shp.drawn)
        paint.setColor(SkColorSetARGB(0xFF, shp.r, shp.g, shp.b));
    else
        paint.setColor(SkColorSetARGB(0xFF, 255, 255, 255));

    shp.drawn = !shp.drawn;
    target->drawPath(path, paint);

    target->flush();
}



int
SDL_main(int argc, char *argv[])
{
    SDL_DisplayMode dm;
    bool running;


    JNIEnv *env;
    jobject activity, assets;
    jclass activity_class;
    jmethodID method;
    AAssetManager *amgr;
    sle::rsrcmgr *mgr;

    __android_log_print(ANDROID_LOG_INFO, TAG, "here i am");

    running = true;

    if (SDL_Init(SDL_INIT_EVERYTHING) != 0) {
        __android_log_print(ANDROID_LOG_ERROR, TAG, "Error initializing SDL: %s\n",
                SDL_GetError());
        return 1;
    }

    env = (JNIEnv *)SDL_AndroidGetJNIEnv();
    activity = (jobject)SDL_AndroidGetActivity();
    activity_class = env->GetObjectClass(activity);

    method = env->GetMethodID(activity_class, "getAssetManager", "()Ljava/lang/Object;");
    assets = (jobject)env->CallObjectMethod(activity, method);

    SDL_GetCurrentDisplayMode(0, &dm);

    amgr = AAssetManager_fromJava(env, assets);
    mgr = new sle::rsrcmgr(env, amgr);

    sle::rsrc coloring = mgr->readrsrc((char *)"greek.bshp", AASSET_MODE_BUFFER);
    auto tuple = painter_read(coloring.data(), coloring.size());
    shapes = std::get<0>(tuple);
    shps = std::get<1>(tuple);

    for (int i = 0; i < shapes->size(); i++) {
        std::vector<painter_contour> *contours;
        std::vector<cmdcntr> *cmdcntrs;

        contours = shapes->at(i).contours;
        cmdcntrs = shps->at(i).cntrs;

        for (int j = 0; j < contours->size(); j++)
            cmdcntrs->at(j).outer = contours->at(j).outer;
    }

    std::vector<plt_cl> palette;
    sle::rsrc palette_rsrc = mgr->readrsrc((char *)"candies.plt", AASSET_MODE_BUFFER);
    unsigned  char *plt_data = palette_rsrc.data();
    for (int i = 0; i < 8; i++) {
        plt_cl cl = {
                .r = plt_data[i * 3],
                .g = plt_data[i * 3 + 1],
                .b = plt_data[i * 3 + 2]
        };

        palette.push_back(cl);
    }


    if (SDL_GL_SetAttribute(SDL_GL_CONTEXT_MAJOR_VERSION, 2) < 0) {
        __android_log_print(ANDROID_LOG_ERROR, TAG, "err setting major version of gl %s",
                SDL_GetError());
        return 1;
    }
    if (SDL_GL_SetAttribute(SDL_GL_CONTEXT_MINOR_VERSION, 0) < 0) {
        __android_log_print(ANDROID_LOG_ERROR, TAG, "err setting minor version of gl %s",
                SDL_GetError());
        return 1;
    }

    if (SDL_GL_SetAttribute(SDL_GL_CONTEXT_PROFILE_MASK,
                            SDL_GL_CONTEXT_PROFILE_ES) < 0) {
        __android_log_print(ANDROID_LOG_ERROR, TAG, "err setting profile of gl %s",
                SDL_GetError());
        return 1;
    }

    if (SDL_GL_SetAttribute(SDL_GL_STENCIL_SIZE, 1) < 0) {
        __android_log_print(ANDROID_LOG_ERROR, TAG, "err setting stencil size to 1 %s",
                SDL_GetError());
        return 1;
    }

    if (IMG_Init(IMG_INIT_PNG) != IMG_INIT_PNG) {
        std::cerr << "err setting img init " << SDL_GetError() << std::endl;
        exit(1);
    }

   // load_img();

    ww = dm.w;
    wh = dm.h;
    pxratio = ww / wh;
    window = SDL_CreateWindow("", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
                              dm.w, dm.h, SDL_WINDOW_SHOWN | SDL_WINDOW_OPENGL);
    renderer = SDL_CreateRenderer(window, -1,
                                  SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
    // Create an OpenGL context associated with the window.

    glctx = SDL_GL_CreateContext(window);

    lt = shapes->at(0).lt;
    rt = shapes->at(0).rt;
    bm = shapes->at(0).bm;
    tp = shapes->at(0).tp;
    for (int i = 1; i < shapes->size(); i++) {
        if (shapes->at(i).lt < lt)
            lt = shapes->at(i).lt;
        if (shapes->at(i).rt > rt)
            rt = shapes->at(i).rt;

        if (shapes->at(i).bm < bm)
            bm = shapes->at(i).bm;
        if (shapes->at(i).tp > tp)
            tp = shapes->at(i).tp;
    }
    scale = 0.9 * ww / (rt - lt);
    offsetx = -lt * scale  + 0.05f * ww;
    offsety = (wh - (tp - bm) * scale) /  2.0f - bm * scale;
    maxzoom *= scale;
    minzoom = scale;
    diag = sqrtf(ww * ww + wh * wh);

    maxl = 0.05f * ww;
    minr = 0.95f * ww;
    maxb = 0.05f * wh;
    mint = 0.95f * wh;

    //glcreateContext
    if (glctx == NULL) {
        __android_log_print(ANDROID_LOG_ERROR, TAG, "err creating gl context %s %d",
                SDL_GetError(), glGetError());
        return 1;
    } else {
        /* 	GLenum glewError;

            glewError = glewInit();
            if( glewError != GLEW_OK ) {
                std::cerr << "Error initializing GLEW! " <<
                    glewGetErrorString(glewError) << std::endl;
                exit(1);
            }
     */
        if (SDL_GL_SetSwapInterval(1) < 0) {
            __android_log_print(ANDROID_LOG_ERROR, TAG, "err setting swap interval %s",
                    SDL_GetError());
            // exit(1);
        }

        /*if (!init_gl()) {
            __android_log_print(ANDROID_LOG_ERROR, TAG, "err initing opengl");
            return 1;
        }*/

        vg = nvgCreateGLES2(/*NVG_ANTIALIAS |*/ NVG_STENCIL_STROKES/* | NVG_DEBUG*/);
        if (vg == NULL) {
            __android_log_print(ANDROID_LOG_ERROR, TAG, "error initializing nanovg");
            return 1;
        }

        auto skia_interface= GrGLMakeNativeInterface();
        sk_sp<GrContext> context(GrContext::MakeGL(skia_interface));
        if (context == NULL) {
            __android_log_print(ANDROID_LOG_INFO, TAG, "Err creating skia context");
            return 1;
        }

        GrGLint buffer;
        GR_GL_GetIntegerv(skia_interface.get(), GR_GL_FRAMEBUFFER_BINDING, &buffer);
        GrGLFramebufferInfo info;
        info.fFBOID = (GrGLuint)buffer;
        SkColorType colorType;

        //SkDebugf("%s", SDL_GetPixelFormatName(windowFormat));
        // TODO: the windowFormat is never any of these?
        uint32_t windowFormat = SDL_GetWindowPixelFormat(window);
        int contextType;
        SDL_GL_GetAttribute(SDL_GL_CONTEXT_PROFILE_MASK, &contextType);
        if (SDL_PIXELFORMAT_RGBA8888 == windowFormat) {
            info.fFormat = GR_GL_RGBA8;
            colorType = kRGBA_8888_SkColorType;
        }
        else {
            colorType = kBGRA_8888_SkColorType;
            if (SDL_GL_CONTEXT_PROFILE_ES == contextType) {
                info.fFormat = GR_GL_BGRA8;
            }
            else {
                // We assume the internal format is RGBA8 on desktop GL
                info.fFormat = GR_GL_RGBA8;
            }
        }

         GrBackendRenderTarget target(ww, wh, 0, 8, info);

        //setup SkSurface
        // To use distance field text, use commented out SkSurfaceProps instead
        // SkSurfaceProps props(SkSurfaceProps::kUseDeviceIndependentFonts_Flag,
        //                      SkSurfaceProps::kLegacyFontHost_InitType);
        SkSurfaceProps props(SkSurfaceProps::kLegacyFontHost_InitType);

        sk_sp<SkSurface> surface(SkSurface::MakeFromBackendRenderTarget(context.get(), target,
            kBottomLeft_GrSurfaceOrigin,
            colorType, nullptr, &props));

        canvas = surface->getCanvas();

        int imgw = (rt - lt) * maxzoom;
        int imgh = (tp - bm) * maxzoom;
        float imgscale = (float)imgw / (rt - lt);

        float buffer_offsetx = 0.05 * ww;
        float buffer_offsety = (wh - imgh * scale / maxzoom) /  2.0f;

//        SkImageInfo offscreen_info = SkImageInfo::MakeN32Premul(imgw, imgh);
//        sk_sp<SkSurface> contour_surface(SkSurface::MakeRaster(offscreen_info));

        SkImageInfo offscreen_info = SkImageInfo::MakeN32(imgw, imgh, kOpaque_SkAlphaType);
        sk_sp<SkSurface> contour_surface = SkSurface::MakeRenderTarget(context.get(),
                SkBudgeted::kNo, offscreen_info, 0,
                kTopLeft_GrSurfaceOrigin, nullptr, false);
        SkCanvas* contour_canvas = contour_surface->getCanvas();
        contour_canvas->clear(SkColorSetARGB(0, 0, 0, 0));
        render_to_image(contour_canvas, imgw, imgh, true);
        sk_sp<SkImage> contour_img = contour_surface->makeImageSnapshot() ;


        contour_canvas->clear(SK_ColorWHITE);
       // render_to_image(contour_canvas, imgw, imgh, false);
        sk_sp<SkImage> region_img = contour_surface->makeImageSnapshot();

        glClearColor(0.9f, 0.9f, 0.9f, 1.f);

        std::chrono::time_point<std::chrono::system_clock> now = std::chrono::system_clock::now();
        auto duration = now.time_since_epoch();
        float start = (int)std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();

        int ww1 = 0, wh1 = 0;
       // SDL_GetWindowSize(window, &ww1, &wh1);
//        __android_log_print(ANDROID_LOG_INFO, TAG, "ww=%d wh=%d ww1=%d wh1=%d", ww, wh, ww1, wh1);

        float total_time = 0.0f;
        std::vector<painter_pointf> clicks;
        float clickx = 0.0f;
        float clicky = 0.0f;

        float plt_offset = 0.03 * ww;
        float plt_spacing = 0.005 * ww;
        float plt_w = 0.1 * ww;
        float plt_y = wh - plt_w - plt_spacing;
        float usr_plt_y = plt_y - plt_w - plt_spacing;

        float plt_space_h = 2 * plt_w + 3 * plt_spacing;
        float plt_pos_y = wh - plt_space_h;
        float usr_plt_space_y = wh - 2 * plt_space_h;
        float plt_r = 0.05 * plt_w;

        while (running) {
            SDL_Event event;
            static float click_start  = 0.0f;
            static bool may_be_click = false;

          //  glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT|GL_STENCIL_BUFFER_BIT);

            now = std::chrono::system_clock::now();
            duration = now.time_since_epoch();
            float curr = (int)std::chrono::duration_cast<std::chrono::milliseconds>(duration).count();
            float delta = curr - start;
            total_time += delta / 1000.0f;
            if (delta >= 1.0f) {
                start = curr;
            }
            while (SDL_PollEvent(&event)) {
                if (event.type == SDL_KEYDOWN &&
                    event.key.keysym.scancode == SDL_SCANCODE_AC_BACK) {

//                LOG("pressed back button, terminating");
                    running = false;
                } else if (event.type == SDL_FINGERDOWN) {
                    click_start = total_time;
                    nfingers++;

                    if (nfingers == 1) {
                        may_be_click = true;
                        clickx = event.tfinger.x * ww;
                        clicky = event.tfinger.y * wh;
                    } else
                        may_be_click = false;
                    __android_log_print(ANDROID_LOG_INFO, TAG, "sdlevent FINGER DOWN %d", nfingers);
                } else if (event.type == SDL_FINGERMOTION &&
                    (fabsf(event.tfinger.dx )> 0.01 || (fabsf(event.tfinger.dy ) > 0.01))) {
                    may_be_click = false;
                    if (nfingers == 1) {
                        offsetx += event.tfinger.dx * ww;
                        buffer_offsetx += event.tfinger.dx * ww;

                        if (lt * scale + offsetx > maxl)
                            offsetx = maxl - lt * scale;
                        if (rt * scale + offsetx < minr )
                            offsetx = minr - rt * scale;

                        if (buffer_offsetx > maxl)
                            buffer_offsetx = maxl;
                        if (buffer_offsetx + imgw * scale/maxzoom < minr)
                            buffer_offsetx = minr - imgw * scale/maxzoom;

                        offsety += event.tfinger.dy * wh;
                        buffer_offsety += event.tfinger.dy * wh;

                        if (scale * (tp - bm) > 0.9 * wh) {
                            if (bm * scale + offsety > maxb)
                                offsety = maxb - bm * scale;
                            if (tp * scale + offsety < mint)
                                offsety = mint - tp * scale;

                            if (buffer_offsety > maxb)
                                buffer_offsety = maxb;
                            if (buffer_offsety + imgh * scale / maxzoom < mint)
                                buffer_offsety = mint - imgh * scale / maxzoom;
                        } else {
                            if (bm * scale + offsety < maxb)
                                offsety = maxb - bm * scale;
                            if (tp * scale + offsety > mint)
                                offsety = mint - tp * scale;

                            if (buffer_offsety < maxb)
                                buffer_offsety = maxb;
                            if (buffer_offsety + imgh * scale / maxzoom > mint)
                                buffer_offsety = mint - imgh * scale / maxzoom;
                        }

                    }
                    __android_log_print(ANDROID_LOG_INFO, TAG, "FINGER MOTION %d", nfingers);
                } else if (event.type == SDL_FINGERUP) {
                    nfingers--;
                    __android_log_print(ANDROID_LOG_INFO, TAG, "FINGER UP %d", nfingers);
                    if (nfingers == 0 && may_be_click) {
                        if (total_time - click_start < 0.5f) {
                           // __android_log_print(ANDROID_LOG_INFO, TAG, "CLICK");

                           //auto begin = std::chrono::steady_clock::now();

                           for (int i = 0; i < shapes->size(); i++) {
                               if ((shapes->at(i).r < 50 && shapes->at(i).g < 50 &&
                                    shapes->at(i).b < 50)) continue;
                               if (isinshp(shapes->at(i), clickx, clicky, offsetx, offsety, scale)) {

//                                   for (int k = 0; k < 10000; k++) {
//                                       isinshp(shapes->at(i), clickx, clicky, offsetx, offsety, scale);
//                                   }

                                   //auto end = std::chrono::steady_clock::now();
                                   //auto elapsed_ms = std::chrono::duration_cast<std::chrono::milliseconds>(end - begin);

                                   //__android_log_print(ANDROID_LOG_INFO, TAG, "time diff: %lld", elapsed_ms.count());

                                   __android_log_print(ANDROID_LOG_INFO, TAG, "isinshp");
                                   render_shp(contour_canvas, shps->at(i), imgw, imgh);
                                   region_img = contour_surface->makeImageSnapshot();
                                   break;
                               }
                           }

                           painter_pointf clickpt;
                           clickpt.x = clickx;
                           clickpt.y = clicky;
                           __android_log_print(ANDROID_LOG_INFO, TAG, "clickx=%f clicky=%f", clickx, clicky);
                           clicks.push_back(clickpt);
                           clicks.push_back(clickpt);
                        }
                    }
                } else if (event.type == SDL_MULTIGESTURE) {

                    float newscale = scale * (1.0f + event.mgesture.dDist * 2);

                    if (minzoom > newscale)
                        newscale = minzoom;
                    else if (maxzoom < newscale)
                        newscale = maxzoom;

                    offsetx = ((offsetx - event.mgesture.x * ww) * newscale) / scale + event.mgesture.x * ww;
                    offsety = ((offsety - event.mgesture.y * wh) * newscale) / scale + event.mgesture.y * wh;

                    buffer_offsetx = ((buffer_offsetx - event.mgesture.x * ww) * newscale) / scale + event.mgesture.x * ww;
                    buffer_offsety = ((buffer_offsety - event.mgesture.y * wh) * newscale) / scale + event.mgesture.y * wh;

                    scale = newscale;

             //       __android_log_print(ANDROID_LOG_INFO, TAG, "FINGER MULTI %f", nfingers);
                }
            }



            canvas->clear(SkColorSetARGB(0xFF, 200, 200, 200));
            canvas->save();
            canvas->translate(buffer_offsetx, buffer_offsety);
            canvas->scale(scale/maxzoom, scale/maxzoom);
            canvas->drawImage(region_img, 0.f, 0.f);
            canvas->drawImage(contour_img, 0.f, 0.f);
            canvas->restore();

            SkPaint paint;
            paint.setAntiAlias(true);
            paint.setColor(SK_ColorBLACK);
            SkRect rect;
            rect.setXYWH(0, plt_pos_y, ww, plt_space_h);
            canvas->drawRect(rect, paint);

            //GAME PALETTE
            float plt_x = plt_offset;
            paint.setColor(SK_ColorGREEN);
            rect.setXYWH(plt_x, plt_y, plt_w, plt_w);
            canvas->drawRect(rect, paint);
            plt_x += plt_w + plt_spacing;

            for (int i = 0; i < palette.size(); i++) {
                paint.setColor(SkColorSetARGB(0xFF, palette[i].r, palette[i].g, palette[i].b));
                rect.setXYWH(plt_x, plt_y, plt_w, plt_w);
                canvas->drawRoundRect(rect, plt_r, plt_r, paint);
                plt_x += plt_w + plt_spacing;
            }

            //USER PALETTE
            plt_x = plt_offset;
            paint.setColor(SK_ColorGREEN);
            rect.setXYWH(plt_x, usr_plt_y, plt_w, plt_w);
            canvas->drawRect(rect, paint);
            plt_x += plt_w + plt_spacing;

            for (int i = 0; i < palette.size(); i++) {
                paint.setColor(SkColorSetARGB(0xFF, palette[i].r, palette[i].g, palette[i].b));
                rect.setXYWH(plt_x, usr_plt_y, plt_w, plt_w);
                canvas->drawRoundRect(rect, plt_r, plt_r, paint);
                plt_x += plt_w + plt_spacing;
            }



            //render();
//            for (int i = 0; i < clicks.size(); i++) {
//                SkPaint paint;
//                paint.setColor(SK_ColorGREEN);
//                canvas->drawCircle(clicks[i].x, clicks[i].y, 10.0f, paint);
//            }

            canvas->flush();
            SDL_GL_SwapWindow(window);
        }
    }



    env->DeleteGlobalRef(activity);
    env->DeleteGlobalRef(activity_class);
    return 0;
}
