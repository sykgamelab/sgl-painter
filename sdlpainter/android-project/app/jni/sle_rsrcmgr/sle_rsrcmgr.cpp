#include <unistd.h>

#include "sle_rsrcmgr.h"

using namespace sle;

rsrcmgr::rsrcmgr(JNIEnv *env, AAssetManager *amgr)
{
    this->amgr = amgr;
}

rsrc
rsrcmgr::readrsrc(char *path, int mode)
{
    AAsset *aasset;
    off_t len;
    char *buf;
    int fd;

    aasset = AAssetManager_open(this->amgr, path, mode);
    len = AAsset_getLength(aasset);

    buf = new char[len];
    AAsset_read(aasset, buf, len);

 //   close(fd);
    AAsset_close(aasset);

    return rsrc((unsigned char *)buf, len);
}

rsrc::rsrc(unsigned char *data, int size)
{
    this->_data = data;
    this->_size = size;
}

int
rsrc::size()
{
    return _size;
}

unsigned  char *
rsrc::data()
{
    return _data;
}

rsrc::~rsrc()
{
    delete _data;
}