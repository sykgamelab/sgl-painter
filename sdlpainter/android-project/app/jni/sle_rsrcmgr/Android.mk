LOCAL_PATH := $(call my-dir)

include $(CLEAR_VARS)

LOCAL_MODULE := sle_rsrcmgr

LOCAL_C_INCLUDES := $(LOCAL_PATH)/include

# Add your application source files here...
LOCAL_CPPFLAGS += -std=c++17

LOCAL_SRC_FILES := sle_rsrcmgr.cpp

LOCAL_SHARED_LIBRARIES := hidapi

LOCAL_LDLIBS := -landroid

include $(BUILD_SHARED_LIBRARY)
