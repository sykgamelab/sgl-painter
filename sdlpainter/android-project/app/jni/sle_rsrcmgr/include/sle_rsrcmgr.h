#ifndef SLE_RSRCMGR_H
#define SLE_RSRCMGR_H

#ifdef __ANDROID__
#include <android/asset_manager_jni.h>
#include <android/asset_manager.h>
#include <jni.h>
#endif

namespace sle {
    class rsrc {
    private:
        unsigned char *_data;
        int _size;

    public:
        rsrc(unsigned char *data, int size);
        int size();
        unsigned  char *data();
        ~rsrc();
    };

    class rsrcmgr {
    private:
        AAssetManager *amgr;
    public:
            rsrcmgr();
#ifdef __ANDROID__
        rsrcmgr(JNIEnv *env, AAssetManager *amgr);
        rsrc readrsrc(char *path, int mode);
#endif
    };
};

#endif