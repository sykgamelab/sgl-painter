﻿using System;
using System.IO;

namespace PaletteParser
{
    class Program
    {
        static void Main(string[] args)
        {
            StreamReader sr = new StreamReader("palette.csv");
            sr.ReadLine();
            while (!sr.EndOfStream) 
            {
                string str = sr.ReadLine();
                string[] strParts = str.Trim().Split(new char[] {','});
                Console.WriteLine(strParts[1]);
                using (StreamWriter sw = 
                    new StreamWriter("palletes/" + strParts[1] + ".plt")) {
                        BinaryWriter bw = new BinaryWriter(sw.BaseStream);
                    for (int i = 0; i < 8; i++) {
                        str = sr.ReadLine();
                        strParts = str.Trim().Split(new char[] {','}, 
                            StringSplitOptions.RemoveEmptyEntries);
                        byte r = Convert.ToByte(strParts[0]);
                        byte g = Convert.ToByte(strParts[1]);
                        byte b = Convert.ToByte(strParts[2]);
                        bw.Write(r);
                        bw.Write(g);
                        bw.Write(b);
                    }
                }
            }
        }
    }
}
