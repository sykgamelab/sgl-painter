using System;
using System.IO;


namespace Uploader
{
    static class Log
    {
        static bool initialized = false;
        static StreamWriter errLog, stdLog;

        private static void Initialize() 
        {
            errLog = new StreamWriter("err.log");
            stdLog = new StreamWriter("std.log");
            initialized = true;
        }



        private static void Write(StreamWriter log, string msg)
        {
            log.WriteLine(msg);
            log.Flush();
        }
        public static void LogStd(string msg)
        {
            if (!initialized) Initialize();
            Write(stdLog, msg);
        } 
        public static void LogErr(string msg)
        {
            if (!initialized) Initialize();
            Write(errLog, msg);
        } 
      /*  public void Dispose()
        {
            if (!disposed)
            {
                errLog.Dispose();
                stdLog.Dispose();
                disposed = true;
            }
        }*/
    }
}