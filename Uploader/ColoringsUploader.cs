using System;
using System.Collections.Generic;
using System.IO;

using LiteDB;

using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace Uploader
{
    class ColoringsUploader
    {

        public class UploadRecord
        {
            public int Id { get; set; }
            public string ColoringID { get; set; }
        }

        private static void WriteToDB(LiteCollection<UploadRecord> uploaded, string coloringID)
        {
            uploaded.Insert(coloringID, new UploadRecord(){ColoringID = coloringID});
        }

        public static void Upload(IWorkbook workbook, WebHelper wh, 
            string ticket, string coloringsPath, bool uploadOnlyInfo, 
            string concreteID, bool force)
        {
            LiteDatabase db = new LiteDatabase("upload.db");
            var uploaded = db.GetCollection<UploadRecord>("uploaded");
            
            for (int i = 2; true ;i++) 
            {
                IRow row = workbook.GetSheetAt(0).GetRow(i);
                ICell cell = row.GetCell(1);
                if (cell.NumericCellValue < 0) break;

                
                string coloringID = ((int)cell.NumericCellValue).ToString();
                if (concreteID != null && concreteID != coloringID) continue;
                
                bool exists = false;

                exists  = uploaded.Exists(x => x.ColoringID == coloringID);
                if (exists && !force)
                {   
                    Log.LogStd($"Раскраска заливалась ранее {coloringID}");
                    continue;
                }

                string coloringType = row.GetCell(7).StringCellValue;
                bool isPixel = false;

                if (coloringType.Contains("пиксельные"))
                {
                    isPixel = true;
                };

                string basePath = coloringsPath + coloringType + "/";
                string paidStr = row.GetCell(16).StringCellValue;

                bool paid = paidStr.ToLower().Contains("да");

                List<int> ids = new List<int>();

                Dictionary<string, object> localizedColoring = new Dictionary<string, object>();
                for (int j = 0; j < workbook.NumberOfSheets; j++) 
                {
                    ISheet langsheet = workbook.GetSheetAt(j);
                    IRow langrow = langsheet.GetRow(i);
                    
                    string lang = langsheet.SheetName; 
                    

                    string name =  langrow.GetCell(2).StringCellValue;
                    string description =  langrow.GetCell(3).StringCellValue;
                    Dictionary<string, object> coloringInfo = new Dictionary<string, object>();
                    coloringInfo.Add("name", name);
                    coloringInfo.Add("description", description);
                    localizedColoring.Add(lang, coloringInfo);
                }

                for (int j = 7; row.GetCell(j) != null && 
                    row.GetCell(j).StringCellValue != "" && j < 16; j++) 
                {
                    string tag = row.GetCell(j).StringCellValue;
                    if (j > 9)
                    {
                        if (tag[0] == '!')
                        {
                            tag = tag.Substring(1);
                        }
                    }

                    int tagID = -1;
                    wh.FindTagByName(
                        (code, response)  =>
                        {
                            if (code != 404 && code != 200)
                            {
                                Log.LogErr("Ошибка поиска тэга" + 
                                " на сервере " + response);
                                Environment.Exit(1);
                            }
                            else if (code == 200)
                            {
                                Dictionary<string, object> respdictp = 
                                    (Dictionary<string, object>)Json.Deserialize(response);
                                tagID = Convert.ToInt32(respdictp["id"]);
                            }
                        },
                    ticket, workbook.GetSheetAt(0).SheetName, tag);

                    if (tagID >= 0) ids.Add(tagID);
                }

                if (ids.Count == 0) 
                {
                    Log.LogErr($"тэги для {row.GetCell(2).StringCellValue} не найдены");                     
                    Environment.Exit(1);
                }

                string tags = Json.Serialize(ids);
                string body = Json.Serialize(localizedColoring);
                string file = "";
                try 
                {
                    string format = isPixel ? "png" : "svg";
                    using (StreamReader sr = new StreamReader($"{coloringsPath}/{coloringType}/{coloringID}." + format))
                    {   
                        if (!isPixel)
                        {
                            file = sr.ReadToEnd();
                        }
                        else
                        {
                            using (MemoryStream ms = new MemoryStream())
                            {
                                sr.BaseStream.CopyTo(ms);
                                byte[] data = ms.ToArray();
                                file = Convert.ToBase64String(data, Base64FormattingOptions.None);
                            }
                        }
                    }

                    
                }
                catch (Exception e)
                {
                    Log.LogErr($"Ошибка чтения {coloringID}");
                    Log.LogErr(e.Message);
                    Log.LogErr(e.StackTrace);
                }

                if (file.Length > 0)
                {
                    wh.SaveColoring((code, response) =>
                    {
                        Console.WriteLine(coloringID);
                        if (code != 200 && code != 409)
                        {
                            Log.LogErr($"Ошибка заливки {coloringID}\n{response}\n\n");
                        }
                        else if (code == 409)
                        {
                             Log.LogStd($"Раскраска уже существует {coloringID}");
                             if (!exists) WriteToDB(uploaded, coloringID);
                        }
                        else
                        {
                            Log.LogStd($"Залита раскраска {coloringID}");
                            if (!exists) WriteToDB(uploaded, coloringID);
                        }
                    }
                    , ticket, coloringID, body, tags, file, paid, isPixel, uploadOnlyInfo);
                }
            }
        }
    }
}