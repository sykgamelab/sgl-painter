﻿using System;
using System.Collections.Generic;
using System.IO;

using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace Uploader
{
    class Program
    {
        static WebHelper wh;
        static string ticket = null;
       
        static void Main(string[] args)
        {
            string user = null;
            string password = null;
            string domain = null;
            string xlsxPath = null;
            string coloringsPath = null;
            bool onlyUpdate = false;
            string concreteID = null;
            bool force = false;

            for (int i = 0; i < args.Length; i++) 
            {
                switch(args[i])
                {
                    case "-f":
                    xlsxPath = args[++i];
                    break;

                    case "-d":
                    coloringsPath = args[++i];
                    break;

                    case "-cid":
                    concreteID = args[++i];
                    break;

                    case "-u":
                    onlyUpdate = true;
                    break;

                    case "-force":
                    force = true;
                    break;


                    default:
                    Console.WriteLine($"Нераспознанный аргумент {args[i]}");
                    return;
                }
            }

            if (xlsxPath == null)
            {
                Console.WriteLine("Не указан путь к файлу с описанием раскрасок");
                return;
            }

            if (coloringsPath == null)
            {
                Console.WriteLine("Не указан путь к дректории с раскрасками");
            }

            //Чтение конфига
            using (StreamReader sr = new StreamReader("config.conf"))
            {
                string line = null;
                int ln = 1;

                while (!sr.EndOfStream)
                {
                    line = sr.ReadLine().Trim();
                    int idx = line.IndexOf("#");
                    if (idx >= 0) 
                    {
                        line = line.Substring(0, idx);
                    }

                    if (line.Length == 0)
                    {
                        continue;
                    }

                    string[] parts = line.Split(new char[] {'='}, 2);
                    if (parts.Length != 2) 
                    {
                        Console.WriteLine($"Ошибка чтения конфига: в строке {ln} отсутствует =");
                        return;
                    }

                    switch(parts[0])
                    {
                        case "user":
                            user = parts[1];
                            break;
                        case "password":
                            password = parts[1];
                            break;
                        case "domain":
                            domain=parts[1];
                            break;
                        default:
                            Console.WriteLine($"Неизвестный параметр ${parts[0]} в строке {ln}");
                            return;
                    }

                    ln++;
                }
            }

            if (user == null)
            {
                Console.WriteLine($"Параметр user должен быть указан в конфигурационном файле");
                return;
            }

            if (password == null)
            {
                Console.WriteLine($"Параметр password должен быть указан в конфигурационном файле");
                return;
            }

            if (domain == null)
            {
                Console.WriteLine($"Параметр domain должен быть указан в конфигурационном файле");
                return;
            }


            wh = new WebHelper(domain);
            ticket = null;
            wh.Authenticate(
            (code, response) =>
            {
                if (code != 200)
                {
                    Console.WriteLine("Ошибка аутентификации");
                    Console.WriteLine(response);
                    Environment.Exit(1);
                }
                
                Console.WriteLine(response);
                Dictionary<string, object> dict = (Dictionary<string, object>)Json.Deserialize(response);
                ticket = (string)dict["ticket"];
            }, user, password);

            Console.WriteLine(ticket);

            List<string> languages = new List<string>();
            wh.SelectLanguages(
                (code, response) => 
                {
                    if (code != 200)
                    {
                        Console.WriteLine("Ошибка выборки языков");
                        Console.WriteLine(response);
                        Environment.Exit(1);
                    }

                    Dictionary<string, object> dict = 
                        (Dictionary<string, object>)Json.Deserialize(response);
                    foreach (var pair in dict) 
                    {
                        languages.Add((string)pair.Value);
                    }
                }, ticket);

            using (StreamReader sr = new StreamReader(xlsxPath))
            {
                IWorkbook workbook = new XSSFWorkbook(sr.BaseStream);
                for (int i = 0; i < workbook.NumberOfSheets; i++) {
                    ISheet sheet = workbook.GetSheetAt(i);
                    string lang = sheet.SheetName;                    
                    if (!languages.Contains(lang))
                    {
                        Console.WriteLine($"На сервере не создан язык {lang}");
                        return;
                    }
                }

                // TagsUploader.UploadTags(workbook, wh, ticket);
                ColoringsUploader.Upload(workbook, wh, ticket, coloringsPath, 
                    onlyUpdate, concreteID, force);
            }

            Console.WriteLine("Finished");
        }
    }
}
