using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Web;

namespace Uploader
{
  
    class WebHelper
    {
        private string ticket = null;
        private string host = null;
        private string domain = null;

        public WebHelper(string domain) 
        {
            ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            this.domain = domain;
            host = $"https://{domain}/";
        }

        private Dictionary<int, string> statusCodes = new Dictionary<int, string>
        {
            { 200, "Ok" },
            { 400, "BadRequest" },
            { 404, "NotFound" },
            { 472, "NoAuthorization" },
            { 503, "ServerError" }
        };

        private string GetCorrectStatusCode(int statusCode)
        {
            if (statusCodes.ContainsKey(statusCode))
            {
                return statusCodes[statusCode];
            }
            else
            {
                return statusCode.ToString();
            }
        }

        public void Authenticate(Action<int, string> callback, string login, string password)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(host);
            builder.Append("Administration/Authenticate");
            builder.Append($"?login={HttpUtility.UrlEncode(login)}");
            builder.Append($"&password={HttpUtility.UrlEncode(password)}");
            GetRequest(builder.ToString(), callback);
        }

        public void SelectLanguages(Action<int, string> callback, string ticket)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(host);
            builder.Append("BigColoringBook/SelectLanguages");
            GetRequest(builder.ToString(), callback, ticket);
        }

        public void FindTagByName(Action<int, string> callback, string ticket, 
            string langCode, string name)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(host);
            builder.Append("BigColoringBook/GetTag");
            builder.Append($"?name={HttpUtility.UrlEncode(name)}");
            builder.Append($"&langCode={HttpUtility.UrlEncode(langCode)}");
            GetRequest(builder.ToString(), callback, ticket);
        }

        public void SaveTag(Action<int, string> callback, string ticket, 
            int tagID, string json)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(host);
            builder.Append("BigColoringBook/SaveTag");
            
            string postData = $"tag={HttpUtility.UrlEncode(json)}";
            if (tagID >= 0) {
                postData += $"&tagID={HttpUtility.UrlEncode(tagID.ToString())}";
            }

            PostRequest(builder.ToString(), postData, callback, ticket);
        }

        public void SaveColoring(Action<int, string> callback, string ticket,
            string id, string body, string tags, string file, bool paid, 
            bool isPixel, bool onlyUpdate)
        {
            StringBuilder builder = new StringBuilder();
            builder.Append(host);
            builder.Append("BigColoringBook/SaveColoring");
            
            string postData = $"body={HttpUtility.UrlEncode(body)}";
            postData += $"&tags={HttpUtility.UrlEncode(tags)}";
            postData += $"&id={HttpUtility.UrlEncode(id)}";
            postData += $"&file={HttpUtility.UrlEncode(file)}";
            postData += $"&paid={HttpUtility.UrlEncode(paid.ToString())}";
            postData += $"&isPixel={HttpUtility.UrlEncode(isPixel.ToString())}";
            postData += $"&overwriteData={HttpUtility.UrlEncode(onlyUpdate.ToString())}";
            
            PostRequest(builder.ToString(), postData, callback, ticket);
        }

        private void GetRequest(string url, Action<int, string> action, string ticket = null)
        {
            HttpWebResponse response = null;
            StreamReader reader = null;

            try
            {
                HttpWebRequest request = WebRequest.CreateHttp(url);
                if (ticket != null)
                {
                    if (request.CookieContainer == null) 
                    {
                        request.CookieContainer = new CookieContainer();
                    }
                    Cookie cookie = new Cookie("ticket", ticket,"/", "192.168.1.60");
                    request.CookieContainer.Add(cookie);
                }

                request.Timeout = 10 * 60 * 1000;
                response = (HttpWebResponse)request.GetResponse();
                string status = GetCorrectStatusCode((int)response.StatusCode);

                reader = new StreamReader(response.GetResponseStream());
                string result = reader.ReadToEnd();
                action((int)response.StatusCode, result);
            }
            catch (WebException e)
            {
                response = e.Response as HttpWebResponse;
                if (response != null)
                {
                    string err = "";
                    reader = new StreamReader(response.GetResponseStream());
                    err = reader.ReadToEnd();
                    action((int)response.StatusCode, err + "|" + e.Message);
                    reader.Close();
                }
                else
                {
                    action(-1, e.Message);
                }
            }
            catch (Exception e)
            {
                action(-1, e.Message);
            }
        }

        private static void PostRequest(string url, string postData, Action<int, string> action, string ticket = null)
        {
            Stream stream = null;
            HttpWebResponse response = null;
            StreamReader reader = null;

            try
            {
                HttpWebRequest request = WebRequest.CreateHttp(url);

                request.Timeout = 10 * 60 * 1000;
                if (ticket != null)
                {
                    if (request.CookieContainer == null) 
                    {
                        request.CookieContainer = new CookieContainer();
                    }
                    Cookie cookie = new Cookie("ticket", ticket,"/", "192.168.1.60");
                    request.CookieContainer.Add(cookie);
                }
                var data = Encoding.UTF8.GetBytes(postData);
                request.Method = "POST";
                request.ContentType = "application/x-www-form-urlencoded";
                request.ContentLength = data.Length;

                stream = request.GetRequestStream();
                stream.Write(data, 0, data.Length);
                stream.Flush();
                stream.Close();

                response = (HttpWebResponse)request.GetResponse();

                reader = new StreamReader(response.GetResponseStream());
                string result = reader.ReadToEnd();
                action((int)response.StatusCode, result);
            }
            catch (WebException e)
            {
                response = e.Response as HttpWebResponse;
                if (response != null)
                {
                    string err = "";
                    reader = new StreamReader(response.GetResponseStream());
                    err = reader.ReadToEnd();
                    action((int)response.StatusCode, err + "|" + e.Message);
                    reader.Close();
                }
                else
                {
                    action(-1, e.Message);
                }
            }
            catch (Exception e)
            {
                action(-1, e.Message);
            }
            finally
            {
                if (stream != null) stream.Close();
                if (response != null) response.Close();
                if (reader != null) reader.Close();
            }
        }

    }
}