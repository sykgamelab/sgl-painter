using System;
using System.Collections.Generic;
using System.IO;

using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace Uploader
{
    static class TagsUploader
    {
        public static void UploadTags(IWorkbook workbook, WebHelper wh, string ticket)
        {
  

            for (int i = 2; true ;i++) 
            {
                
                IRow row = workbook.GetSheetAt(0).GetRow(i);
                ICell cell = row.GetCell(1);
                if (cell.NumericCellValue < 0) break;

                for (int j = 7; row.GetCell(j) != null && row.GetCell(j).StringCellValue != ""; j++) 
                {
                    
                    Dictionary<string, object> tagdict = new Dictionary<string, object>();
                    int id = -1;      

                    for (int k = 0; k < workbook.NumberOfSheets; k++) 
                    {
                        ISheet langsheet = workbook.GetSheetAt(k);
                        IRow langrow = langsheet.GetRow(i);
                        string lang = langsheet.SheetName; 
                        string tag = langrow.GetCell(j).StringCellValue;

                        bool isMain = false;
                        bool isBasis = false;

                        if (j < 9) 
                        {
                            isBasis = true;
                        }
                        else 
                        {
                            if (tag[0] == '!')
                            {
                                isMain = true;
                                tag = tag.Substring(1);
                            }
                        }
                        

                        tagdict.TryAdd("is_basis", isBasis);
                        tagdict.TryAdd("is_main", isMain);
                        tagdict.Add(lang, tag);

                        wh.FindTagByName(
                            (code, response)  =>
                            {
                                // Console.WriteLine(lang);
                                // Console.WriteLine(tag);
                                if (code != 404 && code != 200)
                                {
                                    Log.LogErr("Ошибка поиска тэга на сервере " + response);
                                    Environment.Exit(1);
                                }
                                else if (code == 200)
                                {
                                    Dictionary<string, object> respdictp = (Dictionary<string, object>)Json.Deserialize(response);
                                    id = Convert.ToInt32(respdictp["id"]);
                                }
                            },
                        ticket, lang, tag);
                    }

                    wh.SaveTag(
                        (code, response) =>
                        {
                            if (code != 200)
                            {
                                Log.LogErr("Ошибка вставки тэга " + response);
                                Environment.Exit(1);
                            }
                        },
                        ticket, id, Json.Serialize(tagdict));
                }

                
            }
            
            
        }
    }
}