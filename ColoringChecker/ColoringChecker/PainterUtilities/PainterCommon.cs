﻿using System;
using System.Collections.Generic;

namespace ColoringChecker
{
	public static class PainterCommon
	{
		public struct PainterPoint
		{
			public int x, y;
		}

		public struct PainterPointf
		{
			public float x, y;
		}

		public struct PainterContour
		{
			public List<PainterPoint> points;
			public bool outer;
			public float lt, rt, tp, bm;
		}

		public struct PainterShape
		{
			public List<PainterContour> contours;
			public float r, g, b;
			public float lt, rt, tp, bm, cx, cy, radius;
		}

		public static float CalculationStep = 0.1f;
		public static void Bezier3ToPoints(List<PainterPoint> points, PainterPointf p0,
			   PainterPointf p1, PainterPointf p2, PainterPointf p3, float step)
		{
			bool flag = true;

			float t = 0;
			while (flag)
			{

				PainterPoint pt = new PainterPoint() { x = 0, y = 0 };
				float x, y;

				if (t > 1.0f)
				{
					t = 1.0f;
					flag = false;
				}

				x = (1 - t) * (1 - t) * (1 - t) * p0.x +
						3 * t * (1 - t) * (1 - t) * p1.x +
						3 * t * t * (1 - t) * p2.x + t * t * t *
													p3.x;

				y = (1 - t) * (1 - t) * (1 - t) * p0.y +
						3 * t * (1 - t) * (1 - t) * p1.y +
						3 * t * t * (1 - t) * p2.y + t * t * t *
													p3.y;


				pt.x = (int)x;
				pt.y = (int)y;
				points.Add(pt);

				t += step;
			}
		}

		public static void Bezier2ToPoints(List<PainterPoint> points, PainterPointf p0,
					   PainterPointf p1, PainterPointf p2, float step)
		{
			float t = 0;
			bool flag = true;
			while (flag)
			{
				PainterPoint pt = new PainterPoint() { x = 0, y = 0 };
				float x, y;

				if (t > 1.0f)
				{
					t = 1.0f;
					flag = false;
				}

				x = (1 - t) * (1 - t) * p0.x +
					2 * t * (1 - t) * p1.x +
					t * t * p2.x;

				y = (1 - t) * (1 - t) * p0.y +
					2 * t * (1 - t) * p1.y +
					t * t * p2.y;


				pt.x = (int)x;
				pt.y = (int)y;
				points.Add(pt);

				t += step;
			}

		}

		static float SvgSignum(float n)
		{
			if (n < 0)
				return -1;
			else
				return 1;
		}

		static float ClampAngle(float angle)
		{
			int mult = angle < 0 ? -1 : 1;
			while (Math.Abs(angle) >= 360.0f)
				angle -= mult * 360.0f;

			return angle;
		}

		static PainterPointf EllepticArcPoint(float cx, float cy, float rx, float ry, float xangle, float t)
		{
			PainterPointf pt = new PainterPointf();
			pt.x = (float)Math.Cos(xangle) * (float)Math.Cos(t) * rx -
				   (float)Math.Sin(xangle) * (float)Math.Sin(t) * ry + cx;
			pt.y = (float)Math.Sin(xangle) * (float)Math.Cos(t) * rx +
				   (float)Math.Cos(xangle) * (float)Math.Sin(t) * ry + cy;

			return pt;
		}

		static PainterPointf DerevativeEllepticArcPoint(float cx, float cy, float rx, float ry, float xangle, float t)
		{
			PainterPointf pt = new PainterPointf();

			pt.x = -rx * (float)Math.Cos(xangle) * (float)Math.Sin(t) - ry * (float)Math.Sin(xangle) * (float)Math.Cos(t);
			pt.y = -rx * (float)Math.Sin(xangle) * (float)Math.Sin(t) + ry * (float)Math.Cos(xangle) * (float)Math.Cos(t);

			return pt;
		}

		public static void ArcToPoints(List<PainterPoint> points, float rx, float ry, float x1,
				   float y1, float x2, float y2, float xaxisRotation, int largeArcFlag,
				   int sweepFlag, float step)
		{
			float radphi, newx, newy, lambda;
			float s, tcx, tcy, cx, cy, theta;
			float vx, vy, ux, uy, dtheta;
			float arcStep, ang;

			radphi = (float)Math.PI * xaxisRotation / 180.0f;


			newx = (float)Math.Cos(radphi) * ((x1 - x2) / 2) + (float)Math.Sin(radphi) * ((y1 - y2) / 2);
			newy = (-1) * (float)Math.Sin(radphi) * ((x1 - x2) / 2) + (float)Math.Cos(radphi) * ((y1 - y2) / 2);
			lambda = (newx * newx) / (rx * rx) + (newy * newy) / (ry * ry);

			if (lambda > 1.0f)
			{
				rx = (float)Math.Sqrt(lambda) * rx;
				ry = (float)Math.Sqrt(lambda) * ry;
			}

			s = (float)Math.Sqrt(Math.Abs((rx * rx * ry * ry - rx * rx * newy * newy - ry * ry * newx * newx)
							/ (rx * rx * newy * newy + ry * ry * newx * newx)));
			if (largeArcFlag == sweepFlag)
				s = -s;

			tcx = s * (rx * newy) / ry;
			tcy = s * ((-1) * ry * newx) / rx;
			cx = (float)Math.Cos(radphi) * tcx - (float)Math.Sin(radphi) * tcy + (x1 + x2) / 2;
			cy = (float)Math.Sin(radphi) * tcx + (float)Math.Cos(radphi) * tcy + (y1 + y2) / 2;

			vx = (newx - tcx) / rx;
			vy = (newy - tcy) / ry;
			ux = 1;
			uy = 0;

			theta = (ux * vx + uy * vy) / ((float)Math.Sqrt(ux * ux + uy * uy) * (float)Math.Sqrt(vx * vx + vy * vy));
			theta = (float)Math.Acos(theta) * 180.0f / (float)Math.PI;
			theta = Math.Abs(theta) * SvgSignum(ux * vy - vx * uy);

			ux = (newx - tcx) / rx;
			uy = (newy - tcy) / ry;
			vx = (-newx - tcx) / rx;
			vy = (-newy - tcy) / ry;
			dtheta = (ux * vx + uy * vy) / ((float)Math.Sqrt(ux * ux + uy * uy) * (float)Math.Sqrt(vx * vx + vy * vy));

			if (dtheta < -1.0f)
				dtheta = -1.0f;
			else if (dtheta > 1.0f)
				dtheta = 1.0f;

			dtheta = ClampAngle((float)Math.Acos(dtheta) * 180.0f / (float)Math.PI);
			dtheta = Math.Abs(dtheta) * SvgSignum(ux * vy - vx * uy);

			if (sweepFlag == 0 && dtheta > 0)
				dtheta -= 360;
			else if (sweepFlag == 1 && dtheta < 0)
				dtheta += 360;

			int ndivs = 1;
			ang = theta;
			arcStep = Math.Abs(ang - (theta + dtheta)) / ndivs;
			while (arcStep > 45)
			{
				ndivs++;
				arcStep = Math.Abs(ang - (theta + dtheta)) / ndivs;
			}

			arcStep = (sweepFlag == 0) ? -arcStep : arcStep;
			for (int i = 0; i < ndivs; i++, ang += arcStep)
			{
				PainterPointf p1, p2, dp1, dp2, c1, c2;

				float t = ang * (float)Math.PI / 180;
				float t1 = (ang + arcStep) * (float)Math.PI / 180;
				float tans = (float)Math.Tan((t1 - t) / 2.0f);
				tans *= tans;
				float alpha = (float)Math.Sin(t1 - t) * (((float)Math.Sqrt(4.0f + 3.0f * tans) - 1.0f) / 3.0f);

				p1 = EllepticArcPoint(cx, cy, rx, ry, radphi, t);
				p2 = EllepticArcPoint(cx, cy, rx, ry, radphi, t1);
				dp1 = DerevativeEllepticArcPoint(cx, cy, rx, ry, radphi, t);
				dp2 = DerevativeEllepticArcPoint(cx, cy, rx, ry, radphi, t1);

				c1.x = p1.x + alpha * dp1.x;
				c1.y = p1.y + alpha * dp1.y;
				c2.x = p2.x - alpha * dp2.x;
				c2.y = p2.y - alpha * dp2.y;

				Bezier3ToPoints(points, p1, c1, c2, p2, CalculationStep);
			}
		}

		public static void SimplifyShape(ref PainterShape shape)
		{
			List<PainterPoint> pointsNew;


			for (int i = 0; i < shape.contours.Count; i++)
			{
				List<PainterPoint> points = shape.contours[i].points;
				while ((points[0].x == points[points.Count - 1].x) &&
					   (points[0].y == points[points.Count - 1].y) &&
					   (points.Count > 1))
				{
					points.RemoveAt(points.Count - 1);
				}
			}


			for (int i = 0; i < shape.contours.Count; i++)
			{
				List<PainterPoint> points = shape.contours[i].points;

				pointsNew = new List<PainterPoint>();
				int k = 0;
				pointsNew.Add(points[0]);
				for (int j = 1; j < points.Count; j++)
				{
					PainterPoint prevpt, nextpt;
					prevpt = pointsNew[k];
					nextpt = points[j];
					if (nextpt.x != prevpt.x || nextpt.y != prevpt.y)
					{
						k++;
						pointsNew.Add(nextpt);
					}
				}

				PainterContour contour = shape.contours[i];
				contour.points = pointsNew;
				shape.contours[i] = contour;
			}
		}

		public static void PrepareShapes(List<PainterShape> shapes)
		{

			for (int i = 0; i < shapes.Count; i++)
			{
				List<PainterContour> contours;
				int minx, minidx;

				contours = shapes[i].contours;
				minx = contours[0].points[0].x;
				minidx = 0;
				for (int j = 0; j < contours.Count; j++)
				{
					float cl, cr, ct, cb;
					List<PainterPoint> points;

					points = contours[j].points;

					cl = points[0].x;
					cr = points[0].x;
					ct = points[0].y;
					cb = points[0].y;

					for (int k = 0; k < points.Count; k++)
					{
						int x, y;

						x = points[k].x;
						y = points[k].y;

						if (x < minx)
						{
							minidx = j;
							minx = x;
						}

						if (y < cb)
							cb = y;
						if (y > ct)
							ct = y;
						if (x < cl)
							cl = x;
						if (x > cr)
							cr = x;
					}

					PainterContour contour = contours[j];
					contour.lt = cl;
					contour.rt = cr;
					contour.bm = cb;
					contour.tp = ct;
					contours[j] = contour;
				}

				for (int j = 0; j < contours.Count; j++)
				{
					PainterContour contour = contours[j];
					if (j == minidx)
						contour.outer = true;
					else
						contour.outer = false;

					contours[j] = contour;
				}
			}

			for (int i = 0; i < shapes.Count; i++)
			{
				List<PainterContour> contours;
				float l, r, t, b, cx, cy, radius;

				contours = shapes[i].contours;
				l = contours[0].points[0].x;
				r = contours[0].points[0].x;
				t = contours[0].points[0].y;
				b = contours[0].points[0].y;
				for (int j = 0; j < contours.Count; j++)
				{

					List<PainterPoint> points;
					if (!contours[j].outer)
						continue;

					points = contours[j].points;




					for (int k = 1; k < points.Count; k++)
					{
						float x, y;

						x = points[k].x;
						y = points[k].y;

						if (y < b)
							b = y;
						if (y > t)
							t = y;
						if (x < l)
							l = x;
						if (x > r)
							r = x;


					}

				}

				cx = (r + l) / 2.0f;
				cy = (t + b) / 2.0f;
				radius = (float)Math.Sqrt((r - l) * (r - l) + (t - b) * (t - b));

				PainterShape shape = shapes[i];
				shape.lt = l;
				shape.rt = r;
				shape.tp = t;
				shape.bm = b;
				shape.cx = cx;
				shape.cy = cy;
				shape.radius = radius;
				shapes[i] = shape;
			}
		}

		public static void ArcToCommands(List<PainterCommand> commands, float rx, float ry, float x1,
		   float y1, float x2, float y2, float xaxisRotation, int largeArcFlag,
		   int sweepFlag, float step)
		{
			float radphi, newx, newy, lambda;
			float s, tcx, tcy, cx, cy, theta;
			float vx, vy, ux, uy, dtheta;
			float arcStep, ang;

			radphi = (float)Math.PI * xaxisRotation / 180.0f;


			newx = (float)Math.Cos(radphi) * ((x1 - x2) / 2) + (float)Math.Sin(radphi) * ((y1 - y2) / 2);
			newy = (-1) * (float)Math.Sin(radphi) * ((x1 - x2) / 2) + (float)Math.Cos(radphi) * ((y1 - y2) / 2);
			lambda = (newx * newx) / (rx * rx) + (newy * newy) / (ry * ry);

			if (lambda > 1.0f)
			{
				rx = (float)Math.Sqrt(lambda) * rx;
				ry = (float)Math.Sqrt(lambda) * ry;
			}

			s = (float)Math.Sqrt(Math.Abs((rx * rx * ry * ry - rx * rx * newy * newy - ry * ry * newx * newx)
							/ (rx * rx * newy * newy + ry * ry * newx * newx)));
			if (largeArcFlag == sweepFlag)
				s = -s;

			tcx = s * (rx * newy) / ry;
			tcy = s * ((-1) * ry * newx) / rx;
			cx = (float)Math.Cos(radphi) * tcx - (float)Math.Sin(radphi) * tcy + (x1 + x2) / 2;
			cy = (float)Math.Sin(radphi) * tcx + (float)Math.Cos(radphi) * tcy + (y1 + y2) / 2;

			vx = (newx - tcx) / rx;
			vy = (newy - tcy) / ry;
			ux = 1;
			uy = 0;

			theta = (ux * vx + uy * vy) / ((float)Math.Sqrt(ux * ux + uy * uy) * (float)Math.Sqrt(vx * vx + vy * vy));
			theta = (float)Math.Acos(theta) * 180.0f / (float)Math.PI;
			theta = Math.Abs(theta) * SvgSignum(ux * vy - vx * uy);

			ux = (newx - tcx) / rx;
			uy = (newy - tcy) / ry;
			vx = (-newx - tcx) / rx;
			vy = (-newy - tcy) / ry;
			dtheta = (ux * vx + uy * vy) / ((float)Math.Sqrt(ux * ux + uy * uy) * (float)Math.Sqrt(vx * vx + vy * vy));

			if (dtheta < -1.0f)
				dtheta = -1.0f;
			else if (dtheta > 1.0f)
				dtheta = 1.0f;

			dtheta = ClampAngle((float)Math.Acos(dtheta) * 180.0f / (float)Math.PI);
			dtheta = Math.Abs(dtheta) * SvgSignum(ux * vy - vx * uy);

			if (sweepFlag == 0 && dtheta > 0)
				dtheta -= 360;
			else if (sweepFlag == 1 && dtheta < 0)
				dtheta += 360;

			int ndivs = 1;
			ang = theta;
			arcStep = Math.Abs(ang - (theta + dtheta)) / ndivs;
			while (arcStep > 45)
			{
				ndivs++;
				arcStep = Math.Abs(ang - (theta + dtheta)) / ndivs;
			}

			arcStep = (sweepFlag == 0) ? -arcStep : arcStep;
			for (int i = 0; i < ndivs; i++, ang += arcStep)
			{
				PainterPointf p1, p2, dp1, dp2, c1, c2;

				float t = ang * (float)Math.PI / 180;
				float t1 = (ang + arcStep) * (float)Math.PI / 180;
				float tans = (float)Math.Tan((t1 - t) / 2.0f);
				tans *= tans;
				float alpha = (float)Math.Sin(t1 - t) * (((float)Math.Sqrt(4.0f + 3.0f * tans) - 1.0f) / 3.0f);

				p1 = EllepticArcPoint(cx, cy, rx, ry, radphi, t);
				p2 = EllepticArcPoint(cx, cy, rx, ry, radphi, t1);
				dp1 = DerevativeEllepticArcPoint(cx, cy, rx, ry, radphi, t);
				dp2 = DerevativeEllepticArcPoint(cx, cy, rx, ry, radphi, t1);

				c1.x = p1.x + alpha * dp1.x;
				c1.y = p1.y + alpha * dp1.y;
				c2.x = p2.x - alpha * dp2.x;
				c2.y = p2.y - alpha * dp2.y;

				commands.Add(new CCommand(c1.x, c1.y, c2.x, c2.y, p2.x, p2.y));
			}
		}
	}
}