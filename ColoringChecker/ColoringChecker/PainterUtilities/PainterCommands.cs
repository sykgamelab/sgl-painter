﻿using System.Collections.Generic;
using SkiaSharp;

namespace ColoringChecker
{
	public abstract class PainterCommand
	{
		public abstract void Exec(SKPath path, float offsetx, float offsety, float scale);
		public void Exec(SKPath path)
		{
			Exec(path, 0, 0, 1);
		}
	}

	class LCommand : PainterCommand
	{
		float x, y;

		public LCommand(float x, float y)
		{
			this.x = x;
			this.y = y;
		}

		public override void Exec(SKPath path, float offsetx, float offsety, float scale)
		{
			path.LineTo(x * scale + offsetx, y * scale + offsety);
		}
	}

	class MCommand : PainterCommand
	{
		float x, y;

		public MCommand(float x, float y)
		{
			this.x = x;
			this.y = y;
		}

		public override void Exec(SKPath path, float offsetx, float offsety, float scale)
		{
			path.MoveTo(x * scale + offsetx, y * scale + offsety);
		}

		public LCommand GetLCommand() => new LCommand(x, y);
	}

	class CCommand : PainterCommand
	{
		float c1x, c1y, c2x, c2y, x, y;

		public CCommand(float c1x, float c1y, float c2x, float c2y, float x, float y)
		{
			this.c1x = c1x;
			this.c1y = c1y;
			this.c2x = c2x;
			this.c2y = c2y;
			this.x = x;
			this.y = y;
		}
		public override void Exec(SKPath path, float offsetx, float offsety, float scale)
		{
			SKPoint p1 = new SKPoint(c1x * scale + offsetx, c1y * scale + offsety);
			SKPoint p2 = new SKPoint(c2x * scale + offsetx, c2y * scale + offsety);
			SKPoint p3 = new SKPoint(x * scale + offsetx, y * scale + offsety);

			path.CubicTo(p1, p2, p3);
		}
	}

	class QCommand : PainterCommand
	{
		float cx, cy, x, y;
		public QCommand(float cx, float cy, float x, float y)
		{
			this.cx = cx;
			this.cy = cy;
			this.x = x;
			this.y = y;
		}

		public override void Exec(SKPath path, float offsetx, float offsety, float scale)
		{
			SKPoint p1 = new SKPoint(cx * scale + offsetx, cy * scale + offsety);
			SKPoint p2 = new SKPoint(x * scale + offsetx, y * scale + offsety);

			path.QuadTo(p1, p2);
		}
	}

	public struct CommandContour
	{
		public bool outer;
		public List<PainterCommand> commands;
	}

	public struct CommandShape
	{
		public int r, g, b;
        public bool isDrawn;
        public bool isHighlighted;
        public (float rx, float ry, float rw) textRect;
		public List<CommandContour> contours;
	}
}