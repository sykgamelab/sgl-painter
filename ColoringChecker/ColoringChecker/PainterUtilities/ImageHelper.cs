using System;
using System.Collections.Generic;
using System.Xml;

namespace ColoringChecker
{
    static class ImageHelper
    {
        public static void AppendImage(XmlNode node, List<byte> data, bool isBackground)
        {
            string xlink = node.Attributes["xlink:href"].Value;
            string check = xlink.Split(new char[] { ',' })[0];
            xlink = xlink.Split(new char[','])[1];
            byte[] image;

            check = check.Trim();
            if (check == "data:image/png;base64")
            {
                image = Convert.FromBase64String(xlink);
                if (isBackground) data.Add((byte)PainterRead.Commands.BGImage);
                else data.Add((byte)PainterRead.Commands.FGImage);

                int size = image.Length;
                data.Add((byte)((size >> 24) & 0xFF));
                data.Add((byte)((size >> 16) & 0xFF));
                data.Add((byte)((size >> 8) & 0xFF));
                data.Add((byte)(size & 0xFF));

                data.AddRange(image);
            }
            else
            {
                throw new Exception("Неподдерживаемый формат изображения " + check);
            }
        }
    }
}