using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ColoringChecker
{
    class SVGPoint
    {
        private double x, y;
        public SVGPoint(double x, double y)
        {
            this.x = x;
            this.y = y;
        }

        public void ApplyTransform(double[,] matrix)
        {
            double newx = matrix[0, 0] * x + matrix[0, 1] * y + matrix[0, 2];
            double newy = matrix[1, 0] * x + matrix[1, 1] * y + matrix[1, 2];
            x = newx;
            y = newy;
        }

        public SVGPoint Clone()
        {
            return new SVGPoint(x, y);
        }

        public double X
        {
            get
            {
                return x;
            }

            set
            {
                x = value;
            }
        }

        public double Y
        {
            get
            {
                return y;
            }

            set
            {
                y = value;
            }
        }

        public override string ToString() 
        {
            return String.Format("{0} {1}", x, y).Replace(",", ".");
        }

        public static SVGPoint operator +(SVGPoint a, SVGPoint b) 
        {
            double newx = a.x + b.x;
            double newy = a.y + b.y;
            
            return new SVGPoint(newx, newy);
        }

        public static SVGPoint operator *(double c, SVGPoint p) 
        {
            double newx = c * p.x;
            double newy = c * p.y;

            return new SVGPoint(newx, newy);
        }

        public static SVGPoint operator -(SVGPoint a, SVGPoint b) 
        {
            double newx = a.x - b.x;
            double newy = a.y - b.y;
            
            return new SVGPoint(newx, newy);
        }
    }
}