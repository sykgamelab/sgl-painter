using System;
using System.Collections.Generic;

namespace  svg_preparer
{
    class TextRectAppender
    {

        private unsafe static void WriteFloat(List<byte> bytes, float val)
        {
          //  BitConverter.GetBytes()
            /* unsafe
            {
                UInt32 *ptr = (UInt32 *)&val;
                UInt32 ival = *ptr;
                bytes.Add((byte)((ival >> 24) & 0xFF));
                bytes.Add((byte)((ival >> 16) & 0xFF));
                bytes.Add((byte)((ival >> 8) & 0xFF));
                bytes.Add((byte)((ival & 0xFF)));
            }*/

            byte neg = 0;
            if (val < 0) neg = 1;

            bytes.Add(neg);
            UInt16 uival = (UInt16)Math.Abs(val * 10);
            UInt16 b1 = (UInt16)((uival >> 8) & 0xFF);
            UInt16 b2 = (UInt16)(uival & 0xFF);
            bytes.Add((byte)((uival >> 8) & 0xFF));
            bytes.Add((byte)((uival & 0xFF)));
        }

        public static List<byte> Append(List<byte> data, List<(float rx, float ry, float rw)?> textRects)
        {
            List<byte> result = new List<byte>();
            int currShape = 0;

            PainterCommon.PainterShape shape = new PainterCommon.PainterShape();
			PainterCommon.PainterContour contour = new PainterCommon.PainterContour();
			PainterCommon.PainterPoint pt = new PainterCommon.PainterPoint();
			List<PainterCommon.PainterShape> shapes = null;
			List<PainterCommon.PainterContour> contours = null;
			List<PainterCommon.PainterPoint> points = null;
		
			shapes = new List<PainterCommon.PainterShape>();

			for (int pos = 0; pos < data.Count;)
			{
				if ((PainterRead.Commands)data[pos] == PainterRead.Commands.BeginShape)
				{
					contours = new List<PainterCommon.PainterContour>();
					points = new List<PainterCommon.PainterPoint>();
					shape = new PainterCommon.PainterShape();
					contour = new PainterCommon.PainterContour();

                    result.Add(data[pos]);
					pos++;
				}
				else if ((PainterRead.Commands)data[pos] == PainterRead.Commands.Color)
				{
					shape.r = data[pos + 1];
					shape.g = data[pos + 2];
					shape.b = data[pos + 3];

                    result.AddRange(data.GetRange(pos, 4));
					pos += 4;
				}
				else if ((PainterRead.Commands)data[pos] == PainterRead.Commands.Line)
				{
					float x1, y1, x2, y2;

					x1 = PainterRead.BytesToFloat(data[pos + 1] > 0, data[pos + 2], data[pos + 3]);
					y1 = PainterRead.BytesToFloat(data[pos + 4] > 0, data[pos + 5], data[pos + 6]);
					x2 = PainterRead.BytesToFloat(data[pos + 7] > 0, data[pos + 8], data[pos + 9]);
					y2 = PainterRead.BytesToFloat(data[pos + 10] > 0, data[pos + 11], data[pos + 12]);

					if (points.Count == 0)
					{
						pt.x = (int)x1;
						pt.y = (int)y1;
						points.Add(pt);
					}
					pt.x = (int)x2;
					pt.y = (int)y2;
					points.Add(pt);

				
                    result.AddRange(data.GetRange(pos, 13));
					pos += 13;
				}
				else if ((PainterRead.Commands)data[pos] == PainterRead.Commands.Linez)
				{
					contour.points = points;
					contours.Add(contour);
					// std::cout <<"linez " << contours.Count << std::endl;
					points = new List<PainterCommon.PainterPoint>();
					contour = new PainterCommon.PainterContour();

                    result.Add(data[pos]);
					pos++;
				}
				else if ((PainterRead.Commands)data[pos] == PainterRead.Commands.Bezier3)
				{
					// std::cout << "b3" << std::endl;
					PainterCommon.PainterPointf p0, p1, p2, p3;
					//			stream >> p0.x >> p0.y >> p1.x >> p1.y >> p2.x >> p2.y >> p3.x >> p3.y;

					p0.x = PainterRead.BytesToFloat(data[pos + 1] > 0, data[pos + 2], data[pos + 3]);
					p0.y = PainterRead.BytesToFloat(data[pos + 4] > 0, data[pos + 5], data[pos + 6]);

					p1.x = PainterRead.BytesToFloat(data[pos + 7] > 0, data[pos + 8], data[pos + 9]);
					p1.y = PainterRead.BytesToFloat(data[pos + 10] > 0, data[pos + 11], data[pos + 12]);

					p2.x = PainterRead.BytesToFloat(data[pos + 13] > 0, data[pos + 14], data[pos + 15]);
					p2.y = PainterRead.BytesToFloat(data[pos + 16] > 0, data[pos + 17], data[pos + 18]);

					p3.x = PainterRead.BytesToFloat(data[pos + 19] > 0, data[pos + 20], data[pos + 21]);
					p3.y = PainterRead.BytesToFloat(data[pos + 22] > 0, data[pos + 23], data[pos + 24]);


					if (points.Count == 0)
					{
						pt.x = (int)p0.x;
						pt.y = (int)p0.y;
						points.Add(pt);
					}

				

					// std::cout << "before b3 size " << points.Count << std::endl;
					PainterCommon.Bezier3ToPoints(points, p0, p1, p2, p3, PainterCommon.CalculationStep);

                    result.AddRange(data.GetRange(pos, 25));
					pos += 25;
				}
				else if ((PainterRead.Commands)data[pos] == PainterRead.Commands.Bezier2)
				{
					PainterCommon.PainterPointf p0, p1, p2;
					// std::cout << "b2" << std::endl;
					//stream >> p0.x >> p0.y >> p1.x >> p1.y >> p2.x >> p2.y;

					p0.x = PainterRead.BytesToFloat(data[pos + 1] > 0, data[pos + 2], data[pos + 3]);
					p0.y = PainterRead.BytesToFloat(data[pos + 4] > 0, data[pos + 5], data[pos + 6]);

					p1.x = PainterRead.BytesToFloat(data[pos + 7] > 0, data[pos + 8], data[pos + 9]);
					p1.y = PainterRead.BytesToFloat(data[pos + 10] > 0, data[pos + 11], data[pos + 12]);

					p2.x = PainterRead.BytesToFloat(data[pos + 13] > 0, data[pos + 14], data[pos + 15]);
					p2.y = PainterRead.BytesToFloat(data[pos + 16] > 0, data[pos + 17], data[pos + 18]);

					if (points.Count == 0)
					{
						pt.x = (int)p0.x;
						pt.y = (int)p0.y;
						points.Add(pt);
					}

					// std::cout << "before b2 size " << points.Count << std::endl;
					PainterCommon.Bezier2ToPoints(points, p0, p1, p2, PainterCommon.CalculationStep);

                    result.AddRange(data.GetRange(pos, 19));
					pos += 19;
					// std::cout << "after b2 size " << points.Count << std::endl;
				}
				else if ((PainterRead.Commands)data[pos] == PainterRead.Commands.Arc)
				{
					float rx, ry, x1, y1, x2, y2, xaxisRotation;
					int largeArcFlag, sweepFlag;
					//stream >> rx >> ry >> x1 >> y1 >> x2 >> y2 >> xaxis_rotation >>
					//large_arc_flag >> sweep_flag;

					rx = PainterRead.BytesToFloat(data[pos + 1] > 0, data[pos + 2], data[pos + 3]);
					ry = PainterRead.BytesToFloat(data[pos + 4] > 0, data[pos + 5], data[pos + 6]);

					x1 = PainterRead.BytesToFloat(data[pos + 7] > 0, data[pos + 8], data[pos + 9]);
					y1 = PainterRead.BytesToFloat(data[pos + 10] > 0, data[pos + 11], data[pos + 12]);

					x2 = PainterRead.BytesToFloat(data[pos + 13] > 0, data[pos + 14], data[pos + 15]);
					y2 = PainterRead.BytesToFloat(data[pos + 16] > 0, data[pos + 17], data[pos + 18]);

					xaxisRotation = PainterRead.BytesToFloat(data[pos + 19] > 0, data[pos + 20], data[pos + 21]);

					largeArcFlag = data[pos + 22];
					sweepFlag = data[pos + 23];

					if (points.Count == 0)
					{
						pt.x = (int)x1;
						pt.y = (int)y1;
						points.Add(pt);
					}

					PainterCommon.ArcToPoints(points, rx, ry, x1, y1, x2, y2, xaxisRotation,
						largeArcFlag, sweepFlag, PainterCommon.CalculationStep);

					pt.x = (int)x2;
					pt.y = (int)y2;
					points.Add(pt);

				
                    result.AddRange(data.GetRange(pos, 24));
					pos += 24;
				}
				else if ((PainterRead.Commands)data[pos] == PainterRead.Commands.EndShape)
				{
					if (contours.Count > 0)
					{
						shape.contours = contours;
						shapes.Add(shape);
						contours = new List<PainterCommon.PainterContour>();

                        if (textRects[currShape] != null)
                        {
                            result.Add((byte)PainterRead.Commands.Text);
                            WriteFloat(result, textRects[currShape].Value.rx);
                            WriteFloat(result, textRects[currShape].Value.ry);
                            WriteFloat(result, textRects[currShape].Value.rw);
                        }

                        
                        

                        currShape++;
					}

                    result.Add(data[pos]);
					pos++;

				}
			}


            for (int pos = 0; pos < data.Count;)
			{
				if ((PainterRead.Commands)data[pos] == PainterRead.Commands.BeginShape)
				{
                   
					pos++;
				}
				else if ((PainterRead.Commands)data[pos] == PainterRead.Commands.Color)
				{
                    
					pos += 4;
				}
				else if ((PainterRead.Commands)data[pos] == PainterRead.Commands.Line)
				{
                    
					pos += 13;
				}
				else if ((PainterRead.Commands)data[pos] == PainterRead.Commands.Linez)
				{
                    
					pos++;
				}
				else if ((PainterRead.Commands)data[pos] == PainterRead.Commands.Bezier3)
				{
                    
					pos += 25;
				}
				else if ((PainterRead.Commands)data[pos] == PainterRead.Commands.Bezier2)
				{
                    
					pos += 19;
				}
				else if ((PainterRead.Commands)data[pos] == PainterRead.Commands.Arc)
				{
                    
					pos += 24;
				}
				else if ((PainterRead.Commands)data[pos] == PainterRead.Commands.EndShape)
				{
                    
					pos++;

				}
			}

            return result;
        }
    }
}