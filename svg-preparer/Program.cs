﻿using System;
using System.Collections.Generic;
using System.Xml;
using System.Threading;
using System.IO;
using System.IO.Compression;

namespace svg_preparer
{
    class Program
    {

        private static void RelToAbsLine(SVGPoint[] points, SVGPoint curr) 
        {
            points[0] = points[0] + curr;
            for (int i = 1; i < points.Length; i++)
                points[i] = points[i] + points[i - 1];
        }

        private static void RelToAbsBezier(SVGPoint[] points, SVGPoint curr, int degree)
        {
            SVGPoint c = curr.Clone();
            for (int i = 0; i < points.Length; i += degree) 
            {
                for (int j = 0; j < degree; j++)
                    points[i + j] = points[i + j] + c;
                c = points[i + degree - 1].Clone();
            }
        }

        private static void RelToAbsArc(SVGPoint[] points, SVGPoint curr)
        {
            points[0] = points[0] + curr;
            for (int i = 1; i < points.Length; i++)
                points[i] = points[i] + points[i - 1];
        }
        private static List<List<Tuple<string, double[]>>> GetSubpaths(string cmdStr) 
        {
            List<List<Tuple<string, double[]>>> result = new List<List<Tuple<string, double[]>>>();
            List<Tuple<string, double[]>> subPath = new List<Tuple<string, double[]>>();            
            string commandSet = "mMlLzZhHvVcCsSqQtTaA";
            cmdStr = cmdStr.Trim().Replace("-", ",-");;
            if (cmdStr.Substring(0, 1) == ",") {
                cmdStr = cmdStr.Substring(1);
            }
            cmdStr = cmdStr.Replace("\r", "");
            cmdStr = cmdStr.Replace("\n", " ");
            cmdStr = cmdStr.Replace("\t", " ");
            cmdStr = cmdStr.Replace(" ", ",");
            cmdStr = cmdStr.Replace(",,", ",");
            cmdStr = cmdStr.Replace("e,", "e");
            cmdStr = cmdStr.Replace("-.", "-0.");
            
            for (int i = 0; i < cmdStr.Length;) {
                string cmd = cmdStr.Substring(i, 1);
                string argsStr = "", curr = "";
                if (commandSet.IndexOf(cmd) >= 0) {
                    i++;
                    for (; i < cmdStr.Length; i++) {
                        curr = cmdStr.Substring(i, 1);
                        if (commandSet.IndexOf(curr) >= 0)
                            break;
                        argsStr += cmdStr.Substring(i, 1);
                    }

                }
                argsStr = argsStr.Trim();
                string[] argsStrArr = argsStr.Split(new char[] { ',' }, 
                    StringSplitOptions.RemoveEmptyEntries);
                // double[] args = new double[argsStrArr.Length];
                List<double> args = new List<double>();
                for (int j = 0; j < argsStrArr.Length; j++) 
                {
                    // Console.WriteLine($"args {argsStrArr[j]}");
                    List<string> parts = new List<string>();
                    int dotIdx = argsStrArr[j].IndexOf(".");
                    int lastDotIdx = argsStrArr[j].LastIndexOf(".");
                    int start = 0;
                    if (dotIdx != lastDotIdx) {
                        // Console.WriteLine(argsStrArr[j]);
                        while (dotIdx != -1) {
                            int end = 
                                argsStrArr[j].IndexOf(".", dotIdx + 1);
                            dotIdx = end;
                            if (end < 0) end = argsStrArr[j].Length;

                            int len = end - start;
                            string part = argsStrArr[j].Substring(start, 
                                len);
                            parts.Add(part); 

                            start = end;                          
                        }

                        int a = 2 + 2;
                        a = 2 + a;
                    }
                    else
                    {
                        parts.Add(argsStrArr[j]);
                    }

                    // args[j] = Convert.ToDouble(argsStrArr[j]);
                    foreach (string part in parts)
                    {
                        //Console.WriteLine(part);
                        args.Add(Convert.ToDouble(part));
                    }
                }
                subPath.Add(new Tuple<string, double[]>(cmd, args.ToArray()));
                if ("mM".IndexOf(curr) >= 0) {
                    result.Add(subPath);
                    subPath = new List<Tuple<string, double[]>>();
                } 
            }

            result.Add(subPath);

            return result;
        }

        private static double svgSignum(double n) {
            if (n < 0) 
                return -1;
            else 
                return 1;
        }

        private unsafe static void WriteFloat(List<byte> bytes, float val)
        {
          //  BitConverter.GetBytes()
            /* unsafe
            {
                UInt32 *ptr = (UInt32 *)&val;
                UInt32 ival = *ptr;
                bytes.Add((byte)((ival >> 24) & 0xFF));
                bytes.Add((byte)((ival >> 16) & 0xFF));
                bytes.Add((byte)((ival >> 8) & 0xFF));
                bytes.Add((byte)((ival & 0xFF)));
            }*/

            byte neg = 0;
            if (val < 0) neg = 1;

            bytes.Add(neg);
            UInt16 uival = (UInt16)Math.Abs(val * 10);
            UInt16 b1 = (UInt16)((uival >> 8) & 0xFF);
            UInt16 b2 = (UInt16)(uival & 0xFF);
            bytes.Add((byte)((uival >> 8) & 0xFF));
            bytes.Add((byte)((uival & 0xFF)));
        }

        private static (string, List<byte>) PathToPoints(string d, int color)
        {
            List<List<Tuple<string, double[]>>> subPaths = GetSubpaths(d);
            SVGPoint first = new SVGPoint(0, 0);
            SVGPoint curr = new SVGPoint(0, 0);
            SVGPoint prev = new SVGPoint(0, 0);
            bool prevCubic, prevQuadr;
            prevCubic = prevQuadr = false;

            string figureCommand = "BEGIN_SHAPE";
            List<byte> bytes = new List<byte>();


            figureCommand += "\nCOLOR";
            figureCommand += $"\n{(color >> 16) & 0xFF} {(color >> 8) & 0xFF} {color & 0xFF}";

            bytes.Add((byte)PainterRead.Commands.BeginShape);
            bytes.Add((byte)PainterRead.Commands.Color);
            bytes.Add((byte)((color >> 16) & 0xFF));
            bytes.Add((byte)((color >> 8) & 0xFF));
            bytes.Add(((byte)(color & 0xFF)));

            
            for (int sp = 0; sp < subPaths.Count; sp++) {
                List<Tuple<string, double[]>> commands = subPaths[sp];
        
                for (int i = 0; i < commands.Count; i++) {
                    SVGPoint[] pathPoints;
                    string cmd = commands[i].Item1;
                    double[] args = commands[i].Item2;            
                    int degree;

                    switch (cmd) {
                        case "m":
                        case "M":
                        case "l":
                        case "L":
                            pathPoints = new SVGPoint[args.Length / 2];
                            for (int j = 0; j < args.Length; j += 2) 
                                pathPoints[j/2] = new SVGPoint(args[j], args[j + 1]);

                            if ("ml".IndexOf(cmd) >= 0)
                                RelToAbsLine(pathPoints, curr);

                            if (pathPoints.Length > 1 || "lL".IndexOf(cmd) >= 0) {
                                int start = 0;
                                if ("mM".IndexOf(cmd) >= 0) 
                                {
                                    curr = pathPoints[0];
                                    start = 1; 
                                }

                                for (int j = start; j < pathPoints.Length; j++) {
                                    figureCommand += "\nLINE";
                                    figureCommand += $"\n{curr.X} {curr.Y} " + 
                                        $"{pathPoints[j].X} {pathPoints[j].Y}";
                                    
                                    bytes.Add((byte)PainterRead.Commands.Line);
                                    WriteFloat(bytes, (float)curr.X);
                                    WriteFloat(bytes, (float)curr.Y);
                                    WriteFloat(bytes, (float)pathPoints[j].X);
                                    WriteFloat(bytes, (float)pathPoints[j].Y);
                                    
                                    curr = pathPoints[j].Clone();

                                    
                                }
                            }
                            first = ("mM".IndexOf(cmd) >= 0) ? pathPoints[0].Clone() : first;
                            curr = pathPoints[pathPoints.Length - 1].Clone();

                            prevCubic = prevQuadr = false;
                            
                        break;

                        case "z":
                        case "Z":
							figureCommand += "\nLINEZ";
                            bytes.Add((byte)PainterRead.Commands.Linez);
                            // figureCommand += $"\n{curr.X} {curr.Y} {first.X} {first.Y}";
                            curr = first.Clone();
                            prevCubic = prevQuadr = false;
                        break;

                        case "v":
                        case "V":
                        case "h":
                        case "H":
                            pathPoints = new SVGPoint[args.Length];
                            for (int j = 0; j < args.Length; j++) {
                                if (cmd == "v")
                                    pathPoints[j] = new SVGPoint(0, args[j]);
                                else if (cmd == "V")
                                    pathPoints[j] = new SVGPoint(curr.X, args[j]);
                                else if (cmd == "h")
                                    pathPoints[j] = new SVGPoint(args[j], 0);
                                else if (cmd == "H")
                                    pathPoints[j] = new SVGPoint(args[j], curr.Y);                  
                            }

                            if ("vh".IndexOf(cmd) >= 0)
                                RelToAbsLine(pathPoints, curr);
     

                            for (int j = 0; j < pathPoints.Length; j++) {
                                figureCommand += "\nLINE";
                                figureCommand += $"\n{curr.X} {curr.Y} " + 
                                    $"{pathPoints[j].X} {pathPoints[j].Y}";

                                bytes.Add((byte)PainterRead.Commands.Line);
                                WriteFloat(bytes, (float)curr.X);
                                WriteFloat(bytes, (float)curr.Y);
                                WriteFloat(bytes, (float)pathPoints[j].X);
                                WriteFloat(bytes, (float)pathPoints[j].Y);

                                curr = pathPoints[j].Clone();              
                            }
                            
                            curr = pathPoints[pathPoints.Length - 1].Clone();

                            prevCubic = prevQuadr = false;                                                
                        break;

                        case "c":
                        case "C":
                        case "q":
                        case "Q":
                            pathPoints = new SVGPoint[args.Length / 2];
                            for (int j = 0; j < args.Length; j += 2) 
                                pathPoints[j/2] = new SVGPoint(args[j], args[j + 1]);
                            
                            degree = ("cC".IndexOf(cmd) >= 0) ? 3 : 2;
                            if ("cq".IndexOf(cmd) >= 0)
                                RelToAbsBezier(pathPoints, curr, degree);
                            
                            for (int j = 0; j < pathPoints.Length; j+=degree)
                            {
                                if (degree == 3) 
                                {
                                    figureCommand += "\nBEZIER3";
                                    figureCommand += $"\n{curr.X} {curr.Y} " + 
                                        $"{pathPoints[j].X} {pathPoints[j].Y} " +
                                        $"{pathPoints[j + 1].X} {pathPoints[j + 1].Y} " +
                                        $"{pathPoints[j + 2].X} {pathPoints[j + 2].Y}";

                                    bytes.Add((byte)PainterRead.Commands.Bezier3);
                                    WriteFloat(bytes, (float)curr.X);
                                    WriteFloat(bytes, (float)curr.Y);
                                    WriteFloat(bytes, (float)pathPoints[j].X);
                                    WriteFloat(bytes, (float)pathPoints[j].Y);
                                    WriteFloat(bytes, (float)pathPoints[j + 1].X);
                                    WriteFloat(bytes, (float)pathPoints[j + 1].Y);
                                    WriteFloat(bytes, (float)pathPoints[j + 2].X);
                                    WriteFloat(bytes, (float)pathPoints[j + 2].Y);
                                    
                                    prev = pathPoints[j + 1].Clone();
                                    curr = pathPoints[j + 2].Clone();                
                                } 
                                else
                                {
                                    figureCommand += "\nBEZIER2";
                                    figureCommand += $"\n{curr.X} {curr.Y} " + 
                                        $"{pathPoints[j].X} {pathPoints[j].Y} " +
                                        $"{pathPoints[j + 1].X} {pathPoints[j + 1].Y}";

                                    bytes.Add((byte)PainterRead.Commands.Bezier2);
                                    WriteFloat(bytes, (float)curr.X);
                                    WriteFloat(bytes, (float)curr.Y);
                                    WriteFloat(bytes, (float)pathPoints[j].X);
                                    WriteFloat(bytes, (float)pathPoints[j].Y);
                                    WriteFloat(bytes, (float)pathPoints[j + 1].X);
                                    WriteFloat(bytes, (float)pathPoints[j + 1].Y);
                                    
                                    prev = pathPoints[j].Clone();
                                    curr = pathPoints[j + 1].Clone();
                                }
                            }
                            
                            if ("cC".IndexOf(cmd) >= 0) {
                                prevCubic = true;
                                prevQuadr = false;
                            } else if ("qQ".IndexOf(cmd) >= 0) {
                                prevCubic = false;
                                prevQuadr = true;
                            } 
                            
                        break;
                        
                        case "s":
                        case "S":
                        case "t":
                        case "T":
                            pathPoints = new SVGPoint[args.Length / 2];
                            for (int j = 0; j < args.Length; j += 2) 
                                pathPoints[j/2] = new SVGPoint(args[j], args[j + 1]);

                            degree = ("sS".IndexOf(cmd) >= 0) ? 2 : 1;
                            if ("st".IndexOf(cmd) >= 0)
                                RelToAbsBezier(pathPoints, curr, degree);
                            
                            if (("sScC".IndexOf(cmd) >= 0 && !prevCubic) || ("tTqQ".IndexOf(cmd) >= 0 && !prevQuadr))
                                prev = curr.Clone();

                            for (int j = 0; j < pathPoints.Length; j += degree)
                            {
                                // SVGPoint[] p;
                                SVGPoint cp = curr + curr - prev;
                                if (degree == 2) 
                                {
                                    figureCommand += "\nBEZIER3";
                                    figureCommand += $"\n{curr.X} {curr.Y} " + 
                                        $"{cp.X} {cp.Y} " +
                                        $"{pathPoints[j].X} {pathPoints[j ].Y} " +
                                        $"{pathPoints[j + 1].X} {pathPoints[j + 1].Y}";

                                    bytes.Add((byte)PainterRead.Commands.Bezier3);
                                    WriteFloat(bytes, (float)curr.X);
                                    WriteFloat(bytes, (float)curr.Y);
                                    WriteFloat(bytes, (float)cp.X);
                                    WriteFloat(bytes, (float)cp.Y);
                                    WriteFloat(bytes, (float)pathPoints[j].X);
                                    WriteFloat(bytes, (float)pathPoints[j].Y);                                    
                                    WriteFloat(bytes, (float)pathPoints[j + 1].X);
                                    WriteFloat(bytes, (float)pathPoints[j + 1].Y); 

                                    prev = pathPoints[j].Clone();
                                    curr = pathPoints[j + 1].Clone();
                                    // p = new SVGPoint[]{ curr.Clone(), cp.Clone(), pathPoints[j].Clone(), pathPoints[j + 1].Clone() };
                                }
                                else
                                {
                                    figureCommand += "\nBEZIER2";
                                    figureCommand += $"\n{curr.X} {curr.Y} " + 
                                        $"{cp.X} {cp.Y} " +
                                        $"{pathPoints[j].X} {pathPoints[j].Y}";

                                    bytes.Add((byte)PainterRead.Commands.Bezier2);
                                    WriteFloat(bytes, (float)curr.X);
                                    WriteFloat(bytes, (float)curr.Y);
                                    WriteFloat(bytes, (float)cp.X);
                                    WriteFloat(bytes, (float)cp.Y);
                                    WriteFloat(bytes, (float)pathPoints[j].X);
                                    WriteFloat(bytes, (float)pathPoints[j].Y);
                                    
                                    prev = cp.Clone();
                                    curr = pathPoints[j].Clone();
                                    //p = new SVGPoint[]{ curr.Clone(), cp.Clone(), pathPoints[j].Clone() };
                                }
                            }

                            if ("sS".IndexOf(cmd) >= 0) {
                                prevCubic = true;
                                prevQuadr = false;
                            } else if ("tT".IndexOf(cmd) >= 0) {
                                prevCubic = false;
                                prevQuadr = true;
                            } 

                        break;

                        case "a":
                        case "A":
                            pathPoints = new SVGPoint[args.Length / 7];
                            double[] rxArr = new double[args.Length / 7];
                            double[] ryArr = new double[args.Length / 7];
                            double[] xAxisRotation = new double[args.Length / 7];
                            double[] largeArcFlag = new double[args.Length / 7];
                            double[] sweepFlag = new double[args.Length / 7];
                            for (int j = 0; j < args.Length; j += 7) 
                            {
                                rxArr[j/7] = Math.Abs(args[j]);
                                ryArr[j/7] = Math.Abs(args[j + 1]);
                                xAxisRotation[j/7] = args[j + 2];
                                largeArcFlag[j/7] = args[j + 3];
                                sweepFlag[j/7] = args[j + 4];
                                pathPoints[j/7] = new SVGPoint(args[j + 5], args[j + 6]);
                            }

                            if (cmd == "a")
                                RelToAbsArc(pathPoints, curr);

                            
                            for (int j = 0; j < pathPoints.Length; j++) 
                            {
                                
                                double rx = rxArr[j];
                                double ry = ryArr[j];
                                double x1 = curr.X;
                                double y1 = curr.Y;
                                double x2 = pathPoints[j].X;
                                double y2 = pathPoints[j].Y;

                                figureCommand += "\nARC";
                                figureCommand += $"\n{rx} {ry} {x1} {y1}" + 
                                    $" {x2} {y2} {xAxisRotation[j]} {largeArcFlag[j]} {sweepFlag[j]}";

                                bytes.Add((byte)PainterRead.Commands.Arc);
                                WriteFloat(bytes, (float)rx);
                                WriteFloat(bytes, (float)ry);
                                WriteFloat(bytes, (float)x1);
                                WriteFloat(bytes, (float)y1);
                                WriteFloat(bytes, (float)x2);
                                WriteFloat(bytes, (float)y2);
                                WriteFloat(bytes, (float)xAxisRotation[j]);
                                bytes.Add((byte)largeArcFlag[j]);
                                bytes.Add((byte)sweepFlag[j]);
                                // WriteFloat(bytes, (float)largeArcFlag[j]);
                                // WriteFloat(bytes, (float)sweepFlag[j]);

                            
                           double radphi = Math.PI * xAxisRotation[j] / 180.0;
                           double newx = Math.Cos(radphi)*((x1-x2)/2) + Math.Sin(radphi)*((y1-y2)/2);
	                            double newy = (-1) * Math.Sin(radphi)*((x1-x2)/2) + Math.Cos(radphi)*((y1-y2)/2);
                                double lambda = (newx*newx)/(rx*rx) + (newy*newy)/(ry*ry);

                                if (lambda > 1) {
                                    rx = Math.Sqrt(lambda)*rx;
                                    ry = Math.Sqrt(lambda)*ry;
                                }

                                double s = Math.Sqrt(Math.Abs((rx*rx * ry*ry - rx*rx * newy*newy - ry*ry * newx*newx) 
                                    / (rx*rx * newy*newy + ry*ry * newx*newx)));

                                if (largeArcFlag[j] == sweepFlag[j])
                                    s = -s;
                                
                                double tcx = s * (rx*newy)/ry;
	                            double tcy = s * ((-1)*ry*newx)/rx;

                                double cx = Math.Cos(radphi)*tcx - Math.Sin(radphi)*tcy + (x1+x2)/2;
                                double cy = Math.Sin(radphi)*tcx + Math.Cos(radphi)*tcy + (y1+y2)/2;

                                double vx = (newx - tcx) / rx;
                                double vy = (newy - tcy) / ry;
                                double ux = 1;
                                double uy = 0;
                                
                                double theta = (ux*vx + uy*vy) / (Math.Sqrt(ux*ux + uy*uy) * Math.Sqrt(vx*vx + vy*vy));
	                            theta = Math.Acos(theta) * 180 / Math.PI;
	                            theta = Math.Abs(theta) * svgSignum(ux*vy - vx*uy);

                                ux = (newx - tcx) / rx;
                                uy = (newy - tcy) / ry;
                                vx = (-newx - tcx)/rx;
                                vy = (-newy - tcy)/ry;
                                
                                double dtheta = (ux*vx + uy*vy) / (Math.Sqrt(ux*ux + uy*uy) * Math.Sqrt(vx*vx + vy*vy));
                                // Console.WriteLine($"dftheta0 {dtheta}");
                                dtheta = (Math.Acos(dtheta) * 180 / Math.PI) % 360;
                                // Console.WriteLine($"dftheta1 {dtheta}");
                                dtheta = Math.Abs(dtheta) * svgSignum(ux*vy - vx*uy);

                                if (sweepFlag[j] == 0 && dtheta > 0)
                                    dtheta -= 360;
                                else if (sweepFlag[j] == 1 && dtheta < 0)
                                    dtheta += 360;

                                curr = pathPoints[j].Clone();
                            }

                            prevCubic = prevQuadr = false;
                        break;
                    }

                    
                }
            }   

			figureCommand += "\nEND_SHAPE";
            bytes.Add((byte)PainterRead.Commands.EndShape);
			return (figureCommand, bytes); 
        }

        static void ParseFile(string input, string output)
        {
            Console.WriteLine(input);
            XmlDocument doc = new XmlDocument();
            // doc.Load(args[0]);
            doc.Load("tests/" + input);
            Dictionary<string, int> colors = new Dictionary<string, int>();

            XmlNodeList styleNodes = doc.GetElementsByTagName("style");
            if (styleNodes.Count > 0) 
            {
                string[] styles= styleNodes[0].InnerText.Replace("\r", "").Replace("\n", "").
                    Replace("\t", "").Replace(" ", "").Split(new char[] {'.'}, 
                    StringSplitOptions.RemoveEmptyEntries);
                
                foreach (string style in styles)
                {
                    int idx = style.IndexOf("{");
                    string className = style.Substring(0, idx);
                    idx = style.IndexOf("fill:");
                    idx += 5;
                    int end = style.IndexOf(";");
                    int len = end - idx;
                    string fillValue = style.Substring(idx, len);
                  //  Console.WriteLine($"{className} {fillValue}");
                    int color = 0;
                    if (fillValue != "display:none" && fillValue != "display:inline") 
                    color = Convert.ToInt32(fillValue.Trim().
                        Replace("#", "0x"), 16);
                    colors.Add(className, color);
                }                
            }

            XmlNodeList pathNodes = doc.GetElementsByTagName("path");
            Thread.CurrentThread.CurrentCulture = System.Globalization.CultureInfo.InvariantCulture;
            List<string> shapes = new List<string>();
            List<List<byte>> bshapes = new List<List<byte>>();
            foreach (XmlNode node in pathNodes)
            {
                string d = node.Attributes["d"].Value;
                int color = 0;
                if (node.Attributes["class"] != null)
                {
                    string className = node.Attributes["class"].Value;
                    if (colors.ContainsKey(className)) 
                    {
                        color = colors[className];
                    } 
                }

                if (node.Attributes["style"] != null)
                {
                    string[] styles = node.Attributes["style"].Value.Split(new char[] { ';' }, StringSplitOptions.None);
                    
                    foreach (var style in styles)
                    {
                        string[] styleArgs = style.Replace("#", "0x").Split(new char[] { ':' });
                        if (styleArgs[0].Trim() == "fill" && styleArgs[1] != "none")
                        {
                            
                            color = Convert.ToInt32(styleArgs[1].Trim().Replace("#", "0x"), 16);

                        //    Console.WriteLine(color);
                            break;
                        }
                    }
                }
                else if (node.Attributes["fill"] != null)
                {
                    color = Convert.ToInt32(node.Attributes["fill"].Value.Trim().Replace("#", "0x"), 16);
                }

                var parsed = PathToPoints(d, color);

                shapes.Add(parsed.Item1);
                bshapes.Add(parsed.Item2);
            }

            List<byte> bshapeData = new List<byte>();
            foreach (var bshape in bshapes)
            {
                bshapeData.AddRange(bshape);
            }
    
            var painterShapes = PainterRead.Read(bshapeData.ToArray());
            List<(float rx, float ry, float rw)?> textRects = new List<(float rx, float ry, float rw)?>();
            for (int i = 0; i < painterShapes.Count; i++)
            {
                var shape = painterShapes[i];
                if (!Check.IsContour(shape))
                {
                    textRects.Add(TextRectanglesHelper.Calculate(ref shape, 1.0f));
                }
                else
                {
                    textRects.Add(null);
                }
            }
            var withText = TextRectAppender.Append(bshapeData, textRects);

            XmlNodeList imageNodes = doc.GetElementsByTagName("image");
            
            if (imageNodes.Count > 0)
            {
                ImageHelper.AppendImage(imageNodes[0], withText, true);
                if (imageNodes.Count == 2)
                {
                    ImageHelper.AppendImage(imageNodes[1], withText, false);
                }
                else
                {
                    throw new Exception("Не поддерживаемое число растровых изображений " + imageNodes.Count);
                }

            }

            XmlNodeList rootNode = doc.GetElementsByTagName("svg");
            if (rootNode.Count != 1) 
            {
                throw new Exception($"Должен быть только один тэг SVG, найдено {rootNode.Count}");
            }

            XmlAttribute viewBox = rootNode[0].Attributes["viewBox"];
            if (viewBox == null)
            {
                throw new Exception("Не найден viewBox в теге svg");
            }

            string[] viewBoxParts = viewBox.Value.Split(new char[] {' '}, StringSplitOptions.RemoveEmptyEntries);
            if (viewBoxParts.Length != 4)
            {
                throw new Exception($"Во viewBox должно быть 4 элемента, найдено {viewBoxParts.Length}");
            }

            short l = (short)Convert.ToSingle(viewBoxParts[0]);
            short b = (short)Convert.ToSingle(viewBoxParts[1]);
            short r = (short)Convert.ToSingle(viewBoxParts[2]);
            short t = (short)Convert.ToSingle(viewBoxParts[3]);

            if (l != b || l != 0 || r != t)
            {
                throw new Exception("Не квадратный viewBox");
            }

            withText.Add((byte)PainterRead.Commands.Viewbox);
            
            withText.Add((byte)((r >> 8) & 0xFF));
            withText.Add((byte)(r & 0xFF));

            string result = string.Join('\n', shapes.ToArray());
        //    Console.WriteLine(result);
           /*  using (StreamWriter sw = new StreamWriter("results/" + output + ".shp"))
            {
                sw.Write(result);
            }*/

            /*/using (BinaryWriter bw = new BinaryWriter(new StreamWriter("results/" + output + ".bshp").BaseStream)) 
            {
                bw.Write(withText.ToArray());
                
            }*/

            using (FileStream fStream = new FileStream("results/" + output + ".zbshp", FileMode.Create, FileAccess.Write))
            {
                using (ZipArchive archive = new ZipArchive(fStream, ZipArchiveMode.Create, false))
                {
                    ZipArchiveEntry entry = archive.CreateEntry(output + ".bshp", CompressionLevel.Optimal);
                    using (var entryStream = entry.Open())
                    {
                        entryStream.Write(withText.ToArray());
                        /*for (int i = 0; i < bshapes.Count; i++) 
                        {
                            entryStream.Write(bshapes[i].ToArray(), 0, bshapes[i].Count);
                        }*/
                    }
                }          
            }           
        }

        static void Main(string[] args)
        {
            Thread.CurrentThread.CurrentCulture = 
                System.Globalization.CultureInfo.InvariantCulture;
            ParseFile("test1.svg", "shapes1");
            ParseFile("test2.svg", "shapes2");
            ParseFile("test3.svg", "shapes3");  
            ParseFile("bublik.svg", "bublik");
            ParseFile("t130.svg", "t130");
            //ParseFile("t127.svg", "t127");
          //  ParseFile("styled.svg", "styled");
           // ParseFile("styled2.svg", "styled2");
            //ParseFile("pony.svg", "pony");
            //ParseFile("dudle.svg", "dudle");
            //ParseFile("jeep.svg", "jeep");
            // ParseFile("greek.svg", "greek");
         //   ParseFile("bridge.svg", "bridge");
            // ParseFile("tiger.svg", "tiger");
           //ParseFile("ucat.svg", "ucat");
            //ParseFile("madgroup.svg", "madgroup");

            Console.WriteLine("Finished");
        }
    }
}
