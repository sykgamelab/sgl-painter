using System;
using System.Collections.Generic;
using System.Linq;

namespace svg_preparer
{
    static class Check
    {
        public static bool IsInShape(PainterCommon.PainterShape shape, int x, int y, float offsetX, float offsetY, float scale)
        {
            int i;
            bool inOuter = false;
            bool inInner = false;

            PainterCommon.PainterPoint testPoint;

            testPoint.x = x;
            testPoint.y = y;

            int lt = (int)(shape.lt * scale + offsetX);
            int rt = (int)(shape.rt * scale + offsetX);
            int bm = (int)(shape.bm * scale + offsetY);
            int tp = (int)(shape.tp * scale + offsetY);

            int llt = lt;
            int rrt = rt;
            int bbm = bm;
            int ttp = tp;

            if (x >= lt && x <= rt && y >= bm && y <= tp)
            {
                for (i = 0; i < shape.contours.Count; i++)
                {
                    if (!shape.contours[i].outer)
                    {
                        continue;
                    }

                    if (PointInPolygon(testPoint, shape.contours[i].points, offsetX, offsetY, scale) != 0)
                    {
                        inOuter = true;
                        break;
                    }
                }
            }

            if (inOuter)
            {
                for (i = 0; i < shape.contours.Count; i++)
                {
                    if (shape.contours[i].outer || i == 0)
                    {
                        continue;
                    }

                    lt = (int)(shape.contours[i].lt * scale + offsetX);
                    rt = (int)(shape.contours[i].rt * scale + offsetX);
                    bm = (int)(shape.contours[i].bm * scale + offsetY);
                    tp = (int)(shape.contours[i].tp * scale + offsetY);

                    // if (llt > lt || rrt < rt || bbm > bm || ttp < tp)
                    // continue;

                    if (x >= lt && x <= rt &&
                        y >= bm && y <= tp)
                    {
                        if (PointInPolygon(testPoint, shape.contours[i].points, offsetX, offsetY, scale) != 0)
                        {
                            inInner = true;
                            break;
                        }
                    }
                }
            }

            return inOuter && !inInner;
        }

        private static int PointInPolygon(PainterCommon.PainterPoint point, List<PainterCommon.PainterPoint> points, float offsetX, float offsetY, float scale)
        {
            int[,] qPatt = new int[2, 2] { { 0, 1 }, { 3, 2 } };

            if (points.Count < 3)
                return 0;

            PainterCommon.PainterPoint predPoint = points.Last();

            predPoint.x = (int)(predPoint.x * scale + offsetX);
            predPoint.y = (int)(predPoint.y * scale + offsetY);

            predPoint.x -= point.x;
            predPoint.y -= point.y;

            int indexX = predPoint.x < 0 ? 1 : 0;
            int indexY = predPoint.y < 0 ? 1 : 0;

            int predQ = qPatt[indexY, indexX];

            int w = 0;

            for (int i = 0; i < points.Count; i++)
            {
                PainterCommon.PainterPoint currPoint = points[i];

                currPoint.x = (int)(currPoint.x * scale + offsetX);
                currPoint.y = (int)(currPoint.y * scale + offsetY);

                currPoint.x -= point.x;
                currPoint.y -= point.y;


                indexX = currPoint.x < 0 ? 1 : 0;
                indexY = currPoint.y < 0 ? 1 : 0;

                int q = qPatt[indexY, indexX];

                switch (q - predQ)
                {
                    case -3:
                        ++w;
                        break;
                    case 3:
                        --w;
                        break;
                    case -2:
                        if (predPoint.x * currPoint.y >= predPoint.y * currPoint.x)
                            ++w;
                        break;
                    case 2:
                        if (!(predPoint.x * currPoint.y >= predPoint.y * currPoint.x))
                            --w;
                        break;
                }

                predPoint = currPoint;
                predQ = q;
            }

            return w;
        }

        public static bool IsContour(PainterCommon.PainterShape shape)
        {
            const int darkness = 5;
            const int diff = 7;

            int rg = (int)Math.Abs(shape.r - shape.g);
            int gb = (int)Math.Abs(shape.g - shape.b);
            int rb = (int)Math.Abs(shape.r - shape.b);

            bool isDark = shape.r < darkness &&
                shape.b < darkness && shape.g < darkness;
            bool isGrey = rg < diff && gb < diff && rb < diff;

            return isDark && isGrey;
        }
    }
}