using System;
using System.Collections.Generic;

namespace svg_preparer
{
	static class PainterRead
	{
		public enum Commands
		{
            BeginShape,
            Color,
            Line,
            Linez,
            Bezier2,
            Bezier3,
            Arc,
            BGImage,
			FGImage,
			Text,
			Viewbox,
            EndShape
		};

		static public float BytesToFloat(bool minus, ushort b1, ushort b2)
		{
			int val = (b1 << 8) | b2;
			return (float)(((minus) ? -1 : 1) * val) / 10.0f;
		}

		public static List<PainterCommon.PainterShape> Read(byte[] data)
		{
			PainterCommon.PainterShape shape = new PainterCommon.PainterShape();
			PainterCommon.PainterContour contour = new PainterCommon.PainterContour();
			PainterCommon.PainterPoint pt = new PainterCommon.PainterPoint();
			List<PainterCommon.PainterShape> shapes = null;
			List<PainterCommon.PainterContour> contours = null;
			List<PainterCommon.PainterPoint> points = null;
		
			shapes = new List<PainterCommon.PainterShape>();

			for (int pos = 0; pos < data.Length;)
			{
				if ((Commands)data[pos] == Commands.BeginShape)
				{
					contours = new List<PainterCommon.PainterContour>();
					points = new List<PainterCommon.PainterPoint>();
					shape = new PainterCommon.PainterShape();
					contour = new PainterCommon.PainterContour();

					pos++;
				}
				else if ((Commands)data[pos] == Commands.Color)
				{
					shape.r = data[pos + 1];
					shape.g = data[pos + 2];
					shape.b = data[pos + 3];

				
					pos += 4;
				}
				else if ((Commands)data[pos] == Commands.Line)
				{
					float x1, y1, x2, y2;

					x1 = BytesToFloat(data[pos + 1] > 0, data[pos + 2], data[pos + 3]);
					y1 = BytesToFloat(data[pos + 4] > 0, data[pos + 5], data[pos + 6]);
					x2 = BytesToFloat(data[pos + 7] > 0, data[pos + 8], data[pos + 9]);
					y2 = BytesToFloat(data[pos + 10] > 0, data[pos + 11], data[pos + 12]);

					if (points.Count == 0)
					{
						pt.x = (int)x1;
						pt.y = (int)y1;
						points.Add(pt);
					}
					pt.x = (int)x2;
					pt.y = (int)y2;
					points.Add(pt);

				

					pos += 13;
				}
				else if ((Commands)data[pos] == Commands.Linez)
				{
					contour.points = points;
					contours.Add(contour);
					// std::cout <<"linez " << contours.Count << std::endl;
					points = new List<PainterCommon.PainterPoint>();
					contour = new PainterCommon.PainterContour();


					pos++;
				}
				else if ((Commands)data[pos] == Commands.Bezier3)
				{
					// std::cout << "b3" << std::endl;
					PainterCommon.PainterPointf p0, p1, p2, p3;
					//			stream >> p0.x >> p0.y >> p1.x >> p1.y >> p2.x >> p2.y >> p3.x >> p3.y;

					p0.x = BytesToFloat(data[pos + 1] > 0, data[pos + 2], data[pos + 3]);
					p0.y = BytesToFloat(data[pos + 4] > 0, data[pos + 5], data[pos + 6]);

					p1.x = BytesToFloat(data[pos + 7] > 0, data[pos + 8], data[pos + 9]);
					p1.y = BytesToFloat(data[pos + 10] > 0, data[pos + 11], data[pos + 12]);

					p2.x = BytesToFloat(data[pos + 13] > 0, data[pos + 14], data[pos + 15]);
					p2.y = BytesToFloat(data[pos + 16] > 0, data[pos + 17], data[pos + 18]);

					p3.x = BytesToFloat(data[pos + 19] > 0, data[pos + 20], data[pos + 21]);
					p3.y = BytesToFloat(data[pos + 22] > 0, data[pos + 23], data[pos + 24]);


					if (points.Count == 0)
					{
						pt.x = (int)p0.x;
						pt.y = (int)p0.y;
						points.Add(pt);
					}

				

					// std::cout << "before b3 size " << points.Count << std::endl;
					PainterCommon.Bezier3ToPoints(points, p0, p1, p2, p3, PainterCommon.CalculationStep);

					pos += 25;
				}
				else if ((Commands)data[pos] == Commands.Bezier2)
				{
					PainterCommon.PainterPointf p0, p1, p2;
					// std::cout << "b2" << std::endl;
					//stream >> p0.x >> p0.y >> p1.x >> p1.y >> p2.x >> p2.y;

					p0.x = BytesToFloat(data[pos + 1] > 0, data[pos + 2], data[pos + 3]);
					p0.y = BytesToFloat(data[pos + 4] > 0, data[pos + 5], data[pos + 6]);

					p1.x = BytesToFloat(data[pos + 7] > 0, data[pos + 8], data[pos + 9]);
					p1.y = BytesToFloat(data[pos + 10] > 0, data[pos + 11], data[pos + 12]);

					p2.x = BytesToFloat(data[pos + 13] > 0, data[pos + 14], data[pos + 15]);
					p2.y = BytesToFloat(data[pos + 16] > 0, data[pos + 17], data[pos + 18]);

					if (points.Count == 0)
					{
						pt.x = (int)p0.x;
						pt.y = (int)p0.y;
						points.Add(pt);
					}

					// std::cout << "before b2 size " << points.Count << std::endl;
					PainterCommon.Bezier2ToPoints(points, p0, p1, p2, PainterCommon.CalculationStep);


					pos += 19;
					// std::cout << "after b2 size " << points.Count << std::endl;
				}
				else if ((Commands)data[pos] == Commands.Arc)
				{
					float rx, ry, x1, y1, x2, y2, xaxisRotation;
					int largeArcFlag, sweepFlag;
					//stream >> rx >> ry >> x1 >> y1 >> x2 >> y2 >> xaxis_rotation >>
					//large_arc_flag >> sweep_flag;

					rx = BytesToFloat(data[pos + 1] > 0, data[pos + 2], data[pos + 3]);
					ry = BytesToFloat(data[pos + 4] > 0, data[pos + 5], data[pos + 6]);

					x1 = BytesToFloat(data[pos + 7] > 0, data[pos + 8], data[pos + 9]);
					y1 = BytesToFloat(data[pos + 10] > 0, data[pos + 11], data[pos + 12]);

					x2 = BytesToFloat(data[pos + 13] > 0, data[pos + 14], data[pos + 15]);
					y2 = BytesToFloat(data[pos + 16] > 0, data[pos + 17], data[pos + 18]);

					xaxisRotation = BytesToFloat(data[pos + 19] > 0, data[pos + 20], data[pos + 21]);

					largeArcFlag = data[pos + 22];
					sweepFlag = data[pos + 23];

					if (points.Count == 0)
					{
						pt.x = (int)x1;
						pt.y = (int)y1;
						points.Add(pt);
					}

					PainterCommon.ArcToPoints(points, rx, ry, x1, y1, x2, y2, xaxisRotation,
						largeArcFlag, sweepFlag, PainterCommon.CalculationStep);

					pt.x = (int)x2;
					pt.y = (int)y2;
					points.Add(pt);

				

					pos += 24;
				}
				else if ((Commands)data[pos] == Commands.EndShape)
				{
					if (contours.Count > 0)
					{
						shape.contours = contours;
						PainterCommon.SimplifyShape(ref shape);

						for (int b = 0; b < shape.contours.Count; b++)
						{
							for (int d = 0; d < shape.contours[b].points.Count - 1; d++)
							{
								PainterCommon.PainterPoint p1, p2;
								p1 = shape.contours[b].points[d];
								p2 = shape.contours[b].points[d + 1];
								if (p1.x == p2.x && p1.y == p2.y)
								{
									throw new Exception("SAME POINTS DETECTED");
								}
							}
						}

						shapes.Add(shape);
						contours = new List<PainterCommon.PainterContour>();
					}

					pos++;

				}
			}

			PainterCommon.PrepareShapes(shapes);

			return shapes;
		}
	}
}