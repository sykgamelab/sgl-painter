﻿using System;
using System.Collections.Generic;
using System.IO;

using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;



namespace TagAnalytics
{
    class Program
    {
        static void CreateReport(Dictionary<string, Dictionary<string, int>> analytics)
        {
            using (StreamWriter sw = new StreamWriter("report.csv"))
            {
                string line = "";
            
                foreach (var pair in analytics)
                {
                    line += pair.Key + ",";
                }

                sw.WriteLine(line);


                int maxLen = 0;
                foreach (var pair in analytics)
                {
                    maxLen = Math.Max(maxLen, pair.Value.Count);
                }

                string[,] matrix = new string[maxLen, analytics.Count * 2];

                for (int i = 0; i < maxLen; i++)
                {
                    for (int j = 0; j < analytics.Count * 2; j++)
                    {
                        matrix[i, j] = "";
                    }
                }


                int col = 0;
                foreach (var langReport in analytics)
                {
                    int row = 0;
                    foreach (var pair in langReport.Value)
                    {
                        matrix[row, col * 2] = pair.Key;
                        matrix[row, col * 2 + 1] = pair.Value.ToString();
                        row++;
                    }

                    col++;
                }

                for (int i = 0; i < maxLen; i++)
                {
                    line = "";
                    for (int j = 0; j < analytics.Count * 2; j++)
                    {
                        line += matrix[i, j].Trim() + ",";
                    }

                    sw.WriteLine(line);
                }
            }
        }


        static Dictionary<string, int> AnalyzeSheet(ISheet sheet)
        {
            int start = sheet.FirstRowNum;
            int end = sheet.LastRowNum;

            Dictionary<string, int> analytics = new Dictionary<string, int>();

            for (int i = 2; true ;i++) {
                IRow row = sheet.GetRow(i);
                ICell cell = row.GetCell(1);

                if (cell.NumericCellValue < 0) break;

                // Console.WriteLine(cell);
                
                for (int j = 7; row.GetCell(j) != null && row.GetCell(j).StringCellValue != ""; j++) 
                {
                    string tag = row.GetCell(j).StringCellValue;
                    if (analytics.ContainsKey(tag)) analytics[tag] += 1;
                    else analytics.Add(tag, 1);
                }
            }

            return analytics;
        }

        static void Main(string[] args)
        {
            if (args.Length == 0)
            {
                Console.WriteLine("Укажите в аргументах путь к файлу");
                return;
            }

            try
            {
                using (StreamReader sr = new StreamReader(args[0]))
                {
                    Dictionary<string, Dictionary<string, int>> analytics =    
                        new Dictionary<string, Dictionary<string, int>>();

                    IWorkbook workbook = new XSSFWorkbook(sr.BaseStream); 
                    for (int i = 0; i < workbook.NumberOfSheets; i++) {
                        ISheet sheet = workbook.GetSheetAt(i);
                        Console.WriteLine(sheet.SheetName);
                        analytics.Add(sheet.SheetName, AnalyzeSheet(sheet));
                    }
                    
                    CreateReport(analytics);
                }
            } 
            catch (Exception e)
            {
                Console.WriteLine("Ошибка открытия " + args[0]);
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
            }
        }
    }
}
